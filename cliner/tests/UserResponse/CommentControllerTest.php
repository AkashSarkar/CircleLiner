<?php
/**
 * Created by PhpStorm.
 * User: tuli
 * Date: 5/6/18
 * Time: 4:07 PM
 */

use Modules\UserResponse\Controllers\CommentController;

class CommentControllerTest extends TestCase
{
    public function testStore_comment_withdata()
    {
        $response = $this->call('POST', '/api/user-response/comment',
            [   'note'=> 'Comment from unit test',
                'parent_id'=> 2,
                'user_id'=> 1,
                'post_id'=> 1,

            ]);
        $this->assertEquals(201, $response->status());
    }

    public function testStore_comment_without_data()
    {
        $response = $this->call('POST', '/api/user-response/comment',['note'=> " ", 'post_id'=> " "]);
        $this->assertEquals(422, $response->status());
    }

    public function testUpdate_comment()
    {
        $response = $this->call('PATCH', '/api/user-response/comment', ['id'=> 6, 'note'=>"edited comment"]);
        $this->assertEquals( 200, $response->status());
    }

    public function testDelete_comment()
    {
        $response = $this->call('DELETE', '/api/user-response/comment', ['id'=> 7]);
        $this->assertEquals( 200, $response->status());
    }

    public function testGetcomments_by_postid()
    {
        $response = $this->call('GET', '/api/user-response/comment', ['post_id'=> 2]);
        $this->assertEquals( 200, $response->status());
    }

    public function testComments_with_userlist()
    {
        $response = $this->call('GET', '/api/user-response/commentswithuserlist');
        $this->assertEquals( 200, $response->status());
    }

    public function testCommentscount_by_postid()
    {
        $response = $this->call('GET', '/api/user-response/getcommentscount', ['post_id'=> 2]);
        $this->assertEquals( 200, $response->status());
    }

    public function testAll_comments_count()
    {
        $response = $this->call('GET', '/api/user-response/allcommentscount');
        $this->assertEquals( 200, $response->status());
    }

}
