<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 5/12/18
 * Time: 1:05 PM
 */use Modules\UserResponse\Controllers\ShareController;

class ShareControllerTest extends TestCase
{
    public function testStoreShare()
    {
        $response = $this->call('POST', '/api/user-response/share', ['post_id' => '8oAf','user_id' => 'PRpiF']);
        $this->assertEquals(201, $response->status());


    }
    public function testDestroyShare()
    {
        $response = $this->call('DELETE', '/api/user-response/share', ['id' => 1]);
        $this->assertEquals(200, $response->status());

    }
}