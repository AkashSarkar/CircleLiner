<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 5/6/18
 * Time: 4:25 PM
 */

use Modules\UserResponse\Controllers\LikeController;

class LikeControllerTest extends TestCase
{

    public function testDestroy()
    {
        $response = $this->call('DELETE', '/api/user-response/like', ['id' => 1]);
        $this->assertEquals(200, $response->status());


    }

    public function testLike_by_post_id()
    {
        $response = $this->call('POST', '/api/user-response/like', ['post_id' => '8oAf','user_id' => 'PRpiF']);
        $this->assertEquals(201, $response->status());

    }

    public function testAll_likecount()
    {
        $response = $this->call('GET', '/api/user-response/like-all') ;
        $this->assertEquals(200, $response->status());

    }

    public function testGet_like_by_post_id()
    {
        $response = $this->call('GET', '/api/user-response/like-post',['post_id' => '8oAf']);
        $this->assertEquals(200, $response->status());

    }

    public function testGet_likes_with_userlist()
    {

        $response = $this->call('GET', '/api/user-response/like-usrs',['post_id' => '8oAf']) ;
        $this->assertEquals(200, $response->status());
    }
}
