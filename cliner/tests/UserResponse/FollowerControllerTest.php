<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 5/14/18
 * Time: 4:29 PM
 */
use Modules\UserResponse\Controllers\FollowerController;

class FollowerControllerTest extends TestCase
{

    public function testStoreFollower()
    {
        $response = $this->call('POST', '/api/user-response/store-follow', ['follower' => 'YWXDQU','user_id' => 'TBD992']);
        $this->assertEquals(201, $response->status());
    }

    public function testStoreFollowerMissing()
    {
        $response = $this->call('POST', '/api/user-response/store-follow', ['user_id' => 'TBD992']);
        $this->assertEquals(422, $response->status());
    }
    public function testStoreFollowerWrong()
    {
        $response = $this->call('POST', '/api/user-response/store-follow', ['follower' => 'YWXDQU','user_id' => 'TBD92']);
        $this->assertEquals(500, $response->status());
    }
    public function testDeleteFollower()
    {
        $response = $this->call('DELETE', '/api/user-response/unfollow?follower=YWXDQU & user_id=TBD992');
        $this->assertEquals(200, $response->status());
    }
    public function testDeleteFollowerMissingId()
    {
        $response = $this->call('DELETE', '/api/user-response/unfollow?follower=YWXDQU');
        $this->assertEquals(422, $response->status());
    }
    public function testDeleteFollowerWithWrongCredentials()
    {
        $response = $this->call('DELETE', '/api/user-response/unfollow?follower=YWXDQ & user_id=TBD992');
        $this->assertEquals(422, $response->status());
    }
}