<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 5/6/18
 * Time: 4:58 PM
 */

use Modules\Authentication\Controllers\AuthController;

class AuthControllerTest extends TestCase
{

    public function testSubscribe()
    {
        $response = $this->call('POST', '/authentication/subscribe', ['email' => "akash@gmail.com"]);
        $this->assertEquals(201, $response->status());


    }

    public function testSubscribeWithWrongInput()
    {

        $response = $this->call('POST', '/authentication/subscribe', ['token' => "akash@gmail.com"]);
        $this->assertEquals(422, $response->status());
    }

    public function testSubscribeWithoutEmail()
    {

        $response = $this->call('POST', '/authentication/subscribe');
        $this->assertEquals(422, $response->status());

    }

    public function testSubscribeInvalidEmail()
    {

        $response = $this->call('POST', '/authentication/subscribe',["email"=>"email.com"]);
        $this->assertEquals(422, $response->status());

    }

//    public function testSubscribeForJSON()
//    {
//
//        $response=$this->json('POST', '/authentication/subscribe', ['email' => 'email@email.com']);
//
//        $this->assertEquals();
//
//    }


    public function testLoginCorrect()
    {
        $response = $this->call('POST', '/authentication/login',["email"=>"mozzammeluiu@gmail.com","password"=>"123456"]);
        $this->assertEquals(200, $response->status());
    }
    public function testLoginWrong()
    {
        $response = $this->call('POST', '/authentication/login',["email"=>"mozzammeluiu@gmail.com","password"=>"12345678"]);
        $this->assertEquals(401, $response->status());
    }

    public function testLoginMissingEmail()
    {
        $response = $this->call('POST', '/authentication/login',["email"=>"","password"=>"12345678"]);
        $this->assertEquals(422, $response->status());
    }

    public function testLoginMissingPassword()
    {
        $response = $this->call('POST', '/authentication/login',["email"=>"mozzammeluiu@gmail.com","password"=>""]);
        $this->assertEquals(422, $response->status());
    }

    public function testLoginEmailFormatError()
    {
        $response = $this->call('POST', '/authentication/login',["email"=>"mozzammel@gmail.com","password"=>""]);
        $this->assertEquals(422, $response->status());
    }

    public function testLoginJsonResponse()
    {
        $response =  $response=$this->json('POST', '/authentication/login',["email"=>"mozzammeluiu@gmail.com","password"=>"123456"])
            ->seeJson([
                "status" => true,
                "statusCode"=>200
            ]);
        //$this->assertEquals(200, $response->status());
    }


    public function testRegisterWithoutToken()
    {
        $response = $this->call('POST', '/authentication/register',
            ["first_name"=>"Akash",
            "last_name"=> "Sarkar",
            "email"=>"akash1@gmail.com",
            "password"=>"123456",
            "gender"=>"",
            "dob"=>"",
            "status"=>0]
        );
        $this->assertEquals(401, $response->status());
    }

//    public function testRegisterWithToken()
//    {
//        $response = $this->call('POST', '/authentication/register',
//            [   "first_name"=>"Akash",
//                "last_name"=> "Sarkar",
//                "email"=>"akash1@gmail.com",
//                "password"=>"123456",
//                "gender"=>"",
//                "dob"=>"",
//                "status"=>0
//            ]
//        );
//        $this->assertEquals(401, $response->status());
//    }

//    public function testGetUserAgent()
//    {
//
//    }
}
