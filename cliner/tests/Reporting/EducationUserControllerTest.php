<?php
/**
 * Created by PhpStorm.
 * User: tuli
 * Date: 5/15/18
 * Time: 12:43 PM
 */

use Modules\Reporting\Controllers\EducationUserController;

class EducationUserControllerTest extends TestCase
{
    public function testStore_education()
    {
        $response = $this->call('POST', '/api/user-details/education', ['education_id' => '3','user_id'=> 'vfsq2']);
        $this->assertEquals(200, $response->status());
    }

    public function testGet_education_by_userid()
    {
        $response = $this->call('GET', '/api/user-details/education',['user_id' => 'vfsq2']) ;
        $this->assertEquals(200, $response->status());
    }

    public function testUpdate_education()
    {
        $response = $this->call('PATCH', '/api/user-details/education', ['id'=> 2,'user_id'=>'vfsq2','education_id'=> 6]);
        $this->assertEquals( 201, $response->status());

    }

    public function testDelete_education_by_id()
    {
        $response = $this->call('DELETE', '/api/user-details/education', ['id'=> 1]);
        $this->assertEquals( 200, $response->status());
    }


}
