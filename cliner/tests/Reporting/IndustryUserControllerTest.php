<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 5/15/18
 * Time: 3:55 PM
 */

use Modules\Reporting\Controllers\IndustryUserController;

class IndustryUserControllerTest extends TestCase
{

    public function testStore_industry()
    {
        $response = $this->call('POST', '/api/user-details/industry', ['industry_id' => '1','user_id'=> 'PRpiF']);
        $this->assertEquals(200, $response->status());

    }

    public function testDelete_industry_by_id()
    {
        $response = $this->call('DELETE', '/api/user-details/industry', ['id' => 1]);
        $this->assertEquals(200, $response->status());

    }

    public function testGet_industry_all()
    {
        $response = $this->call('GET', '/api/user-details/industry-users',['industry_id' =>2]) ;
        $this->assertEquals(200, $response->status());

    }

    public function testGet_industry_by_userid()
    {
        $response = $this->call('GET', '/api/user-details/industry',['user_id' => 'PRpiF']) ;
        $this->assertEquals(200, $response->status());

    }

    public function testUpdate_industry()
    {
        $response = $this->call('PATCH', '/api/user-details/industry', ['industry_id' => '2','user_id'=> 'PRpiF', 'id'=>4]);
        $this->assertEquals(201, $response->status());

    }

}
