<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 5/16/18
 * Time: 2:34 PM
 */

use Modules\Reporting\Controllers\InterestController;

class InterestControllerTest extends TestCase
{

    public function testGet_interests()
    {
        $response = $this->call('GET', '/api/user-details/interests');
        $this->assertEquals(200, $response->status());
    }

    public function testStore_interests()
    {
        $response = $this->call('POST', '/api/user-details/interests', ['interest' => 'Cartoon']);
        $this->assertEquals(200, $response->status());
    }
}
