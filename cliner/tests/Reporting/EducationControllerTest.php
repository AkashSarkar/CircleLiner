<?php
/**
 * Created by PhpStorm.
 * User: tuli
 * Date: 5/15/18
 * Time: 12:42 PM
 */

use Modules\Reporting\Controllers\EducationController;

class EducationControllerTest extends TestCase
{

    public function testStore_education()
    {
        $response = $this->call('POST', '/api/user-details/educations', ['education' => 'Test Store']);
        $this->assertEquals(200, $response->status());

    }

    public function testGet_education()
    {
        $response = $this->call('GET', '/api/user-details/educations') ;
        $this->assertEquals(200, $response->status());
    }

    public function testStore_eduaction_with_null()
    {
        $response = $this->call('POST', '/api/user-details/educations', ['education' => ' ']);
        $this->assertEquals(422, $response->status());
    }

    public function testStore_education_with_number()
    {
        $response = $this->call('POST', '/api/user-details/educations', ['education' => 1]);
        $this->assertEquals(422, $response->status());
    }
}
