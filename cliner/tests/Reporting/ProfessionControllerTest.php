<?php
/**
 * Created by PhpStorm.
 * User: tuli
 * Date: 5/15/18
 * Time: 12:43 PM
 */

use Modules\Reporting\Controllers\ProfessionController;

class ProfessionControllerTest extends TestCase
{

    public function testGet_profession()
    {
        $response = $this->call('GET', '/api/user-details/professions') ;
        $this->assertEquals(200, $response->status());
    }

    public function testStore_profession()
    {
        $response = $this->call('POST', '/api/user-details/professions', ['profession' => 'Engineer']);
        $this->assertEquals(200, $response->status());
    }

    public function testStore_profession_with_null()
    {
        $response = $this->call('POST', '/api/user-details/professions', ['profession' => ' ']);
        $this->assertEquals(422, $response->status());
    }

    public function testStore_profession_with_number()
    {
        $response = $this->call('POST', '/api/user-details/professions', ['profession' => 1]);
        $this->assertEquals(422, $response->status());
    }
}
