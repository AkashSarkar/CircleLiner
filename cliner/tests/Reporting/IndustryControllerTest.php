<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 5/15/18
 * Time: 1:29 PM
 */

use Modules\Reporting\Controllers\IndustryController;

class IndustryControllerTest extends TestCase
{

    public function testGet_industry()
    {
        $response = $this->call('GET', '/api/user-details/industries') ;
        $this->assertEquals(200, $response->status());

    }

    public function testStore_industry()
    {  
        $response = $this->call('POST', '/api/user-details/industries', ['industry' => 'UIU']);
        $this->assertEquals(200, $response->status());

    }
}
