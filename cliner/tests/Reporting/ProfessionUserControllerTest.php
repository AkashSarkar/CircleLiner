<?php
/**
 * Created by PhpStorm.
 * User: tuli
 * Date: 5/15/18
 * Time: 12:44 PM
 */

use Modules\Reporting\Controllers\ProfessionUserController;

class ProfessionUserControllerTest extends TestCase
{

    public function testStore_profession()
    {
        $response = $this->call('POST', '/api/user-details/profession', ['profession_id' => '1','user_id'=> 'vfsq2']);
        $this->assertEquals(200, $response->status());
    }

    public function testGet_profession_by_userid()
    {
        $response = $this->call('GET', '/api/user-details/profession',['user_id' => 'vfsq2']) ;
        $this->assertEquals(200, $response->status());
    }

    public function testUpdate_profession()
    {
        $response = $this->call('PATCH', '/api/user-details/profession', ['id'=> 2,'user_id'=>'vfsq2','profession_id'=> 6]);
        $this->assertEquals( 200, $response->status());
    }

    public function testDelete_profession_by_id()
    {
        $response = $this->call('DELETE', '/api/user-details/profession', ['id'=> 3]);
        $this->assertEquals( 200, $response->status());
    }
}
