<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 5/16/18
 * Time: 2:34 PM
 */

use Modules\Reporting\Controllers\InterestUserController;

class InterestUserControllerTest extends TestCase
{

    public function testStore_interest()
    {
        $response = $this->call('POST', '/api/user-details/interest', ['interest_id' => '1','user_id'=> 'PRpiF']);
        $this->assertEquals(200, $response->status());
    }

    public function testGet_interest_by_userid()
    {
        $response = $this->call('GET', '/api/user-details/interest', ['user_id'=> 'PRpiF']);
        $this->assertEquals(200, $response->status());

    }

    public function testUpdate_interest()
    {
        $response = $this->call('PATCH', '/api/user-details/interest', ['interest_id' => '2','user_id'=> 'PRpiF','id' =>1]);
        $this->assertEquals(201, $response->status());

    }

    public function testDelete_interest_by_id()
    {
        $response = $this->call('DELETE', '/api/user-details/interest', ['id' => 1]);
        $this->assertEquals(200, $response->status());
    }
}
