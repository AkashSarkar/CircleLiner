<?php
/**
 * Created by PhpStorm.
 * User: tuli
 * Date: 5/6/18
 * Time: 3:33 PM
 */

use Modules\Posting\Controllers\PostController;



class PostControllerTest extends TestCase
{

    public function testGetPostData()
    {
        $response = $this->call('POST', '/api/posting/post', ['name' => 'Sally']);
        $this->assertEquals(500, $response->status());

    }

    public function testPostType()
    {
        $response = $this->call('GET', '/api/posting/post-types');
        $this->assertEquals(200, $response->status());

    }

    public function testGetPostWithId()
    {
        $response = $this->call('GET', '/api/posting/single-post?post_id=LVNK1D');
        $this->assertEquals(200, $response->status());

    }
    public function testGetPosts()
    {
        $response = $this->call('GET', '/api/posting/multi-post');
        $this->assertEquals(200, $response->status());

    }

    public function testGetAllPost()
    {
        $response = $this->call('GET', '/api/posting/single-post');
        $this->assertEquals(500, $response->status());

    }

    public function testPostData()
    {
        $response = $this->call('POST', '/api/posting/post',['tag'=> 'award']);
        $this->assertEquals(500, $response->status());
    }

    public function testPostDataWithInfo()
    {
        $response = $this->call('POST', '/api/posting/post',
                ['project_title'=> 'Circleliner',
                'role'=>'Developer',
                'project_start_date'=>'10-08-12',
                'skills_required'=>'Laravel',
                'project_description'=>'Project build with lumen',
                'tag'=>'project',
                'hash_id'=>'YWXDQU'
                ]);
        $this->assertEquals(201, $response->status());

    }

    public function testPostdestroy()
    {
        $response = $this->call('DELETE', '/api/posting/post?id=HAWENG');
        $this->assertEquals(200, $response->status());
    }


}
