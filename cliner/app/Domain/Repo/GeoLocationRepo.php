<?php

namespace App\Domain\Repo;

use modules\GeoLocation\Models\GeoLocationModel;

interface GeoLocationRepo
{

    function storeGeoLocation($geolocation, $user_id);
    function getGeoLocation();

}