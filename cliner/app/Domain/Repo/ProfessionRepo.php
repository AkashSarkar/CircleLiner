<?php

namespace App\Domain\Repo;
use Modules\Reporting\Models\ProfessionModel;

interface ProfessionRepo
{
    function storeProfession($profession);
    function getProfession();
}