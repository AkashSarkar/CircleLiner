<?php

namespace App\Domain\Repo;

interface InterestUserRepo
{
    function storeUserInterest($interest_id, $user_id);
    function getUserInterest($user_id);
    function updateUserInterest($id,$user_id,$interest_id);
    function deleteUserInterest($id);
}