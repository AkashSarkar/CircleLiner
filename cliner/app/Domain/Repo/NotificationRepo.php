<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 7/8/18
 * Time: 5:53 PM
 */
namespace App\Domain\Repo;

interface NotificationRepo
{
    function hashIdToId($hid);

    function getNotifications($limit, $offset);

    function getNotificationsUser($uid, $limit, $offset);

    function getNotification($hid);

    function storeNotification($data);

    function updateNotification($id,$data);

    function destroyNotification($id);

}