<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 7/4/18
 * Time: 8:36 PM
 */

namespace App\Domain\Repo;


interface PageRepo
{
    function hashIdToId($hid);

    function getPages($limit, $offset);

    function getPagesUser($uid, $limit, $offset);

    function getPage($hid);

    function storePage($data);

    function updatePage($id,$data);

    function destroyPage($id);

}