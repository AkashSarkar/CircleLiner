<?php

namespace App\Domain\Repo;

interface InterestRepo
{
    function storeInterest($interest);
    function getInterest();
}