<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 7/4/18
 * Time: 8:36 PM
 */

namespace App\Domain\Repo;


interface GroupRepo
{
    function hashIdToId($hid);

    function getGroups($limit, $offset);

    function getGroupsUser($uid, $limit, $offset);

    function getGroup($hid);

    function storeGroup($data);

    function updateGroup($id,$data);

    function destroyGroup($id);

}