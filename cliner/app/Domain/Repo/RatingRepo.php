<?php

namespace App\Domain\Repo;

interface RatingRepo
{

    function getRating($post_id);
    function postRating($post_id,$user_id,$rating);
    function deleteRating($id);
    function updateRating($id,$rating);

}