<?php

namespace App\Domain\Repo;

interface LikeRepo
{

    function getLikePost($post_id);

    function storeLike($post_id, $user_id);

    function storeLikePost($post_id, $user_id);

    function deleteLikePost($post_id, $user_id);

    function allLikeCount();

    function allLikeWithUserlist($post_id);

}