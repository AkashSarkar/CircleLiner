<?php
/**
 * Created by PhpStorm.
 * User: Crazy Turtle PC 2
 * Date: 4/22/2018
 * Time: 10:05 PM
 */

namespace App\Domain\Repo;


interface PostRepo
{
    function hashIdToId($hashid);

    function getPosts($limit, $offset);

    function getPostsUser($uid, $limit, $offset);

    function getPost($hashId);

    function storePostData($data);

    function getPostType();

    function getDefaultType();

    function tagTypeToId($type);

    function idToTagType($id);

    function destroy($id);

    function isCommentable($id, $user_id);

    function isShareable($id, $user_id);

    //    function get_like_userlists();
//    function get_like_count_with_userlists();
//    function get_comment_count();
//    function get_comment_userlists();
//    function get_comments_count_with_userlists();
//    function get_share_count();
//    function get_share_userlists();
//    function get_shares_count_with_userlists();
//    function get_rating_count();
//    function get_rating_userlists();
//    function get_ratings_count_with_userlists();
}