<?php
namespace App\Domain\Repo;

interface UserAuthRepo {
    function save($params);
    function saveUserProvider($params);
    function login($email,$password);
    function checkSocialUser($provider_id);
    function getUserByEmail($email);
    function register($params);
    function subscribe($params);
//    function confirmEmail($payload);
    function sendMail($data);
    function confirmedUserData($params);
    function updateEmailUsedStatus($token);
    function updateEmailVerified($email);
    function isExpired($token);
    function checkPassword($oldPassword,$user);
    function updatePassword($newPassword,$user);
}