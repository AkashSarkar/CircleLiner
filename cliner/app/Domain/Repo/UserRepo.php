<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/23/18
 * Time: 5:21 PM
 */

namespace App\Domain\Repo;


interface UserRepo
{
    function save($params);

    function getUser($uid);

    function userIsVerified($uid);

    function getUserHashed($id);

    function getUserId($hid);

    function getUserProfile($hid);

    function getUserHashID($uid);

}