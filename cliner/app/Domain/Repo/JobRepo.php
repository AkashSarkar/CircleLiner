<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 7/4/18
 * Time: 8:36 PM
 */

namespace App\Domain\Repo;


interface JobRepo
{
    function hashIdToId($hid);

    function getJobs($limit, $offset);

    function getJobsUser($uid, $limit, $offset);

    function getJob($hid);

    function storeJob($data);

    function updateJob($id,$data);

    function destroyJob($id);

}