<?php
/**
 * Created by PhpStorm.
 * User: Tuli
 * Date: 5/7/2018
 * Time: 7:01 PM
 */



namespace App\Domain\Repo;

interface IndustryRepo
{
    function storeIndustry($industry);
    function getIndustry();
}