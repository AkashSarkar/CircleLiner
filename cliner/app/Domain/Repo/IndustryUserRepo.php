<?php

namespace App\Domain\Repo;
use Modules\Reporting\Models\IndustryUserModel;

interface IndustryUserRepo
{
    function storeIndustryUser($industry_id, $user_id);
    function getIndustryUser($user_id);
    function updateIndustryUser($id, $user_id, $industry_id);
    function deleteIndustryUser($id);
    function getIndustryUsers($params);
}