<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/25/18
 * Time: 4:09 PM
 */

namespace App\Domain\Repo;


interface PrivacyRepo
{
    public function get();

}