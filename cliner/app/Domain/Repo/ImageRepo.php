<?php

namespace App\Domain\Repo;

interface ImageRepo
{
    function getUserImg($hid);

    public function updateAvatar($avatar, $hid);

    public function updateCoverPhoto($coverPhoto, $hid);

    public function save($coverPhoto, $hid, $album = 'timeline');

    public function download($coverPhoto, $hid);

    public function delete($coverPhoto, $hid);
}