<?php

namespace App\Domain\Repo;
use Modules\Reporting\Models\ProfessionUser;

interface ProfessionUserRepo
{
    function storeProfessionUser($user_id, $profession_id);
    function getProfessionUser($user_id);
    function updateProfessionUser($id, $user_id, $profession_id);
    function deleteProfessionUser($id);
    function getProfessionUsers($id);
}