<?php

namespace App\Domain\Repo;

interface CommentRepo
{
    function storeComment($note,$parent_id,$user_id,$post_id);
    function getCommentsByPostId($post_id);
    function updateComment($id, $note);
    function deleteComment($id);
    function commentsCountByPostId($post_id);
    function allCommentsCount();
    function commentsWithUserlist($post_id);
}