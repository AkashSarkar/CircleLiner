<?php

namespace App\Domain\Repo;

interface FollowerRepo
{

    function getFollowerByUserId();
    function storeFollow($data);
    function unfollow($data);

}