<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/24/18
 * Time: 4:45 PM
 */

namespace App\Domain\Repo;


interface ApiTokenRepo
{
    function insertToken($user_id, $payload);
}