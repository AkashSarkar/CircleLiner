<?php

namespace App\Domain\Repo;

interface ShareRepo
{

    function deleteShare($id);
    function storeShare($post_id,$user_id);
}