<?php

namespace App\Domain\Repo;

interface AchievementRepo
{
    function getAchievement();
}