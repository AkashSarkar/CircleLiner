<?php

namespace App\Domain\Repo;

interface EducationUserRepo
{
        function storeEducationUser($education_id, $user_id);
        function getEducationUser($user_id);
        function updateUserEducation($id,$user_id,$education_id);
        function deleteUserEducation($id);
}