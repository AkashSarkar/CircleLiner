<?php
namespace App\Domain\Repo;

interface SearchKeyRepo{
    function search();
}