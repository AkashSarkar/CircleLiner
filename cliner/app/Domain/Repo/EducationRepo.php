<?php

namespace App\Domain\Repo;

interface EducationRepo
{
    function storeEducation($education);
    function getEducation();
}