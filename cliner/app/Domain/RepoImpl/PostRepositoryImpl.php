<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/23/18
 * Time: 2:57 PM
 */

namespace App\Domain\RepoImpl;

use App\Domain\Repo\PostRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Posting\Models\DataModel\Post;
use Modules\Posting\Models\PostModel;
use Modules\Posting\Models\PostTypeModel;
use Modules\UserResponse\Models\LikeModel;

//use Hashids\Hashids;
use DB;

class PostRepositoryImpl implements PostRepo
{
    function getPosts($limit, $offset)
    {
        $dataModel = [];
        $posts = PostModel::with(['user'])
            ->select('*')
            ->skip($offset)
            ->take($limit)
            ->get();
        foreach ($posts as $idx => $post) {
            $filter = New Post();
            $filter->castMe($post);
            $dataModel[] = $filter;
        }
        return $dataModel;
    }

    function getPostsUser($uid, $limit, $offset)
    {
        $dataModel = [];
        $posts = PostModel::with(['user'])
            ->select('*')
            ->where('user_id', $uid)
            ->skip($offset)
            ->take($limit)
            ->get();
        foreach ($posts as $idx => $post) {
            $filter = New Post();
            $filter->castMe($post);
            $dataModel[] = $filter;
        }
        return $dataModel;
    }

    function getPost($id)
    {
        $post = PostModel::where('id', '=', $id)
            ->select('*')
            ->first();
        $filter = New Post();
        $filter->castMe($post);
        return $filter;
    }

    function getPostType()
    {
        return PostTypeModel::select('label', 'hash_id', '_tag as tag')->get();
    }

    public function storePostData($payload)
    {
        $post = new PostModel;
        $post->hash_id = $this->generateHashId();
        $post->post_data = $payload['post_data'];
        $post->post_type_id = $payload['post_type_id'];
        $post->user_id = $payload['user_id'];
        $post->is_commentable = $payload['isCommentable'];
        $post->is_shareable = $payload['isShareable'];
        $post->save();
        return $post->hash_id;
    }

    public function generateHashId()
    {
        $hashId = getRandomTokenString(28, env('HASHID_CHAR'));
        $exists = PostModel::where('hash_id', $hashId)->count();
        if ($exists > 0) {
            $this->generateHashId();
        } else {
            return $hashId;
        }
    }

    public function hashIdToId($hashid)
    {
        if (PostModel::where('hash_id', $hashid)->count()) {
            return PostModel::where('hash_id', $hashid)->first()->id;
        } else {
            return 0;
        }
    }

    public function getDefaultType()
    {
        return PostTypeModel::where('default', 1)->select('_tag')->first();

    }

    function tagTypeToId($type)
    {
        if (PostTypeModel::where('_tag', $type)->count()) {
            return PostTypeModel::where('_tag', $type)->first()->id;
        } else {
            return 0;
        }

    }

    function idToTagType($id)
    {
        $type = PostTypeModel::where('id', $id)->select('_tag')->first();
        return $type->_tag;
    }

    function destroy($id)
    {
        $post = PostModel::find($id);
        if ($post) {
            $post->likes()->delete();
            $post->comments()->delete();
            $x = $post->delete();
            return true;
        }
        return false;
    }

    function isCommentable($id, $user_id)
    {
        if (PostModel::where('id', $id)->where('user_id', $user_id)->count() > 0) {
            $post = PostModel::where('id', $id)->where('user_id', $user_id)->first();
            $post->is_commentable = !$post->is_commentable;
            return $post->save();
        } else {
            return false;
        }
    }

    function isShareable($id, $user_id)
    {
        if (PostModel::where('id', $id)->where('user_id', $user_id)->count() > 0) {
            $post = PostModel::where('id', $id)->where('user_id', $user_id)->first();
            $post->is_shareable = !$post->is_shareable;
            return $post->save();
        } else {
            return false;
        }
    }

//    function get_like_userlists(){
//
//    }
//
//    function get_like_count_with_userlists(){
//
//    }
//
//    function get_comment_count(){
//
//    }
//
//    function get_comment_userlists(){
//
//    }
//
//    function get_comments_count_with_userlists(){
//
//    }
//
//    function get_share_count(){
//
//    }
//
//    function get_share_userlists(){
//
//    }
//
//    function get_shares_count_with_userlists(){
//
//    }
//
//    function get_rating_count(){
//
//    }
//
//    function get_rating_userlists(){
//
//    }
//
//    function get_ratings_count_with_userlists(){
//
//    }


}