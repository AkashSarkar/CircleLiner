<?php

namespace App\Domain\RepoImpl;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\LikeRepo;
use Modules\Posting\Models\PostModel;
use Modules\UserResponse\Models\DataModel\Like;
use Modules\UserResponse\Models\LikeModel;
use Modules\Authentication\Models\UserModel;
use Illuminate\Support\Facades\DB;

class LikeRepositoryImpl implements LikeRepo
{

    public function storeLike($post_id, $user_id)
    {

        //TODO: Raw query
        $likes = PostModel::where('id', $post_id)
            ->update('like->' . $user_id, ['oc' => time()]);
        return $likes;
    }


    public function allLikeCount()
    {
        return LikeModel::count();
    }

    public function allLikeWithUserlist($post_id)
    {

        $user_id_list = LikeModel::where('post_id', $post_id)
            ->pluck('user_id');
        $user_list = UserModel::whereIn('id', $user_id_list)
            ->select('first_name', 'last_name', 'hash_id')
            ->get();
        return $user_list;


    }

    public function getLikePost($post_id)
    {
        $likes = json_decode(PostModel::where('id', $post_id)->select("likes")->first()->likes);
        $likeData = [];
        foreach ($likes->liked as $key => $value) {
            $like = new Like();
            $like->userId = $key;
            $like->castMe($value);
            $likeData[$key] = $like;
        }

        return $likeData;
    }

    function storeLikePost($post_id, $user_id)
    {
        if (PostModel::where('id', $post_id)->count()) {

            if (PostModel::where('id', $post_id)->whereNull("likes->liked->$user_id")->orWhereNotNull("likes->liked->" . $user_id . "->r")->count()) {
                DB::statement("UPDATE posts SET likes = JSONB_SET(\"likes\",?, TO_JSONB(?::json),true ) WHERE id = ?", ['{liked}', '{}', $post_id]);
                DB::statement("UPDATE posts SET likes = JSONB_SET(\"likes\",?, TO_JSONB(?::json),true) WHERE id = ?", ['{liked,' . $user_id . '}', json_encode(['oc' => time()]), $post_id]);
                $score = PostModel::where('id', $post_id)->select("likes->total as score")->first()->score;
                DB::statement("UPDATE posts SET likes = JSONB_SET(\"likes\",?, TO_JSONB(?::json),true) WHERE id = ?", ['{total}', $score + 1, $post_id]);
            }
            return true;
        }
        return false;
    }

    public function deleteLikePost($post_id, $user_id)
    {
        if (PostModel::where('id', $post_id)->count()) {
            if (PostModel::where('id', $post_id)->whereNotNull("likes->liked->$user_id")->whereNull("likes->liked->" . $user_id . "->r")->count()) {
                $score = PostModel::where('id', $post_id)->select("likes->total as score")->first()->score;
                DB::statement("UPDATE posts SET likes = JSONB_SET(\"likes\",?, TO_JSONB(?::json),true) WHERE id = ?", ['{total}', $score - 1, $post_id]);
                DB::statement("UPDATE posts SET likes = JSONB_SET(\"likes\",?, TO_JSONB(?::json),true) WHERE id = ?", ['{liked,' . $user_id . '}', json_encode(['oc' => time(), 'r' => true]), $post_id]);
            }

            return true;
        }
        return false;
    }
}