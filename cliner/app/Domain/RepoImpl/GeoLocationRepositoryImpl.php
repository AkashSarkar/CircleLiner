<?php
namespace App\Domain\RepoImpl;

use App\Domain\Repo\GeoLocationRepo;
use Modules\GeoLocation\Models\GeoLocationModel;

class GeoLocationRepositoryImpl implements GeoLocationRepo {

    public function storeGeoLocation($geolocation,$user_id){
        //return "ok";
        $geo_locations = new GeoLocationModel;

        $geo_locations->geolocation = $geolocation;
        $geo_locations->user_id = $user_id;
        $geo_locations->geolocation_type_id = 1;
        $geo_locations->connection_type_id = 1;

        return $geo_locations->save();
    }

    public function getGeoLocation(){
        //return "hello ";
    }
}