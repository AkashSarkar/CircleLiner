<?php
namespace App\Domain\RepoImpl;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\ProfessionUserRepo;
use Modules\Reporting\Models\ProfessionUser;
use Modules\Reporting\Models\ProfessionModel;

class ProfessionUserRepositoryImpl implements ProfessionUserRepo {

    public function storeProfessionUser($user_id, $profession_id)
    {

        $user_profession = new ProfessionUser;
        $user_profession->user_id = $user_id;
        $user_profession->profession_id =  $profession_id;

        return $user_profession->save();
    }

    public function getProfessionUser($user_id)
    {

        $profession_ids = DB::table('professions_users')
            ->where('user_id', $user_id)
            ->pluck('profession_id');

        $profession_list = DB::table('professions')
            ->select('name')
            ->whereIn('id', $profession_ids)
            ->get();
        return $profession_list;
    }

    public function updateProfessionUser($id, $user_id, $profession_id){

        return DB::table('professions_users')
            ->where('id', $id)
            ->where('user_id', $user_id)
            ->update(['profession_id' => $profession_id]);
    }

    public function deleteProfessionUser($id){

       return ProfessionUser::where('id', $id)->delete();

    }
    public function getProfessionUsers($id)
    {
        return ProfessionUser::where('profession_id',$id)
            ->pluck('user_id');
    }


}