<?php
namespace App\Domain\RepoImpl;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\ShareRepo;
use Modules\UserResponse\Models\ShareModel;
use Illuminate\Support\Facades\DB;
class ShareRepositoryImpl implements ShareRepo {


    public function deleteShare($id){
        $share=ShareModel::where('id', $id)
            ->delete();
        return $share;
    }
    public function storeShare($post_id,$user_id)
    {
        $share=ShareModel::updateOrCreate(
            ['post_id' => $post_id,'user_id' => $user_id ],
            ['post_id' => $post_id,'user_id' => $user_id ]
        );
        return $share;
    }
}