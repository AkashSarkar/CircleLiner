<?php
namespace App\Domain\RepoImpl;

use App\Domain\Repo\EducationUserRepo;
use Modules\Reporting\Models\EducationUserModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Reporting\Models\EducationModel;

class EducationUserRepoImpl implements EducationUserRepo {

    public function storeEducationUser($user_id, $education_id)
    {
        $user_education = new EducationUserModel;
        $user_education->user_id = $user_id;
        $user_education->education_id =  $education_id;

        return $user_education->save();
    }

    public function getEducationUser($user_id){
        $education_ids = DB::table('educations_users')
            ->where('user_id', $user_id)
            ->pluck('education_id');

        $education_list = DB::table('educations')
            ->select('name')
            ->whereIn('id', $education_ids)
            ->get();
        return $education_list;
    }

     public function updateUserEducation($id,$user_id,$education_id){
         return DB::table('educations_users')
             ->where('id', $id)
             ->where('user_id', $user_id)
             ->update(['education_id' => $education_id]);
     }

      public function deleteUserEducation($id){
          return EducationUserModel::where('id', $id)->delete();
      }

}