<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/29/18
 * Time: 12:56 PM
 */

namespace App\Domain\RepoImpl;


use App\Domain\Repo\ApiTokenRepo;
use Illuminate\Support\Facades\DB;
class ApiTokenRepoImpl implements ApiTokenRepo
{
    public function insertToken($user_id,$payload)
    {
        if(app('db')->table('api_tokens')->where('user_id',$user_id)->count()){
            return app('db')->table('api_tokens')->update([
                'api_token' => $payload['api_token'],
                'user_agent' => json_encode($payload['user_agent']),
                'last_activity' =>$payload['last_activity']
            ]);
        }else{
            return app('db')->table('api_tokens')->insert([
                'api_token' => $payload['api_token'],
                'user_agent' => json_encode($payload['user_agent']),
                'last_activity' =>$payload['last_activity'],
                'user_id' => $user_id,
            ]);
        }

    }
}