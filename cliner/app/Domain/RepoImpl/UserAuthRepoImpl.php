<?php

namespace App\Domain\RepoImpl;

use App\Domain\Repo\UserAuthRepo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Modules\Authentication\Models\Subscription;
use Modules\Authentication\Models\UserAuthModel;

class UserAuthRepoImpl implements UserAuthRepo
{

    public function save($params)
    {
        $usrData = app("db")->table("user_auth")->insert([
            'email' => $params['email'],
            'password' => $params['password'],
            'user_id' => $params['user_id']
        ]);
        if ($usrData) {
            return $usrData;
        } else {
            return 0;
        }
    }

    public function saveUserProvider($params)
    {
        return app('db')->table('social_providers')->insert([
            'provider_id' => $params['provider_id'],
            'user_id' => $params['user_id'],
            'provider' => $params['provider']
        ]);
    }

    public function subscribe($params)
    {
        $exp = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s") . " +30 minutes"));
        Subscription::insert([
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'password' => $params['password'],
            'token' => $params['token'],
            'expire_time' => $exp,
        ]);
        return app("db")->table("subscriptions")->select("id as uid", "email", "token")->where("token", $params["token"])->first();
    }

    public function sendMail($data)
    {

        Mail::send('emails.subscription', ['reg_url' => url('http://'.env('SERVER_IP').':3000/complete-registration/' . $data['token']), 'token' => $data['token'], 'email' => $data['email']],
            function ($msg) use ($data) {
                $msg->to($data['email'], 'welcome')->subject('Laravel Basic Testing Mail');
                $msg->from('kazijamilhossain@gmail.com', 'Kazi Jamil Hossain');
            });

        return true;
    }

//    public function confirmEmail($payload)
//    {
//        if(app('db')->table('email_subscriptions')->where('token',$payload)->count()){
//            $res = app('db')->table('email_subscriptions')->where('token',$payload)->first();
//        }
//    }

    public function register($params)
    {
        $usrData = app("db")->table("user_auth")->insert([
            'user_name' => $params['user_name'],
            'email' => $params['email'],
            'password' => $params['password'],
            'user_id' => $params['user_id']
        ]);
        if ($usrData) {
            return $usrData;
        } else {
            return 0;
        }
    }

    public function login($email, $password)
    {
        $user = DB::table('user_auth')->where('email', $email)->first();
        if ($user) {
            if (hash_equals($password, Crypt::decrypt($user->password))) {
                return $user->user_id;
            }
        }
        return 0;
    }

    public function confirmedUserData($params)
    {
        if (DB::table('subscriptions')->where('token', $params['token'])->count()) {
            $usrData = DB::table('subscriptions')
                ->where(
                    'token', $params['token']
                )->first();

            return $usrData;

        } else {
            return 0;
        }
    }

    public function checkSocialUser($provider_id)
    {
        return app('db')->table('social_providers')->where('provider_id', $provider_id)->first();
    }

    public function getUserByEmail($email)
    {
        return app('db')->table('user_auth')->where('email', $email)->first();
    }

    public function updateEmailUsedStatus($token)
    {
        return app('db')->table('subscriptions')->where('token', $token)->update([
            'used' => 1
        ]);
    }

    public function updateEmailVerified($email)
    {
        $user_id = app('db')->table('user_auth')->where('email', $email)->first()->user_id;

        return app('db')->table('users')->where('id', $user_id)->update([
            'email_verified' => 1
        ]);
    }

    public function isExpired($token)
    {
        $now = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")));
        $expired = Subscription::where('token', '=', $token)->first()->expire_time;
        if ($now <= $expired) {
            return true;
        } else {
            return false;
        }

    }

    public function checkPassword($oldPassword, $user_id)
    {
        $user = DB::table('user_auth')->where('user_id', $user_id)->first();
        if ($user) {
            if (hash_equals($oldPassword, Crypt::decrypt($user->password))) {
                return $user->user_id;
            }
        }
        return 0;
    }

    public function updatePassword($newPassword, $user_id)
    {
        return UserAuthModel::where('user_id', $user_id)->update([
            'password' => $newPassword
        ]);
    }

}