<?php
namespace App\Domain\RepoImpl;

use App\Domain\Repo\FollowerRepo;
use Modules\UserResponse\Models\FollowerModel;

class FollowerRepositoryImpl implements FollowerRepo {


    public function getFollowerByUserId(){
        return "hello from follower";
    }
    public function storeFollow($data)
    {
        $follow_info = FollowerModel::firstOrNew(
            ['follower' => $data->follower],
            ['user_id' => $data->user_id]
        );
        $follow_info->follower = $data->follower;
        $follow_info->user_id = $data->user_id;
        return $follow_info->save();
    }
    public function unfollow($data)
    {
        if(FollowerModel::where('follower', $data->follower)
            ->where('user_id',$data->user_id)->count()){
            FollowerModel::where('follower', $data->follower)
                ->where('user_id',$data->user_id)->delete();
            return true;
        }else{
            return false;
        }
    }
}