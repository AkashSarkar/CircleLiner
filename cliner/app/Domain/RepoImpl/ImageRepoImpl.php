<?php

namespace App\Domain\RepoImpl;

use App\Domain\Repo\ImageRepo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Modules\Authentication\Models\Subscription;
use Modules\Authentication\Models\UserAuthModel;
use Modules\Authentication\Models\UserModel;
use Modules\UserInfo\Models\DataModel\Image;

class ImageRepoImpl implements ImageRepo
{
    function getUserImg($hid)
    {
        $imageData = [];
        if (UserModel::where('hash_id', $hid)->count()) {
            $images = json_decode(UserModel::where('hash_id', $hid)->first()->images)->album;
            foreach ($images as $album) {
                foreach ($album as $key => $value) {
                    $image = new Image();
                    $image->id = $key;
                    $image->ownerUserId = $hid;
                    $image->castMe($value);
                    $imageData[$key] = $image;
                }
            }
        }
        return $imageData;
    }

    public function updateAvatar($avatar, $hid)
    {
        if (UserModel::where('id', $hid)->count() != 0) {
            DB::statement("UPDATE users SET images = JSONB_SET(\"images\",'{cpp,url}', TO_JSONB(?::TEXT),true) WHERE id = ?", [$avatar, $hid]);
            return true;
        }
        return false;
    }

    public function updateCoverPhoto($coverPhoto, $hid)
    {
        if (UserModel::where('id', $hid)->count() != 0) {
            DB::statement("UPDATE users SET images = JSONB_SET(\"images\",'{ccp,url}', TO_JSONB(?::TEXT),true) WHERE id = ?", [$coverPhoto, $hid]);
            return true;
        }
        return false;
    }

    public function save($photo, $hid, $album = 'timeline')
    {
        $id = '';
        if (UserModel::where('id', $hid)->count() != 0) {
            $id = getRandomTokenString(28);
            if (UserModel::where('id', $hid)->whereNull("images->album->$album")->count()) {
                DB::statement("UPDATE users SET images = JSONB_SET(\"images\",?, TO_JSONB(?::json),true ) WHERE id = ?", ['{album,' . $album . '}', '{}', $hid]);
            }
            DB::statement("UPDATE users SET images = JSONB_SET(\"images\",?, TO_JSONB(?::json),true) WHERE id = ?", ['{album,' . $album . ',' . $id . '}', json_encode(['oc' => time(), 'url' => $photo, 'likes' => ['liked' => [], 'total' => 0], 'comments' => []]), $hid]);
            return $id;
        }
        return $id;
    }

    public function download($coverPhoto, $hid)
    {
        // TODO: Implement download() method.
    }

    public function delete($coverPhoto, $hid)
    {
        // TODO: Implement delete() method.
    }
}