<?php

namespace App\Domain\RepoImpl;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\CommentRepo;
use Modules\Posting\Models\PostModel;
use Modules\UserResponse\Models\CommentModel;
use Modules\Authentication\Models\UserModel;
use Illuminate\Support\Facades\DB;
use Modules\UserResponse\Models\DataModel\Comment;
use Modules\UserResponse\Models\DataModel\Comments;

class CommentRepoImpl implements CommentRepo
{

    public function storeComment($note, $parent_id, $user_id, $post_id)
    {//TODO::
        $comment_id = getRandomTokenString(28);
        if (PostModel::where('id', $post_id)->count()) {
            DB::statement("UPDATE posts SET comments = JSONB_SET(\"comments\",?, TO_JSONB(?::json),true) WHERE id = ?", ['{' . $user_id . '}', json_encode([
                "text" => $note,
                "oc" => time(),
                "tag" => array(),
            ]), $post_id]);
        }


        return $comment_id;
    }

    public function getCommentsByPostId($post_id)
    {
        //TODO: Exception handle
        $data = json_decode(PostModel::where('hash_id', $post_id)->select('comments')->first()->comments);
        $comments = new Comments();
        $comments->postId = $post_id;
        $comments->castMe($data);
        return $comments->comments;

    }

    public function updateComment($id, $note)
    {

        return DB::table('comments')
            ->where('id', $id)
            ->update(['note' => $note]);
    }

    public function deleteComment($id)
    {

        $deleteComment = CommentModel::where('id', $id)
            ->orWhere('parent_id', $id)
            ->delete();
        return $deleteComment;
    }

    public function commentsCountByPostId($post_id)
    {

        return CommentModel::where('post_id', $post_id)
            ->count();
    }

    public function allCommentsCount()
    {

        return DB::table('comments')->count();
    }

    public function commentsWithUserlist($post_id)
    {

        $user_ids = DB::table('comments')
            ->where('post_id', $post_id)
            ->pluck('user_id');

        $user_list = DB::table('users')
            ->select('first_name', 'last_name')
            ->whereIn('id', $user_ids)
            ->get();
        return $user_list;

        // return DB::table('comments')
        //     ->join('users', 'users.id', '=', 'comments.user_id')
        //     ->select('users.first_name', 'comments.post_id' )
        //     ->get();
    }


}