<?php
namespace App\Domain\RepoImpl;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\InterestUserRepo;
use Modules\Reporting\Models\InterestUserModel;
use Modules\Reporting\Models\InterestModel;

class InterestUserRepositoryImpl implements InterestUserRepo {

    public function storeUserInterest($user_id, $interest_id){
          $user_interest = InterestUserModel::create([
            'user_id' => $user_id,
            'interest_id' => $interest_id,
          ]);

          return $user_interest;
 
    }

    public function getUserInterest($user_id){
        $interest_ids = DB::table('interests_users')
            ->where('user_id', $user_id)
            ->pluck('interest_id');

        $interest_list = DB::table('interests')
            ->select('name')
            ->whereIn('id', $interest_ids)
            ->get();
        return $interest_list;
        //return DB::table('interests_users')->get();
    }

    function updateUserInterest($id,$user_id,$interest_id){
        return DB::table('interests_users')
            ->where('id', $id)
            ->where('user_id', $user_id)
            ->update(['interest_id' => $interest_id]);
    }

    function deleteUserInterest($id){
        return InterestUserModel::where('id', $id)->delete();
    }
}