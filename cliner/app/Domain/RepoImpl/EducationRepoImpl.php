<?php
namespace App\Domain\RepoImpl;

use App\Domain\Repo\EducationRepo;
use Modules\Reporting\Models\EducationModel;
use Illuminate\Support\Facades\DB;

class EducationRepoImpl implements EducationRepo {


    public function storeEducation($education){

        $educations = EducationModel::firstOrNew(
            ['name' => $education]
        );
        $educations->name = $education;

        return $educations->save();
    }

    public function getEducation(){

        return DB::table('educations')->select('name')->get();
    }
}