<?php
namespace App\Domain\RepoImpl;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\ProfessionRepo;
use Modules\Reporting\Models\ProfessionModel;

class ProfessionRepositoryImpl implements ProfessionRepo {

    public function storeProfession($profession)
    {

        $professions = ProfessionModel::firstOrNew(
            ['name' => $profession],
            ['connection_type_id' => 2 ]
        );
         $professions->profession = $profession;
         $professions->connection_type_id =  2;

         return $professions->save();
    }

    public function getProfession()
    {
        //return "ok";
        return ProfessionModel::select('name')->get();
    }
}