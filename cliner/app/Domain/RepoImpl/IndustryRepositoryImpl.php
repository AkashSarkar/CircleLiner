<?php
/**
 * Created by PhpStorm.
 * User: Tuli
 * Date: 5/7/2018
 * Time: 7:03 PM
 */


namespace App\Domain\RepoImpl;

use App\Domain\Repo\IndustryRepo;
use Modules\Reporting\Models\IndustryModel;
use Illuminate\Support\Facades\DB;

class IndustryRepositoryImpl implements IndustryRepo {


    public function storeIndustry($industry){
        $industries = new IndustryModel;
        $industries->industry = $industry;
        $industries->connection_type_id =  3;

        return $industries->save();
    }

    public function getIndustry(){
        return DB::table('industries')->select('name')->get();
    }
}