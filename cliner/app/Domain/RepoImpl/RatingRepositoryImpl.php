<?php
namespace App\Domain\RepoImpl;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\RatingRepo;
use Modules\UserResponse\Models\RatingModel;

class RatingRepositoryImpl implements RatingRepo {


    public function getRating($post_id){
        $getRating = RatingModel::where('post_id', $post_id)->get();
        return $getRating;
    }
    public function postRating($post_id,$user_id,$rating){


        $postRating=RatingModel::updateOrCreate(
    
            ['post_id' => $post_id,'user_id' => $user_id ],
        
            ['rating' => $rating]
           
        );
        
        return $postRating;
    }

    public function deleteRating($id){
       
        //TODO: Raw query
         $del_Rate = RatingModel::where('id', $id)
                    ->delete();
     
         
         return $del_Rate;
     }
     public function updateRating($id,$rating){
       
        //TODO: Raw query
         $update_rate = RatingModel::where('id', $id)
                            ->update(['rating'=> $rating]);
     
         
         return $update_rate;
     }
}