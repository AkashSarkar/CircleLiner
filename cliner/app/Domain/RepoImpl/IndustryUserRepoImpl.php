<?php
namespace App\Domain\RepoImpl;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Repo\IndustryUserRepo;
use Modules\Reporting\Models\IndustryUserModel;
use Modules\Reporting\Models\IndustryModel;

class IndustryUserRepoImpl implements IndustryUserRepo {

    public function storeIndustryUser($user_id, $industry_id)
    {
       //return "ok";
       $user_industry = IndustryUserModel::create([
           'user_id' => $user_id,
           'industry_id' => $industry_id,
       ]);
       return $user_industry;
    }

    public function getIndustryUser($user_id){

        $industry_ids = DB::table('industries_users')
            ->where('user_id', $user_id)
            ->pluck('industry_id');

        $industry_list = DB::table('industries')
            ->select('name')
            ->whereIn('id', $industry_ids)
            ->get();
        return $industry_list;
            //return DB::table('industries_users')->get();
    }

    public function updateIndustryUser($id, $user_id, $industry_id){

        return DB::table('industries_users')
            ->where('id', $id)
            ->where('user_id', $user_id)
            ->update(['industry_id' => $industry_id]);
    }

    public function deleteIndustryUser($id){
        return IndustryUserModel::where('id', $id)->delete();
    }

    public function getIndustryUsers($params){

        return IndustryUserModel::where('industry_id',$params)
                            ->pluck('user_id');

    }


}