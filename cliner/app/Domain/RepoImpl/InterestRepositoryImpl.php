<?php
namespace App\Domain\RepoImpl;

use App\Domain\Repo\InterestRepo;
use Modules\Reporting\Models\InterestModel;
use Illuminate\Support\Facades\DB;

class InterestRepositoryImpl implements InterestRepo {

    public  function storeInterest($interest){
        $interests = new InterestModel;
        $interests->interest = $interest;

        return $interests->save();
    }

    public function getInterest(){
        return DB::table('interests')->select('name')->get();
    }
}