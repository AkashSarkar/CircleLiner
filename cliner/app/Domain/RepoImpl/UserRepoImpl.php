<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/23/18
 * Time: 5:21 PM
 */

namespace App\Domain\RepoImpl;

use App\Domain\Repo\UserRepo;
use Modules\Authentication\Models\DataModel\LoginUser;
use Modules\Authentication\Models\DataModel\User;
use Modules\Authentication\Models\UserModel;
use Modules\UserInfo\Models\DataModel\Profile;

//use Hashids\Hashids;

class UserRepoImpl implements UserRepo
{

    public function save($params)
    {
        $hid = $this->generateHashId();
        return UserModel::create([
            'hash_id' => $hid,
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'gender' => $params['gender'],
            'dob' => $params['dob'],
            'images' => $params['images'],
            'email_verified' => 0,
            'role_id' => $params['role_id'],
        ]);

    }

    public function getUserHashed($id)
    {
        $user = new LoginUser();
        $userData = UserModel::with(['auth', 'apitoken'])->select(
            '*',
            'images->cpp->url as cpp'
        )
            ->where("id", $id)->first();
        $user->castMe($userData);
        return $user;

    }

    public function getUserId($hid)
    {
        if (UserModel::where('hash_id', $hid)->count()) {
            return UserModel::where('hash_id', $hid)->first()->id;
        } else {
            return 0;
        }
    }


    public function generateHashId()
    {
        $hashId = getRandomTokenString(28, env('HASHID_CHAR'));
        $exists = UserModel::select('id')
            ->where('hash_id', '=', $hashId)
            ->count();
        if ($exists > 0) {
            $this->generateHashId();
        } else {
            return $hashId;
        }
    }

    function getUserProfile($hid)
    {
        $oneData = UserModel::with(['auth', 'apitoken'])->select('*', 'images->cpp->url as cpp', 'images->ccp->url as ccp')
            ->where('hash_id', $hid)
            ->first();
        $profile = new Profile();
        $profile->castMe($oneData);
        return $profile;
    }

    function getUser($uid)
    {
        $user = new User();
        $user->castMe(UserModel::with(['auth', 'apitoken'])->select('*', 'images->cpp->url as cpp', 'images->ccp->url as ccp')
            ->where("id", $uid)
            ->first());
        return $user;
    }

    function userIsVerified($uid)
    {
        $user = new User();
        $user->castMe(UserModel::with(['auth', 'apitoken'])->select('*', 'images->cpp->url as cpp', 'images->ccp->url as ccp')
            ->where("id", $uid)
            ->first());
        return $user->emailVerified;
    }

    function getUserHashID($uid)
    {
        return UserModel::where('id', '=', $uid)
            ->first()->hash_id;
    }
}