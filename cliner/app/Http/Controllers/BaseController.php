<?php
/**
 * Created by PhpStorm.
 * User: Crazy Turtle PC 2
 * Date: 4/28/2018
 * Time: 8:19 PM
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

class BaseController extends Controller
{

    public $queries;

    public function __construct()
    {
        $queries = DB::getQueryLog();
    }

    protected function resp($response, $statusCode)
    {
        if ($statusCode >= 200 && $statusCode < 300) {
            return $this->makeRespone($response, $statusCode);
        } else {
            return $this->makeErrorResponse($response['message'], $statusCode);
        }

    }

    protected function makeRespone($data, $statusCode)
    {
        return makeRespone($data, $statusCode);
    }

    protected function makeErrorResponse($errorMessage, $errorCode)
    {

        return makeErrorResponse($errorMessage, $errorCode);
    }


}