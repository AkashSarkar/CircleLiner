<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use DB;
use Auth;
 
use App\User;

class UserController extends Controller
{
    public function __construct()
    {

//          $this->middleware('auth:api');

    }

    public function register(Request $request){
      
        try{
            $validate = $this->validate($request, [
                'first_name' => "required",
                'last_name' => "required",
                'email' => "required|email|unique:users",
                'password' => "required|min:6",
            ]);
           //return Crypt::encrypt($request->input("password"));
          
            if (!$validate) {
                $errors = $validate->errors();
                $errors = json_decode($errors);
                return response()->json(['success' => FALSE, 'message' => $errors,], 422);
            } else {
                
                $user = User::create([
                    'first_name' => $request->input("first_name"),
                    'last_name' => $request->input("last_name"),
                    'email' => $request->input('email'),
                    'password' => Crypt::encrypt($request->input("password"))
                ]);
                
                $responseData["success"] = $user;

                if($user==true){
                    $responseData["message"] = "User created successfully ";
                    return response()->json(($responseData));
                }else{
                    return response()->json($responseData,422);
                }
            }
        }catch(Exception $e) {
            $e->getMessage();
            abort(500);
        }
    }

    public function authentication(Request $request){
      
            $validate = $this->validate($request, [
                
                'email' => "required",
                'password' => "required"
            ]);
           
            $user=DB::table('users')->where('email',$request->email)->first();
            //return response()->json($user);
            //return $request->password;
           if(hash_equals($request->password,Crypt::decrypt($user->password)))
           {
               $apiToken=Crypt::encrypt(time());
               User::where('email',$request->email)
               ->update([
                   'api_token'=>$apiToken
               ]);
               
               return response()->json(['status' => 'success','api_token'=>$apiToken]);
           }else{
               return response()->json(['status' => 'fail'],401);
           }

    }

    public function user(){
        return Auth::user();
    }
}