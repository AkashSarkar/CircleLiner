<?php
/**
 * Created by PhpStorm.
 * User: Crazy Turtle PC 2
 * Date: 4/22/2018
 * Time: 9:46 PM
 */

namespace App\Http\Controllers;

use App\validator\TestFormValidator;
use App\validator\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Posting\Models\PostModel;
use Modules\Posting\Models\PostTypeModel;

class TestController extends BaseController
{


    public function __construct()
    {


    }

    public function register(Request $request)
    {

        $post=$this->postRepo->getPostByUserId(1);
        foreach ($post as $p){
            $p['comments']=$this->commentRepo->getCommentByPostId($p->id);
        }
        return  response()->json($post);
        return $this->authRepo->register();

        //return response()->json($this->reg->register());
    }

    public function test(Request $request,TestFormValidator $testFormValidator){




        try {
            $validate_data =  $testFormValidator->registrationValidation($request->all());

        } catch (ValidationException $e) {

           return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        $this->page=1;
        return $this->makeRespone($request->all(),201);
    }
}