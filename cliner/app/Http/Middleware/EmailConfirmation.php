<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 5/6/18
 * Time: 3:36 PM
 */

namespace App\Http\Middleware;

use Closure;
use DB;

class EmailConfirmation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $valid_token=DB::table('email_subscriptions')->select("email","token")->where([
            ['token',$request->header('token')],
            ['email',$request->header('email')]
        ])->first();
        if(!$valid_token){
            return makeErrorResponse('Unauthorized request.', 401);
        }


        return $next($request);
    }
}