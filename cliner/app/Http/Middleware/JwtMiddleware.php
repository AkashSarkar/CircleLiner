<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 6/9/18
 * Time: 9:15 PM
 */

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {

        if (!$request->hasHeader('Authorization')) {
            return response()->json('Authorization Header not found', 401);
        }

        $token = $request->bearerToken();

        if ($request->header('Authorization') == null || $token == null) {
            return response()->json('No token provided', 401);
        }
        $this->retrieveAndValidateToken($token);
        return $next($request);
    }

    public function retrieveAndValidateToken($token)
    {

        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], 400);
        } catch (Exception $e) {
            return response()->json([
                'Authorization' => $token,
                'error' => 'An error while decoding token.'
            ], 400);
        }

    }
}