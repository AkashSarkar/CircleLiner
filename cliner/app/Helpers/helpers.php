<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/28/18
 * Time: 1:55 PM
 */


if (!function_exists('getRandomTokenString')) {
    function getRandomTokenString($length, $characters = null)
    {
        if ($characters == null) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }
}


if (!function_exists('getToken')) {
    function getToken($user_id)
    {
        $key = env('JWT_SECRET');
        $payload = [];
        $payload['iss'] = 'cliner';
        $payload['sub'] = $user_id;
        $payload['iat'] = time();
        $payload['exp'] = time() + 60 * 60;
        $payload['api_token'] = \Firebase\JWT\JWT::encode($payload, $key);

        return $payload;
    }
}


function isJson($string)
{
    return is_object(json_decode($string));
}

//if (!function_exists('userResponse')) {
//    function userResponse($usr,$usrAuth)
//    {
//        return [
//            'hash_id' => $usr->hash_id,
//            'first_name' => $usr->first_name,
//            'last_name' => $usr->last_name,
//            'email' => $usrAuth->email,
//            'created_at' => $usr->created_at,
//            'updated_at' => $usr->updated_at,
//        ];
//    }
//}
if (!function_exists('makeRespone')) {
    function makeRespone($data, $statusCode)
    {
        $response = [];
        $response['data'] = $data;
        $response['status'] = true;
//        if ($this->page){
//            $response['page']=$this->page;
//        }
        $response["statusCode"] = $statusCode;
        return response()->json($response, $statusCode);
    }
}

if (!function_exists('makeErrorResponse')) {
    function makeErrorResponse($errorMessage, $errorCode)
    {
        $response = [];
        if (isJson($errorMessage)) {
            $errorMessage = json_decode($errorMessage);
        }
        $response['errors'] = $errorMessage;
        $response['status'] = false;
        $response["statusCode"] = $errorCode;

        return response()->json($response, $errorCode);
    }
}

function commentText($faker, $user_hid, $reply = [])
{
    return array(
        "uid" => $faker->randomElement($user_hid),
        "text" => $faker->text,
        'oc' => strtotime($faker->date()),
        "tag" => array(
            $faker->randomElement($user_hid),
            $faker->randomElement($user_hid),
            $faker->randomElement($user_hid)
        ),
        "reply" => ['comments' => $reply, 'commentCounter' => count($reply)]
    );
}

function commentImage($faker, $user_hid, $reply = [])
{
    return array(
        "uid" => $faker->randomElement($user_hid),
        "image" => $faker->imageUrl(),
        'oc' => strtotime($faker->date()),
        "tag" => array(
            $faker->randomElement($user_hid),
            $faker->randomElement($user_hid),
            $faker->randomElement($user_hid)
        ),
        "reply" => ['comments' => $reply, 'commentCounter' => count($reply)]
    );
}

function commentJsonB($faker, $user_hid)
{
    $reply = [];
    for ($i = 0; $i < rand(1, 10); $i++) {
        $reply[getRandomTokenString(28)] = commentText($faker, $user_hid);
    }
    $comments = [];
    for ($i = 0; $i < rand(1, 10); $i++) {
        $comments[getRandomTokenString(28)] = commentText($faker, $user_hid, $reply);
    }
    return ['comments' => $comments, 'commentCounter' => count($comments)];
}


function likeJsonB($faker, $user_hid)
{
    return array(
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
        ),
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
        ),
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
        ),

    );
}

function likeFullJsonB($faker, $user_hid)
{
    $likes = likeJsonB($faker, $user_hid);
    return array(
        'liked' => $likes,
        'total' => count($likes)
    );
}

function imageJsonB($faker, $user_hid, $like = true, $commnet = true)
{
    $response = [
        "url" =>'http://localhost:8000/img/' . rand(1, 25) . '.jpeg',
        'oc' => strtotime($faker->date()),
    ];
    if ($like) {
        $response['likes'] = likeFullJsonB($faker, $user_hid);
    }
    if ($commnet) {
        $response['comments'] = commentJsonB($faker, $user_hid);
    }
    return $response;
}

function industryJsonB($faker)
{
    return array(
        'name' => $faker->company,
        'oc' => strtotime($faker->date()),
    );

}

function interestJsonB($faker)
{
    return array(
        'name' => $faker->company,
        'oc' => strtotime($faker->date()),
    );

}
function membersJsonB($faker, $user_hid)
{
    return array(
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
        ),
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
        ),
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
        ),

    );
}
function ownersJsonB($faker, $user_hid)
{
    return array(
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
            'role'=>'creator',
        ),
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
            'role'=>'admin',
        ),
        $faker->randomElement($user_hid) => array(
            'oc' => strtotime($faker->date()),
            'role'=>'admin',
        ),

    );
}
