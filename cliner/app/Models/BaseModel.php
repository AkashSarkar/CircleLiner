<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 6/28/18
 * Time: 9:01 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}