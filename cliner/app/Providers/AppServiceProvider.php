<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     *Register any route in modules.
     * @return void
     */
    public function boot()
    {
        // For each of the registered modules, include their routes and Views
        $modules = config("module.modules");


        if (count($modules)) {
            foreach ($modules as $key => $module) {
                if (file_exists(__DIR__ . '/../../modules/' . $module . '/routes.php')) {
                    include __DIR__ . '/../../modules/' . $module . '/routes.php';
                }
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //TODO: Bind all the repository and repository implimentation in this function
        $this->app->bind('App\Domain\Repo\UserRepo',
            'App\Domain\RepoImpl\UserRepoImpl');

        $this->app->bind('App\Domain\Repo\UserAuthRepo',
            'App\Domain\RepoImpl\UserAuthRepoImpl');

        $this->app->bind('App\Domain\Repo\ApiTokenRepo',
            'App\Domain\RepoImpl\ApiTokenRepoImpl');

        $this->app->bind('App\Domain\Repo\PostRepo',
            'App\Domain\RepoImpl\PostRepositoryImpl');

        $this->app->bind('App\Domain\Repo\PrivacyRepo',
            'App\Domain\RepoImpl\PrivacyRepositoryImpl');

        $this->app->bind('App\Domain\Repo\SearchKeyRepo',
            'App\Domain\RepoImpl\SearchKeyRepositoryImpl');

        $this->app->bind('App\Domain\Repo\GeoLocationRepo',
            'App\Domain\RepoImpl\GeoLocationRepositoryImpl');

        $this->app->bind('App\Domain\Repo\CommentRepo',
            'App\Domain\RepoImpl\CommentRepoImpl');

        $this->app->bind('App\Domain\Repo\FollowerRepo',
            'App\Domain\RepoImpl\FollowerRepositoryImpl');

        $this->app->bind('App\Domain\Repo\LikeRepo',
            'App\Domain\RepoImpl\LikeRepositoryImpl');

        $this->app->bind('App\Domain\Repo\RatingRepo',
            'App\Domain\RepoImpl\RatingRepositoryImpl');

        $this->app->bind('App\Domain\Repo\ShareRepo',
            'App\Domain\RepoImpl\ShareRepositoryImpl');

        $this->app->bind('App\Domain\Repo\AchievementRepo',
            'App\Domain\RepoImpl\AchievementRepoImpl');

        $this->app->bind('App\Domain\Repo\EducationRepo',
            'App\Domain\RepoImpl\EducationRepoImpl');

        $this->app->bind('App\Domain\Repo\InterestRepo',
            'App\Domain\RepoImpl\InterestRepositoryImpl');

        $this->app->bind('App\Domain\Repo\ProfessionRepo',
            'App\Domain\RepoImpl\ProfessionRepositoryImpl');

        $this->app->bind('App\Domain\Repo\EducationUserRepo',
            'App\Domain\RepoImpl\EducationUserRepoImpl');

        $this->app->bind('App\Domain\Repo\InterestUserRepo',
            'App\Domain\RepoImpl\InterestUserRepositoryImpl');

        $this->app->bind('App\Domain\Repo\ProfessionUserRepo',
            'App\Domain\RepoImpl\ProfessionUserRepositoryImpl');

        $this->app->bind('App\Domain\Repo\IndustryRepo',
            'App\Domain\RepoImpl\IndustryRepositoryImpl');

        $this->app->bind('App\Domain\Repo\IndustryUserRepo',
            'App\Domain\RepoImpl\IndustryUserRepoImpl');

        $this->app->bind('App\Domain\Repo\ImageRepo',
            'App\Domain\RepoImpl\ImageRepoImpl');

        $this->app->bind('App\Domain\Repo\JobRepo',
            'App\Domain\RepoImpl\JobRepoImpl');

        $this->app->bind('App\Domain\Repo\GroupRepo',
            'App\Domain\RepoImpl\GroupRepoImpl');

        $this->app->bind('App\Domain\Repo\PageRepo',
            'App\Domain\RepoImpl\PageRepoImpl');

        $this->app->bind('App\Domain\Repo\NotificationRepo',
            'App\Domain\RepoImpl\NotificationRepoImpl');
    }
}
