<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 5/8/18
 * Time: 3:40 PM
 */

namespace App\validator;


class UserRelationValidation extends Validator
{
    public function industryFormValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'industry_id' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function professionFormValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'industry_id' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }
}