<?php
/**
 * Created by PhpStorm.
 * User: Crazy Turtle PC 2
 * Date: 4/28/2018
 * Time: 7:43 PM
 */

namespace App\validator;


class TestFormValidator extends Validator
{


    public function loginValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'email' => 'required',
            'password' => 'required',
            'title' => 'required');

        return parent::validate($data, $rules, $custom_errors);
    }


    public function registrationValidation(array $data, array $rules = array(), array $custom_errors = array())
    {

        $rules = array(
            'name' => 'required');
        return parent::validate($data, $rules, $custom_errors);
    }
}