<?php
/**
 * Created by PhpStorm.
 * User: Crazy Turtle PC 2
 * Date: 4/28/2018
 * Time: 7:43 PM
 */

namespace App\validator;


class CommentValidator extends Validator
{


    public function commentStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'text' => 'required',
            'postId' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function commentUpdateValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'text' => 'required',

        );
        return parent::validate($data, $rules, $custom_errors);
    }

    public function getPostIdValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'post_id' => 'required',

        );
        return parent::validate($data, $rules, $custom_errors);
    }

    public function getCommentId(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'id' => 'required',

        );
        return parent::validate($data, $rules, $custom_errors);
    }


}