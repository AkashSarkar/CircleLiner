<?php
/**
 * Created by PhpStorm.
 * User: Crazy Turtle PC 2
 * Date: 4/28/2018
 * Time: 7:40 PM
 */

namespace App\validator;


use Exception;
use Illuminate\Support\MessageBag;

class BaseException extends Exception
{
    protected $_errors;

    public function __construct( $errors = null, $message = null, $code = 0, Exception $previous = null ) {
        $this->_set_errors( $errors );

        parent::__construct( $message, $code, $previous );
    }

    protected function _set_errors( $errors ) {
        if ( is_string( $errors ) ) {
            $errors = array(
                'error' => $errors,
            );
        }

        if ( is_array( $errors ) ) {
            $errors = new MessageBag( $errors );
        }

        $this->_errors = $errors;
    }

    public function get_errors() {
        return $this->_errors;
    }
}