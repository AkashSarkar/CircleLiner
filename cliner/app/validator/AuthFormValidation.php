<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/29/18
 * Time: 1:33 PM
 */

namespace App\validator;


class AuthFormValidation extends Validator
{
    public function loginValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'email' => 'required|email',
            'password'=>'required|min:6'
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function registrationValidation(array $data, array $rules = array(), array $custom_errors = array())
    {

        $rules = array(
            'token'=>"required"
        );
        return parent::validate($data, $rules, $custom_errors);
    }

    public function subscriptionValidation(array $data, array $rules = array(), array $custom_errors = array())
    {

        $rules = array(
            'first_name' => "required",
            'last_name' => "",
            'email' => "required|email",
            'password' => "required|min:6",
        );
        return parent::validate($data, $rules, $custom_errors);
    }
    public function passwordValidation(array $data, array $rules = array(), array $custom_errors = array())
    {

        $rules = array(
            'old_password' => "required|min:6",
            'new_password' => "required|min:6",
        );
        return parent::validate($data, $rules, $custom_errors);
    }
}