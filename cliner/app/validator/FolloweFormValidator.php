<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 5/14/18
 * Time: 4:25 PM
 */

namespace App\validator;


class FolloweFormValidator extends Validator
{
    public function followIdValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'follower' => 'required',
            'user_id' => 'required',
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function unfollowIdValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'follower' => 'required',
            'user_id' => 'required',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

}