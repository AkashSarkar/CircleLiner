<?php


namespace App\validator;


class ShareFormValidator extends Validator
{

    public function shareValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'user_id' => 'required',
            'post_id' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }
}