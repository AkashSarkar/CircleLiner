<?php
/**
 * Created by PhpStorm.
 * User: Tuli
 * Date: 5/7/2018
 * Time: 5:26 PM
 */

namespace App\validator;

class EducationValidator extends Validator
{

    public function educationsStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'education'=>'bail|required|string',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function educationStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'education_id'=>'required',
            'user_id'=> 'required',

        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function educationUpdateValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'education_id'=>'required',
            'user_id'=> 'required',

        );
        return parent::validate($data, $rules, $custom_errors);
    }

}