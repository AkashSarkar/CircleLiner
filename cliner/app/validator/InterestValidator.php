<?php
/**
 * Created by PhpStorm.
 * User: Tuli
 * Date: 5/7/2018
 * Time: 7:22 PM
 */

namespace App\validator;

class InterestValidator extends Validator
{
    public function interestsStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'interest'=>'bail|required|string',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function interestStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'interest_id'=>'required',
            'user_id'=> 'required',

        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function interestUpdateValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'interest_id'=>'required',
            'user_id'=> 'required',

        );
        return parent::validate($data, $rules, $custom_errors);
    }

}