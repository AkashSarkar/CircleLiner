<?php
/**
 * Created by PhpStorm.
 * User: Tuli
 * Date: 5/7/2018
 * Time: 1:00 PM
 */

namespace App\validator;

class ProfessionValidator extends Validator
{

    public function professionsStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'profession'=>'bail|required|string',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function professionStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'profession_id'=>'required',
            'user_id'=> 'required',

        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function professionUpdateValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'profession_id'=>'required',
            'user_id'=> 'required',

        );
        return parent::validate($data, $rules, $custom_errors);
    }

}