<?php


namespace App\validator;


class LikeFormValidator extends Validator
{


    public function likeValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'post_id' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function getLikeValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'post_id' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function getLikeUserListValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'post_id' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }


}