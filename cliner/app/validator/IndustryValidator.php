<?php
/**
 * Created by PhpStorm.
 * User: Tuli
 * Date: 5/7/2018
 * Time: 8:30 PM
 */

namespace App\validator;

class IndustryValidator extends Validator
{

    public function industriesStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'industry'=>'bail|required|string',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function industryStoreValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'industry_id'=>'required',
            'user_id'=> 'required',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function industryUpdateValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'industry_id'=>'required',
            'user_id'=> 'required',

        );
        return parent::validate($data, $rules, $custom_errors);
    }

}