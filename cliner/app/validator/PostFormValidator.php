<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/29/18
 * Time: 3:39 PM
 */

namespace App\validator;

class PostFormValidator extends Validator
{
    public function idValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'tag' => 'required',
            'ownerUserId' => 'required',
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function commentPermissionValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'hash_id' => 'required',
            'ownerUserId' => 'required',
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function sharePermissionValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'hash_id' => 'required',
            'ownerUserId' => 'required',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function statusPostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'description' => 'required|max:1000',
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function awardPostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'title' => 'required',
            'publishingDate' => 'required|date',
            'receivingPlace' =>'required',
            'description' => 'max:255'
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function promotionPostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'jobPositionName' => 'required',
            'companyName' => 'required',
            'departmentName' =>'required',
            'description' => 'max:255'
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function eventPostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'title' => 'required',
            'startDate' => 'required',
            'endDate ' =>'required',
            'description' => 'max:255'
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function articlePostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'title' => 'required',
            'referenceLink' => 'required',
            'publishingDate' =>'required',
            'description' => 'required|max:1000'
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function conferencePostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'title' => 'required',
            'topic' => 'required',
            'role' =>'required',
            'description' => 'max:255'
        );

        return parent::validate($data, $rules, $custom_errors);
    }
    public function projectPostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'title' => 'required',
            'role' => 'required',
            'startDate' =>'required|date',
            'skillRequired' => 'required',
            'description' => 'required|max:255'
        );

        return parent::validate($data, $rules, $custom_errors);
    }

    public function getPostValidation(array $data, array $rules = array(), array $custom_errors = array())
    {
        $rules = array(
            'post_id' => 'required'
        );

        return parent::validate($data, $rules, $custom_errors);
    }

}