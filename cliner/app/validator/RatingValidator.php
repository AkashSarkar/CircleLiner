<?php

namespace App\validator;


class RatingValidator extends Validator
{

    public $rules = array(
        'rating' => 'required',
        'user_id'=>'required',
        'post_id'=>'required'

    );
}