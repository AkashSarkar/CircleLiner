<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('user_auth', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')
                ->references('id')->on('roles');
        });

        Schema::table('api_tokens', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('post_type_id')
                ->references('id')->on('post_types');
        });

        Schema::table('geolocations', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('geolocation_type_id')
                ->references('id')->on('geolocation_types');
            $table->foreign('connection_type_id')
                ->references('id')->on('connection_types');
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('post_id')
                ->references('id')->on('posts');
        });


        Schema::table('shares', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('post_id')
                ->references('id')->on('posts');
        });

        Schema::table('followers', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('follower')
                ->references('id')->on('users');
        });

        Schema::table('connections', function (Blueprint $table) {
            $table->foreign('connection_type_id')
                ->references('id')->on('connection_types');
            $table->foreign('user_id')
                ->references('id')->on('users');
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->foreign('type_id')
                ->references('id')->on('page_types');
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->foreign('type_id')
                ->references('id')->on('group_types');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('groups', function (Blueprint $table) {
            $table->dropForeign([
                'type_id'
            ]);
        });
        Schema::table('pages', function (Blueprint $table) {
            $table->dropForeign([
                'type_id'
            ]);
        });


        Schema::table('connections', function (Blueprint $table) {
            $table->dropForeign([
                'connection_type_id'

            ]);
            $table->dropForeign([
                'user_id'

            ]);
        });

        Schema::table('followers', function (Blueprint $table) {
            $table->dropForeign([
                'user_id',

            ]);
            $table->dropForeign([
                'follower'
            ]);
        });


        Schema::table('shares', function (Blueprint $table) {
            $table->dropForeign([
                'user_id'

            ]);
            $table->dropForeign([
                'post_id'

            ]);

        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->dropForeign([
                'user_id'

            ]);
            $table->dropForeign([
                'post_id'

            ]);
        });

        Schema::table('geolocations', function (Blueprint $table) {
            $table->dropForeign([
                'user_id'
            ]);
            $table->dropForeign([

                'geolocation_type_id'
            ]);
            $table->dropForeign([

                'connection_type_id'
            ]);
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign([
                'user_id'
            ]);
            $table->dropForeign([
                'post_type_id'
            ]);
        });

        Schema::table('api_tokens', function (Blueprint $table) {
            $table->dropForeign([
                'user_id'
            ]);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign([
                'role_id'
            ]);
        });

        Schema::table('user_auth', function (Blueprint $table) {
            $table->dropForeign([
                'user_id'
            ]);
        });
    }
}
