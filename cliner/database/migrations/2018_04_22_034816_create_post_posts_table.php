<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash_id')->unique();
            $table->text('post_data');
            $table->boolean('is_commentable')->default(1);
            $table->boolean('is_shareable')->default(1);
            $table->integer('post_type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->jsonb('likes')->nullable();
            $table->jsonb('comments')->nullable();
            $table->jsonb('images')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
