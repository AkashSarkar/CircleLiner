<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsrRespRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
           
           
            $table->decimal('rating')->unsigned();
          
            $table->integer('user_id')->unsigned();
            $table->integer('post_id')->unsigned();
            
            $table->timestamps();
            //TODO:prev foreign key user_id->users ,post_id->posts
            
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
