<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string("hash_id", 30)->unique();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('display_name')->nullable();

            $table->string('gender', 15)->nullable();
            $table->date('dob')->nullable();
            $table->boolean('email_verified');
            $table->integer('role_id')->unsigned();
            $table->jsonb('images')->nullable();
            $table->jsonb('professions')->nullable();
            $table->jsonb('educations')->nullable();
            $table->jsonb('industries')->nullable();
            $table->jsonb('interests')->nullable();
            $table->jsonb('achievements')->nullable();
            $table->jsonb('notifications')->nullable();
            $table->jsonb('acitivities')->nullable();
            $table->jsonb('settings')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
