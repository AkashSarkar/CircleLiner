<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsrRelPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string("hash_id", 30)->unique();
            $table->integer('type_id')->unsigned();
            $table->string('name',30);
            $table->text('sdesc')->nullable();
            $table->text('ldesc')->nullable();
            $table->jsonb('details')->nullable();
            $table->jsonb('likes')->nullable();
            $table->jsonb('owners')->nullable();
            $table->jsonb('members')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
