<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 7/8/18
 * Time: 1:32 PM
 */
use Illuminate\Database\Seeder;
class PageSeeder extends Seeder
{
    public function run()
    {
        //
        factory(Modules\UserRelation\Models\PageModel::class,5)->create();
    }
}