<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role' => 'User',
            '_tag' => 'user',
        ]);

        DB::table('roles')->insert([
            'role' => 'Admin',
            '_tag' => 'admin',
        ]);

        DB::table('roles')->insert([
            'role' => 'Guest',
            '_tag' => 'guest',
        ]);

        DB::table('roles')->insert([
            'role' => 'GroupAdmin',
            '_tag' => 'groupAdmin',
        ]);

        DB::table('roles')->insert([
            'role' => 'GroupMember',
            '_tag' => 'groupMember',
        ]);
    }
}
