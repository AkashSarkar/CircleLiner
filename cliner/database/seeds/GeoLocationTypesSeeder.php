<?php

use Illuminate\Database\Seeder;

class GeoLocationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('geolocation_types')->insert([
        //     'label' => 'User',
        //     '_tag' => 'user',
        // ]);

        DB::table('geolocation_types')->insert([
            'label' => 'user',
            '_tag' => 'user',
        ]);

        DB::table('geolocation_types')->insert([
            'label' => 'post',
            '_tag' => 'post',
        ]);

        DB::table('geolocation_types')->insert([
            'label' => 'group',
            '_tag' => 'group',
        ]);

       
    }
}
