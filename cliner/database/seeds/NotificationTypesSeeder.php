<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/10/18
 * Time: 5:09 PM
 */

use Illuminate\Database\Seeder;
class NotificationTypesSeeder extends Seeder
{
    public function run()
    {


        DB::table('notification_types')->insert([
            'n_type' => 'job'
        ]);
        DB::table('notification_types')->insert([
            'n_type' => 'post'
        ]);
        DB::table('notification_types')->insert([
            'n_type' => 'like'
        ]);
        DB::table('notification_types')->insert([
            'n_type' => 'comment'
        ]);
       DB::table('notification_types')->insert([
            'n_type' => 'share'
        ]);
        DB::table('notification_types')->insert([
            'n_type' => 'follow'
        ]);
        DB::table('notification_types')->insert([
            'n_type' => 'block'
        ]);


    }
}