<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        factory(Modules\Authentication\Models\UserModel::class, 1)->create(['hash_id' => 'YbgYJR1BY3WGfhtyGlQAQigCglj1'])
            ->each(function ($u) {
                $u->auth()->save(factory(Modules\Authentication\Models\UserAuthModel::class)->make(['email' => 'user@netexpro.com']));
                $u->apitoken()->save(factory(Modules\Authentication\Models\ApiTokenModel::class)->make());
                $u->posts()->save(factory(Modules\Posting\Models\PostModel::class)->make(['hash_id' => '9LgIZBDoa1MOlcUbzz7I']));
                for ($i = 0; $i != 49; $i++) {
                    $u->posts()->save(factory(Modules\Posting\Models\PostModel::class)->make());
                }
            });

        factory(Modules\Authentication\Models\UserModel::class, 9)->create()
            ->each(function ($u) {
                $u->auth()->save(factory(Modules\Authentication\Models\UserAuthModel::class)->make());
                $u->apitoken()->save(factory(Modules\Authentication\Models\ApiTokenModel::class)->make());
                for ($i = 0; $i != 15; $i++) {
                    $u->posts()->save(factory(Modules\Posting\Models\PostModel::class)->make());
                }
            });
        factory(Modules\Authentication\Models\UserModel::class, 1)->create(['hash_id' => 'vsyrVEWjn4lESRyPzX2PozqG1jUqJB'])
            ->each(function ($u) {
                $u->auth()->save(factory(Modules\Authentication\Models\UserAuthModel::class)->make());
                $u->apitoken()->save(factory(Modules\Authentication\Models\ApiTokenModel::class)->make());
                $u->posts()->save(factory(Modules\Posting\Models\PostModel::class)->make(['hash_id' => '2a6ia4xy811rwxtqwavs']));
                for ($i = 0; $i != 15; $i++) {
                    $u->posts()->save(factory(Modules\Posting\Models\PostModel::class)->make());
                }
            });

    }
}
