<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 7/8/18
 * Time: 12:59 PM
 */
use Illuminate\Database\Seeder;
class PageTypeSeeder extends Seeder
{
    public function run()
    {
        DB::table('page_types')->insert([
            'name' => 'Entertainment',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),
        ]);
        DB::table('page_types')->insert([
            'name' => 'Education',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),
        ]);
        DB::table('page_types')->insert([
            'name' => 'Profession',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),
        ]);
        DB::table('page_types')->insert([
            'name' => 'Company',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),
        ]);

    }
}