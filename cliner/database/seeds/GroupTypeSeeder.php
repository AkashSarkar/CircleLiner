<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 7/8/18
 * Time: 1:00 PM
 */
use Illuminate\Database\Seeder;
class GroupTypeSeeder extends Seeder
{

    public function run()
    {
        DB::table('group_types')->insert([
            'name' => 'Entertainment',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),
        ]);
        DB::table('group_types')->insert([
            'name' => 'Education',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),

        ]);
        DB::table('group_types')->insert([
            'name' => 'Profession',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),

        ]);
        DB::table('group_types')->insert([
            'name' => 'Company',
            'rules' => json_encode(array(
                'privacy'=>'public'
            )),

        ]);

    }
}