<?php

use Illuminate\Database\Seeder;

class ConnectionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('connection_types')->insert([
            'label' => 'Location',
            '_tag' => 'location',
        ]);

        DB::table('connection_types')->insert([
            'label' => 'Profession',
            '_tag' => 'profession',
        ]);

        DB::table('connection_types')->insert([
            'label' => 'Industry',
            '_tag' => 'industry',
        ]);
        DB::table('connection_types')->insert([
            'label' => 'Company',
            '_tag' => 'company',
        ]);
        DB::table('connection_types')->insert([
            'label' => 'Job',
            '_tag' => 'job',
        ]);
    }
}
