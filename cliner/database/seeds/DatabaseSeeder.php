<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            ConnectionTypesSeeder::class,
            PostTypesSeeder::class,
            GeoLocationTypesSeeder::class,
            NotificationTypesSeeder::class,
            UserSeeder::class,
            JobSeeder::class,
            GroupTypeSeeder::class,
            PageTypeSeeder::class,
            GroupSeeder::class,
            PageSeeder::class
        ]);


    }
}
