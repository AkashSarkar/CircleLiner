<?php

use Illuminate\Database\Seeder;
use Hashids\Hashids;

class PostTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $h = getRandomTokenString(28);
        DB::table('post_types')->insert([
            'hash_id' => $h,
            'label' => 'Status',
            '_tag' => 'status',
        ]);

        $h = getRandomTokenString(28);
        DB::table('post_types')->insert([
            'hash_id' => $h,
            'label' => 'Award',
            '_tag' => 'award',
        ]);

        $h = getRandomTokenString(28);
        DB::table('post_types')->insert([
            'hash_id' => $h,
            'label' => 'Promotion',
            '_tag' => 'promotion',
        ]);
        $h = getRandomTokenString(28);
        DB::table('post_types')->insert([
            'hash_id' => $h,
            'label' => 'Event',
            '_tag' => 'event',
        ]);
        $h = getRandomTokenString(28);
        DB::table('post_types')->insert([
            'hash_id' => $h,
            'label' => 'Article',
            '_tag' => 'article',
        ]);
        $h = getRandomTokenString(28);
        DB::table('post_types')->insert([
            'hash_id' => $h,
            'label' => 'Conference',
            '_tag' => 'conference',
        ]);
        $h = getRandomTokenString(28);
        DB::table('post_types')->insert([
            'hash_id' => $h,
            'label' => 'Project',
            '_tag' => 'project',
        ]);
    }
}
