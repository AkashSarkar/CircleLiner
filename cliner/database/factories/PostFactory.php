<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/11/18
 * Time: 1:13 PM
 */

$factory->define(Modules\Posting\Models\PostModel::class, function (Faker\Generator $faker) {
    //static $password;
    $award = array(
        'title' => $faker->streetName,
        'publishingDate' => $faker->date(),
        'receivingPlace' => $faker->address,
        'description' => $faker->paragraph,
    );
    $promotion = array(
        'jobPositionName' => $faker->jobTitle,
        'companyName' => $faker->company,
        'departmentName' => $faker->text,
        'description' => $faker->paragraph,
    );
    $event = array(
        'title' => $faker->name,
        'startDate' => $faker->date(),
        'endDate ' => $faker->date(),
        'description' => $faker->paragraph
    );
    $article = array(
        'title' => $faker->domainName,
        'referenceLink' => $faker->url,
        'publishingDate' => $faker->date(),
        'description' => $faker->paragraph,
    );
    $conference = array(
        'title' => $faker->streetName,
        'topic' => $faker->sentence,
        'role' => $faker->jobTitle,
        'description' => $faker->paragraph
    );

    $project = array(
        'title' => $faker->name,
        'role' => $faker->jobTitle,
        'startDate' => $faker->date(),
        'skillRequired' => $faker->sentence,
        'description' => $faker->paragraph
    );
    $status = array(
        'description' => $faker->text
    );
    $p_type = rand(1, 7);
    $p = array();
    $p[1] = json_encode($status);
    $p[2] = json_encode($award);
    $p[3] = json_encode($promotion);
    $p[4] = json_encode($event);
    $p[5] = json_encode($article);
    $p[6] = json_encode($conference);
    $p[7] = json_encode($project);
    $users = Modules\Authentication\Models\UserModel::pluck('id')->toArray();
    $user_hid = Modules\Authentication\Models\UserModel::pluck('hash_id')->toArray();;

    $likes = likeFullJsonB($faker, $user_hid);

    $comment = commentJsonB($faker, $user_hid);

    $image = array();
    for ($i = 0; $i < $faker->randomNumber(1); $i++) {
        $image[getRandomTokenString(28)] = imageJsonB($faker, $user_hid);
    }
    return [
        'hash_id' => getRandomTokenString(28),
        'post_data' => $p[$p_type],
        'post_type_id' => $p_type,
        'user_id' => $faker->randomElement($users),
        'likes' => json_encode($likes),
        'comments' => json_encode($comment),
        'images' => json_encode($image),

    ];
});
