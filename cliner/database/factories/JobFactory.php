<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/30/18
 * Time: 10:24 AM
 */

$factory->define(Modules\UserResponse\Models\JobModel::class, function (Faker\Generator $faker) {

    $uhid = Modules\Authentication\Models\UserModel::pluck('hash_id')->toArray();
    $industries = array(
        getRandomTokenString(28) => industryJsonB($faker),
        getRandomTokenString(28) => industryJsonB($faker),
        getRandomTokenString(28) => industryJsonB($faker),
    );
    $application = array(
        $faker->randomElement($uhid) => array(
            'oc' => $faker->randomNumber(8),
            'status' => $faker->sentence,
            'body' => $faker->paragraph,
        )
    );
    $skills = array(
        $faker->randomElement(['Python', 'Java', 'C++', 'Angular', 'Database']),
        $faker->randomElement(['Python', 'Java', 'C++', 'Angular', 'Database']),
        $faker->randomElement(['Python', 'Java', 'C++', 'Angular', 'Database']),
    );
    return [
        'title' => $faker->jobTitle,
        'type' => $faker->company,
        'description' => $faker->text(255),
        'location' => $faker->city,
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'label_of_skils' => $faker->randomElement(['Entry Label', 'Mid Label', 'C Label', 'Senior Label']),
        'industry' => json_encode($industries),
        'job_application' => json_encode($application),
        'core_of_skills' => json_encode($skills),
        'no_of_vacancy' => rand(1, 8),
    ];
});