<?php

use Illuminate\Support\Facades\Crypt;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(Modules\Authentication\Models\UserAuthModel::class, function (Faker\Generator $faker) {
    return [
        'user_name' => $faker->userName,
        'email' => $faker->email,
        'password' => Crypt::encrypt("12345678"),
    ];

});

$factory->define(Modules\Authentication\Models\ApiTokenModel::class, function (Faker\Generator $faker) {

    $payload = getToken(function (array $apiToken) {
        return $apiToken['user_id'];
    });
    return [
        'api_token' => $payload['api_token'],
        'user_agent' => $faker->userAgent,
        'last_activity' => $faker->unixTime,
    ];

});


