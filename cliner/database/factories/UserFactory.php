<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/10/18
 * Time: 5:37 PM
 */

use Illuminate\Support\Facades\Crypt;
use Modules\Authentication\Models\UserModel;
use Illuminate\Support\Facades\DB;

$factory->define(Modules\Authentication\Models\UserModel::class, function (Faker\Generator $faker) {


//    $images = [
//        "pp" => [
//            [
//                "url" => "image1.jpg",
//                "created" => 1454667
//            ],
//            [
//                "url" => "image2.jpg",
//                "created" => 1454667
//            ]
//        ],
//        "cp" => [
//            [
//                "url" => "image1.jpg",
//                "created" => 1454667
//            ],
//            [
//                "url" => "image2.jpg",
//                "created" => 1454667
//            ]
//        ],
//        "cpp" => [
//            "url" => "image1.jpg",
//            "changed" => 242452
//        ],
//        "ccp" => [
//            "url" => "image2.jpg",
//            "changed" => 132442
//        ]
//    ];
    $hid = getRandomTokenString(28);
    $user_hid = Modules\Authentication\Models\UserModel::pluck('hash_id')->toArray();;
    if ($user_hid == null) {
        $user_hid = array($hid);
    }

    $info = array(
        'website' => $faker->url,
        'phone' => $faker->phoneNumber,
        'birthPlace' => $faker->country,
        'maritalStatus' => $faker->randomElement(['Married', 'Unmarried']),
        'description' => $faker->title(),
        'acc_info' => array(
            'follow' => $faker->randomElement(['public', 'custom', 'followers']),
            'n_s' => $faker->boolean,//Notifications Sound
            'n_e' => $faker->boolean,//Notifications Email
            'f_birth' => $faker->boolean,//Friend’s Birthdays
            'm_s' => $faker->boolean,//Chat Message Sound
        ),
        'block' => array(
            getRandomTokenString(28) => $faker->randomElement($user_hid),
            getRandomTokenString(28) => $faker->randomElement($user_hid),
            getRandomTokenString(28) => $faker->randomElement($user_hid),
            getRandomTokenString(28) => $faker->randomElement($user_hid),
            getRandomTokenString(28) => $faker->randomElement($user_hid),
        ),
    );
    $social = array(
        'facebook' => array(
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
        ),
        'twitter' => array(
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
        ),
        'linked_in' => array(
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
            getRandomTokenString(28) => $faker->url,
        ),
    );
    $job = array(
        'skills' => array(
            getRandomTokenString(28) => $faker->randomElement(['java', 'c', 'python']),
            getRandomTokenString(28) => $faker->randomElement(['ruby', 'angular']),
            getRandomTokenString(28) => $faker->randomElement(['english', 'php', 'laravel']),
        ),
        'active' => $faker->boolean,
        'location' => array(
            getRandomTokenString(28) => $faker->city,
            getRandomTokenString(28) => $faker->city,
            getRandomTokenString(28) => $faker->city,
        ),
        'role' => $faker->randomElement(['manager', 'developer', 'officer']),
    );
    $setting = array(
        'info' => $info,
        'social' => $social,
        'job' => $job,
    );
    $image = array(
        'album' => array(
            "pp" => array(
                getRandomTokenString(28) => imageJsonB($faker, $user_hid),
                getRandomTokenString(28) => imageJsonB($faker, $user_hid),
                getRandomTokenString(28) => imageJsonB($faker, $user_hid),

            ),
            "cp" => array(
                getRandomTokenString(28) => imageJsonB($faker, $user_hid),
                getRandomTokenString(28) => imageJsonB($faker, $user_hid),
                getRandomTokenString(28) => imageJsonB($faker, $user_hid),
                getRandomTokenString(28) => imageJsonB($faker, $user_hid),
            ),
        ),
        'cpp' => imageJsonB($faker, $user_hid, false, false),
        'ccp' => imageJsonB($faker, $user_hid, false, false),
    );
    $n_type = \Illuminate\Support\Facades\DB::table('notification_types')->pluck('id')->toArray();

    $notification = array(
        'msg' => $faker->text,
        'r' => $hid,      //receiver
        'n' => $faker->randomElement($user_hid),     //notifier
        'oc' => $faker->randomNumber(8),
        '_t' => $faker->url,//target
        'typ' => $faker->randomElement($n_type),
    );
//    $eid = \Illuminate\Support\Facades\DB::table('educations')->pluck('id')->toArray();
//    $education = array(
//        $faker->randomElement($eid)=>$faker->randomNumber(6),
//        $faker->randomElement($eid)=>$faker->randomNumber(6),
//        $faker->randomElement($eid)=>$faker->randomNumber(6),
//    );
//    $pid = \Illuminate\Support\Facades\DB::table('professions')->pluck('id')->toArray();
//    $profession = array(
//        $faker->randomElement($pid)=>$faker->randomNumber(6),
//        $faker->randomElement($pid)=>$faker->randomNumber(6),
//    );
//    $inter = \Illuminate\Support\Facades\DB::table('interests')->pluck('id')->toArray();
//    $interests = array(
//        $faker->randomElement($inter)=>$faker->randomNumber(6),
//        $faker->randomElement($inter)=>$faker->randomNumber(6),
//        $faker->randomElement($inter)=>$faker->randomNumber(6),
//        $faker->randomElement($inter)=>$faker->randomNumber(6),
//    );


    $industries = array(
        getRandomTokenString(28) => industryJsonB($faker),
        getRandomTokenString(28) => industryJsonB($faker),
        getRandomTokenString(28) => industryJsonB($faker),
        getRandomTokenString(28) => industryJsonB($faker),
    );
    $interest = array(
            'name' => $faker->title,
        'oc' =>strtotime($faker->date()),
    );
    $interests = array(
        getRandomTokenString(28) => interestJsonB($faker),
        getRandomTokenString(28) => interestJsonB($faker),
        getRandomTokenString(28) => interestJsonB($faker),
        getRandomTokenString(28) => interestJsonB($faker),
    );


    return [
        'hash_id' => $hid,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'display_name' => function (array $user) {
            return $user['first_name'] . " " . $user['last_name'];
        },
        'gender' => $faker->randomElement(['female', 'male']),
        'dob' => $faker->date(),
        'email_verified' => rand(0, 1),
        'role_id' => rand(1, 5),
        'images' => json_encode($image),
        'professions' => json_encode($notification),
        'educations' => json_encode($notification),
        'interests' => json_encode($interests),
        'achievements' => json_encode($notification),
        'industries' => json_encode($industries),
        'acitivities' => json_encode($notification),
        'notifications' => json_encode($notification),
        'settings' => json_encode($setting),
    ];
});
