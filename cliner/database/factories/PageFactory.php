<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/30/18
 * Time: 10:24 AM
 */

$factory->define(Modules\UserRelation\Models\PageModel::class, function (Faker\Generator $faker) {

    $user_hid = Modules\Authentication\Models\UserModel::pluck('hash_id')->toArray();
    //page Type id
    $ptid=Modules\UserRelation\Models\PageTypeModel::pluck('id')->toArray();
    $hid=getRandomTokenString(28);
    $members = membersJsonB($faker, $user_hid);
    $owners = ownersJsonB($faker, $user_hid);
    $likes = likeFullJsonB($faker, $user_hid);
    $details=array(
        'oc'=>strtotime($faker->time()),
    );
    return [
        'hash_id'=>$hid,
        'name'=>$faker->lastName,
        'type_id'=>$faker->randomElement($ptid),
        'members'=>json_encode($members),
        'owners'=>json_encode($owners),
        'sdesc'=>$faker->text,
        'ldesc'=>$faker->paragraph,
        'details'=>json_encode($details),
        'likes'=>json_encode($likes),

    ];
});