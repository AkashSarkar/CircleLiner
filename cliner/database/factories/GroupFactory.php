<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/30/18
 * Time: 10:24 AM
 */

$factory->define(Modules\UserRelation\Models\GroupModel::class, function (Faker\Generator $faker) {

    $user_hid = Modules\Authentication\Models\UserModel::pluck('hash_id')->toArray();
    //group Type id
    $gtid = Modules\UserRelation\Models\GroupTypeModel::pluck('id')->toArray();
    $hid = getRandomTokenString(28);
    $members = membersJsonB($faker, $user_hid);
    $owners = ownersJsonB($faker, $user_hid);
    return [
        'hash_id' => $hid,
        'name' => $faker->lastName,
        'type_id' => $faker->randomElement($gtid),
        'members' => json_encode($members),
        'owners' => json_encode($owners),
    ];
});