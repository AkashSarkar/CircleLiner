##This is all about jsonb columns
####Table name users:
#####jsonb columns are:
1. images,
2. professions
3. educations,
4. industries,
5. interests,
6. activities,
7. achievements,
8. notifications,
9. settings

#####structure of images:-

- elements:
 1. $like:like array
 2. $user_hid:user hash_id
 3. $comment: comment array
 4. $i:singel image element
 5. url:image url
 6. oc:creation date
 7. cpp:current profile photo
 8. ccp:current cover photo
 9. cp:cover photos 
 10. pp:profile photos
 11. album:array of cp and pp
 12. each array indexing with random string length of 4
- example:
<pre>
$i = array(
    "url" => $faker->imageUrl(),**
    "oc" => $faker->randomNumber(6),
); 
$like = array(
        $faker->randomElement($user_hid) => array(
            "oc" => $faker->randomNumber(7),
        ),
        $faker->randomElement($user_hid) => array(
            "oc" => $faker->randomNumber(7),
        ),
);
$comment=array();
</pre>
- each cpp consists of $i
- each ccp consists of $i
- each cp consists array of $i with likes : $like and comments : $comment   
- each pp consists array of $i with likes : $like and comments : $comment   
- https://jsoneditoronline.org/?id=e564055978154ffdad1fefe200b52492