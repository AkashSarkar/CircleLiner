<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 7:05 PM
 */

namespace Modules\UserInfo\Controllers;


use App\Domain\Repo\UserRepo;
use App\Domain\Repo\IndustryUserRepo;
use Modules\UserInfo\Models\DataModel\Profile;
use Modules\Authentication\Models\UserModel;
use App\Http\Controllers\BaseController;
use App\validator\ValidationException;

class UserInfoController extends BaseController
{
    protected $userRepo;
    protected $industryUserRepo;

    public function __construct(UserRepo $userRepo, IndustryUserRepo $industryUserRepo)
    {
        $this->userRepo = $userRepo;
        $this->industryUserRepo = $industryUserRepo;
    }

    public function userInfo($hid)
    {
        $dataModel = [];
        try {
            $dataModel[] = $this->userRepo->getUserProfile($hid);
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
            return $this->resp($response, $response['status']);
        }
        return $this->makeRespone($dataModel, 201);
    }
}