<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 6/28/18
 * Time: 11:24 AM
 */

namespace Modules\UserInfo\Controllers;


use App\Domain\Repo\ImageRepo;
use App\Http\Controllers\BaseController;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

class ImageController extends BaseController
{

    protected $imageRepo;

    public function __construct(ImageRepo $imageRepo)
    {
        $this->imageRepo = $imageRepo;
    }


    public function index($hid)
    {
        $response = [];
        $status_code = 200;
        try {
            $response['images'] = $this->imageRepo->getUserImg($hid);
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $status_code = 500;
        }
        return $this->resp($response, $status_code);
    }

    public function updateAvatar(Request $request)
    {
        $response = [];
        $status_code = 200;
        if ($request->has('avatar')) {
            $this->imageRepo->updateAvatar($request->avatar, JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256'])->sub);
        }
        return $this->resp($response, $status_code);


    }

    public function updateCoverPhoto(Request $request)
    {
        $response = [];
        $status_code = 200;
        if ($request->has('coverphoto')) {
            $this->imageRepo->updateCoverPhoto($request->coverphoto, JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256'])->sub);
        }
        return $this->resp($response, $status_code);


    }

    public function upload(Request $request)
    {
        $response = [];
        $statusCode = 201;
        try {
            $response['metadata'] = [];
            if ($request->hasFile('file')) {
                if ($request->file('file')->isValid()) {
//                    $response['metadata'] = $request->file('file');
                    $file = $request->file('file')->move('img/y' . date('Y') . '/m' . date('m') . '/d' . date('d'), $request->fileName);
                    $response['metadata']['fullPath'] = $file->getPath() . '/' . $file->getFilename();
                    $response['metadata']['mimetype'] = $file->getMimeType();
                    $response['metadata']['filename'] = $file->getFilename();
                    $response['metadata']['size'] = $file->getSize();
                    $response['downloadURL'] = url($file->getPath() . '/' . $file->getFilename());

                }
            }
        } catch (\Exception $e) {
            $response['message'] = "Something went wrong";
            $statusCode = $e->getCode();
        }
        return $this->resp($response, $statusCode);

    }

    public function download(Request $request)
    {

    }

    public function save(Request $request)
    {
        $response = [];
        $status_code = 200;
        try {
            $response['request'] = $request;
            if ($request->has('image')) {
                $response['image'] = $request->image;
                $response['imageURL'] = $request->image['URL'];
                $response['id'] = $this->imageRepo->save($request->image['URL'], JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256'])->sub);
            } else {
                $response['message'] = "No image found to save";
//                $status_code = 401;
            }

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $status_code = 500;
        }
        return $this->resp($response, $status_code);

    }

    public function delete(Request $request)
    {

    }

}