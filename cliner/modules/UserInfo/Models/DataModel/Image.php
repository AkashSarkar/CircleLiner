<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 6/28/18
 * Time: 11:26 AM
 */

namespace Modules\UserInfo\Models\DataModel;


use Modules\UserResponse\Models\DataModel\Comment;
use Modules\UserResponse\Models\DataModel\Comments;
use Modules\UserResponse\Models\DataModel\Like;

class Image
{
    public $id;
    public $creationDate;
    public $URL;
    public $fullPath;
    public $ownerUserId;
    public $deleteDate;
    public $deleted;
    public $likes;
    public $score;
    public $comments;
    public $commentCounter;

    function __construct()
    {
        $this->id = '';
        $this->creationDate = 0;
        $this->URL = '';
        $this->fullPath = '';
        $this->ownerUserId = '';
        $this->deleteDate = '';
        $this->deleted = false;
        $this->likes = [];
        $this->score = 0;
        $this->comments = [];
        $this->commentCounter = 0;
    }

    public function castMe($obj)
    {
        if ($obj) {
            if (isset($obj->oc)) {
                $this->creationDate = $obj->oc;
            }
            if (isset($obj->url)) {
                $this->URL = $obj->url;
            }
            if (isset($obj->url)) {
                $this->fullPath = $obj->url;
            }

            if (isset($obj->likes)) {
                $likes = $obj->likes;
                foreach ($likes->liked as $key => $value) {
                    $like = new Like();
                    $like->userId = $key;
                    $like->castMe($value);
                    $this->likes[$key] = $like;
                }
                $this->score = $likes->total;
            }

            if (isset($obj->comments)) {
                $comments = new Comments();
                $comments->castMe($obj->comments);
                $this->comments = $comments->comments;
                $this->commentCounter = $comments->commentCounter;
            }

        }
    }
}