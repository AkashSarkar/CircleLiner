<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 3:44 PM
 */

namespace Modules\UserInfo\Models\DataModel;


class Profile
{
    public $avatar;
    public $fullName;
    public $banner;
    public $tagLine;
    public $creationDate;
    public $industry;
    public $concentratedIndustry;
    public $email;
    public $birthday;
    public $webUrl;
    public $companyName;
    public $twitterId;

    function __construct()
    {
        $this->avatar = '';
        $this->fullName = '';
        $this->banner = '';
        $this->tagLine = '';
        $this->creationDate = '';
        $this->industry = [];
        $this->concentratedIndustry = '';
        $this->email = '';
        $this->birthday = 0;
        $this->webUrl = '';
        $this->companyName = '';
        $this->twitterId = '';
    }

    public function castMe($obj)
    {
        if ($obj != null) {
            $this->avatar = $obj->cpp;
            $this->fullName = $obj->first_name . ' ' . $obj->last_name;
            $this->banner = $obj->ccp;
            $this->tagLine = $obj->tagLine;
            $this->creationDate = strtotime($obj->created_at);
            $this->industry = $obj->industries;
            $this->concentratedIndustry = $obj->concentratedIndustry;
            if ($obj->auth) {
                $this->email = (string)$obj->auth->email;
            }
            $this->birthday = $obj->dob;
            $this->webUrl = $obj->webUrl;
            $this->companyName = $obj->companyName;
            $this->twitterId = $obj->twitterId;
        }
    }
}