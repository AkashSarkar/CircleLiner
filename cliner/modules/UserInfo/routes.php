<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 6/8/18
 * Time: 8:54 PM
 */

$route = 'user-info';
$multiRoute = function () use ($route) {
    $this->app->router->group([
        'prefix' => $route,
        'module' => 'UserRelation',
        'namespace' => 'Modules\UserInfo\Controllers',
        'middleware' => 'jwt.auth'
    ], function ($router) {

        $router->get('{hid}', 'UserInfoController@userInfo');

        $router->get('images/{hid}', 'ImageController@index');
        $router->post('update-avatar', 'ImageController@updateAvatar');
        $router->post('update-cover', 'ImageController@updateCoverPhoto');

        $router->get('image/{hid}', 'ImageController@download');
        $router->post('image', 'ImageController@upload');
        $router->post('image/save', 'ImageController@save');
        $router->delete('image/{hid}', 'ImageController@delete');

    });
};

$this->app->router->group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);


Route::get('routes', function () {
    $routeCollection = Route::getRoutes();
    echo "<table style='width:100%'>";
    echo "<tr>";
    echo "<td width='10%'><h4>HTTP Method</h4></td>";
    echo "<td width='10%'><h4>Route</h4></td>";
//    echo "<td width='10%'><h4>Prefix</h4></td>";
//    echo "<td width='70%'><h4>Corresponding Action</h4></td>";
    echo "</tr>";
    foreach ($routeCollection as $value) {
        echo "<tr>";
        echo "<td>" . (string)$value['method'] . "</td>";
        echo "<td>" . (string)$value['uri'] . "</td>";
//        echo "<td>" . $value->getPrefix() . "</td>";
//        echo "<td>" . (string)$value['action'] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
});