<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 3:13 PM
 */

namespace Modules\UserRelation\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class GroupModel extends BaseModel
{
    protected $table = 'groups';
    protected $fillable = [
        'hash_id',
        'name',
        'type_id',
        'members',
        'owners',
        'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'updated_at'
    ];
}