<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 7/8/18
 * Time: 1:26 PM
 */
namespace Modules\UserRelation\Models\DataModel;
class Page{
    public $id;
    public $hid;
    public $typeId;
    public $name;
    public $sdesc;
    public $ldesc;
    public $details;
    public $likes;
    public $owners;
    public $members;

    function __construct()
    {
        $this->id='';
        $this->hid='';
        $this->typeId='';
        $this->name='';
        $this->sdesc='';
        $this->ldesc='';
        $this->details='';
        $this->likes='';
        $this->owners='';
        $this->members='';
    }

    public function castMe($obj){
        if($obj){
            if($obj->id){
                $this->id=$obj->id;
            }
            if($obj->hash_id){
                $this->hid=$obj->hash_id;
            }
            if($obj->type_id){
                $this->typeId=$obj->type_id;
            }
            if($obj->name){
                $this->name=$obj->name;
            }
            if($obj->sdesc){
                $this->sdesc=$obj->sdesc;
            }
            if($obj->ldesc){
                $this->ldesc=$obj->ldesc;
            }
            if($obj->details){
                $this->details=json_decode($obj->details);
            }
            if($obj->likes){
                $this->likes=json_decode($obj->likes);
            }
            if($obj->owners){
                $this->owners=json_decode($obj->owners);
            }
            if($obj->members){
                $this->members=json_decode($obj->members);
            }
        }

    }

}