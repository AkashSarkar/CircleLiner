<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 7/8/18
 * Time: 1:26 PM
 */
namespace Modules\UserRelation\Models\DataModel;
class Group{
      public $id;
      public $hid;
      public $name;
      public $typeId;
      public $members;
      public $owners;

      function __construct()
      {
          $this->id=(string)'';
          $this->hashId=(string)'';
          $this->name=(string)'';
          $this->typeId=(string)'';
          $this->members='';
          $this->owners='';
      }

      public function castMe($obj){
          if($obj){
              if($obj->id){
                  $this->id=$obj->id;
              }
              if($obj->hash_id){
               $this->hid=$obj->hash_id;
              }
              if($obj->name){
                  $this->name=$obj->name;
              }
              if($obj->type_id){
                  $this->typeId=$obj->type_id;
              }
              if($obj->members){
                  $this->members=$obj->members;
              }
              if($obj->owners){
                  $this->owners=$obj->owners;
              }
          }

      }

}