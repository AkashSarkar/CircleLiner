<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 3:13 PM
 */

namespace Modules\UserRelation\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PageModel extends BaseModel
{
    protected $table = 'pages';
    protected $fillable = [
        'hash_id',
        'type_id',
        'name',
        'sdesc',
        'ldesc',
        'details',
        'likes',
        'owners',
        'members',
        'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'updated_at'
    ];
}
