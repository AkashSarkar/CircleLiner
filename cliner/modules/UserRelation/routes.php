<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 5/8/18
 * Time: 3:20 PM
 */

$route = 'user-relation';
$multiRoute = function () use ($route) {
    $this->app->router->group(['prefix' => $route, 'module' => 'UserRelation', 'namespace' => 'Modules\UserRelation\Controllers',
        'middleware' => 'auth'], function ($router) use ($route) {

        $router->get('connection/industry', 'ConnectionController@get_industry_posts');
        $router->get('connection/profession', 'ConnectionController@get_profession_posts');
        /*
         * Group CRUD
         **/
        $router->get($route . '/groups', 'GroupController@index');
        $router->get($route . '/groups/{hid}', 'GroupController@show');
        $router->post($route . '/groups', 'GroupController@store');
        $router->put($route . '/groups/{hid}', 'GroupController@update');
        $router->delete($route . '/groups/{hid}', 'GroupController@destroy');
        /*
        * Page CRUD
        **/
        $router->get($route . '/pages', 'PageController@index');
        $router->get($route . '/pages/{hid}', 'PageController@show');
        $router->post($route . '/pages', 'PageController@store');
        $router->put($route . '/pages/{hid}', 'PageController@update');
        $router->delete($route . '/pages/{hid}', 'PageController@destroy');

    });
};

$this->app->router->group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);