<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 5/16/18
 * Time: 3:41 PM
 */

namespace Modules\UserRelation\Controllers;


use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use App\validator\ValidationException;
use App\Domain\Repo\GroupRepo;
class GroupController extends BaseController
{
    private $groupRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GroupRepo $groupRepo)
    {
        parent::__construct();
        $this->groupRepo = $groupRepo;
    }

    public function index(){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function show(){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function store(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function update(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function destroy(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

}