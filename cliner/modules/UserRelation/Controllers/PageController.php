<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 7/8/18
 * Time: 11:03 AM
 */

namespace Modules\UserRelation\Controllers;


use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\validator\ValidationException;
use App\Domain\Repo\PageRepo;
class PageController extends BaseController
{
    private $pageRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PageRepo $pageRepo)
    {
        parent::__construct();
        $this->pageRepo = $pageRepo;
    }

    public function index(){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function show(){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function store(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function update(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function destroy(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

}