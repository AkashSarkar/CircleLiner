<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 5/8/18
 * Time: 3:24 PM
 */

namespace Modules\UserRelation\Controllers;


use App\Domain\Repo\IndustryUserRepo;
use App\Domain\Repo\PostRepo;
use App\Domain\Repo\ProfessionUserRepo;
use App\validator\UserRelationValidation;
use Illuminate\Http\Request;

use App\validator\ValidationException;
use App\Http\Controllers\BaseController;

class ConnectionController extends BaseController
{
    private $industry_user_repo;
    private $profession_user_repo;
    private $post_repo;
    public function __construct(IndustryUserRepo $industry_user_repo,ProfessionUserRepo $profession_user_repo, PostRepo $post_repo)
    {
        $this->industry_user_repo = $industry_user_repo;
        $this->profession_user_repo=$profession_user_repo;
        $this->post_repo = $post_repo;
    }

    public function get_industry_posts(Request $request,UserRelationValidation $userRelationValidation){
        $response=[];
        try{
            $userRelationValidation->industryFormValidation($request->all());
            $user_ids=$this->industry_user_repo->getIndustryUsers($request->industry_id);
            $posts=$this->post_repo->getPosts($user_ids,0);
            if($posts){
                $response['industry_posts']=$posts;
            }

        }catch(ValidationException $e){
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }

        return $this->makeRespone($response, 201);
    }

    public function get_profession_posts(Request $request,UserRelationValidation $userRelationValidation){
        $response=[];
        try{
            $userRelationValidation->professionFormValidation($request->all());
            $user_ids=$this->profession_user_repo->getProfessionUsers($request->profession_id);
            $posts=$this->post_repo->getPosts($user_ids);
            if($posts){
                $response['profession_posts']=$posts;
            }

        }catch(ValidationException $e){
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }

        return $this->makeRespone($response, 201);
    }
}