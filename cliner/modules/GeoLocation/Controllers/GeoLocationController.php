<?php

namespace Modules\GeoLocation\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\GeoLocationRepo;


class GeoLocationController extends BaseController{

    private $geoLocationRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeoLocationRepo $geoLocationRepo)
    {
        $this->geoLocationRepo = $geoLocationRepo;
    }

//    public function map_location(){
//        return view(map);
//    }

    public function store_location(Request $request)
    {
        //return $request;
        try{

            //$validate_data = $this->commentValidator->commentStoreValidation($request->all());
            $geolocation = [];

            $geolocation['lat'] = $request['lat'];
            $geolocation['lng'] = $request['lng'];
            
            $user_id =$request->user_id;
            //$post_id=$request->post_id;

            $location = $this->geoLocationRepo->storeGeoLocation(json_encode($geolocation), $user_id);

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        //$this->page=1;
        return $this->makeRespone($location,200);

    }

    public function get()
    {
        return $this->geoLocationRepo->getGeoLocation();
    }
   
}