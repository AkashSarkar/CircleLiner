<?php

namespace modules\GeoLocation\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;


class GeoLocationModel extends BaseModel
{

    protected $table="geolocations";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'geolocation', 'user_id' , 'geolocation_type_id', 'connection_type_id'
  
    ];


    
}