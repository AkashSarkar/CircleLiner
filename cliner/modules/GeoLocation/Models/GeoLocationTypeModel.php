<?php

namespace modules\GeoLocation\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;


class GeoLocationTypeModel extends BaseModel
{

    protected $table="geolocation_types";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label' , '_tag ',
    ];

    
}