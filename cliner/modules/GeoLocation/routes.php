<?php
/**
 * Created by PhpStorm.
 * User: tuli
 * Date: 5/9/18
 * Time: 10:13 AM
 */


$route = 'geo-location';
$multiRoute = function () use ($route) {
    //TODO: Make the api seo friendly
    $this->app->router->group(['module' => 'GeoLocation', 'namespace' => 'Modules\GeoLocation\Controllers', 'middleware' => 'jwt.auth'], function ($router) use ($route) {

        $router->post($route . '/location', 'GeoLocationController@store_location');
        //$router->get($route.'/location', 'GeoLocationController@get_users_by_location');
        //$router->get($route.'/posts_location', 'GeoLocationController@get_posts_by_location');
        //$router->get($route.'/groups_location', 'GeoLocationController@get_groups_by_location');
        //$router->patch($route.'/location', 'GeoLocationController@update_location');
        //$router->delete($route.'/location', 'GeoLocationController@delete_location');


    });
};
//TODO: define V_PREFIX in ..env
$this->app->router->group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);
