<?php

$route = 'user-response';

$multiRoute = function () use ($route) {
    $this->app->router->group(['module' => 'UserResponse', 'namespace' => 'Modules\UserResponse\Controllers'/*, 'middleware' => 'jwt.auth'*/],
        function ($router) use ($route) {

            //START COMMENT ROUTES
            $router->get($route . '/getcommentscount', 'CommentController@commentscount_by_postid');
            $router->get($route . '/allcommentscount', 'CommentController@all_comments_count');
            $router->get($route . '/commentswithuserlist', 'CommentController@comments_with_userlist');

            $router->get($route . '/comment/{post_id}', 'CommentController@get');
            $router->post($route . '/comment', 'CommentController@store');
            $router->put($route . '/comment/{id}', 'CommentController@update');
            $router->delete($route . '/comment', 'CommentController@destroy');
            //END COMMENT ROUTES

            //START LIKE
            $router->get($route . '/like-all', 'LikeController@all_likecount');
            $router->get($route . '/like-user', 'LikeController@get_likes_with_userlist');

            $router->get($route . '/like-post/{post_id}', 'LikeController@getLikePost');
            $router->post($route . '/like-post/{post_id}', 'LikeController@storePost');
            $router->delete($route . '/like-post/{post_id}', 'LikeController@destroyPost');
            //delete
            //post

            //END LIKE

            //START RATING ROUTES
            $router->post($route . '/postrating', 'RatingController@post');
            $router->delete($route . 'deleterate/{id}', 'RatingController@destroy');
            $router->put($route . 'updaterate/{id}', 'RatingController@update');
            $router->get($route . '/getrating/{post_id}', 'RatingController@get');
            //END RATING ROUTES

            //START FOLLOWER ROUTES
            $router->get($route . '/getfollower', 'FollowerController@get');

            //$router->get($route.'/getfollower', 'FollowerController@get');
            //END FOLLOWER ROUTES
            $router->get($route . '/getfollower', 'FollowerController@gets');

            //START SHARE ROUTES

            $router->get($route . '/getshare', 'ShareController@get');
            //START SHARE ROUTES

            //delete
            $router->delete($route . '/share', 'ShareController@destroy_share');
            //post
            $router->post($route . '/share', 'ShareController@store_share');

            $router->post($route . '/store-follow', 'FollowerController@store_follow');
            $router->delete($route . '/unfollow', 'FollowerController@unfollow');

          /*
          * Page CRUD
          **/
            $router->get($route . '/notifications', 'NotificationController@index');
            $router->get($route . '/notifications/{hid}', 'NotificationController@show');
            $router->post($route . '/notifications', 'NotificationController@store');
            $router->put($route . '/notifications/{hid}', 'NotificationController@update');
            $router->delete($route . '/notifications/{hid}', 'NotificationController@destroy');


        });
};

//TODO: define V_PREFIX in .env
Route::group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);
