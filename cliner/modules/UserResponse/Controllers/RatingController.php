<?php

namespace Modules\UserResponse\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
//controller
use App\Http\Controllers\BaseController;
//repo
use App\Domain\Repo\RatingRepo;
//validator
use App\validator\RatingValidator;
use App\validator\ValidationException;

class RatingController extends BaseController{
    private $ratingRepo,$ratingValidator;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RatingRepo $ratingRepo,RatingValidator $ratingValidator)
    {
        $this->ratingRepo = $ratingRepo;
        $this->ratingValidator=$ratingValidator;
    }

 
    public function get($post_id)
    {
        try{
            return $this->ratingRepo->getRating($post_id);
        }catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        } 
        
    }

    public function post(Request $request)
    {
        try {
            $validate_data = $this->ratingValidator->validate($request->all());
            $post_id=$request->post_id;
            $user_id=$request->user_id;
            $rating=$request->rating;
           // return $rating;
        return $this->ratingRepo->postRating($post_id,$user_id,$rating);
        } catch (ValidationException $e) {

           return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        
       return $this->makeRespone($request->all(),201);
       
    }

    public function destroy($id)
    {

        try{
            return $this->ratingRepo->deleteRating($id);
        }catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        } 
       
    }
    public function update(Request $request, $id)
    {
        try{
            $rating=$request->rating;
            return $this->ratingRepo->updateRating($id,$rating);
        }catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
       
        
       
    }
   
}