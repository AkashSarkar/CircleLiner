<?php

namespace Modules\UserResponse\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Repo\FollowerRepo;
use App\Domain\Repo\UserRepo;
use Illuminate\Http\Response;
use App\Http\Controllers\BaseController;
use App\validator\FolloweFormValidator;
use App\validator\ValidationException;

class FollowerController extends BaseController{
    private $followerRepo;
    private $userRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FollowerRepo $followerRepo,UserRepo $userRepo)
    {
        $this->followerRepo = $followerRepo;
        $this->userRepo = $userRepo;
    }


    public function get()
    {
        return $this->followerRepo->getFollowerByUserId();
    }
    public function store_follow(Request $request,FolloweFormValidator $followertFormValidator)
    {
        try {
            $validate_data = $followertFormValidator->followIdValidation($request->all());
            $request['follower'] = $this->userRepo->getUserId($request->follower);
            $request['user_id'] = $this->userRepo->getUserId($request->user_id);
            $store_data = $this->followerRepo->storeFollow($request);
            if($store_data){
                $response['message'] = "You followed successfully";
                $response['status'] = 201;
            }else{
                $response['message'] = "Something went worng";
                $response['status'] = 500;
            }
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
        }
        return $this->resp($response,$response['status']);

    }
    public function unfollow(Request $request,FolloweFormValidator $followertFormValidator)
    {
        try {
            $validate_data = $followertFormValidator->unfollowIdValidation($request->all());
            $request['follower'] = $this->userRepo->getUserId($request->follower);
            $request['user_id'] = $this->userRepo->getUserId($request->user_id);
            $result = $this->followerRepo->unfollow($request);
            if($result){
                $response['message'] = "deleted successfully";
                $response['status'] = 200;
            }else{
                $response['message'] = "To unfollow follow user first";
                $response['status'] = 422;
            }
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
        }
        return $this->resp($response, $response['status']);
    }
    
}