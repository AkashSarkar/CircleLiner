<?php

namespace Modules\UserResponse\Controllers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
//controller
use App\Http\Controllers\BaseController;
//Repo
use App\Domain\Repo\LikeRepo;
use App\Domain\Repo\PostRepo;
use App\Domain\Repo\UserRepo;
//validator
use App\validator\LikeFormValidator;
use App\validator\ValidationException;

class LikeController extends BaseController
{
    private $likeRepo;
    private $like_form_validator;
    private $userRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LikeRepo $likeRepo, PostRepo $postRepo, UserRepo $userRepo)
    {
        $this->likeRepo = $likeRepo;
        $this->postRepo = $postRepo;
        $this->userRepo = $userRepo;
    }

    public function storePost(Request $request, $post_id)
    {
        $uid = JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256'])->sub;
        $uhid = $this->userRepo->getUserHashID($uid);
        $pid = $this->postRepo->hashIdToId($post_id);
        $res = $this->likeRepo->storeLikePost($pid, $uhid);

    }

    public function update(Request $request, LikeFormValidator $like_form_validator)
    {

        try {
            $post_like = 'user_id or post_id is not valid';
            $validate_data = $like_form_validator->likeValidation($request->all());
            $user_id = $this->userRepo->getUserId($request->user_id);
            $post_id = $this->postRepo->hashIdToId($request->post_id);
            if ($post_id && $user_id) {
                $post_like = $this->likeRepo->storeLike($post_id, $user_id);
            }

        } catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }


        return $this->makeRespone($post_like, 201);
    }


    public function destroyPost(Request $request, $post_id)
    {
        try {
            $uid = JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256'])->sub;
            $uhid = $this->userRepo->getUserHashID($uid);
            $pid = $this->postRepo->hashIdToId($post_id);
            $del_like = $this->likeRepo->deleteLikePost($pid, $uhid);

        } catch (\Exception $e) {
            $e->getMessage();
            abort(500);
        }
        return $this->makeRespone($del_like, 200);
    }

    public function getLikePost(Request $res, $post_id)
    {

        $get_like = 0;
        try {
            $id = $this->postRepo->hashIdToId($post_id);
            if ($id) {
                $get_like = $this->likeRepo->getLikePost($id);
            }
        } catch (\Exception $e) {

            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }

        return $this->makeRespone($get_like, 200);

    }


    public function all_likecount()
    {
        $total_like = $this->likeRepo->allLikeCount();
        return $this->makeRespone($total_like, 200);
    }

    public function get_likes_with_userlist(Request $request, LikeFormValidator $like_form_validator)
    {
        $response = [];
        // return $request->all();
        try {
            if ($request->has('post_id')) {
                $validate_data = $like_form_validator->getLikeUserListValidation($request->all());
                $post_id = $this->postRepo->hashIdToId($request->post_id);

                if ($post_id) {
                    $userData = $this->likeRepo->allLikeWithUserlist($post_id);
                    foreach ($userData as $idx => $usr) {
                        $user = [];
                        $user['name'] = $usr->first_name . " " . $usr->last_name;
                        $user['hashId'] = $usr->hash_id;
                        $response[] = $user;
                    }
                }
            } else {
                return $this->makeErrorResponse("Request-URI is not valid", 404);

            }

        } catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
        return $this->makeRespone($response, 200);

    }


}

