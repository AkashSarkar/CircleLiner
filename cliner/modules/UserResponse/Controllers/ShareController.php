<?php

namespace Modules\UserResponse\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
//controller
use App\Http\Controllers\BaseController;
//repo
use App\Domain\Repo\ShareRepo;
use App\Domain\Repo\PostRepo;
use App\Domain\Repo\UserRepo;
//validator
use App\validator\ShareFormValidator;
use App\validator\ValidationException;
class ShareController extends BaseController{
    private $shareRepo;
    private $userRepo;
    private $postRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ShareRepo $shareRepo,PostRepo $postRepo,UserRepo $userRepo)
    {
        $this->shareRepo = $shareRepo;
        $this->postRepo=$postRepo;
        $this->userRepo=$userRepo;
    }

    public function destroy_share(Request $request)
    {
        try{
            if($request->has('id'))
            {
                $share= $this->shareRepo->deleteShare($request->id);
            }
            else{
                return $this->makeErrorResponse("Request-URI is not valid", 404);
            }
        }catch(Exception $e) {
            $e->getMessage();
            abort(500);
        }
        return $this->makeRespone($share,200);
    }
   
    public function store_share(Request $request,ShareFormValidator $shareFormValidator){

         try {
             $post_share='user_id or post_id is not valid';
             $validate_data =  $shareFormValidator->shareValidation($request->all());
             $user_id=$this->userRepo->getUserId($request->user_id);
             $post_id=$this->postRepo->hashIdToId($request->post_id);
             if( $post_id && $user_id) {
                 $post_share = $this->shareRepo->storeShare($post_id, $user_id);
             }
         } catch (ValidationException $e) {
 
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
         }
 
       
         return $this->makeRespone($post_share,201);
     }

}