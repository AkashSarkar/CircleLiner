<?php

namespace Modules\UserResponse\Controllers;

use App\validator\CommentValidator;
use App\validator\ValidationException;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\CommentRepo;
use App\Domain\Repo\PostRepo;
use App\Domain\Repo\UserRepo;

class CommentController extends BaseController
{
    private $commentRepo, $commentValidator, $postRepo, $userRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CommentRepo $commentRepo, CommentValidator $commentValidator, PostRepo $postRepo, UserRepo $userRepo)
    {
        $this->commentRepo = $commentRepo;
        $this->commentValidator = $commentValidator;
        $this->postRepo = $postRepo;
        $this->userRepo = $userRepo;
    }

    public function store(Request $request)
    {
        $response = [];
        $response['statusCode'] = 201;
        $response['message'] = "Successful Comment";
        try {
            $validate_data = $this->commentValidator->commentStoreValidation($request->all());

            //DATA FETCH
            $note = $request->text;
            $parent_id = 0;
            $user_id = $this->userRepo->getUserId($request->userId);
            $post_id = $this->postRepo->hashIdToId($request->postId);
            if ($request->has('parent_id')) {
                $parent_id = $request->parent_id;
            }
            //DATA FETCH END


            $response['comment_id'] = $this->commentRepo->storeComment($note, $parent_id, $user_id, $post_id);

        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['statusCode'] = 401;
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage() . " " . $e->getLine() . " " . $e->getFile() . " " . $e->getTraceAsString();
            $response['statusCode'] = 500;
        }
        return $this->resp($response, $response['statusCode']);

    }


    //Get comments of a post
    public function get(Request $request, $post_hid)
    {
        try {
            $post_id = $this->postRepo->hashIdToId($post_hid);
            $comments = $this->commentRepo->getCommentsByPostId($post_hid);
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }

        if ($comments) {
            return $this->makeRespone($comments, 200);
        } else {
            return "No comments yet";
        }

    }


    //Update a comment
    public function update(Request $request)
    {
        try {

            if ($request->has('id')) {
                $validate_data = $this->commentValidator->commentUpdateValidation($request->all());
                $id = $request->id;
                $note = $request->text;
                $updated_comment = $this->commentRepo->updateComment($id, $note);
            }
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            return $e;
        }

        //$this->page=1;
        return $this->makeRespone($updated_comment, 200);
    }


    //delete comment
    public function destroy(Request $request)
    {
        try {

            if ($request->has('id')) {
                $validate_data = $this->commentValidator->getCommentId($request->all());
                $comment_delete = $this->commentRepo->deleteComment($request->id);
            }
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());

        } catch (Exception $e) {
            return $this->makeErrorResponse("Something went worng", 500);
        }
        if ($comment_delete) {
            return $this->makeRespone($comment_delete, 200);
        } else {
            return "Comment already deleted";
        }
    }

    //Comments count for single post
    public function commentscount_by_postid(Request $request)
    {

        try {

            if ($request->has('post_id')) {
                $validate_data = $this->commentValidator->getPostIdValidation($request->all());

                $post_id = $this->postRepo->hashIdToId($request->post_id);
                $comments_count_by_postid = $this->commentRepo->commentsCountByPostId($post_id);
            }
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }

        return $this->makeRespone($comments_count_by_postid, 200);
    }


    public function comments_with_userlist(Request $request)
    {

        $response = [];

        try {

            if ($request->has('post_id')) {
                $validate_data = $this->commentValidator->getPostIdValidation($request->all());
                $post_id = $this->postRepo->hashIdToId($request->post_id);
                $userData = $this->commentRepo->commentsWithUserlist($post_id);

                foreach ($userData as $idx => $usr) {
                    $user = [];
                    $user['name'] = $usr->first_name . " " . $usr->last_name;
                    //$user['hashId']=$usr->uid;
                    $response[] = $user;
                }
            }
        } catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
        return $this->makeRespone($response, 200);
    }


    public function all_comments_count()
    {

        try {
            $all_comments_count = $this->commentRepo->allCommentsCount();
        } catch (\Exception $e) {
            $e->getMessage();
            abort(500);
        }
        return $this->makeRespone($all_comments_count, 200);
    }


}

