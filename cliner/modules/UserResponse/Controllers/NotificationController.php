<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 7/8/18
 * Time: 11:03 AM
 */

namespace Modules\UserResponse\Controllers;


use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\validator\ValidationException;
use App\Domain\Repo\NotificationRepo;

class NotificationController extends BaseController
{
    private $notificationRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NotificationRepo $notificationRepo)
    {
        parent::__construct();
        $this->notificationRepo = $notificationRepo;
    }

    public function index(){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function show(){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function store(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function update(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function destroy(Request $request){
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

}