<?php
namespace Modules\UserResponse\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class FollowerModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'followers';
    protected $fillable = [

        'follower',
        'user_id'
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}