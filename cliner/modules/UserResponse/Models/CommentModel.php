<?php
namespace Modules\UserResponse\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class CommentModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'comments';
    protected $fillable = [
        'note',
        'parent_id',
        'user_id',
        'post_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];


    public function countComments(){

    }
}