<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/22/18
 * Time: 4:56 PM
 */

namespace Modules\UserResponse\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class JobModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'jobs';
    protected $fillable = [
        'title',
        'type',
        'description',
        'location',
        'start_date',
        'end_date',
        'label_of_skils',
        'industry',
        'job_application',
        'core_of_skills',
        'no_of_vacancy',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}

