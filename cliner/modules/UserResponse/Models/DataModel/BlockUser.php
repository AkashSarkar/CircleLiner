<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 5:29 PM
 */

namespace Modules\UserResponse\Models\DataModel;


class BlockUser
{
public $blockedBy;
public $blockedUserId;

function __construct()
{
    $this->blockedBy='';
    $this->blockedUserId='';
}
public function castMe($obj)
{
    $this->blockedBy=$obj->blockedBy;
    $this->blockedUserId=$obj->blockedUserId;

}
}