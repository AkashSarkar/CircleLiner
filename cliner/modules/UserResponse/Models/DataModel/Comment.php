<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/24/18
 * Time: 3:55 PM
 */

namespace Modules\UserResponse\Models\DataModel;


class Comment
{
    public $id;
    public $tag;
    public $postId;
    public $text;
    public $reply;
    public $replyCounter;
    public $image;
    public $score;
    public $creationDate;
    public $userDisplayName;
    public $userAvatar;
    public $userId;
    public $hasChild;

    function __construct()
    {
        $this->uid = '';
        $this->tag = [];
        $this->postId = '';
        $this->text = '';
        $this->reply = [];
        $this->replyCounter = 0;
        $this->image = '';
        $this->score = 0;
        $this->creationDate = 0;
        $this->userDisplayName = 'Test System';
        $this->userAvatar = 'https://lorempixel.com/640/480/?48456';
        $this->userId = '';
        $this->hasChild = false;
    }

    public function castMe($obj)
    {
        if ($obj) {
            if (isset($obj->uid)) {
                $this->uid = $obj->uid;
            }
            if (isset($obj->oc)) {
                $this->creationDate = $obj->oc;
            }
            if (isset($obj->tag)) {
                $this->tag = $obj->tag;
            }
            if (isset($obj->text)) {
                $this->text = $obj->text;
            }
            if (isset($obj->image)) {
                $this->image = $obj->image;
            }
            if (isset($obj->uid)) {
                $this->userId = $obj->uid;
            }
            if (isset($obj->reply)) {
                $reply = new Comments();
                $reply->postId = $this->postId;
                $reply->castMe($obj->reply);
                $this->reply = $reply->comments;
                $this->replyCounter = $reply->commentCounter;
                if (count($this->reply)) {
                    $this->hasChild = true;
                }

            }
        }

    }
}