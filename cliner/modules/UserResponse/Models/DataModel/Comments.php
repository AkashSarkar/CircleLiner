<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/24/18
 * Time: 3:55 PM
 */

namespace Modules\UserResponse\Models\DataModel;


class Comments
{
    public $postId;
    public $comments;
    public $commentCounter;


    function __construct()
    {
        $this->postId = '';
        $this->comments = [];
        $this->commentCounter = 0;

    }

    public function castMe($obj)
    {
        if ($obj) {
            foreach ($obj->comments as $key => $value) {
                $comment = new Comment();
                $comment->id = $key;
                $comment->postId = $this->postId;
                $comment->postId2 = $key;
                $comment->castMe($value);
                $this->comments[$key] = $comment;
            }
            $this->commentCounter = $obj->commentCounter;
        }

    }
}