<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 7/8/18
 * Time: 6:03 PM
 */
namespace Modules\UserResponse\Models\DataModel;
class Notification{
    public $notifier;
    public $receiver;
    public $url;
    public $oc;
    public $msg;
    public $type;

    function __construct()
    {
        $this->sender='';
        $this->receiver='';
        $this->url='';
        $this->oc='';
        $this->msg='';
        $this->type='';
    }

    public function castMe($obj){
        if($obj){
            if($obj->n){
                $this->sender=$obj->n;
            }
            if($obj->r){
                $this->receiver=$obj->r;
            }
            if($obj->_t){
                $this->url=$obj->_t;
            }
            if($obj->oc){
                $this->oc=$obj->oc;
            }
            if($obj->msg){
                $this->msg=$obj->msg;
            }
            if($obj->type){
                $this->type=$obj->type;
            }
        }

    }
}