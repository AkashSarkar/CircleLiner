<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/24/18
 * Time: 4:48 PM
 */

namespace Modules\UserResponse\Models\DataModel;


class Like
{
    public $creationDate;
    public $userId;

    function __construct()
    {
        $this->creationDate = 0;
        $this->userId = '';
    }

    public function castMe($obj)
    {
        if (isset($obj)) {
            if (isset($obj->oc)) {
                $this->creationDate = $obj->oc;
            }
        }

    }
}