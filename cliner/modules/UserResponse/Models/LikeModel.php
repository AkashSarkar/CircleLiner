<?php
namespace Modules\UserResponse\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Modules\Posting\Models\PostModel;

class LikeModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="likes";
    protected $fillable = [
        'post_id',
        'user_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];



    /*
    public function countLikes(){
        return $this->belongsToMany(Post::class,'post_id');
    }*/
}