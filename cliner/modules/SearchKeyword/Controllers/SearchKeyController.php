<?php


namespace Modules\SearchKeyword\Controllers;

use App\Http\Controllers\Controller;
use App\Domain\Repo\SearchKeyRepo;

class SearchKeyController extends Controller
{
    private $searchKeyRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SearchKeyRepo $searchKeyRepo)
    {
        $this->searchKeyRepo = $searchKeyRepo;
    }

    public function search()
    {
     return $this->searchKeyRepo->search();
   }
}