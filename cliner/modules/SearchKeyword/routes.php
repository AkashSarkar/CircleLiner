<?php

$multiRoute = function () {
    Route::group(['module' => 'SearchKeyword', 'namespace' => 'Modules\SearchKeyword\Controllers','middleware' =>'jwt.auth'], function () {
        Route::get('searchResult', 'SearchKeyController@search');
    });
};

//TODO: define V_PREFIX in ..env
Route::group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);




