<?php

namespace modules\SearchKeywords\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;


class SearchKey extends BaseModel
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'keyword' , 'count ',
    ];

    
}