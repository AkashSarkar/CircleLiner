<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/25/18
 * Time: 3:49 PM
 */

namespace Modules\Privacy\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PrivacyModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'privacies';
    protected $fillable = [
        'name','rule','_tag'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}