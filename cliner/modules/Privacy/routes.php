<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/25/18
 * Time: 3:39 PM
 */

$multiRoute = function () {
    Route::group(['module' => 'Privacy', 'namespace' => 'Modules\Privacy\Controllers','middleware' =>'jwt.auth'], function () {
        Route::get('privacyget','PrivacyController@getdata');
    });
};

//TODO: define V_PREFIX in ..env
Route::group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);
