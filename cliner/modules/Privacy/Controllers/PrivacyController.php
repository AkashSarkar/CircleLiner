<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/25/18
 * Time: 3:44 PM
 */
namespace Modules\Privacy\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Domain\Repo\PrivacyRepo;


class PrivacyController extends Controller
{
    private $privacyRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PrivacyRepo $privacyRepo)
    {
     $this->privacyRepo = $privacyRepo;
    }


    public function getdata()
    {
        return $this->privacyRepo->get();

    }
}