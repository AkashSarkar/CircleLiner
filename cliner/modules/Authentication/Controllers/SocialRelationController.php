<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/22/18
 * Time: 5:29 PM
 */

namespace Modules\Authentication\Controllers;


use App\Domain\Repo\ApiTokenRepo;
use App\Domain\Repo\UserRepo;
use App\Domain\Repo\UserAuthRepo;
use App\validator\AuthFormValidation;
use App\validator\ValidationException;
use Firebase\JWT\JWT;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\BaseController;
use Socialite;

class SocialRelationController extends BaseController
{
    protected $userRepo;
    protected $userAuthRepo;
    protected $apiTokenRepo;
    protected $authFormValidation;

    public function __construct(UserRepo $userRepo, UserAuthRepo $userAuthRepo, ApiTokenRepo $apiTokenRepo,
                                AuthFormValidation $authFormValidation)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
        $this->userAuthRepo = $userAuthRepo;
        $this->apiTokenRepo = $apiTokenRepo;
        $this->authFormValidation = $authFormValidation;
    }

    public function redirectToGoogleContactProvider()
    {
        try {
            return redirect("https://accounts.google.com/o/oauth2/auth?redirect_uri=" . env('GOOGLE_CLIENT_REDIRECT_URI') . "&response_type=code&client_id=" . env('GOOGLE_CLIENT_ID') . "&scope=https://www.googleapis.com/auth/contacts&approval_prompt=force&access_type=offline");
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = 500;
            return $this->resp($response, $response['status']);
        }
    }

    public function handleProviderGoogleContactCallback()
    {
        $response = [];
        $client = new Google_Client();
        return $response;
    }

}