<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/22/18
 * Time: 5:29 PM
 */

namespace Modules\Authentication\Controllers;


use App\Domain\Repo\ApiTokenRepo;
use App\Domain\Repo\UserRepo;
use App\Domain\Repo\UserAuthRepo;
use App\validator\AuthFormValidation;
use App\validator\ValidationException;
use Firebase\JWT\JWT;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\BaseController;
use Socialite;

class AuthController extends BaseController
{
    protected $userRepo;
    protected $userAuthRepo;
    protected $apiTokenRepo;
    protected $authFormValidation;

    public function __construct(UserRepo $userRepo, UserAuthRepo $userAuthRepo, ApiTokenRepo $apiTokenRepo,
                                AuthFormValidation $authFormValidation)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
        $this->userAuthRepo = $userAuthRepo;
        $this->apiTokenRepo = $apiTokenRepo;
        $this->authFormValidation = $authFormValidation;
    }

    public function subscribe(Request $request)
    {


        try {
            $this->authFormValidation->subscriptionValidation($request->all());

            $user['first_name'] = $request->input("first_name");
            $user['last_name'] = $request->input("last_name");
            $user['email'] = $request->input("email");
            $user['password'] = Crypt::encrypt($request->input("password"));
            $user['token'] = getRandomTokenString(60);

            $res = $this->userAuthRepo->subscribe($user);

            $params['first_name'] = $user['first_name'];
            $params['last_name'] = $user['last_name'];
            $params['role_id'] = 1;
            $params['dob'] = null;
            $params['images'] = null;
            $params['gender'] = null;
            $registered_user = $this->userRepo->save($params);

            $user['user_name'] = "";
            $user['user_id'] = $registered_user->id;

            $this->userAuthRepo->register($user);

            $responseData["success"] = $res;
            $responseData["form_data"] = array(
                "first_name" => "required",
                "last_name" => "required",
                "email" => "required|email",
                "password" => "required|min:6",
                "gender" => "optional",
                "dob" => "optional"
            );

//            if ($res) {
//                $confirmation = $this->userAuthRepo->sendMail($res);
//            }


        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }

        return $this->resp($responseData, 201);
    }

    public function sendConfirmEmail(Request $request)
    {

        $response = [];
        $confirmation = '';
        try {
            $resp['email'] = $request->email;
            $resp['token'] = $request->token;
            if ($request->has('email') && $request->has('token')) {
                $confirmation = $this->userAuthRepo->sendMail($resp);
            }
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = 500;
        }
        if ($confirmation != '') {
            $response['message'] = 'email send successful';
            $response['status'] = 200;
        }
        return $this->resp($response, $response['status']);
    }


    public function confirmEmail(Request $request)
    {
        $response = [];
        $response['message'] = "";
        $response['status'] = 200;
        try {
            $this->authFormValidation->registrationValidation($request->all());
            $params['token'] = $request->input("token");
            $params['gender'] = "";
            $params['dob'] = "";
            $params['status'] = "";

            $confirmed_user = $this->userAuthRepo->confirmedUserData($params);
            if ($confirmed_user) {
                if ($confirmed_user->used == 0) {

                    $notExpired = $this->userAuthRepo->isExpired($params['token']);
                    if ($notExpired) {
                        $params['first_name'] = $confirmed_user->first_name;
                        $params['last_name'] = $confirmed_user->last_name;
                        $params['role_id'] = 1;
                        $params['dob'] = null;
                        $params['images'] = null;
//                    $registered_user = $this->userRepo->save($params);

                        $user['user_name'] = "";
                        $user['email'] = $confirmed_user->email;
                        $user['password'] = $confirmed_user->password;
//                    $user['user_id'] = $registered_user->id;

//                    $registerd = $this->userAuthRepo->register($user);
                        $this->userAuthRepo->updateEmailVerified($user['email']);

                        $this->userAuthRepo->updateEmailUsedStatus($params['token']);


                        $user_id = $this->userAuthRepo->login($user['email'], Crypt::decrypt($user['password']));

                        if ($user_id) {
                            $agent = $request->server('HTTP_USER_AGENT');
                            $token = $this->getToken($user_id, $agent);

                            if (!$token) {

                            } else {
                                $response['user'] = $this->userRepo->getUserHashed($user_id);
                                $response['api_token'] = $token;
                                $response['message'] = "Successful";
                            }
                        }

                    } else {
                        $response['message'] = "Token has been Expired";
                        $response['status'] = 401;
                    }

                } else {
                    $response['message'] = "Token is already Used";
                    $response['status'] = 401;
                }
            } else {
                $response['message'] = "Eamil not found";
                $response['status'] = 404;
            }

        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = "Something went wrong";
            $response['status'] = 500;
        }
//        return view('confirmation.confirmation',['user'=>$response['user']]);
        return $this->resp($response, $response['status']);
    }

    public function login(Request $request)
    {
        $response = [];
        $response['message'] = "";
        $response['status'] = 200;
        try {

            $this->authFormValidation->loginValidation($request->all());

            $email = $request->email;
            $password = $request->password;

            $user_id = $this->userAuthRepo->login($email, $password);


            if ($user_id) {
                $agent = $request->server('HTTP_USER_AGENT');
                $token = $this->getToken($user_id, $agent);

                if (!$token) {
                    //TODO:: error message
                } else {
                    $response['user'] = $this->userRepo->getUserHashed($user_id);
                    $response['uid'] = trim($response['user']->_uid);
                    $response['api_token'] = $token;
                }

            } else {
                $response['message'] = "User Name and Password Wrong";
                $response['status'] = 401;
            }

        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = 500;
        }

        return $this->resp($response, $response['status']);

    }

    public function checkState(Request $request)
    {
        $response = [];
        try {

            if ($request->bearerToken()) {
                $response['user'] = $this->userRepo->getUser(JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256'])->sub);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return response()->json($response);
    }

    public function logout()
    {
        $response = [];
        $response['message'] = "Successfully logout";
        $response['status'] = 200;
        return $this->resp($response, $response['status']);
    }

    public function getToken($user_id, $agent)
    {
        try {
            $payload = getToken($user_id);
            $payload['user_agent'] = $agent;
            $payload['last_activity'] = time();
            $token = $this->apiTokenRepo->insertToken($user_id, $payload);
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
        }

        if ($token) {
            return $payload['api_token'];
        } else {
            return 0;
        }
    }


    public function changePassword(Request $request)
    {
        $response = [];
        $response['message'] = 'Password Update successful';
        $response['status'] = 200;
        try {
            $this->authFormValidation->passwordValidation($request->all());
            $user = $this->userRepo->getUser($this->userRepo->getUserId($request->user_hid));
            if ($user) {
                $isCorrectOldPassword = $this->userAuthRepo->checkPassword($request->old_password, $this->userRepo->getUserId($user->userId));
                if ($isCorrectOldPassword) {
                    $this->userAuthRepo->updatePassword(Crypt::encrypt($request->new_password), $this->userRepo->getUserId($user->userId));
                } else {
                    $response['message'] = 'Incorrect password';
                }
            }
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $this->resp($response, $response['status']);
    }

    public function getUserAgent(Request $request)
    {
        $result['device'] = $request->server('HTTP_USER_AGENT');
        $result['ip'] = $request->ip();
        return $this->makeRespone($result, 200);
    }


}