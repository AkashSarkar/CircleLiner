<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/22/18
 * Time: 5:29 PM
 */

namespace Modules\Authentication\Controllers;


use App\Domain\Repo\ApiTokenRepo;
use App\Domain\Repo\UserRepo;
use App\Domain\Repo\UserAuthRepo;
use App\validator\AuthFormValidation;
use App\validator\ValidationException;
use Firebase\JWT\JWT;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\BaseController;
use Socialite;

class SocialAuthController extends BaseController
{
    protected $userRepo;
    protected $userAuthRepo;
    protected $apiTokenRepo;
    protected $authFormValidation;

    public function __construct(UserRepo $userRepo, UserAuthRepo $userAuthRepo, ApiTokenRepo $apiTokenRepo,
                                AuthFormValidation $authFormValidation)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
        $this->userAuthRepo = $userAuthRepo;
        $this->apiTokenRepo = $apiTokenRepo;
        $this->authFormValidation = $authFormValidation;
    }

    public function redirectToGoogleProvider()
    {
        try {
            return Socialite::driver('google')->stateless()->redirect();
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
            return $this->resp($response, $response['status']);
        }
    }

    public function handleProviderGoogleCallback()
    {
        $response = [];
        try {
            $socialUser = Socialite::driver('google')->stateless()->user();
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
        if ($socialUser) {
            $response = $this->socialLogin($socialUser, 'google');
        } else {
            $response['message'] = "User Name and Password Wrong";
            $response['status'] = 401;
        }

        //return response()->json($socialUser);
        return $response;
    }

    public function redirectToFacebookProvider()
    {
        try {
            return Socialite::driver('facebook')->stateless()->redirect();
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    public function handleProviderFacebookCallback()
    {
        $response = [];
        try {
            $socialUser = Socialite::driver('facebook')->stateless()->user();
        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
        if ($socialUser) {
            //$response = $this->socialLogin($socialUser, 'google');
        } else {
            $response['message'] = "User Name and Password Wrong";
            $response['status'] = 401;
        }

        return response()->json($socialUser);
        return $response;
    }

    public function socialLogin($socialUser, $provider)
    {
        $response = [];
        $response['message'] = "";
        $response['status'] = 200;
        $userProvider = [];
        try {

            $socialProvider = $this->userAuthRepo->checkSocialUser($socialUser->getId());
            if (!$socialProvider) {
                $user = $this->userAuthRepo->getUserByEmail($socialUser->getEmail());

                if (!$user) {
                    $userProvider['provider_id'] = $socialUser->getId();
                    $userProvider ['email'] = $socialUser->getEmail();
                    $userProvider['password'] = "";
                    $userProvider['role_id'] = 1;
                    $userProvider['gender'] = "";
                    $userProvider['images'] = "";
                    $userProvider['dob'] = "";
                    switch ($provider) {
                        case "google":
                            $userProvider['first_name'] = $socialUser->user['name']['givenName'];
                            $userProvider['last_name'] = $socialUser->user['name']['familyName'];
                            break;
                        case "twitter":
                            $userProvider['first_name'] = $socialUser->getName();
                            $userProvider['last_name'] = $socialUser->getNickname();
                            break;
                        case "facebook":
                            $userProvider['first_name'] = $socialUser->getName();
                            break;
                        default:
                            $userProvider['first_name'] = $socialUser->getName();
                    }

                    $saveUser = $this->userRepo->save($userProvider);
                    $userProvider['user_id'] = $saveUser->id;
                    $userProvider['provider'] = $provider;
                    $this->userAuthRepo->save($userProvider);
                    $this->userAuthRepo->saveUserProvider($userProvider);
                }

            } else {
                $payload = [];
                //TODO::get user agent
                $payload['user_agent'] = 0;
                $payload['api_token'] = $socialUser->token;
                $payload['last_activity'] = time();
                $token = $this->apiTokenRepo->insertToken($socialProvider->user_id, $payload);

                $response['user'] = $this->userRepo->getUserHashed($socialProvider->user_id);
                $response['api_token'] = $socialUser->token;
            }


        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        }/* catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
        }*/

        return $this->resp($response, $response['status']);
    }
}