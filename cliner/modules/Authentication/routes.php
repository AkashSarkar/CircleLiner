<?php
$route = "authentication";
$multiRoute = function () use ($route) {
    $this->app->router->group([
        'prefix' => $route,
        'module' => 'Authentication',
        'namespace' => 'Modules\Authentication\Controllers'
    ], function ($router) {

        $router->post('register', 'AuthController@register', ['as' => 'register']);
        $router->post('login', 'AuthController@login');

        $router->get('logout', 'AuthController@logout');
        $router->post('subscribe', 'AuthController@subscribe');
        $router->get('confirmation', 'AuthController@confirmEmail', ['as' => 'confirmEmail']);
        $router->post('confirmation-mail', 'AuthController@sendConfirmEmail', ['as' => 'sendConfirmEmail']);

        $router->post('check-state', 'AuthController@checkState');
        $router->post('change-password', 'AuthController@changePassword',['middleware'=>'jwt.auth']);
        $router->post('reset-password', 'AuthController@resetPassword');

        //social login
        $router->get('login/google', 'SocialAuthController@redirectToGoogleProvider');
        $router->get('login/google/callback', 'SocialAuthController@handleProviderGoogleCallback');

        $router->get('login/facebook', 'SocialAuthController@redirectToFacebookProvider');
        $router->get('login/facebook/callback', 'SocialAuthController@handleProviderFacebookCallback');


        $router->get('google/contact', 'SocialRelationController@redirectToGoogleContactProvider');
        $router->get('google/contact/callback', 'SocialRelationController@handleProviderGoogleContactCallback');

    });

};
$this->app->router->group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);



