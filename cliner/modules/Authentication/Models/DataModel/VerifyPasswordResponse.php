<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 6/9/18
 * Time: 9:36 PM
 */

namespace Modules\Authentication\Models\DataModel;


class VerifyPasswordResponse
{
    private $displayName;
    private $email;
    private $expiresIn;
    private $idToken;
    private $localId;
    private $refreshToken;
    private $registered;

    function __construct()
    {
        $this->displayName = (string)"";
        $this->email = (string)"";
        $this->expiresIn = (int)0;
        $this->idToken = (string)"";
        $this->localId = (string)"";
        $this->refreshToken = (string)"";
        $this->registered = (boolean)false;
    }

    public function castMe($obj)
    {
        $this->displayName = (string)$obj->display_name;
        if ($obj->auth) {
            $this->email = (string)$obj->auth->email;
        }
        $this->expiresIn = (int)0;
        if ($obj->apitoken) {
            $this->idToken = (string)$obj->apitoken->api_token;
        }
        $this->localId = (string)"";
        $this->refreshToken = (string)"";
        $this->registered = (boolean)$obj->email_verified;
    }

}