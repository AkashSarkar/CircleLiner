<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/5/18
 * Time: 3:27 PM
 */

namespace Modules\Authentication\Models\DataModel;


class User
{
    public $fullName;
    public $avatar;
    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $userId;
    public $creationDate;
    public $emailVerified;
    public $providerData;


    function __construct()
    {
        $this->fullName = '';
        $this->avatar = '';
        $this->firstName = '';
        $this->lastName = '';
        $this->email = '';
        $this->password = '';
        $this->userId = '';
        $this->creationDate = '';
        $this->emailVerified = (boolean)false;
        $this->providerData = [];

    }

    public function castMe($obj)
    {
        if ($obj != null) {
            $this->fullName = $obj->first_name . ' ' . $obj->last_name;
            $this->avatar = $obj->cpp;
            $this->firstName = $obj->first_name;
            $this->lastName = $obj->last_name;
            if ($obj->auth) {
                $this->email = (string)$obj->auth->email;
            }
            $this->password = $obj->password;
            $this->userId = $obj->hash_id;
            $this->creationDate = strtotime($obj->created_at);
            $this->emailVerified = (boolean)$obj->email_verified;
        }
    }

}