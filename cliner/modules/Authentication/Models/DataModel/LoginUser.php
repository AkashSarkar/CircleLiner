<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 3:26 PM
 */

namespace Modules\Authentication\Models\DataModel;


class LoginUser
{
    public $_uid;
    public $_emailVerified;
    public $_providerId;
    public $_displayName;
    public $_email;
    public $_avatarURL;
    public $_jwt;

    function __construct()
    {
        $this->_uid = (string)"";
        $this->_emailVerified = (int)0;
        $this->_providerId = (string)"";
        $this->_displayName = (string)"";
        $this->_email = (string)"";
        $this->_avatarURL = (string)"";
        $this->_jwt = (string)"";
    }

    public function castMe($obj)
    {
        $this->_uid = (string)$obj->hash_id;
        $this->_emailVerified = $obj->email_verified;

        $this->_displayName = (string)$obj->display_name;
        if ($obj->auth) {
            $this->_email = (string)$obj->auth->email;
        }
        $this->_avatarURL = $obj->cpp;
        if ($obj->apitoken) {
            $this->_jwt = (string)$obj->apitoken->api_token;
        }

    }


}