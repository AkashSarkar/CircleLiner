<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 6:20 PM
 */

namespace Modules\Authentication\Models\DataModel;


class Provider
{

    public $userId;
    public $email;
    public $fullName;
    public $firstName;
    public $lastName;
    public $avatar;
    public $providerId;
    public $provider;
    public $accessToken;

    function __construct()
    {

        $this->userId = '';
        $this->email = '';
        $this->fullName = '';
        $this->firstName = '';
        $this->lastName = '';
        $this->avatar = '';
        $this->providerId = '';
        $this->provider = '';
        $this->accessToken = '';
    }

    public function castMe($obj)
    {
        $this->userId = $obj->userId;
        $this->email = $obj->email;
        $this->fullName = $obj->fullName;
        $this->firstName = $obj->firstName;
        $this->lastName = $obj->lastName;
        $this->avatar = $obj->cpp;
        $this->providerId = $obj->providerId;
        $this->provider = $obj->provider;
        $this->accessToken = $obj->accessToken;

    }
}