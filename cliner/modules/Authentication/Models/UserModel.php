<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/23/18
 * Time: 4:57 PM
 */

namespace Modules\Authentication\Models;

use App\Models\BaseModel;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Modules\UserRelation\Models\RoleModel;
use Modules\Posting\Models\PostModel;


class UserModel extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    protected $table = 'users';
    protected $fillable = [
        'hash_id',
        'first_name',
        'last_name',
        'gender',
        'dob',
        'email_verified',
        'role_id',
        'images',
        'professions',
        'educations',
        'interests',
        'achievements',
        'notifications',
        'activities',
        'settings',
        'created_at',
        'updated_at',

    ];

    public function roles()
    {
        return $this->belongsToMany(RoleModel::class);
    }

    public function auth()
    {
        return $this->hasOne(UserAuthModel::class, 'user_id');
    }

    public function apitoken()
    {
        return $this->hasOne(ApiTokenModel::class, 'user_id');
    }
    public function posts()
    {
        return $this->hasMany(PostModel::class,'user_id');
    }

}