<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/24/18
 * Time: 4:42 PM
 */

namespace Modules\Authentication\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class ApiTokenModel extends BaseModel
{
    protected $table = 'api_tokens';
    protected $fillable = [
        'api_token', "user_id"
    ];
    public $timestamps = false;
}