<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/28/18
 * Time: 11:39 AM
 */

namespace Modules\Authentication\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Subscription extends BaseModel
{
    protected $table = 'subscriptions';
    protected $fillable = [
        'first_name','last_name','email','password' ,'token', 'expire_time', 'used',

    ];
}