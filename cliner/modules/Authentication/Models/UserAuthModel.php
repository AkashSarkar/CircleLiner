<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/23/18
 * Time: 5:00 PM
 */

namespace Modules\Authentication\Models;

use App\Models\BaseModel;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class UserAuthModel extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    protected $table = 'user_auth';
    protected $fillable = [
        'user_name', 'email', 'password', 'user_id', 'created_at', 'updated_at',
    ];

    protected $hidden = [
        'password',
    ];

    public function user()
    {
        return $this->hasOne('Modules\Authentication\Models\UserModel');
    }
}