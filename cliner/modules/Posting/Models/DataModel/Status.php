<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/25/18
 * Time: 5:26 PM
 */

namespace Modules\Posting\Models\DataModel;


class Status
{
 public $description;

    function __construct(){
        $this->description=(string)"";
    }
    public function castMe($obj)
    {
        if($obj!=null) {
            if($obj->description){
                $this->description = $obj->description;
            }
        }
    }
}