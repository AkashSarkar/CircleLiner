<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/7/18
 * Time: 3:46 PM
 */

namespace Modules\Posting\Models\DataModel;


use Modules\UserInfo\Models\DataModel\Profile;
use Modules\UserResponse\Models\DataModel\Comment;
use Modules\UserResponse\Models\DataModel\Comments;
use Modules\UserResponse\Models\DataModel\Like;

class Post
{
    public $id;
    public $postTypeId;
    public $creationDate;
    public $viewCount;
    public $description;
    public $ownerUserId;
    public $ownerDisplayName;
    public $ownerAvatar;
    public $lastEditDate;
    public $tags;
    public $commentCounter;
    public $image;
    public $imageFullPath;
    public $video;
    public $disableComments;
    public $disableSharing;
    public $score;
    public $votes;
    public $comments;
    public $postData;

    function __construct()
    {
        $this->id = (string)'';
        $this->postTypeId = (int)0;
        $this->creationDate = (int)0;
        $this->viewCount = (int)0;
        $this->description = (string)'';
        $this->ownerUserId = (string)'';
        $this->ownerDisplayName = (string)'';
        $this->ownerAvatar = (string)'';
        $this->lastEditDate = (int)0;
        $this->tags = [];
        $this->commentCounter = (int)0;
        $this->image = (string)'';
        $this->imageFullPath = (string)'';
        $this->video = (string)'';
        $this->disableComments = (boolean)0;
        $this->disableSharing = (boolean)0;
        $this->score = (int)0;
        $this->votes = [];
        $this->comments = [];
        $this->postData = [];
    }

    public function castMe($obj)
    {
//         return $obj->user;
        if ($obj) {

            if ($obj->hash_id) {
                $this->id = $obj->hash_id;
            }
            if ($obj->user_id) {
                $this->ownerUserId = $obj->user->hash_id;
            }
            if ($obj->post_type_id) {
                $this->postTypeId = $obj->post_type_id;
            }
            if ($obj->created_at) {
                $this->creationDate = strtotime($obj->created_at);
            }
//            if($obj->description){
//                $this->description=$obj->description;
//            }
            if ($obj->post_data) {
                $p_data = json_decode($obj->post_data);
//                $this->description = $post_data->description;
                switch ($obj->post_type_id) {
                    case 1:
                        $this->postData = new Status();
                        $this->postData->castMe($p_data);
                        break;
                    case 2:
                        $this->postData = new Award();
                        $this->postData->castMe($p_data);
                        break;
                    case 3:
                        $this->postData = new Promotion();
                        $this->postData->castMe($p_data);
                        break;
                    case 4:
                        $this->postData = new Event();
                        $this->postData->castMe($p_data);
                        break;
                    case 5:
                        $this->postData = new Article();
                        $this->postData->castMe($p_data);
                        break;
                    case 6:
                        $this->postData = new Conference();
                        $this->postData->castMe($p_data);
                        break;
                    case 7:
                        $this->postData = new Project();
                        $this->postData->castMe($p_data);
                        break;
                }
            }
            if ($obj->tag) {
                $this->tag = $obj->tag;
            }
            if ($obj->images) {
                $this->image = json_decode($obj->images);

            }
            if ($obj->created_at) {
                $this->creationDate = strtotime($obj->created_at);
            }
            if ($obj->updated_at) {
                $this->lastEditDate = strtotime($obj->updated_at);
            }
            if ($obj->user) {
                $this->ownerDisplayName = $obj->user->display_name;
            }
            if ($obj->user) {
                $this->ownerAvatar = json_decode($obj->user->images)->cpp->url;
            }
            if ($obj->viewCount) {
                $this->viewCount = $obj->viewCount;
            }
            if ($obj->comments) {
                $comments = new Comments();
                $comments->postId = $this->id;
                $comments->castMe(json_decode($obj->comments));
                $this->comments = $comments->comments;
                $this->commentCounter = $comments->commentCounter;

            }
            if ($obj->disableComments) {
                $this->disableComments = $obj->disableComments;
            }
            if ($obj->disableSharing) {
                $this->disableSharing = $obj->disableSharing;
            }
            if ($obj->likes) {
                $likes = json_decode($obj->likes);
                foreach ($likes->liked as $key => $value) {
                    $like = new Like();
                    $like->userId = $key;
                    $like->castMe($value);
                    $this->votes[$key] = $like;
                }
                $this->score = $likes->total;
            }


        }

    }


}