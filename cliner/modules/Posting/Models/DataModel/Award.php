<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/25/18
 * Time: 5:27 PM
 */

namespace Modules\Posting\Models\DataModel;


class Award
{
    public $title;
    public $publishingDate;
    public $recevingPlace;
    public $description;

    function __construct()
    {
        $this->title=(string)"";
        $this->publishingDate=(int)0;
        $this->receivingPlace=(string)"";
        $this->description=(string)"";
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->title=$obj->title;
            $this->publishingDate=strtotime($obj->publishingDate);
            $this->receivingPlace=$obj->receivingPlace;
            $this->description=$obj->description;
        }
    }
}