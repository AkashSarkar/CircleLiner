<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/25/18
 * Time: 5:27 PM
 */

namespace Modules\Posting\Models\DataModel;


use function GuzzleHttp\Psr7\str;

class Promotion
{
    public $jobPositionName;
    public $companyName;
    public $departmentName;
    public $description;

    function __construct()
    {
        $this->jobPositionName = (string)"";
        $this->companyName = (string)"";
        $this->departmentName = (string)"";
        $this->description = (string)"";
    }

    public function castMe($obj)
    {
        if ($obj != null) {
            if (isset($obj->jobPositionName)) {
                $this->jobPositionName = $obj->jobPositionName;
            }
            if (isset($obj->companyName)) {
                $this->companyName = $obj->companyName;
            }
            if (isset($obj->departmentName)) {
                $this->departmentName = $obj->departmentName;
            }
            if (isset($obj->description)) {
                $this->description = $obj->description;
            }
        }
    }
}