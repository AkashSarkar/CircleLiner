<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/25/18
 * Time: 5:29 PM
 */

namespace Modules\Posting\Models\DataModel;


class Article
{
    public $title;
    public $reference_link;
    public $publishing_date;
    public $description;

    function __construct()
    {
        $this->title = (string)"";
        $this->referenceLink = (string)"";
        $this->publishingDate = (int)0;
        $this->description = (string)"";
    }

    public function castMe($obj)
    {
        if ($obj != null) {
            if (isset($obj->title)) {
                $this->title = $obj->title;
            }
            if (isset($obj->referenceLink)) {
                $this->referenceLink = $obj->referenceLink;
            }
            if (isset($obj->publishingDate)) {
                $this->publishingDate = $obj->publishingDate;
            }
            if (isset($obj->description)) {
                $this->description = $obj->description;
            }
        }
    }
}