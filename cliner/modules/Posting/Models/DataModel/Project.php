<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/25/18
 * Time: 5:27 PM
 */

namespace Modules\Posting\Models\DataModel;


class Project
{
    public $title;
    public $role;
    public $startDate;
    public $skillRequired;
    public $description;

    function __construct()
    {
        $this->title = (string)"";
        $this->role = (string)"";
        $this->startDate = (int)0;
        $this->skillRequired = (string)"";
        $this->description = (string)"";

    }

    public function castMe($obj)
    {
        if ($obj != null) {
            $this->title = $obj->title;
            $this->role = $obj->role;
            if (isset($obj->startDate)) {
                $this->startDate = strtotime($obj->startDate);
            }
            if (isset($obj->skillRequired)) {
                $this->skillRequired = $obj->skillRequired;
            }
            $this->description = $obj->description;
        }
    }

}