<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/25/18
 * Time: 5:27 PM
 */

namespace Modules\Posting\Models\DataModel;


use function GuzzleHttp\Psr7\str;

class Conference
{
    public $title;
    public $topic;
    public $role;
    public $description;

    function __construct()
    {
        $this->title = (string)"";
        $this->topic = (string)"";
        $this->role = (string)"";
        $this->description = (string)"";
    }

    public function castMe($obj)
    {
        if ($obj != null) {
            if (isset($obj->title)) {
                $this->title = $obj->title;
            }
            if (isset($obj->topic)) {
                $this->topic = $obj->topic;
            }
            if (isset($obj->role)) {
                $this->role = $obj->role;
            }
            if (isset($obj->description)) {
                $this->description = $obj->description;
            }
        }
    }

}