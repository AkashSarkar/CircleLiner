<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/25/18
 * Time: 5:28 PM
 */

namespace Modules\Posting\Models\DataModel;


class Event
{

    public $title;
    public $start_date;
    public $endDate;
    public $description;

    function __construct()
    {
        $this->title = (string)"";
        $this->start_date = (int)0;
        $this->end_date = (int)0;
        $this->description = (string)"";

    }

    public function castMe($obj)
    {
        if ($obj != null) {
            if (isset($obj->title)) {
                $this->title = $obj->title;
            }
            if (isset($obj->startDate)) {
                $this->start_date = strtotime($obj->startDate);
            }
            if (isset($obj->endDate)) {
                $this->end_date = strtotime($obj->endDate);
            }
            if (isset($obj->description)) {
                $this->description = $obj->description;
            }
        }
    }
}