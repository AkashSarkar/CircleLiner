<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/22/18
 * Time: 4:50 PM
 */

namespace Modules\Posting\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Modules\Authentication\Models\UserModel;
use Modules\UserResponse\Models\LikeModel;
use Modules\UserResponse\Models\CommentModel;

class PostModel extends BaseModel
{
    protected $table = "posts";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_data', 'post_type_id', 'user_id', 'hash_id', 'likes', 'comments','images', 'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'updated_at'
    ];


    public function likes()
    {
        return $this->hasMany(LikeModel::class, 'post_id');
    }

    public function comments()
    {
        return $this->hasMany(CommentModel::class, 'post_id');
    }

    public function user()
    {
        return $this->belongsTo(UserModel::class, 'user_id');
    }
}