<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/22/18
 * Time: 4:56 PM
 */

namespace Modules\Posting\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PostTypeModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'post_types';
    protected $fillable = [
        'label','_tag','hash_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}

