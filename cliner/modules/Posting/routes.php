<?php
$route = 'posting';
$multiRoute = function () use ($route) {
    //TODO: Make the api seo friendly
    $this->app->router->group(['module' => 'Posting', 'namespace' => 'Modules\Posting\Controllers', 'middleware' => 'jwt.auth'], function ($router) use ($route) {
        $router->get($route . '/post', 'PostController@index');
        $router->get($route . '/post/{id}', 'PostController@show');
        $router->post($route . '/post', 'PostController@store');
        $router->put($route . '/post/{id}', 'PostController@update');
        $router->delete($route . '/post/{hash_id}', 'PostController@destroy');

        $router->post($route . '/post-share-permission', 'PostController@is_shareable');
        $router->post($route . '/post-comment-permission', 'PostController@is_commentable');
        $router->get($route . '/post-types', 'PostController@get_post_type');

    });
};
//TODO: define V_PREFIX in ..env
$this->app->router->group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);
