<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 4/22/18
 * Time: 5:22 PM
 */

namespace Modules\Posting\Controllers;

use App\Http\Controllers\BaseController;
use App\validator\ValidationException;
use Illuminate\Http\Request;
use App\Domain\Repo\PostRepo;
use App\Domain\Repo\CommentRepo;
use App\Domain\Repo\LikeRepo;
use App\Domain\Repo\UserRepo;
use App\validator\PostFormValidator;

class PostController extends BaseController
{

    private $postRepo;
    private $commentRepo;
    private $likeRepo;
    private $userRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepo $postRepo, CommentRepo $commentRepo, LikeRepo $likeRepo, UserRepo $userRepo)
    {
        $this->postRepo = $postRepo;
        $this->commentRepo = $commentRepo;
        $this->likeRepo = $likeRepo;
        $this->userRepo = $userRepo;
    }

    public function index(Request $request)
    {
        $posts_data = [];
        try {

            $page = $request->has('page') ? $request->page : 0;
            $limit = $request->has('limit') ? $request->limit : 10;
            if ($request->has('u')) {
                $userId = $this->userRepo->getUserId($request->u);
                if ($userId) {
                    $posts_data = $this->postRepo->getPostsUser($userId, $limit, $page * $limit);
                }
            } else {
                $posts_data = $this->postRepo->getPosts($limit, $page * $limit);

            }


        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }


        return $this->makeRespone($posts_data, 200);
    }

    public function show(Request $request, PostFormValidator $postFormValidator, $hash_id)
    {

        try {
            if ($hash_id) {
                $hashId = $hash_id;
            }
            $comment_store = array();
            $count = array();
            $id = $this->postRepo->hashIdToId($hashId);
            if ($id) {
                $data = $this->postRepo->getPost($id);
                $response['data'] = $data;
//                $response['hash_id'] = $data->hash_id;
//                $response['type'] = $this->postRepo->idToTagType($data->post_type_id);
//                $response['content'] = json_decode($data->post_data);
//
//                $user_name = $this->userRepo->getUserHashed($data->user_id);
//                $user['hash_id'] = $user_name->hash_id;
//                $user['fullname'] = $user_name->first_name . " " . $user_name->last_name;
//                $response['user'] = $user;
//
//                $comments = $this->commentRepo->getCommentsByPostId($id);
//                if ($comments->count() > 0) {
//                    foreach ($comments as $idx => $comment) {
//                        $comment_info['id'] = $comment->id;
//                        $comment_info['body'] = $comment->note;
//                        $comment_user = $this->userRepo->getUserHashed($comment->user_id);
//                        $comment_info["hash_id"] = $comment_user->hash_id;
//                        $comment_info["fullname"] = $comment_user->first_name . " " . $comment_user->last_name;
//                        $comment_info['isComment'] = $comment->parent_id;
//                        array_push($comment_store, $comment_info);
//                    }
//                    $response['comment'] = $comment_store;
//                } else {
//                    $response['comment'] = "No comments yet";
//                }
//
//                $count['like'] = $this->likeRepo->getLikeByPostId($id);
//                $count['comment'] = $this->commentRepo->commentsCountByPostId($id);
//                $response['count'] = $count;
//                $response['created'] = $data->created_at->format('M j,Y g:ia e P');
                $response ['status'] = 200;
            } else {
                $response['message'] = "The post you are looking for not found";
                $response['status'] = 404;
            }
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
        }
        return $this->resp($response, $response['status']);
    }

    public function store(Request $request, PostFormValidator $postFormValidator)
    {
        $payload = [];
        $post_data = [];
        try {

            if ($request->has('tag')) {
                $tag_type_id = $this->postRepo->tagTypeToId($request['tag']);
                $payload['post_type_id'] = $tag_type_id;
            }

            if (!($request->has('tag')) || $request['post_type_id'] == 0) {
                $type = $this->postRepo->getDefaultType();
                $request['tag'] = $type->_tag;
                $tag_type_id = $this->postRepo->tagTypeToId($request['tag']);
                $payload['post_type_id'] = $tag_type_id;
            }

            $payload['user_id'] = $this->userRepo->getUserId($request['ownerUserId']);

            // $payload['user_id'] = JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256'])->sub;

            $postFormValidator->idValidation($request->all());

            switch ($request['postTypeId']) {
                case 1:
                    $postFormValidator->statusPostValidation($request->all());
                    break;
                case 2:
                    $postFormValidator->awardPostValidation($request->all());
                    break;
                case 3:
                    $postFormValidator->promotionPostValidation($request->all());
                    break;
                case 4:
                    $postFormValidator->eventPostValidation($request->all());
                    break;
                case 5:
                    $postFormValidator->articlePostValidation($request->all());
                    break;
                case 6:
                    $postFormValidator->conferencePostValidation($request->all());
                    break;
                case 7:
                    $postFormValidator->projectPostValidation($request->all());
                    break;
            }

            if (!$request->has('isCommentable')) {
                $payload['isCommentable'] = 1;
            }
            if (!$request->has('isShareable')) {
                $payload['isShareable'] = 1;
            }

            $post_data['description'] = $request['description'];

            $post_data['viewCount'] = $request['viewCount'];
            $post_data['commentCounter'] = $request['commentCounter'];
            $post_data['tags'] = $request['tags'];
            $post_data['disableComments'] = $request['disableComments'];
            $post_data['disableSharing'] = $request['disableSharing'];

            $payload['post_data'] = json_encode($post_data);
            $store_data = $this->postRepo->storePostData($payload);
            if ($store_data) {

                $response['message'] = "Your post is successfully posted";
                $response['id'] = $store_data;
                $response['status'] = 201;
            } else {
                $response['message'] = "Something went worng";
                $response['status'] = 500;
            }

        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        }
        return $this->resp($response, $response['status']);
    }

    public function update(Request $request, $id)
    {
        $response = [];
        $response['status'] = 204;
        return $this->resp($response, $response['status']);
    }

    public function get_post_type()
    {
        try {
            $type = $this->postRepo->getPostType();
        } catch (\Exception $e) {
            return $this->makeErrorResponse("Something went worng", 500);
        }
        return $this->makeRespone($type, 200);
    }

    public function destroy(Request $request, $hash_id)
    {
        try {
            if ($hash_id) {
                $id = $this->postRepo->hashIdToId($hash_id);
                $p = $this->postRepo->destroy($id);
                return $this->makeRespone($p, 204);
            } else {
                return $this->makeErrorResponse("Request-URI is not valid", 404);
            }
        } catch (Exception $e) {
            $e->getMessage();
            abort(500);
        }
    }

    public function is_commentable(Request $request, PostFormValidator $postFormValidator)
    {
        try {
            $postFormValidator->commentPermissionValidation($request->all());
            $id = $this->postRepo->hashIdToId($request['hash_id']);
            $user_id = $this->userRepo->getUserId($request['user_id']);
            $result = $this->postRepo->isCommentable($id, $user_id);
            if ($result) {
                $response['message'] = "updated succesfully";
                $response['status'] = 200;
            } else {
                $response['message'] = "can't updated";
                $response['status'] = 422;
            }
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
        }
        return $this->resp($response, $response['status']);
    }

    public function is_shareable(Request $request, PostFormValidator $postFormValidator)
    {
        try {
            $postFormValidator->sharePermissionValidation($request->all());
            $id = $this->postRepo->hashIdToId($request['hash_id']);
            $user_id = $this->userRepo->getUserId($request['user_id']);
            $result = $this->postRepo->isShareable($id, $user_id);
            if ($result) {
                $response['message'] = "updated succesfully";
                $response['status'] = 200;
            } else {
                $response['message'] = "can't updated";
                $response['status'] = 422;
            }
        } catch (ValidationException $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = $e->getCode();
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $response['status'] = 500;
        }
        return $this->resp($response, $response['status']);
    }
}