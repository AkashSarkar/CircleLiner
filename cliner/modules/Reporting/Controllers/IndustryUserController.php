<?php

namespace Modules\Reporting\Controllers;

use App\validator\IndustryValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\IndustryUserRepo;
use App\Domain\Repo\UserRepo;
use App\validator\ValidationException;

class IndustryUserController extends BaseController{

    private $industryUserRepo, $userRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(IndustryUserRepo $industryUserRepo, IndustryValidator $industryValidator, UserRepo $userRepo)
    {
        $this->industryUserRepo = $industryUserRepo;
        $this->industryValidator = $industryValidator;
        $this->userRepo = $userRepo;
    }

    public function store_industry(Request $request)
    {
        try{
            $validate_data = $this->industryValidator->industryStoreValidation($request->all());
            $user_id = $this->userRepo->getUserId($request->user_id);
            $industry_id = $request->industry_id;

            return $this->industryUserRepo->storeIndustryUser($user_id, $industry_id);

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        //$this->page=1;
        return $this->makeRespone($request->all(),200);
    }
    
    public function get_industry_by_userid(Request $request)
    {
        try {
            //$validate_data =  $like_form_validator->getLikeUserListValidation($request->all());
            if($request->has('user_id')){
                $user_id = $this->userRepo->getUserId($request->user_id);
                $industryData= $this->industryUserRepo->getIndustryUser($user_id);
            }

        } catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
        return $this->makeRespone($industryData,200);

    }

    public function update_industry(Request $request)
    {
        try{
            if($request->has('id')) {
                $validate_data = $this->industryValidator->industryUpdateValidation($request->all());
                $id = $request->id;
                $user_id = $this->userRepo->getUserId($request->user_id);
                $industry_id = $request->industry_id;

                $updated_industry =  $this->industryUserRepo->updateIndustryUser($id,$user_id,$industry_id);
            }
        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        //$this->page=1;
        return $this->makeRespone($updated_industry,201);
    }

    public function delete_industry_by_id(Request $request)
    {
        try{
            if($request->has('id'))
            {
                return $this->industryUserRepo->deleteIndustryUser($request->id);
            }
        }catch(Exception $e) {
            $e->getMessage();
            abort(500);
        }
    }

    public function get_industry_all(Request $request)
    {
        $industry=$request->industry_id;
        return $this->industryUserRepo->getIndustryUsers($industry);
    }
   
}