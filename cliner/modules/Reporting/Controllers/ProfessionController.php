<?php

namespace Modules\Reporting\Controllers;

use App\validator\ProfessionValidator;
use App\validator\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\ProfessionRepo;
use Mockery\Exception;

class ProfessionController extends BaseController{
    private $professionRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProfessionRepo $professionRepo, ProfessionValidator $professionValidator)
    {
        $this->professionRepo = $professionRepo;
        $this->professionValidator = $professionValidator;
    }
    
    public function store_profession(Request $request)
    {
        try{
            $validate_data = $this->professionValidator->professionsStoreValidation($request->all());
            $profession = $request->profession;
            $profs = $this->professionRepo->storeProfession($profession);
        }catch(ValidationException $e){
             return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
        if($profs) {
            return $this->makeRespone($profs, 200);
        }

    }

    public function get_profession()
    {
        //return "ok";
       try{
        $all_profs =  $this->professionRepo->getProfession();
       }catch(Exception $e){
           $e->getMessage();
           abort(404);
       }
        if($all_profs) {
            return $this->makeRespone($all_profs, 200);
        }
        else {
            return "No results";
        }
    }
   
}