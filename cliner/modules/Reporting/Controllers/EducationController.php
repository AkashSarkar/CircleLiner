<?php

namespace Modules\Reporting\Controllers;

use App\validator\EducationValidator;
use App\validator\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\EducationRepo;

class EducationController extends BaseController{
    private $educationRepo, $educationValidator;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EducationRepo $educationRepo, EducationValidator $educationValidator)
    {
        $this->educationRepo = $educationRepo;
        $this->educationValidator = $educationValidator;
    }

    public function store_education(Request $request)
    {
        try{
            $validate_data = $this->educationValidator->educationsStoreValidation($request->all());
            $education = $request->education;
            $edu = $this->educationRepo->storeEducation($education);

        }catch(ValidationException $e){
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
            return $this->makeRespone($edu, 200);
    }

    public function get_education()
    {
        try{
            $all_educations = $this->educationRepo->getEducation();
        }catch(Exception $e){
            $e->getMessage();
            abort(404);
        }
        if($all_educations){
            return $this->makeRespone($all_educations, 200);
        }
        else {
            return "No results";
        }
    }
   
}