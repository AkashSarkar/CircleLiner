<?php

namespace Modules\Reporting\Controllers;

use App\validator\EducationValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\EducationUserRepo;
use App\Domain\Repo\UserRepo;
use Illuminate\Http\Response;

use App\validator\ValidationException;

class EducationUserController extends BaseController{
    private $educationUserRepo, $userRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EducationUserRepo $educationUserRepo, EducationValidator $educationValidator, UserRepo $userRepo)
    {
        $this->educationUserRepo = $educationUserRepo;
        $this->educationValidator = $educationValidator;
        $this->userRepo = $userRepo;
    }

    //store data
    public function store_education(Request $request)
    {

        try{
            $validate_data = $this->educationValidator->educationStoreValidation($request->all());
            $user_id = $this->userRepo->getUserId($request->user_id);
            $education_id = $request->education_id;

            $store_education = $this->educationUserRepo->storeEducationUser($user_id,$education_id);

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        //$this->page=1;
        return $this->makeRespone($store_education,200);
    }

    //get data
    public function get_education_by_userid(Request $request)
    {
        try {
            //$validate_data =  $like_form_validator->getLikeUserListValidation($request->all());
            if($request->has('user_id')){
                $user_id = $this->userRepo->getUserId($request->user_id);
                $educationsData= $this->educationUserRepo->getEducationUser($user_id);

            }
        } catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
        return $this->makeRespone($educationsData,200);

    }

    //Update Profession
    public function update_education(Request $request){
        try{
            if($request->has('id')) {
                $validate_data = $this->educationValidator->educationUpdateValidation($request->all());
                $id = $request->id;
                $user_id = $this->userRepo->getUserId($request->user_id);
                $education_id = $request->education_id;

                $updated_education =  $this->educationUserRepo->updateUserEducation($id,$user_id,$education_id);
            }

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        //$this->page=1;
        return $this->makeRespone($updated_education,201);
    }


    //Delete user education
    public function delete_education_by_id(Request $request)
    {
        try{
            if($request->has('id'))
            {
                $delete_education = $this->educationUserRepo->deleteUserEducation($request->id);
            }
        }catch(Exception $e) {
            $e->getMessage();
            abort(500);
        }

        return $this->makeRespone($delete_education,200);

    }

}