<?php

namespace Modules\Reporting\Controllers;

use App\validator\IndustryValidator;
use App\validator\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\IndustryRepo;

class IndustryController extends BaseController{

    private $industryRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IndustryRepo $industryRepo, IndustryValidator $industryValidator)
    {
        $this->industryRepo = $industryRepo;
        $this->industryValidator = $industryValidator;
    }

    public function store_industry(Request $request){

        try{
        $validate_data = $this->industryValidator->industriesStoreValidation($request->all());
        $industry = $request->industry;
        $inds = $this->industryRepo->storeIndustry($industry);

        }catch(ValidationException $e){
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        }
        if($inds){
            return $this->makeRespone($inds, 200);
        }
    }

    public function get_industry()
    {
        try{
            $all_industries =  $this->industryRepo->getIndustry();
        }catch(Exception $e){
            $e->getMessage();
            abort(404);
        }

        if($all_industries){
            return $this->makeRespone($all_industries, 200);
        }
        else {
            return "No results";
        }
    }
   
}