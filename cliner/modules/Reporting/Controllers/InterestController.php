<?php

namespace Modules\Reporting\Controllers;

use App\validator\InterestValidator;
use App\validator\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\InterestRepo;

class InterestController extends BaseController{
    private $interestRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(interestRepo $interestRepo, InterestValidator $interestValidator)
    {
        $this->interestRepo = $interestRepo;
        $this->interestValidator = $interestValidator;
    }

    public function store_interests(Request $request){

        try{
            $validate_data = $this->interestValidator->interestsStoreValidation($request->all());
            $interest = $request->interest;
            $ints= $this->interestRepo->storeInterest($interest);
           }catch(ValidationException $e) {
              return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
        if($ints){
            return $this->makeRespone($ints, 200);
        }
    }

    public function get_interests()
    {
        try{
            $all_interests = $this->interestRepo->getInterest();
        }catch(Exception $e){
            $e->getMessage();
            abort(404);
        }
        if($all_interests){
            return $this->makeRespone($all_interests, 200);
        }
        else {
            return "No results";
        }
    }
   
}