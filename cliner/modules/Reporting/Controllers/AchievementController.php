<?php

namespace Modules\Reporting\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Repo\AchievementRepo;

class AchievementController extends Controller{

    private $achievementRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AchievementRepo $achievementRepo)
    {
        $this->achievementRepo = $achievementRepo;
    }

    public function get()
    {
        return $this->achievementRepo->getAchievement();
    }
   
}