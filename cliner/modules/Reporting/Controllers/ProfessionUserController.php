<?php

namespace Modules\Reporting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\ProfessionUserRepo;
use App\Domain\Repo\UserRepo;
use Illuminate\Http\Response;
use App\validator\ProfessionValidator;
use App\validator\ValidationException;

class ProfessionUserController extends BaseController{
    private $professionUserRepo, $userRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProfessionUserRepo $professionUserRepo, ProfessionValidator $professionValidator, UserRepo $userRepo)
    {
        $this->professionUserRepo = $professionUserRepo;
        $this->professionValidator=$professionValidator;
        $this->userRepo = $userRepo;
    }

    public function store_profession(Request $request)
    {


        try{
            $validate_data = $this->professionValidator->professionStoreValidation($request->all());
            $user_id = $this->userRepo->getUserId($request->user_id);
            $profession_id = $request->profession_id;
            $store_profession =  $this->professionUserRepo->storeProfessionUser($user_id,$profession_id);

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
        //$this->page=1;
        return $this->makeRespone($store_profession,200);

    }

    public function get_profession_by_userid(Request $request)
    {
        $response=[];

        try {
            //$validate_data =  $like_form_validator->getLikeUserListValidation($request->all());
            if($request->has('user_id')){
                $user_id=$this->userRepo->getUserId($request->user_id);
                $professionsData= $this->professionUserRepo->getProfessionUser($user_id);
            }
        } catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
        return $this->makeRespone($professionsData,200);

    }

    //Update Profession
    public function update_profession(Request $request){
        try{
            if($request->has('id')) {
                $validate_data = $this->professionValidator->professionUpdateValidation($request->all());
                $id = $request->id;
                $user_id=$this->userRepo->getUserId($request->user_id);
                $profession_id = $request->profession_id;

                $updated_profession =  $this->professionUserRepo->updateProfessionUser($id,$user_id,$profession_id);
            }

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        //$this->page=1;
        return $this->makeRespone($updated_profession,201);
    }


    //Delete user profession
    public function delete_profession_by_id(Request $request)
    {
        try{
            if($request->has('id'))
            {
                $delete_profession = $this->professionUserRepo->deleteProfessionUser($request->id);
            }
        }catch(Exception $e) {
            $e->getMessage();
            abort(500);
        }

        return $this->makeRespone($delete_profession,200);

    }

}