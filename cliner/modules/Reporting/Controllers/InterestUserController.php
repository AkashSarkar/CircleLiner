<?php

namespace Modules\Reporting\Controllers;

use App\validator\InterestValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Domain\Repo\InterestUserRepo;
use App\Domain\Repo\UserRepo;
use App\validator\ValidationException;

class InterestUserController extends BaseController{
    private $interestUserRepo,$userRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(InterestUserRepo $interestUserRepo, InterestValidator $interestValidator, UserRepo $userRepo)
    {
        $this->interestUserRepo=$interestUserRepo;
        $this->interestValidator = $interestValidator;
        $this->userRepo=$userRepo;
    }

    public function store_interest(Request $request)
    {
        try{
            $validate_data = $this->interestValidator->interestStoreValidation($request->all());
            $user_id = $this->userRepo->getUserId($request->user_id);
            $interest_id = $request->interest_id;

            $store_interest =  $this->interestUserRepo->storeUserInterest($user_id, $interest_id);

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
        //$this->page=1;
        return $this->makeRespone($store_interest,200);
    }

    public function get_interest_by_userid(Request $request)
    {
        try {
            //$validate_data =  $like_form_validator->getLikeUserListValidation($request->all());
            if($request->has('user_id')){
                $user_id=$this->userRepo->getUserId($request->user_id);
                $interestsData= $this->interestUserRepo->getUserInterest($user_id);
            }
        } catch (ValidationException $e) {

            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }
        return $this->makeRespone($interestsData,200);

        //return $this->interestUserRepo->getInterestUser();
    }

    public function update_interest(Request $request)
    {
        try{
            if($request->has('id')) {
                $validate_data = $this->interestValidator->interestUpdateValidation($request->all());
                $id = $request->id;
                $user_id = $this->userRepo->getUserId($request->user_id);
                $interest_id = $request->interest_id;

                $updated_interest =  $this->interestUserRepo->updateUserInterest($id,$user_id,$interest_id);
            }

        }catch(ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(),$e->getCode());
        }

        //$this->page=1;
        return $this->makeRespone($updated_interest,201);

        //return $this->interestUserRepo->getInterestUser();
    }

    public function delete_interest_by_id(Request $request)
    {
        try{
            if($request->has('id'))
            {
                return $this->interestUserRepo->deleteUserInterest($request->id);
            }
        }catch(Exception $e) {
            $e->getMessage();
            abort(500);
        }
        //return $this->interestUserRepo->getInterestUser();
    }
   
}