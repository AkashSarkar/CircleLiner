<?php
namespace Modules\Reporting\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class AchievementModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'achievements';
    protected $fillable = [
        'achievements_type_id',
        'count',
        'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}