<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 6:04 PM
 */


namespace Modules\Reporting\Models\DataModel;

class ReportUser
{

    public $reportedBy;
    public $reportedUserId;
    public $reportText;

    function __construct()
    {
        $this->reportedBy = '';
        $this->reportedUserId = '';
        $this->reportText = '';
    }

    public function castMe($obj)
    {
        $this->reportedBy = $obj->reportedBy;
        $this->reportedUserId = $obj->reportedUserId;
        $this->reportText = $obj->reportText;
    }
}