<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 6:14 PM
 */


namespace Modules\Reporting\Models\DataModel;


class Education
{
public $id;
public $title;
public $period;
public $description;
public $ownerUserId;

function __construct()
{
     $this->id='';
     $this->title='';
     $this->period='';
     $this->description='';
     $this->ownerUserId='';
}

public function castMe($obj){
    $this->id=$obj->id;
    $this->title=$obj->title;
    $this->period=$obj->period;
    $this->description=$obj->description;
    $this->ownerUserId=$obj->ownerUserId;
}
}