<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 5:41 PM
 */


namespace Modules\Reporting\Models\DataModel;


class Industry
{
    public $id;
    /**
     * Full name of user
     *
     * @type {string}
     * @memberof User
     */
    public $industryName;

    public $concentratedIndustryName;
    /**
     * User identifier
     *
     * @type {string}
     * @memberof User
     */
    public $userId;

    function __construct()
    {
        $this->id = '';
        $this->industryName = '';
        $this->concentratedIndustryName = '';
    }

    public function castMe($obj)
    {
        $this->id = $obj->id;
        $this->industryName = $obj->industryName;
        $this->concentratedIndustryName = $obj->concentratedIndustryName;
    }
}