<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 5:50 PM
 */


namespace Modules\Reporting\Models\DataModel;


class Profession
{
public $id;
public $workplaceName;
public $workPeriod;
public $workDescription;
public $userId;
    function __construct()
    {
        $this->id='';
        $this->workplaceName='';
        $this->workPeriod='';
        $this->workDescription='';
        $this->userId='';
    }
    public function castMe($obj)
    {
        $this->id=$obj->id;
        $this->workplaceName=$obj->workplaceName;
        $this->workPeriod=$obj->workPeriod;
        $this->workDescription=$obj->workDescription;
        $this->userId=$obj->userId;

    }

}