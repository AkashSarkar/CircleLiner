<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 5:35 PM
 */


namespace Modules\Reporting\Models\DataModel;


class ConcentratedIndustry
{
    public $id;
    public $concentratedIndustryName;
    public $userId;

    function __construct()
    {
        $this->id='';
        $this->concentratedIndustryName='';
        $this->userId='';
    }

    public function castMe($obj)
    {

        $this->id=$obj->id;
        $this->concentratedIndustryName=$obj->concentratedIndustryName;
        $this->userId=$obj->userId;
    }
}