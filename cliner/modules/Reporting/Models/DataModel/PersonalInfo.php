<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 5:45 PM
 */


namespace Modules\Reporting\Models\DataModel;


class PersonalInfo
{
public $fullName;
public $email;
public $dob;
public $website;
public $phone;
public $birthPlace;
public $gender;
public $status;
public $ownerUserId;

function __construct()
{
    $this->fullName='';
     $this->email='';
     $this->dob='';
     $this->website='';
     $this->phone='';
     $this->birthPlace='';
     $this->gender='';
     $this->status='';
     $this->ownerUserId='';

}
public function castMe($obj)
{
    $this->fullName=$obj->fullName;
    $this->email=$obj->email;
    $this->dob=$obj->dob;
    $this->website=$obj->website;
    $this->phone=$obj->phone;
    $this->birthPlace=$obj->birthPlace;
    $this->gender=$obj->gender;
    $this->status=$obj->status;
    $this->ownerUserId=$obj->ownerUserId;
}

}