<?php
/**
 * Created by PhpStorm.
 * User: deepita
 * Date: 6/6/18
 * Time: 5:24 PM
 */

namespace Modules\Reporting\Models\DataModel;


class AccountSetting
{
    public $followPrivacy;
    public $postPrivacy;
    public $notificationSound;
    public $emailNotificationSound;
    public $followerBirthdayNotificationSoun;
    public $chatSound;
    public $ownerUserId;

    function __construct()
    {
        $this->followPrivacy='';
        $this->postPrivacy='';
        $this->notificationSound='';
        $this->emailNotificationSound='';
        $this->followerBirthdayNotificationSoun='';
        $this->ownerUserId='';
    }

    public function castMe($obj)
    {
        $this->followPrivacy=$obj->followPrivacy;
        $this->postPrivacy=$obj->postPrivacy;
        $this->notificationSound=$obj->notificationSound;
        $this->emailNotificationSound=$obj->emailNotificationSound;
        $this->followerBirthdayNotificationSoun=$obj->followerBirthdayNotificationSoun;
        $this->ownerUserId=$obj->ownerUserId;
    }
}