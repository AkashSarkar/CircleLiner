<?php
namespace Modules\Reporting\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class IndustryUserModel extends BaseModel
{
    protected $table = 'industries_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'industry_id',
        'user_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}