<?php
namespace Modules\Reporting\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class InterestModel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'interests';
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}