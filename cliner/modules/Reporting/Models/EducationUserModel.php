<?php
namespace Modules\Reporting\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class EducationUserModel extends BaseModel
{


    protected $table = 'educations_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'education_id',
        'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}