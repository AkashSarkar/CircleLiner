<?php
namespace Modules\Reporting\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class ProfessionModel extends BaseModel
{
    //protected $table = 'profession';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'professions';
    protected $fillable = [
        'name',
        'connection_type_id'
       
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}