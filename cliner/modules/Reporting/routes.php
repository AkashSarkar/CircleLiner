<?php

$route = 'user-details';
$multiRoute = function () use($route) {
    Route::group(['module' => 'Reporting', 'namespace' => 'Modules\Reporting\Controllers','middleware' =>'jwt.auth'], function ($router) use($route) {

        //User PROFESSION
        $router->post($route.'/profession', 'ProfessionUserController@store_profession');
        $router->get($route.'/profession', 'ProfessionUserController@get_profession_by_userid');
        $router->patch($route.'/profession', 'ProfessionUserController@update_profession');
        $router->delete($route.'/profession', 'ProfessionUserController@delete_profession_by_id');

        //All PROFESSION
        $router->post($route.'/professions', 'ProfessionController@store_profession');
        $router->get($route.'/professions', 'ProfessionController@get_profession');

        //User EDUCATION
        $router->post($route.'/education', 'EducationUserController@store_education');
        $router->get($route.'/education', 'EducationUserController@get_education_by_userid');
        $router->patch($route.'/education', 'EducationUserController@update_education');
        $router->delete($route.'/education', 'EducationUserController@delete_education_by_id');

        //All EDUCATION
        $router->post($route.'/educations', 'EducationController@store_education');
        $router->get($route.'/educations', 'EducationController@get_education');

        //User INDUSTRY
        $router->post($route.'/industry', 'IndustryUserController@store_industry');
        $router->get($route.'/industry', 'IndustryUserController@get_industry_by_userid');
        $router->patch($route.'/industry', 'IndustryUserController@update_industry');
        $router->delete($route.'/industry', 'IndustryUserController@delete_industry_by_id');

        $router->get($route.'/industry-users', 'IndustryUserController@get_industry_all');

        //All INDUSTRY
        $router->post($route.'/industries', 'IndustryController@store_industry');
        $router->get($route.'/industries', 'IndustryController@get_industry');

        //User INTEREST
        $router->post($route.'/interest', 'InterestUserController@store_interest');
        $router->get($route.'/interest', 'InterestUserController@get_interest_by_userid');
        $router->patch($route.'/interest', 'InterestUserController@update_interest');
        $router->delete($route.'/interest', 'InterestUserController@delete_interest_by_id');

        //All INTEREST
        $router->post($route.'/interests', 'InterestController@store_interests');
        $router->get($route.'/interests', 'InterestController@get_interests');

        //User ACHIEVEMENT
        $router->get($route.'/achievement', 'AchievementController@get_achievement');

    });
};

//TODO: define V_PREFIX in ..env
Route::group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);
