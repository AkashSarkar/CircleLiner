<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 7/4/18
 * Time: 8:35 PM
 */

namespace Modules\Job\Models;


use App\Models\BaseModel;

class JobModel extends BaseModel
{
    protected $table = "jobs";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'description',
        'location',
        'start_date',
        'end_date',
        'label_of_skils',
        'industry',
        'job_application',
        'core_of_skills',
        'no_of_vacancy',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'id', 'updated_at'
    ];

}