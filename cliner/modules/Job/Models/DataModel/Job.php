<?php
/**
 * Created by PhpStorm.
 * User: mozzammel
 * Date: 7/8/18
 * Time: 12:00 PM
 */
namespace Modules\Job\Models\DataModel;
class Job{
    public $id;
    public $title;
    public $type;
    public $description;
    public $location;
    public $startDate;
    public $endDate;
    public $labelOfSkills;
    public $industry;
    public $jobApplication;
    public $coreOfSkills;
    public $noOfVacancy;

    function __construct()
    {
        $this->id=(string)'';
        $this->title=(string)'';
        $this->type=(string)'';
        $this->description=(string)'';
        $this->location=(string)'';
        $this->startDate=(int)0;
        $this->endDate=(int)0;
        $this->labelOfSkills=(string)'';
        $this->industry='';
        $this->jobApplication='';
        $this->coreOfSkills='';
        $this->noOfVacancy='';
    }
    public function castMe($obj){
        if($obj){
            if ($obj->hash_id) {
                $this->id = $obj->hash_id;
            }
            if($obj->title){
                $this->title=$obj->title;
            }
            if($obj->type){
                $this->type=$obj->type;
            }
            if($obj->description){
                $this->description=$obj->description;
            }
            if($obj->location){
                $this->location=$obj->location;
            }
            if($obj->start_date){
                $this->startDate=strtotime($obj->start_date);
            }
            if($obj->end_date){
                $this->endDate=strtotime($obj->end_date);
            }
            if($obj->label_of_skils){
                $this->labelOfSkills=$obj->label_of_skils;
            }
            if($obj->industry){
                $this->industry=json_decode($obj->industry);
            }
            if($obj->job_application){
                $this->jobApplication=json_decode($obj->job_application);
            }
            if($obj->core_of_skills){
                $this->coreOfSkills=json_decode($obj->core_of_skills);
            }
            if($obj->no_of_vacanc){
                $this->noOfVacancy=$obj->no_of_vacancy;
            }
        }

    }

}