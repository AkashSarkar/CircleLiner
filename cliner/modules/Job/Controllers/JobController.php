<?php
/**
 * Created by PhpStorm.
 * User: jamil
 * Date: 7/4/18
 * Time: 8:27 PM
 */

namespace Modules\Job\Controllers;


use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\validator\ValidationException;
use App\Domain\Repo\JobRepo;

class JobController extends BaseController
{
    private $jobRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(JobRepo $jobRepo)
    {
        parent::__construct();
        $this->jobRepo = $jobRepo;
    }

    public function index(Request $request)
    {
        $response = [];
        $statusCode = 200;
        try {
            $page = $request->has('page') ? $request->page : 0;
            $limit = $request->has('limit') ? $request->limit : 10;

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);

    }

    public function show(Request $request)
    {
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);
    }

    public function store(Request $request)
    {
        $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);
    }

    public function update(Request $request)
    {   $response = [];
        $statusCode = 200;
        try {

        } catch (ValidationException $e) {
            return $this->makeErrorResponse($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);
    }

    public function destroy(Request $request)
    {
        $response = [];
        $statusCode = 200;
        try {

        } catch (\Exception $e) {
            $response['message'] = "Something went worng";
            $statusCode = 500;
        }
        return $this->makeRespone($response, $statusCode);
    }

}