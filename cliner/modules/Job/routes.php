<?php
$route = 'job';
$multiRoute = function () use ($route) {
    //TODO: Make the api seo friendly
    $this->app->router->group(['module' => 'Job', 'namespace' => 'Modules\Job\Controllers', 'middleware' => 'jwt.auth'], function ($router) use ($route) {
        /*
         * Job CRUD
         * */
        $router->get($route . '/', 'JobController@index');
        $router->get($route . '/{hid}', 'PostController@show');
        $router->post($route . '/', 'PostController@store');
        $router->put($route . '/{hid}', 'PostController@update');
        $router->delete($route . '/{hid}', 'PostController@destroy');

    });
};
//TODO: define V_PREFIX in ..env
$this->app->router->group(['prefix' => env('V_PREFIX', 'api'), 'as' => env('V_PREFIX', 'api') . '.'], $multiRoute);
