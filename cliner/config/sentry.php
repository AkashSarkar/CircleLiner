<?php
/**
 * 2013-2025 hkjamil
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Open Software License
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future. If you wish to customize this codes for your
 * needs please refer to https://github.com/hkazijamil for more information.
 *
 * @author: Kazi Jamil (ghost)<hkazijamil@gmail.com>
 * @date: 5/15/18
 * @time: 3:10 AM
 * @copyright: 2013-2025 hkjamil
 * @source: https://github.com/hkazijamil
 */


return array(
    'dsn' => 'https://<key>:<secret>@sentry.io/<project>',

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
    'user_context' => false,
);