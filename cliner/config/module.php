<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/22/18
 * Time: 5:56 PM
 */

return [
    'modules' => array(
        "Authentication",
        "UserInfo",
        "SearchKeyword",
        "UserResponse",
        "UserRelation",
        "Posting",
        "Privacy",
        "GeoLocation",
        "Reporting",
        "Job",
    )
];