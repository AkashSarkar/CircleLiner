<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//$router->post('register','UserController@register');
//$router->post('login','UserController@authentication');

//$router->get('user','UserController@user');
//$router->get('register', 'AuthController@register');
$router->get('test', 'TestController@register');
$router->post('hello', 'TestController@test');