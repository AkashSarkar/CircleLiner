// - Import react components
import firebase, {firebaseRef, firebaseAuth, db} from 'data/firestoreClient'
import moment from 'moment/moment'
import {SocialError} from 'core/domain/common'
import {Profile, UserProvider} from 'core/domain/users'
import {IUserService} from 'core/services/users'
import {injectable} from 'inversify'
import {PersonalInfo} from 'core/domain/users/personalInfo'
import {Industry} from 'core/domain/users/industry'
import {Education} from 'core/domain/users/education'
import {Profession} from 'core/domain/users/profession'
import {AccountSetting} from 'core/domain/users/accountSettings'
import {BlockUser} from 'core/domain/users/blockUser'
import {ReportUser} from 'core/domain/users/reportUser'
import {SocialUrl} from 'core/domain/users/socialUrl'
import axios from 'axios'

/**
 * Firbase user service
 *
 * @export
 * @class UserService
 * @implements {IUserService}
 */
@injectable()
export class UserService implements IUserService {

    public addUserIndustry: (userId: string, addIndustry: Industry)
        => Promise<string> = (userId, addIndustry) => {
        return new Promise<string>((resolve, reject) => {
            const batch = db.batch()
            let userIndustryRef = db.collection(`industry`).doc()
            batch.set(userIndustryRef, {...addIndustry, id: userIndustryRef.id})
            batch.commit().then(() => {
                resolve(userIndustryRef.id)
            }).catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserProfile' + error.message)))
        })
    }

    public reportProfile: (reportProfile: ReportUser)
        => Promise<string> = (reportProfile) => {
        return new Promise<string>((resolve, reject) => {
            let userProfileRef = db.collection(`report`).doc()
            userProfileRef.set({...reportProfile, id: userProfileRef.id})
                .then(() => {
                    resolve(userProfileRef.id)
                })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public blockPeople: (blockPeople: BlockUser)
        => Promise<string> = (blockPeople) => {
        return new Promise<string>((resolve, reject) => {
            let blockRef = db.collection(`block`).doc()
            blockRef.set({...blockPeople, id: blockRef.id})
                .then(() => {
                    resolve(blockRef.id)
                })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    /**
     * Get user profession
     */
    public getUserProfession: (userId: string)
        => Promise<{ [professionId: string]: Profession }[]> = (userId) => {
        return new Promise<{ [professionId: string]: Profession }[]>((resolve, reject) => {
            let parsedData: { [id: string]: Profession }[] = []
            let userProfessionRef = db.collection(`profession`).where('userId', '==', userId)
            userProfessionRef.get().then((profession) => {
                profession.forEach((professionResult) => {
                    const profession = professionResult.data() as Profession
                    parsedData = [
                        ...parsedData,
                        {
                            [professionResult.id]: {
                                id: professionResult.id,
                                ...profession
                            }
                        }
                    ]
                })
                resolve(parsedData)
            }).catch((error: any) => reject(new SocialError(error.code, 'firestore/getUserProfile :' + error.message)))
        })
    }

    /**
     * Get user industry
     */
    public getUserIndustry: (userId: string)
        => Promise<any> = (userId) => {
        return new Promise<{ [industryId: string]: Industry }[]>((resolve, reject) => {
            let parsedData: { [id: string]: Industry }[] = []
            let userIndustryRef = db.collection(`industry`).where('userId', '==', userId)
            userIndustryRef.get().then((industry) => {
                industry.forEach((industryResult) => {
                    const industry = industryResult.data() as Industry
                    parsedData = [
                        ...parsedData,
                        {
                            [industryResult.id]: {
                                ...industry
                            }
                        }
                    ]
                })
                resolve(parsedData)
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/getUserProfile :' + error.message)))
        })
    }

    public getUserEducation: (userId: string)
        => Promise<any> = (userId) => {
        return new Promise<{ [educationId: string]: Education }[]>((resolve, reject) => {
            let parsedData: { [id: string]: Education }[] = []
            let userEducationRef = db.collection(`usereducation`).where('userId', '==', userId)
            userEducationRef.get().then((education) => {
                education.forEach((educationResult) => {
                    const education = educationResult.data() as Education
                    parsedData = [
                        ...parsedData,
                        {
                            [educationResult.id]: {
                                ...education
                            }
                        }
                    ]
                })
                console.log(parsedData)
                resolve(parsedData)
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/getUserProfile :' + error.message)))
        })
    }

    /**
     * Get user profile
     */
    public getUserProfile: (userId: string)
        => Promise<Profile> = (userId) => {

        return new Promise<Profile>((resolve, reject) => {
            let userProfileRef = db.doc(`userInfo/${userId}`)
            userProfileRef.get().then((result) => {
                if (!result.exists) {
                    this.getUserProviderData(userId).then((providerData: UserProvider) => {
                        if (!UserProvider || !providerData.email) {
                            reject(reject(new SocialError(`firestore/providerdata`, 'firestore/getUserProfile : Provider data or email of provider data is empty!')))
                        }
                        const {avatar, fullName, email, firstName, lastName} = providerData
                        const userProfile = new Profile(avatar, fullName && fullName !== '' ? fullName : email, firstName, lastName, '', '', moment().unix(), email, '', '', -1, '')
                        resolve(userProfile)
                        this.updateUserProfile(userId, userProfile)
                    })
                } else {
                    resolve(result.data() as Profile)
                }
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/getUserProfile :' + error.message)))
        })
    }

    /**
     * Update user profile
     */
    public updateUserProfile: (userId: string, profile: Profile)
        => Promise<void> = (userId, profile) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            const profileRef = db.doc(`userInfo/${userId}`)

            batch.set(profileRef, {...profile, id: userId, state: 'active'})
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserProfile' + error.message)))
        })
    }

    /**
     * Update user personalInfo
     */
    public addPersonalInfo: (userId: string, info: PersonalInfo)
        => Promise<string> = (userId, info) => {
        return new Promise<string>((resolve, reject) => {
            const batch = db.batch()
            const profileRef = db.doc(`userInfo/${userId}`)
            batch.set(profileRef, {...info, id: profileRef.id, state: 'active'})
            batch.commit().then(() => {
                resolve(profileRef.id)
            }).catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserProfile' + error.message)))
        })
    }
    /**
     * Add social url
     */
    public addSocailUrl: (userId: string, socialUrl: SocialUrl)
        => Promise<string> = (userId, socialUrl) => {
        return new Promise<string>((resolve, reject) => {
            const batch = db.batch()
            let userSocialUrlRef = db.collection(`userSocialUrl`).doc()
            batch.set(userSocialUrlRef, {...socialUrl, id: userSocialUrlRef.id})
            batch.commit().then(() => {
                resolve(userSocialUrlRef.id)
            }).catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserProfile' + error.message)))
        })
    }
    /**
     * get user social urls
     */
    public getUserSocialUrls: (userId: string)
        => Promise<any> = (userId) => {
        return new Promise<{ [socialUrlId: string]: SocialUrl }[]>((resolve, reject) => {
            let parsedData: { [id: string]: SocialUrl }[] = []
            let userSocialUrlRef = db.collection(`userSocialUrl`).where('userId', '==', userId)
            userSocialUrlRef.get().then((socialUrl) => {
                socialUrl.forEach((socialUrlResult) => {
                    const userSocialUrl = socialUrlResult.data() as SocialUrl
                    parsedData = [
                        ...parsedData,
                        {
                            [socialUrlResult.id]: {
                                ...userSocialUrl
                            }
                        }
                    ]
                })
                console.log(parsedData)
                resolve(parsedData)
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/getUserProfile :' + error.message)))
        })
    }
    /**
     * update user social url
     */
    public updateSocialUrl: (socialUrl: SocialUrl)
        => Promise<void> = (socialUrl) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            let userSocialUrlRef = db.doc(`userSocialUrl/${socialUrl.id}`)
            batch.update(userSocialUrlRef, {...socialUrl})
            batch.commit().then(() => {
                resolve()
            }).catch((error: any) => {
                reject(new SocialError(error.code, error.message))
            })
        })
    }
    /**
     * delete user social url
     */
    public deleteSocialUrl: (socialUrlId: string)
        => Promise<void> = (socialUrlId) => {
        return new Promise<void>((resolve, reject) => {
            const socialUrlIdCollectionRef = db.collection(`userSocialUrl`)
            const socialUrlIdRef = socialUrlIdCollectionRef.doc(socialUrlId)
            const batch = db.batch()
            batch.delete(socialUrlIdRef)
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }
    /**
     * Update user industry
     */
    public updateUserIndustry: (industry: Industry)
        => Promise<void> = (industry) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            const industryRef = db.doc(`industry/${industry.id}`)
            batch.update(industryRef, {...industry})
            batch.commit().then(() => {
                resolve()
            }).catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserIndustry' + error.message)))
        })
    }

    /**
     * Add profession
     * @param {Profession} postProfession
     * @returns {Promise<string>}
     */

    public addProfession: (userId: string, postProfession: Profession)
        => Promise<string> = (userId, postProfession) => {
        return new Promise<string>((resolve, reject) => {
            const batch = db.batch()
            let professionRef = db.collection(`profession`).doc()
            batch.set(professionRef, {...postProfession, id: professionRef.id})
            batch.commit().then(() => {
                resolve(professionRef.id)
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserProfile' + error.message)))
        })
    }

    /**
     * Updare profession
     */
    public updateProfession: (postProfession: Profession)
        => Promise<void> = (postProfession) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            const professionRef = db.doc(`profession/${postProfession.id}`)

            batch.update(professionRef, {...postProfession})
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public addEducationalInfo: (userId: string, education: Education)
        => Promise<string> = (userId, education) => {
        return new Promise<string>((resolve, reject) => {
            const batch = db.batch()
            let userEducationRef = db.collection(`usereducation`).doc()
            batch.set(userEducationRef, {...education, id: userEducationRef.id})
            batch.commit().then(() => {
                resolve(userEducationRef.id)
            }).catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserProfile' + error.message)))
        })
    }
    public updateEducationalInfo: (education: Education)
        => Promise<void> = (education) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            let userEducationRef = db.doc(`usereducation/${education.id}`)
            batch.update(userEducationRef, {...education})
            batch.commit().then(() => {
                resolve()
            }).catch((error: any) => {
                reject(new SocialError(error.code, error.message))
            })
        })
    }
    public updateProfessionalInfo: (profession: Profession)
        => Promise<void> = (profession) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            let userProfessionRef = db.doc(`profession/${profession.id}`)
            batch.update(userProfessionRef, {...profession})
            batch.commit().then(() => {
                resolve()
            }).catch((error: any) => {
                reject(new SocialError(error.code, error.message))
            })
        })
    }

    public deleteEducation: (educationId: string)
        => Promise<void> = (educationId) => {
        return new Promise<void>((resolve, reject) => {
            const educationCollectionRef = db.collection(`usereducation`)
            const educationRef = educationCollectionRef.doc(educationId)
            const batch = db.batch()
            batch.delete(educationRef)
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public deleteProfession: (professionId: string)
        => Promise<void> = (professionId) => {
        return new Promise<void>((resolve, reject) => {
            const professionCollectionRef = db.collection(`profession`)
            const professionRef = professionCollectionRef.doc(professionId)
            const batch = db.batch()
            batch.delete(professionRef)
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public addAccountsettings: (userId: string, info: AccountSetting)
        => Promise<string> = (userId, info) => {
        return new Promise<string>((resolve, reject) => {
            const batch = db.batch()
            const profileRef = db.doc(`useraccountsettings/${userId}`)
            batch.set(profileRef, {...info, id: profileRef.id, state: 'active'})
            batch.commit().then(() => {
                resolve(profileRef.id)
            }).catch((error: any) => reject(new SocialError(error.code, 'firestore/updateUserProfile' + error.message)))
        })
    }
    /**
     * Get users profile
     */
    public getUsersProfile: (userId: string, lastUserId?: string, page?: number, limit?: number)
        => Promise<{ users: { [userId: string]: Profile }[], newLastUserId: string }> = (userId, lastUserId, page, limit = 10) => {
        return new Promise<{ users: { [userId: string]: Profile }[], newLastUserId: string }>((resolve, reject) => {
            let parsedData: { [userId: string]: Profile }[] = []

            let query = db.collection('userInfo').where('state', '==', 'active')
            if (lastUserId && lastUserId !== '') {
                query = query.orderBy('id').orderBy('creationDate', 'desc').startAfter(lastUserId)
            }
            if (limit) {
                query = query.limit(limit)
            }

            query.get().then((users) => {
                let newLastUserId = users.size > 0 ? users.docs[users.docs.length - 1].id : ''
                users.forEach((result) => {
                    const user = result.data() as Profile
                    parsedData = [
                        ...parsedData,
                        {
                            [result.id]: {
                                ...user
                            }
                        }

                    ]
                })
                resolve({users: parsedData, newLastUserId})
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public updateUserPP: (userId: string, avatar: string) => Promise<void> = (userId, avatar) => {
        return new Promise<void>((resolve, reject) => {
            axios.post('http://localhost:8000/authentication/login', {
                avatar: avatar
            }).then((result) => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }
    public updateUserCP: (userId: string, coverPhoto: string) => Promise<void> = (userId, coverPhoto) => {
        return new Promise<void>((resolve, reject) => {

        })
    }
    /**
     * Get uesr provider data
     */
    private getUserProviderData = (userId: string) => {
        return new Promise<UserProvider>((resolve, reject) => {
            let userProviderRef = db.doc(`userProviderInfo/${userId}`)
            userProviderRef.get().then((snapshot) => {
                if (snapshot.exists) {
                    let userProvider: UserProvider = snapshot.data() as UserProvider || {}
                    resolve(userProvider)
                } else {
                    throw new SocialError(`firestore/getUserProviderData/notExist `, `document of userProviderRef is not exist `)
                }
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, 'firestore/getUserProviderData ' + error.message))
                })
        })
    }
}
