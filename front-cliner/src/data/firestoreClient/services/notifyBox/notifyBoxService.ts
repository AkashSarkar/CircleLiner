// - Import react components
import { firebaseRef, firebaseAuth, db } from 'data/firestoreClient'

import { SocialError } from 'core/domain/common'
import { NotifyBox } from 'core/domain/notifyBox'
import { INotifyBoxService } from 'core/services/notifyBox'
import { injectable } from 'inversify'

/**
 * Firbase notification service
 *
 * @export
 * @class NotifyBoxService
 * @implements {INotificationService}
 */
@injectable()
export class NotifyBoxService implements INotifyBoxService {
    public addNotification: (notification: NotifyBox)
        => Promise<void> = (notification: NotifyBox) => {
        return new Promise<void>((resolve,reject) => {
            db.doc(`users/${notification.notifyRecieverUserId}`).collection(`notifications`)
                .add({...notification})
                .then(() => {
                    resolve()
                })
        })
    }

    public getNotifications: (userId: string, callback: (resultNotifications: { [notifyId: string]: NotifyBox }, newLastNotifyId: string) => void)
        => void = (userId, callback) => {
        var notificationsRef = db.doc(`users/${userId}`)
            .collection('notifications').limit(3)
        notificationsRef.onSnapshot((snapshot) => {
            let parsedData: { [notifyId: string]: NotifyBox } = {}
            let newLastNotifyId = snapshot.size > 0 ? snapshot.docs[snapshot.docs.length - 1].id : ''
            snapshot.forEach((result) => {
                parsedData[result.id] = {
                    id: result.id,
                    ...result.data() as NotifyBox
                }
            })
            callback(parsedData, newLastNotifyId)

        })
    }

    public getPaginatedNotifications: (userId: string, lastNotifyId: string, page: number, limit: number, callback: (resultNotifications: { [notifyId: string]: NotifyBox }, newLastNotifyId: string) => void)
        => void = (userId, lastNotifyId, page, limit, callback) => {
        var notificationsRef = db.doc(`users/${userId}`).collection('notifications')
            .orderBy('id').startAfter(lastNotifyId).limit(limit)

        notificationsRef.onSnapshot((snapshot) => {
            let parsedData: { [notifyId: string]: NotifyBox } = {}
            let newLastNotifyId = snapshot.size > 0 ? snapshot.docs[snapshot.docs.length - 1].id : ''
            snapshot.forEach((result) => {
                parsedData[result.id] = {
                    id: result.id,
                    ...result.data() as NotifyBox
                }
            })
            callback(parsedData, newLastNotifyId)

        })
    }

    public deleteNotification: (notificationId: string, userId: string)
        => Promise < void > = (notificationId, userId) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            const notificationRef = db.doc(`users/${userId}/notifications/${notificationId}`)

            batch.delete(notificationRef)
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public setSeenNotification: (notificationId: string, userId: string, notification: NotifyBox)
        => Promise <void> = (notificationId, userId, notification) => {
        return new Promise<void>((resolve, reject) => {
            const batch = db.batch()
            const notificationRef = db.doc(`users/${userId}/notifications/${notificationId}`)

            batch.update(notificationRef,{...notification})
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

}
