// - Import react components
import {firebaseRef, firebaseAuth, db} from 'data/firestoreClient'

import {SocialError} from 'core/domain/common'
import {Post} from 'core/domain/posts'
import {IJobpostService} from 'core/services/jobpost'
import {injectable} from 'inversify'
import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'
import {JobAsNonCompany} from 'core/domain/jobs/jobAsNonCompany'
import {JobPreferences} from 'core/domain/jobs/jobPreferences'
import {EmployerPreferences} from 'core/domain/jobs/employerPreferences'

/**
 * Firbase post service
 *
 * @export
 * @class JobpostService
 * @implements {IPostService}
 */
@injectable()
export class JobpostService implements IJobpostService {

    public addJobPreferences: (jobPref: JobPreferences)
        => Promise<string> = (jobPref) => {
        return new Promise<string>((resolve, reject) => {
            let jobPrefRef = db.collection(`jobPreferences`).doc()
            jobPrefRef.set({...jobPref, id: jobPrefRef.id})
                .then(() => {
                    resolve(jobPrefRef.id)
                })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public addEmployerPreferences: (employerPref: EmployerPreferences)
        => Promise<string> = (employerPref) => {
        return new Promise<string>((resolve, reject) => {
            let employerPrefRef = db.collection(`jobPreferences`).doc()
            employerPrefRef.set({...employerPref, id: employerPrefRef.id})
                .then(() => {
                    resolve(employerPrefRef.id)
                })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    // add job service as company
    public addJobpost: (jobPost: JobAsCompany)
        => Promise<string> = (jobPost) => {
        return new Promise<string>((resolve, reject) => {
            let companyJobpostRef = db.collection(`job`).doc()
            companyJobpostRef.set({...jobPost, id: companyJobpostRef.id})
                .then(() => {
                    resolve(companyJobpostRef.id)
                })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public addJobpostAsNonCompany: (jobPost: JobAsNonCompany)
        => Promise<string> = (jobPost) => {
        return new Promise<string>((resolve, reject) => {
            let nonCompanyJobpostRef = db.collection(`job`).doc()
            nonCompanyJobpostRef.set({...jobPost, id: nonCompanyJobpostRef.id})
                .then(() => {
                    resolve(nonCompanyJobpostRef.id)
                })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public getJobpostAsCompany: (userId: string)
        => Promise<any> = (userId) => {
        return new Promise<{ [jobAsCompanyId: string]: JobAsCompany }[]>((resolve, reject) => {
            let parsedData: { [id: string]: JobAsCompany }[] = []
            let jobpostAsCompanyRef = db.collection(`job`).where('postType', '==', 1)
            jobpostAsCompanyRef.get().then((jobAsCompany) => {
                console.log('------------------------brebbebbebebebbbbbbbbbbbbbbbbbbbb-----------')
                console.log(jobAsCompany)
                jobAsCompany.forEach((jobAsCompanyResult) => {
                    console.log('-----------jobPosts-----------')
                    console.log(jobAsCompanyResult)
                    const jobAsCompany = jobAsCompanyResult.data() as JobAsCompany
                    parsedData = [
                        ...parsedData,
                        {
                            [jobAsCompanyResult.id]: {
                                id: jobAsCompanyResult.id,
                                ...jobAsCompany
                            }
                        }
                    ]
                })
                resolve(parsedData)
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/getUserProfile :' + error.message)))
        })
    }

    public getJobpostAsNonCompany: (userId: string)
        => Promise<any> = (userId) => {
        return new Promise<{ [jobAsNonCompanyId: string]: JobAsNonCompany }[]>((resolve, reject) => {
            let parsedData: { [id: string]: JobAsNonCompany }[] = []
            let jobpostAsNonCompanyRef = db.collection(`job`).where('postType', '==', 2)
            jobpostAsNonCompanyRef.get().then((jobAsNonCompany) => {
                console.log('------------------------brebbebbebebebbbbbbbbbbbbbbbbbbbb-----------')
                console.log(jobAsNonCompany)
                jobAsNonCompany.forEach((jobAsNonCompanyResult) => {
                    console.log('-----------jobPosts-----------')
                    console.log(jobAsNonCompanyResult)
                    const jobAsNonCompany = jobAsNonCompanyResult.data() as JobAsNonCompany
                    parsedData = [
                        ...parsedData,
                        {
                            [jobAsNonCompanyResult.id]: {
                                id: jobAsNonCompanyResult.id,
                                ...jobAsNonCompany
                            }
                        }
                    ]
                })
                resolve(parsedData)
            })
                .catch((error: any) => reject(new SocialError(error.code, 'firestore/getUserProfile :' + error.message)))
        })
    }

    public updateJobpostAsCompany: (jobPost: JobAsCompany)
        => Promise<void> = (jobPost) => {
        return new Promise<void>((resolve, reject) => {
            console.log('p=p=pp=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p')
            const batch = db.batch()
            let jobAsCompanyRef = db.doc(`job/${jobPost.id}`)
            batch.update(jobAsCompanyRef, {...jobPost})
            batch.commit().then(() => {
                resolve()
            }).catch((error: any) => {
                reject(new SocialError(error.code, error.message))
            })
        })
    }

    public updateJobpostAsNonCompany: (jobPost: JobAsNonCompany)
        => Promise<void> = (jobPost) => {
        return new Promise<void>((resolve, reject) => {
            console.log('p=p=pp=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p')
            const batch = db.batch()
            let jobAsNonCompanyRef = db.doc(`job/${jobPost.id}`)
            batch.update(jobAsNonCompanyRef, {...jobPost})
            batch.commit().then(() => {
                resolve()
            }).catch((error: any) => {
                reject(new SocialError(error.code, error.message))
            })
        })
    }

    public deleteJobpostAsCompany: (jobAsCompanyId: string)
        => Promise<void> = (jobAsCompanyId) => {
        return new Promise<void>((resolve, reject) => {
            console.log('p=p=pp=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p')
            const jobAsCompanyCollectionRef = db.collection(`job`)
            const jobAsCompanyRef = jobAsCompanyCollectionRef.doc(jobAsCompanyId)
            const batch = db.batch()
            batch.delete(jobAsCompanyRef)
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    public deleteJobpostAsNonCompany: (jobAsNonCompanyId: string)
        => Promise<void> = (jobAsNonCompanyId) => {
        return new Promise<void>((resolve, reject) => {
            console.log('p=p=pp=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p=p')
            const jobAsNonCompanyCollectionRef = db.collection(`job`)
            const jobAsNonCompanyRef = jobAsNonCompanyCollectionRef.doc(jobAsNonCompanyId)
            const batch = db.batch()
            batch.delete(jobAsNonCompanyRef)
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }
}
