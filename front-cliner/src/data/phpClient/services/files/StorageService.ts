import {storageRef} from 'data/firestoreClient'
import {IStorageService} from 'core/services/files'
import {FileResult} from 'models/files/fileResult'
import {injectable} from 'inversify'
import {api, apiStore, apiStoreClear, apiToken} from 'data/phpClient'

@injectable()
export class StorageService implements IStorageService {

    /**
     * Upload image on the server
     * @param {file} file
     * @param {string} fileName
     */
    public uploadFile = (file: any, fileName: string, progress: (percentage: number, status: boolean) => void) => {

        return new Promise<FileResult>((resolve, reject) => {
            // Create a storage refrence
            let config = {
                onUploadProgress: function (progressEvent: any) {
                    let percentage = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    if (percentage < 100) {
                        progress(percentage, true)
                    } else {
                        progress(percentage, false)
                    }

                },
            }
            apiStore()
            apiToken(localStorage.getItem('jwt') || '')
            const data = new FormData()
            data.append('file', file)
            data.append('fileName', fileName)
            // Upload file
            let task = api.post('user-info/image', data, config)
            task.then((result) => {
                resolve(new FileResult(result.data.data.downloadURL!, result.data.data.metadata.fullPath))
            }).catch((error) => {
                reject(error)
            })
            apiStoreClear()
        })

    }
}
