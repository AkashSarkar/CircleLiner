import {Profile} from 'core/domain/users'

// - Import react components
import {firebaseRef, firebaseAuth, db} from 'data/firestoreClient'
import {api, apiToken, apiAuth} from 'data/phpClient'
import {IAuthorizeService} from 'core/services/authorize'
import {User, UserProvider} from 'core/domain/users'
import {LoginUser, RegisterUserResult} from 'core/domain/authorize'
import {SocialError} from 'core/domain/common'

import {OAuthType} from 'core/domain/authorize/oauthType'
import moment from 'moment/moment'
import {injectable} from 'inversify'
import axios from 'axios'

/**
 * Firbase authorize service
 *
 * @export
 * @class AuthorizeService
 * @implements {IAuthorizeService}
 */
@injectable()
export class AuthorizeService implements IAuthorizeService {

    /**
     * Login the user
     *
     * @returns {Promise<LoginUser>}
     * @memberof IAuthorizeService
     */
    public login: (email: string, password: string) => Promise<LoginUser> = (email, password) => {

        return new Promise<LoginUser>((resolve, reject) => {
            console.log('login')
            apiAuth.post('/login', {
                email: email,
                password: password
            }).then((result) => {
                localStorage.setItem('jwt', result.data.data.user._jwt)
                resolve(new LoginUser(result.data.data.uid, result.data.data.user._emailVerified))

            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    /**
     * Logs out the user
     *
     * @returns {Promise<void>}
     * @memberof IAuthorizeService
     */
    public logout: () => Promise<void> = () => {
        return new Promise<void>((resolve, reject) => {
            console.log('logout')
            apiAuth.get('logout').then((result) => {
                localStorage.removeItem('jwt')
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    /**
     * Register a user
     *
     * @returns {Promise<void>}
     * @memberof IAuthorizeService
     */
    public registerUser: (user: User) => Promise<RegisterUserResult> = (user) => {
        return new Promise<RegisterUserResult>((resolve, reject) => {
            console.log('register')
            apiAuth.post('subscribe', {
                first_name: user.firstName,
                last_name: user.lastName,
                email: user.email,
                password: user.password
            }).then((signupResult) => {
                const {uid, token, email} = signupResult.data.data.success
                resolve(new RegisterUserResult(uid, email, token))
            }).catch((error: any) => reject(new SocialError(error.code, error.message)))
        })
    }

    /**
     * Update user password
     *
     * @returns {Promise<void>}
     * @memberof IAuthorizeService
     */
    public updatePassword: (uid: string, oldPassword: string, newPassword: string) => Promise<void> = (uid, oldPassword, newPassword) => {

        return new Promise<void>((resolve, reject) => {

            apiAuth.post('change-password', {
                user_hid: uid,
                old_password: oldPassword,
                new_password: newPassword
            }).then(() => {
                // Update successful.
                resolve()
            }).catch((error: any) => {
                // An error happened.
                reject(new SocialError(error.code, error.message))
            })
        })
    }

    /**
     * On user authorization changed event
     *
     * @memberof IAuthorizeService
     */
    public onAuthStateChanged: (callBack: (isVerified: boolean, user: User) => void) => any = (callBack) => {
        console.log('onAuthStateChanged')
        apiToken(localStorage.getItem('jwt') || '')
        apiAuth.post('check-state'
        ).then((result) => {
            let isVerified = false
            let user: any = result.data.user
            console.log(user)
            if (user) {
                console.log(user)
                if (user.emailVerified) {
                    // if (user.emailVerified || user.providerData[0].providerId.trim() !== 'password') {
                    isVerified = true
                } else {
                    isVerified = false
                }
            }
            callBack(isVerified, user)
        })

    }

    /**
     * Reset user password
     *
     * @memberof AuthorizeService
     */
    public resetPassword: (email: string) => Promise<void> = (email) => {
        return new Promise<void>((resolve, reject) => {
            console.log('sd=ds=ds=sd=ds=ds==ds=ds=sd=sd=sd=ds=sd=sd=ds==sd=sd=ds')
            let auth = firebaseAuth()

            auth.sendPasswordResetEmail(email).then(function () {
                resolve()
            }).catch((error: any) => {
                // An error happened.
                reject(new SocialError(error.code, error.message))
            })
        })
    }

    /**
     * Send verfication email to user email
     *
     * @memberof AuthorizeService
     */
    public sendEmailVerification: (email: string, token: string) => Promise<void> = (email, token) => {
        return new Promise<void>((resolve, reject) => {

            apiAuth.post('confirmation-mail', {
                email: email,
                token: token
            }).then(() => {
                resolve()
            }).catch((error: any) => {
                // An error happened.
                reject(new SocialError(error.code, error.message))
            })

        })
    }
    /**
     * Complete verification email to user email
     *
     * @memberof AuthorizeService
     */
    public completeEmailVerification: (token: string) => Promise<void> = (token) => {
        return new Promise<void>((resolve, reject) => {
            console.log(token)
            apiAuth.get(`confirmation?token=${token}`, {}).then(() => {
                resolve()
            }).catch((error: any) => {
                // An error happened.
                console.log(error.response)
                console.log(error.response.data)
                console.log(error.response.data.errors)
                reject(new SocialError(error.code, error.response.data.errors))
            })

        })
    }

    public loginWithOAuth: (type: OAuthType) => Promise<LoginUser> = (type) => {
        return new Promise<LoginUser>((resolve, reject) => {
            console.log('sd=ds=ds=sd=ds=ds==ds=ds=sd=sd=sd=ds=sd=sd=ds==sd=sd=ds')

            let provider: any

            switch (type) {
                case OAuthType.GITHUB:
                    provider = new firebaseAuth.GithubAuthProvider()
                    break
                case OAuthType.FACEBOOK:
                    provider = new firebaseAuth.FacebookAuthProvider()
                    break
                case OAuthType.GOOGLE:
                    provider = new firebaseAuth.GoogleAuthProvider()
                    break
                default:
                    throw new SocialError('authorizeService/loginWithOAuth', 'None of OAuth type is matched!')
            }
            // firebaseAuth().signInWithPopup(provider)
            apiAuth.get('/login/google', {
                headers: {
                    responseType: 'json',
                    withCredintials: true
                }
            }).then((result) => {
                // This gives you a GitHub Access Token. You can use it to access the GitHub API.
                let token = result.data.accessToken
                // The signed-in user info.
                const {user} = result.data
                const {credential} = result.data
                const {uid, displayName, email, photoURL} = user
                const {accessToken, providerId} = credential
                this.storeUserProviderData(uid, email, displayName, photoURL, providerId, accessToken)
                // this.storeUserInformation(uid,email,displayName,photoURL).then(resolve)
                resolve(new LoginUser(user.uid, true, providerId, displayName, email, photoURL))

            }).catch(function (error: any) {
                // Handle Errors here.
                let errorCode = error.code
                let errorMessage = error.message
                // The email of the user's account used.
                let email = error.email
                // The firebase.auth.AuthCredential type that was used.
                let credential = error.credential

            })

        })
    }

    /**
     * Store user information
     *
     * @private
     * @memberof AuthorizeService
     */
    private storeUserInformation = (userId: string, email: string, fullName: string, avatar: string) => {
        return new Promise<RegisterUserResult>((resolve, reject) => {
            console.log('sd=ds=ds=sd=ds=ds==ds=ds=sd=sd=sd=ds=sd=sd=ds==sd=sd=ds')
            db.doc(`userInfo/${userId}`).set(
                {
                    id: userId,
                    state: 'active',
                    avatar,
                    fullName,
                    creationDate: moment().unix(),
                    email
                }
            )
                .then(() => {
                    resolve(new RegisterUserResult(userId, '', ''))
                })
                .catch((error: any) => reject(new SocialError(error.name, 'firestore/storeUserInformation : ' + error.message)))
        })
    }

    /**
     * Store user provider information
     *
     * @private
     * @memberof AuthorizeService
     */
    private storeUserProviderData = (
        userId: string,
        email: string,
        fullName: string,
        avatar: string,
        providerId: string,
        accessToken: string
    ) => {
        return new Promise<RegisterUserResult>((resolve, reject) => {
            console.log('sd=ds=ds=sd=ds=ds==ds=ds=sd=sd=sd=ds=sd=sd=ds==sd=sd=ds')
            db.doc(`userProviderInfo/${userId}`)
                .set(
                    {
                        userId,
                        email,
                        fullName,
                        avatar,
                        providerId,
                        accessToken
                    }
                )
                .then(() => {
                    resolve(new RegisterUserResult(userId, '', ''))
                })
                .catch((error: any) => reject(new SocialError(error.name, error.message)))
        })
    }
}
