// - Import react components
import {firebaseRef, firebaseAuth, db} from 'data/firestoreClient'
import _ from 'lodash'

import {SocialError} from 'core/domain/common'
import {ICommentService} from 'core/services/comments'
import {Comment} from 'core/domain/comments'
import {injectable} from 'inversify'
import {postComments} from 'models/comments/commentTypes'
import {api, apiToken} from 'data/phpClient'

/**
 * Firbase comment service
 *
 * @export
 * @class CommentService
 * @implements {ICommentService}
 */
@injectable()
export class CommentService implements ICommentService {

    /**
     * Add comment
     *
     * @memberof CommentService
     */
    public addComment: (comment: Comment)
        => Promise<string> = (comment) => {
        return new Promise<string>((resolve, reject) => {
            api.post('/user-response/comment', {
                ...comment
            }).then((result) => {
                resolve(result.data.data.comment_id)
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    /**
     * Get comments
     *
     * @memberof CommentService
     */

    public getComments: (postId: string, next: (resultComments: postComments) => void)
        => Promise<void> = (postId, next) => {
        const pop = api.get('/user-response/comment/' + postId).then((result) => {
            let parsedData: { [postId: string]: { [commentId: string]: Comment } } = {[postId]: {}}
            console.log(result.data)
            let comments = result.data.data
            for (var id in comments) {
                if (comments.hasOwnProperty(id)) {
                    parsedData[postId][comments[id].id] = {
                        id: comments[id].id,
                        ...comments[id] as Comment
                    }
                }
            }
            // result.data.data.comment.forEach((comment: any) => {
            //
            // })
            console.log(parsedData)
            if (next) {
                next(parsedData)
            }
        })
        console.log(pop)
        return pop
    }

    /**
     * Update comment
     *
     * @memberof CommentService
     */
    public updateComment: (comment: Comment)
        => Promise<void> = (comment) => {
        return new Promise<void>((resolve, reject) => {
            // const batch = db.batch()
            // const commentRef = db.collection(`comments`).doc(comment.id!)
            //
            // batch.update(commentRef, {...comment})
            // batch.commit()
            api.put('/user-response/comment/' + comment.id, {
                ...comment
            }).then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }

    /**
     * Delete comment
     *
     * @memberof CommentService
     */
    public deleteComment: (commentId: string)
        => Promise<void> = (commentId) => {
        return new Promise<void>((resolve, reject) => {
            const commentCollectionRef = db.collection(`comments`)
            const commentRef = commentCollectionRef.doc(commentId)

            const batch = db.batch()
            batch.delete(commentRef)
            batch.commit().then(() => {
                resolve()
            })
                .catch((error: any) => {
                    reject(new SocialError(error.code, error.message))
                })
        })
    }
}
