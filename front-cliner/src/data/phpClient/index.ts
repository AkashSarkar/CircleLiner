import axios from 'axios'
import {GlobalActionType} from 'constants/globalActionType'

export let apiToken = function (token: string) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token || ''
}
export let apiStore = function () {
    axios.defaults.headers['content-type'] = `multipart/form-data`
}
export let apiStoreClear = function () {
    axios.defaults.headers['content-type'] = `application/json`
}
export let api = axios.create({
    baseURL: `http://${GlobalActionType.SERVER_IP}:8000/api/`,
})

export let apiAuth = axios.create({
    baseURL: `http://${GlobalActionType.SERVER_IP}:8000/api/authentication`
})
