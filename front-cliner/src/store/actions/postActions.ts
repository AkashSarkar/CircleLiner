// - Import domain
import {Post} from 'src/core/domain/posts'
import {PostPromotion} from 'src/core/domain/posts/postPromotion'
import {PostArticle} from 'src/core/domain/posts/postArticle'
import {PostAward} from 'src/core/domain/posts/postAward'
import {PostEvent} from 'src/core/domain/posts/postEvent'
import {PostProject} from 'src/core/domain/posts/postProject'
import {PostConference} from 'src/core/domain/posts/postConference'
import {SocialError} from 'src/core/domain/common'
import {Map} from 'immutable'

// - Import utility components
import moment from 'moment/moment'

// - Import action types
import {PostActionType} from 'constants/postActionType'

// - Import actions
import * as globalActions from 'store/actions/globalActions'
import {IPostService} from 'src/core/services/posts'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {provider} from 'src/socialEngine'

/**
 * Get service providers
 */
const postService: IPostService = provider.get<IPostService>(SocialProviderTypes.PostService)

/* _____________ CRUD DB _____________ */

/**
 * Add a normal post
 */
export let dbAddPost = (newPost: Post, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: Post = {
            postTypeId: 0,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            comments: {},
            votes: {},
            image: '',
            imageFullPath: '',
            video: '',
            disableComments: newPost.disableComments,
            disableSharing: newPost.disableSharing,
            deleted: false
        }

        return postService.addPost(post).then((postKey: string) => {
            dispatch(addPost(uid, {
                ...post,
                id: postKey
            }))
            callBack()
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add a post with image
 */
export const dbAddImagePost = (newPost: Post, callBack: Function) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: Post = {
            postTypeId: 1,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            image: newPost.image || '',
            imageFullPath: newPost.imageFullPath || '',
            video: '',
            disableComments: newPost.disableComments ? newPost.disableComments : false,
            disableSharing: newPost.disableSharing ? newPost.disableSharing : false,
            deleted: false
        }

        return postService.addPost(post).then((postKey: string) => {
            dispatch(addPost(uid, {
                ...post,
                id: postKey
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add a promotion post with image
 */
export const dbAddPromotionPostWithImage = (newPost: PostPromotion, callBack: Function) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: PostPromotion = {
            postTypeId: 3,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            jobPositionName: newPost.jobPositionName,
            companyName: newPost.companyName,
            departmentName: newPost.departmentName,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            image: newPost.image || '',
            imageFullPath: newPost.imageFullPath || '',
            disableComments: newPost.disableComments ? newPost.disableComments : false,
            disableSharing: newPost.disableSharing ? newPost.disableSharing : false,
            deleted: false
        }

        return postService.addPostPromotion(post).then((postKey: string) => {
            dispatch(addPostPromotion(uid, {
                ...post,
                id: postKey
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add a article post with image
 */
export const dbAddArticlePost = (newPost: PostArticle, callBack: Function) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: PostArticle = {
            postTypeId: 5,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            title: newPost.title,
            referenceLink: newPost.referenceLink,
            publishingDate: newPost.publishingDate,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            image: newPost.image || '',
            imageFullPath: newPost.imageFullPath || '',
            disableComments: newPost.disableComments ? newPost.disableComments : false,
            disableSharing: newPost.disableSharing ? newPost.disableSharing : false,
            deleted: false
        }

        return postService.addPostArticle(post).then((postKey: string) => {
            dispatch(addPostArticle(uid, {
                ...post,
                id: postKey
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add a award post with image
 */
export const dbAddAwardPost = (newPost: PostAward, callBack: Function) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: PostAward = {
            postTypeId: 2,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            title: newPost.title,
            receivingPlace: newPost.receivingPlace,
            publishingDate: newPost.publishingDate,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            image: newPost.image || '',
            imageFullPath: newPost.imageFullPath || '',
            disableComments: newPost.disableComments ? newPost.disableComments : false,
            disableSharing: newPost.disableSharing ? newPost.disableSharing : false,
            deleted: false
        }

        return postService.addPostAward(post).then((postKey: string) => {
            dispatch(addPostAward(uid, {
                ...post,
                id: postKey
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add a award post with image
 */
export const dbAddEventPost = (newPost: PostEvent, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: PostEvent = {
            postTypeId: 4,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            title: newPost.title,
            startDate: newPost.startDate,
            endDate: newPost.endDate,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            image: newPost.image || '',
            imageFullPath: newPost.imageFullPath || '',
            disableComments: newPost.disableComments ? newPost.disableComments : false,
            disableSharing: newPost.disableSharing ? newPost.disableSharing : false,
            deleted: false
        }

        return postService.addPostEvent(post).then((postKey: string) => {
            dispatch(addPostEvent(uid, {
                ...post,
                id: postKey
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}
/**
 * Add a award post with image
 */
export const dbAddProjectPost = (newPost: PostProject, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: PostProject = {
            postTypeId: 7,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            title: newPost.title,
            startDate: newPost.startDate,
            endDate: newPost.endDate,
            url: newPost.url,
            clientName: newPost.clientName,
            skillRequired: newPost.skillRequired,
            role: newPost.role,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            image: newPost.image || '',
            imageFullPath: newPost.imageFullPath || '',
            disableComments: newPost.disableComments ? newPost.disableComments : false,
            disableSharing: newPost.disableSharing ? newPost.disableSharing : false,
            deleted: false
        }
        return postService.addPostProject(post).then((postKey: string) => {
            dispatch(addPostProject(uid, {
                ...post,
                id: postKey
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))

    }
}
export const dbAddConferencePost = (newPost: PostConference, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let post: PostConference = {
            postTypeId: 6,
            creationDate: moment().unix(),
            deleteDate: 0,
            score: 0,
            viewCount: 0,
            title: newPost.title,
            topic: newPost.topic,
            url: newPost.url,
            role: newPost.role,
            description: newPost.description,
            ownerUserId: uid,
            ownerDisplayName: newPost.ownerDisplayName,
            ownerAvatar: newPost.ownerAvatar,
            lastEditDate: 0,
            tags: newPost.tags || [],
            commentCounter: 0,
            image: newPost.image || '',
            imageFullPath: newPost.imageFullPath || '',
            disableComments: newPost.disableComments ? newPost.disableComments : false,
            disableSharing: newPost.disableSharing ? newPost.disableSharing : false,
            deleted: false
        }
        return postService.addPostConference(post).then((postKey: string) => {
            dispatch(addPostConference(uid, {
                ...post,
                id: postKey
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}
/**
 * Update a post from database
 */
export const dbUpdatePost = (updatedPost: Map<string, any>, callBack: Function) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())

        return postService.updatePost(updatedPost.toJS()).then(() => {

            dispatch(updatePost(updatedPost))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())

            })
    }

}

/**
 * Delete a post from database
 * @param  {string} id is post identifier
 */
export const dbDeletePost = (id: string) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())

        const state: Map<string, any> = getState()
        // Get current user id
        let uid: string = state.getIn(['authorize', 'uid'])

        return postService.deletePost(id).then(() => {
            dispatch(deletePost(uid, id))
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())
            })
    }

}

/**
 * Get all user posts from data base
 */
export const dbGetPosts = (page: number = 0, limit: number = 10) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        const stream: Map<string, any> = state.getIn(['post', 'stream'])
        const lastPageRequest = stream.get('lastPageRequest')
        const lastPostId = stream.get('lastPostId')

        let uid: string = state.getIn(['authorize', 'uid'])
        if (uid && lastPageRequest !== page) {
            return postService.getPosts(uid, lastPostId, page, limit).then((result) => {
                if (!result.posts || !(result.posts.length > 0) || (result.posts.length < limit)) {
                    return dispatch(notMoreDataStream())
                }

                // Store last post Id
                dispatch(lastPostStream(result.newLastPostId))
                let parsedData: { [userId: string]: { [postId: string]: Post } } = {}
                result.posts.forEach((post) => {
                    const postId = Object.keys(post)[0]
                    const postData = post[postId]
                    const ownerId = postData.ownerUserId!
                    parsedData = {
                        ...parsedData,
                        [ownerId]: {
                            ...parsedData[ownerId],
                            [postId]: {
                                ...postData
                            }
                        }
                    }
                })
                dispatch(addPosts(parsedData))
                dispatch(requestPageProfile(uid, page))
            })
                .catch((error: SocialError) => {
                    dispatch(globalActions.showMessage(error.message))
                })

        }
    }
}

/**
 * Get all user posts from data base
 */
export const dbGetPostsByUserId = (userId: string, page: number = 0, limit: number = 10) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        const {profile} = state.get('post')
        const lastPageRequest = state.getIn(['post', 'profile', userId, 'lastPageRequest'], -1)
        const lastPostId = state.getIn(['post', 'profile', userId, 'lastPostId'], '')

        let uid: string = state.getIn(['authorize', 'uid'])

        if (uid && lastPageRequest !== page) {

            return postService.getPostsByUserId(userId, lastPostId, page, limit).then((result) => {

                if (!result.posts || !(result.posts.length > 0)) {
                    return dispatch(notMoreDataProfile(userId))
                }
                // Store last post Id
                dispatch(lastPostProfile(userId, result.newLastPostId))
                let parsedData: { [userId: string]: { [postId: string]: Post } } = {}
                result.posts.forEach((post) => {
                    const postId = Object.keys(post)[0]
                    const postData = post[postId]
                    const ownerId = postData.ownerUserId!
                    parsedData = {
                        ...parsedData,
                        [ownerId]: {
                            ...parsedData[ownerId],
                            [postId]: {
                                ...postData
                            }
                        }
                    }
                })
                dispatch(addPosts(parsedData))
                dispatch(requestPageProfile(uid, page))
            })
                .catch((error: SocialError) => {
                    dispatch(globalActions.showMessage(error.message))
                })

        }
    }
}

/**
 * Get all user posts from data base
 */
export const dbGetPostById = (uid: string, postId: string) => {
    return (dispatch: any, getState: Function) => {
        if (uid) {

            return postService.getPostById(postId).then((post: Post) => {
                dispatch(addPost(uid, post))
            })
                .catch((error: SocialError) => {
                    dispatch(globalActions.showMessage(error.message))
                })

        }
    }
}

/* _____________ CRUD State _____________ */

/**
 * Add a normal post
 */
export const addPost = (uid: string, post: Post) => {
    return {
        type: PostActionType.ADD_POST,
        payload: {uid, post}
    }
}

/**
 * Add a promotion post
 */
export const addPostPromotion = (uid: string, post: PostPromotion) => {
    return {
        type: PostActionType.ADD_POST_PROMOTION,
        payload: {uid, post}
    }
}

/**
 * Add a article post
 */
export const addPostArticle = (uid: string, post: PostArticle) => {
    return {
        type: PostActionType.ADD_POST_ARTICLE,
        payload: {uid, post}
    }
}

/**
 * Add a Award post
 */
export const addPostAward = (uid: string, post: PostAward) => {
    return {
        type: PostActionType.ADD_POST_AWARD,
        payload: {uid, post}
    }
}

/**
 * Add a Event post
 */
export const addPostEvent = (uid: string, post: PostEvent) => {
    return {
        type: PostActionType.ADD_POST_AWARD,
        payload: {uid, post}
    }
}
/**
 * Add a Project post
 */
export const addPostProject = (uid: string, post: PostProject) => {
    return {
        type: PostActionType.ADD_POST_AWARD,
        payload: {uid, post}
    }
}
/**
 * Add a conference post
 */
export const addPostConference = (uid: string, post: PostConference) => {
    return {
        type: PostActionType.ADD_POST_CONFERENCE,
        payload: {uid, post}
    }
}

/**
 * Update a post
 */
export const updatePost = (post: Map<string, any>) => {
    return {
        type: PostActionType.UPDATE_POST,
        payload: {post}
    }
}

/**
 * Update the comments of post
 */
export const updatePostComments = (comments: Map<string, any>) => {
    return {
        type: PostActionType.UPDATE_POST,
        payload: comments
    }
}

/**
 * Update the votes of post
 */
export const updatePostVotes = (votes: Map<string, any>) => {
    return {
        type: PostActionType.UPDATE_POST,
        payload: votes
    }
}

/**
 * Delete a post
 */
export const deletePost = (uid: string, id: string) => {
    return {
        type: PostActionType.DELETE_POST,
        payload: {uid, id}
    }
}

/**
 * Add a list of post
 */
export const addPosts = (userPosts: { [userId: string]: { [postId: string]: Post } }) => {
    return {
        type: PostActionType.ADD_LIST_POST,
        payload: {userPosts}
    }
}

/**
 * Clea all data in post store
 */
export const clearAllData = () => {
    return {
        type: PostActionType.CLEAR_ALL_DATA_POST
    }
}

/**
 * Add a post with image
 */
export const addImagePost = (uid: string, post: any) => {
    return {
        type: PostActionType.ADD_IMAGE_POST,
        payload: {uid, post}
    }

}

/**
 * Set stream has more data to show
 */
export const hasMoreDataStream = () => {
    return {
        type: PostActionType.HAS_MORE_DATA_STREAM
    }
}

/**
 * Set stream has not data any more to show
 */
export const notMoreDataStream = () => {
    return {
        type: PostActionType.NOT_MORE_DATA_STREAM
    }

}

/**
 * Set last page request of stream
 */
export const requestPageStream = (page: number) => {
    return {
        type: PostActionType.REQUEST_PAGE_STREAM,
        payload: {page}
    }

}

/**
 * Set last post identification of stream
 */
export const lastPostStream = (lastPostId: string) => {
    return {
        type: PostActionType.LAST_POST_STREAM,
        payload: {lastPostId}
    }

}

/**
 * Set profile posts has more data to show
 */
export const hasMoreDataProfile = (userId: string) => {
    return {
        type: PostActionType.HAS_MORE_DATA_PROFILE,
        payload: {userId}
    }

}

/**
 * Set profile posts has not data any more to show
 */
export const notMoreDataProfile = (userId: string) => {
    return {
        type: PostActionType.NOT_MORE_DATA_PROFILE,
        payload: {userId}
    }

}

/**
 * Set last page request of profile posts
 */
export const requestPageProfile = (userId: string, page: number) => {
    return {
        type: PostActionType.REQUEST_PAGE_PROFILE,
        payload: {userId, page}
    }

}

/**
 * Set last post identification of profile posts
 */
export const lastPostProfile = (userId: string, lastPostId: string) => {
    return {
        type: PostActionType.LAST_POST_PROFILE,
        payload: {userId, lastPostId}
    }

}
