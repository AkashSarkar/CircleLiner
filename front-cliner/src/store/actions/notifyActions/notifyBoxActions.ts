// - Import domain
import {Notification} from 'src/core/domain/notifications'
import {SocialError} from 'src/core/domain/common'
import {Map, fromJS} from 'immutable'

// - Import action types
import {NotifyBoxActionType} from 'constants/notifyBoxActionType'

// - Import actions
import * as globalActions from 'store/actions/globalActions'
import * as userActions from 'store/actions/userActions/userActions'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {provider} from 'src/socialEngine'
import {INotifyBoxService} from 'core/services/notifyBox'

const NotifyBoxService: INotifyBoxService = provider.get<INotifyBoxService>(SocialProviderTypes.NotifyBoxService)

/* _____________ CRUD DB _____________ */

/**
 *  Add notificaition to database
 */
export const dbAddNotification = (newNotify: Notification) => {
    return (dispatch: any, getState: Function) => {

        let notify: Notification = {
            isSeen: false,
            description: newNotify.description,
            url: newNotify.url,
            notifierUserId: newNotify.notifierUserId,
            notifyRecieverUserId: newNotify.notifyRecieverUserId
        }

        return NotifyBoxService.addNotification(notify)
            .then(() => {
                dispatch(addNotifyBox())
            })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))

    }
}

/**
 * Get all notificaitions from database
 */
export const dbGetNotifications = () => {
    return (dispatch: Function, getState: Function) => {
        const state: Map<string, any> = getState()
        dispatch(addNotifyBox())
        dispatch(addNotifyListBox())
        // let uid: string = state.getIn(['authorize', 'uid'])
        // const lastNotifyId = state.getIn(['notify', 'lastNotifyId'], '')
        // if (uid) {
        //     return NotifyBoxService.getNotifications(uid,
        //         (notifications: { [notifyId: string]: Notification }, newLastNotifyId: string) => {
        //             Object.keys(notifications).forEach((key => {
        //                 if (!state.getIn(['user', 'info', 'notifications', 'key', 'notifierUserId'])) {
        //                     dispatch(userActions.dbGetUserInfoByUserId(notifications[key].notifierUserId, ''))
        //                 }
        //             }))
        //             dispatch(lastNotifyID(newLastNotifyId))
        //             dispatch(addNotifyListBox())
        //             if (fromJS(notifications).count() > 0) {
        //                 dispatch(hasMoreNotification())
        //             } else {
        //                 dispatch(noMoreNotification())
        //             }
        //
        //         })
        // }
    }
}

export const dbGetPaginatedNotifications = (page: number, limit: number) => {
    return (dispatch: Function, getState: Function) => {
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        const lastNotifyId = state.getIn(['notify', 'lastNotifyId'], '')
        console.log('---------------1----------------')
        if (uid && lastNotifyId) {
            console.log('---------------2----------------')
            return NotifyBoxService.getPaginatedNotifications(uid, lastNotifyId, page, limit,
                (notifications: { [notifyId: string]: Notification }, newLastNotifyId: string) => {
                    Object.keys(notifications).forEach((key => {
                        if (!state.getIn(['user', 'info', 'notifications', 'key', 'notifierUserId'])) {
                            dispatch(userActions.dbGetUserInfoByUserId(notifications[key].notifierUserId, ''))
                        }
                    }))
                    console.log('---------------3**----------------')
                    console.log(notifications)
                    console.log(newLastNotifyId)
                    console.log(fromJS(notifications).count())

                    if (newLastNotifyId !== lastNotifyId && newLastNotifyId !== '' && fromJS(notifications).count() > 0) {
                        console.log('---------------3----------------')
                        dispatch(lastNotifyID(newLastNotifyId))
                        dispatch(addNotifyListBox())
                        dispatch(hasMoreNotification())
                    } else {
                        console.log('---------------4----------------')
                        dispatch(noMoreNotification())
                    }
                })
        }
    }
}

/**
 * Delete a notificaition from database
 * @param  {string} id of notificaition
 */
export const dbDeleteNotification = (id: string) => {
    return (dispatch: any, getState: Function) => {

        // Get current user id
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])

        return NotifyBoxService.deleteNotification(id, uid).then(() => {
            dispatch(deleteNotifyBox(id))
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }

}

/**
 * Make seen a notificaition from database
 * @param  {string} id of notificaition
 */
export const dbSeenNotification = (id: string) => {
    return (dispatch: any, getState: Function) => {

        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let notify: Map<string, any> = state.getIn(['notify', 'userNotifies', id])

        let updatedNotification: Notification = {
            description: notify.get('description'),
            url: notify.get('url'),
            notifierUserId: notify.get('notifierUserId'),
            notifyRecieverUserId: uid,
            isSeen: true
        }

        return NotifyBoxService.setSeenNotification(id, uid, updatedNotification)
            .then(() => {
                dispatch(seenNotifyBox(id))
            })
            .catch((error) => dispatch(globalActions.showMessage(error.message)))
    }

}

/* _____________ CRUD State _____________ */

/**
 * Add notificaition
 */
export const addNotifyBox = () => {

    return {
        type: NotifyBoxActionType.ADD_NOTIFY_BOX
    }
}

/**
 * Add notificaition list
 * @param {[notifyId: string]: Notification} userNotifies an array of notificaitions
 */
export const addNotifyListBox = () => {

    return {
        type: NotifyBoxActionType.ADD_NOTIFY_LIST_BOX,
        payload: ''
    }
}

/**
 * Delete a notificaition
 * @param  {string} id of notificaition
 */
export const deleteNotifyBox = (id: string) => {
    return {type: NotifyBoxActionType.DELETE_NOTIFY_BOX, payload: id}

}

/**
 * Change notificaition to has seen status
 * @param  {string} id of notificaition
 */
export const seenNotifyBox = (id: string) => {
    return {type: NotifyBoxActionType.SEEN_NOTIFY_BOX, payload: id}

}

/**
 * Clear all data
 */
export const clearAllNotificationsBox = () => {
    return {
        type: NotifyBoxActionType.CLEAR_ALL_DATA_NOTIFY_BOX
    }
}

export const hasMoreNotification = () => {
    return {
        type: NotifyBoxActionType.HAS_MORE_DATA
    }
}

export const noMoreNotification = () => {
    return {
        type: NotifyBoxActionType.NO_MORE_DATA
    }
}

export const requestPagePeople = (page: number) => {
    return {
        type: NotifyBoxActionType.REQUEST_PAGE,
        payload: {page}
    }
}

export const lastNotifyID = (lastUserId: string) => {
    return {
        type: NotifyBoxActionType.LAST_NOTIFY_ID,
        payload: {lastUserId}
    }
}
