// - Import react components
import {provider} from 'src/socialEngine'
import {Map} from 'immutable'

// - Import domain
import {SocialError} from 'src/core/domain/common'

// - Import action types
import {UserActionType} from 'constants/userActionType'

// - Import actions
import * as globalActions from 'store/actions/globalActions'

import {IUserService} from 'src/core/services/users'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {Education} from 'core/domain/users/education'

/**
 * Get service providers
 */
const userService: IUserService = provider.get<IUserService>(SocialProviderTypes.UserService)

/* _____________ CRUD DB _____________ */

/**
 * Add educational info
 */
export const dbAddEducationalInfo = (education: Education, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, Education> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let parsedData: Map<string, Education> = Map({})
        let userEducationInfo: Education = {
            educationLevel: education.educationLevel,
            degreeTitle: education.degreeTitle,
            group: education.group,
            instituteName: education.instituteName,
            result: education.result,
            passingYear: education.passingYear,
            achievement: education.achievement,
            duration: education.duration,
            userId: uid,
        }
        return userService.addEducationalInfo(uid,userEducationInfo).then((id: string) => {
            dispatch(addEducationalInfo(uid,{
                ...userEducationInfo,
                id: id
            }))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())

        })
        .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbUpdateEducation = (education: Education) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let getEducation = state.getIn(['user','education',uid,education.id]).toJS()
        console.log(education.id)
        let userEducationInfo: Education = {
            id: education.id,
            educationLevel: education.educationLevel || getEducation.educationLevel || '',
            degreeTitle: education.degreeTitle || getEducation.degreeTitle || '',
            group: education.group || getEducation.group || '',
            instituteName: education.instituteName || getEducation.instituteName || '',
            result: education.result || getEducation.result || '',
            passingYear: education.passingYear || getEducation.passingYear || '',
            achievement: education.achievement || getEducation.achievement || '',
            duration: education.duration || getEducation.duration || '',
            userId: uid,
        }
        return userService.updateEducationalInfo(userEducationInfo).then(() => {
            dispatch(updateEducationalInfo(uid,userEducationInfo))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbDeleteEducationInfo = (educationId: string) => {
    return (dispatch: any, getState: Function) => {

        if (educationId === undefined || educationId === null) {
            dispatch(globalActions.showMessage('education id can not be null or undefined'))
        }
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        return userService.deleteEducation(educationId)
            .then(() => {
                dispatch(deleteEducation(uid,educationId))
                dispatch(globalActions.hideTopLoading())

            }, (error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())

            })
    }
}

export const dbGetUsersEducationInfoById = (userId: string) => {
    return (dispatch: any, getState: Function) => {
        if (userId) {
            return userService.getUserEducation(userId).then((userEducation) => {
                const state: Map<string, any> = getState()
                let parsedData: { [educationId: string]: Education } = {}
                userEducation.forEach((education) => {
                    const educationId = Object.keys(education)[0]
                    const educationData = education[educationId]
                    parsedData = {
                        ...parsedData,
                        [educationId]: {
                            ...educationData
                        }
                    }
                })
                dispatch(getUsersEducationInfo(userId,parsedData))
            }).catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
        }
    }
}

/* _____________ CRUD State _____________ */

export const addEducationalInfo = (uid: string,education: Education) => {
    return {
        type: UserActionType.ADD_USER_EDUCATION,
        payload: {uid,education}
    }
}

export const updateEducationalInfo = (uid: string,education: Education) => {
    return {
        type: UserActionType.UPDATE_USER_EDUCATION,
        payload: {uid,education}
    }
}

/**
 * Delete a comment
 */
export const deleteEducation = (uid: string, id: string) => {
    return {
        type: UserActionType.DELETE_EDUCATION,
        payload: {uid,id}
    }
}

export const getUsersEducationInfo = (uid: string, educations: { [educationId: string]: Education }) => {
    return {
        type: UserActionType.GET_USER_EDUCATION_INFO_BY_ID,
        payload: {uid,educations}
    }
}