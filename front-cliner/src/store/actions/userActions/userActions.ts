// - Import react components
import {provider} from 'src/socialEngine'
import {Map} from 'immutable'
// - Import domain
import {Profile} from 'src/core/domain/users'
import {PersonalInfo} from 'core/domain/users/personalInfo'
import {Industry} from 'src/core/domain/users/industry'
import {SocialError} from 'src/core/domain/common'
// - Import action types
import {UserActionType} from 'constants/userActionType'

// - Import actions
import * as globalActions from 'store/actions/globalActions'
import {IUserService} from 'src/core/services/users'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {push} from 'react-router-redux'
import {AccountSetting} from 'core/domain/users/accountSettings'
import {BlockUser} from 'core/domain/users/blockUser'
import {ReportUser} from 'core/domain/users/reportUser'
import {Education} from 'core/domain/users/education'
import {getUsersEducationInfo} from 'store/actions/userActions/educationActions'

/**
 * Get service providers
 */
const userService: IUserService = provider.get<IUserService>(SocialProviderTypes.UserService)

/* _____________ CRUD DB _____________ */

/**
 * Get user info from database
 */
export const dbGetUserInfo = () => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        if (uid) {
            return userService.getUserProfile(uid).then((userProfile: Profile) => {
                dispatch(addUserInfo(uid, {
                    avatar: userProfile.avatar,
                    email: userProfile.email,
                    fullName: userProfile.fullName,
                    firstName: userProfile.firstName,
                    lastName: userProfile.lastName,
                    banner: userProfile.banner,
                    tagLine: userProfile.tagLine,
                    creationDate: userProfile.creationDate,
                    birthday: userProfile.birthday,
                    companyName: userProfile.companyName,
                    industry: userProfile.industry,
                    concentratedIndustry: userProfile.concentratedIndustry,
                    webUrl: userProfile.webUrl,
                    twitterId: userProfile.twitterId,
                }))
            }).catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
        }
    }
}

/**
 * report people
 */
export const dbReportProfile = (reportUser: ReportUser, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let ReportUser: ReportUser = {
            reportedBy: uid,
            reportedUserId: reportUser.reportedUserId,
            reportText: reportUser.reportText
        }

        return userService.reportProfile(ReportUser).then((Key: string) => {
            dispatch(globalActions.showNotificationSuccess())
            dispatch(reportProfile({
                ...ReportUser
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * block people
 */
export const dbBlockPeople = (blockUser: BlockUser, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let block: BlockUser = {
            blockedUserId: blockUser.blockedUserId,
            blockedBy: uid
        }
        return userService.blockPeople(block).then((Key: string) => {
            dispatch(globalActions.showNotificationSuccess())
            dispatch(blockPeople({
                ...block
            }))
            callBack()
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add users industry
 */
export const dbAddUserIndustry = (userIndustry: Industry) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let addIndustry: Industry = {
            industryName: userIndustry.industryName,
            concentratedIndustryName: userIndustry.concentratedIndustryName,
            userId: uid
        }

        return userService.addUserIndustry(uid, addIndustry).then((id: string) => {
            dispatch(addUserIndustry(uid, {
                id: id,
                ...addIndustry,
            }))
            dispatch(globalActions.hideTopLoading())
            dispatch(push('/registration/education'))
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}
/**
 * Add users industry
 */
export const dbAddUserEducation = (userEducation: Education) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        console.log(userEducation)
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let addEducation: Education = {
            fieldOfStudy: userEducation.fieldOfStudy,
            instituteName: userEducation.instituteName,
            degreeTitle: userEducation.degreeTitle,
            result: userEducation.result,
            passingYear: userEducation.passingYear,
            currentlyStudying: userEducation.currentlyStudying,
            userId: uid
        }

        return userService.addEducationalInfo(uid, addEducation).then((id: string) => {
            dispatch(addUserEducation(uid, {
                id: id,
                ...addEducation,
            }))
            dispatch(globalActions.hideTopLoading())
            dispatch(push('/registration/location'))
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add personalinfo
 */
export const dbAddPersonalInfo = (info: PersonalInfo, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let userInfo: PersonalInfo = {
            firstName: info.firstName,
            lastName: info.lastName,
            fullName: info.fullName,
            email: info.email,
            dob: info.dob,
            website: info.website,
            phone: info.phone,
            birthPlace: info.birthPlace,
            gender: info.gender,
            status: info.status,
            ownerUserId: uid,
        }
        return userService.addPersonalInfo(uid, userInfo).then((Key: string) => {
            dispatch(addPersonalInfo(userInfo))
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 * Add users industry
 */
export const dbUpdateUserIndustry = (userIndustry: Industry) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let getIndustry = state.getIn(['user', 'industry', uid, userIndustry.id])
        let getUserIndustry = Map(getIndustry).toJS()
        let updateIndustry: Industry = {
            id: userIndustry.id,
            industryName: userIndustry.industryName || getUserIndustry.industryName || '',
            concentratedIndustryName: userIndustry.concentratedIndustryName || getUserIndustry.concentratedIndustryName || '',
            userId: uid,
        }
        return userService.updateUserIndustry(updateIndustry).then(() => {
            dispatch(updateUserIndustry(uid, updateIndustry))
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}
/**
 * Add account settings
 */
export const dbAddAccountSettings = (info: AccountSetting, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let accountSetting: AccountSetting = {
            followPrivacy: info.followPrivacy,
            postPrivacy: info.postPrivacy,
            notificationSound: info.notificationSound,
            emailNotificationSound: info.emailNotificationSound,
            followerBirthdayNotificationSound: info.followerBirthdayNotificationSound,
            chatSound: info.chatSound,
            accountDeactivate: info.accountDeactivate,
            ownerUserId: uid,
        }
        return userService.addAccountsettings(uid, accountSetting).then((Key: string) => {
            dispatch(addAccountsettings(accountSetting))
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

/**
 *  Get user info from database
 */
export const dbGetUserIndustryByUserId = (uid: string) => {
    return (dispatch: Function, getState: Function) => {
        if (uid) {
            return userService.getUserIndustry(uid).then((userIndustry) => {
                let parsedData: { [industryId: string]: Industry } = {}
                userIndustry.forEach((industry) => {
                    const industryId = Object.keys(industry)[0]
                    const industryData = industry[industryId]
                    parsedData = {
                        ...parsedData,
                        [industryId]: {
                            ...industryData
                        }
                    }
                })
                // dispatch(getUserIndutry(
                //     userIndustry.industryName
                //     )
                // )
                dispatch(getUserIndutry(uid, parsedData))
            })
                .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
        }
    }
}

/**
 *  Get user info from database
 */
export const dbGetUserInfoByUserId = (uid: string, callerKey: string) => {
    return (dispatch: Function, getState: Function) => {
        if (uid) {

            const state: Map<string, any> = getState()
            let caller = state.getIn(['global', 'temp', 'caller'])
            if (caller && caller.indexOf(`dbGetUserInfoByUserId-${uid}`) > -1) {
                return undefined
            }
            dispatch(globalActions.temp({caller: `dbGetUserInfoByUserId-${uid}`}))
            return userService.getUserProfile(uid).then((userProfile: Profile) => {

                dispatch(addUserInfo(uid, {
                    avatar: userProfile.avatar,
                    email: userProfile.email,
                    fullName: userProfile.fullName,
                    firstName: userProfile.firstName,
                    lastName: userProfile.lastName,
                    banner: userProfile.banner,
                    tagLine: userProfile.tagLine,
                    creationDate: userProfile.creationDate,
                    birthday: userProfile.birthday,
                    companyName: userProfile.companyName,
                    industry: userProfile.industry,
                    concentratedIndustry: userProfile.concentratedIndustry,
                    webUrl: userProfile.webUrl,
                    twitterId: userProfile.twitterId,
                }))

                switch (callerKey) {
                    case 'header':
                        dispatch(globalActions.setHeaderTitle(userProfile.fullName))

                        break

                    default:
                        break
                }
            })
                .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))

        }
    }
}
/**
 * Updata user information
 * @param {object} newInfo
 */
export const dbUpdateUserInfo = (newProfile: Profile) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])

        let profile: Profile = state.getIn(['user', 'info', uid])
        let updatedProfile: Profile = {
            avatar: newProfile.avatar || profile.avatar || '',
            banner: newProfile.banner || profile.banner || 'https://firebasestorage.googleapis.com/v0/b/open-social-33d92.appspot.com/o/images%2F751145a1-9488-46fd-a97e-04018665a6d3.JPG?alt=media&token=1a1d5e21-5101-450e-9054-ea4a20e06c57',
            email: newProfile.email || profile.email || '',
            fullName: newProfile.fullName || profile.fullName || '',
            firstName: newProfile.firstName || profile.firstName || '',
            lastName: newProfile.lastName || profile.lastName || '',
            tagLine: newProfile.tagLine || profile.tagLine || '',
            industry: newProfile.industry || profile.industry || '',
            concentratedIndustry: newProfile.concentratedIndustry || profile.concentratedIndustry || '',
            birthday: newProfile.birthday,
            companyName: newProfile.companyName || '',
            webUrl: newProfile.webUrl || '',
            twitterId: newProfile.twitterId || '',
            creationDate: newProfile.creationDate
        }
        return userService.updateUserProfile(uid, updatedProfile).then(() => {

            dispatch(updateUserInfo(uid, updatedProfile))
            dispatch(closeEditProfile())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))

    }

}
/**
 * Update user avatar
 * @param avatar
 */
export const dbUpdateUserAvatar = (avatar: string) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        console.log('updating avatar')
        let profile: Profile = state.getIn(['user', 'info', uid])
        let updatedPP: string = avatar || profile.avatar || ''
        return userService.updateUserPP(uid, updatedPP).then(() => {
            profile.avatar = updatedPP
            dispatch(updateUserPP(uid, profile))
            dispatch(closeChangeAvatar())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))

    }

}
/**
 * Update user cover photo
 * @param avatar
 */
export const dbUpdateUserCover = (coverPhoto: string) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        console.log('updating cover')
        let profile: Profile = state.getIn(['user', 'info', uid])
        let updatedCP: string = coverPhoto || profile.avatar || ''
        return userService.updateUserCP(uid, updatedCP).then(() => {
            profile.banner = updatedCP
            dispatch(updateUserPP(uid, profile))
            dispatch(closeChangeAvatar())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))

    }

}

// - Get people info from database
export const dbGetPeopleInfo = (page: number, limit: number) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        const people: Map<string, any> = state.getIn(['user', 'people'])
        const lastPageRequest = people.get('lastPageRequest')
        const lastUserId = people.get('lastUserId')

        let uid: string = state.getIn(['authorize', 'uid'])

        if (uid && lastPageRequest !== page) {

            return userService.getUsersProfile(uid, lastUserId, page, limit).then((result) => {
                if (!result.users || !(result.users.length > 0)) {
                    return dispatch(notMoreDataPeople())
                }
                // Store last user Id
                dispatch(lastUserPeople(result.newLastUserId))

                let parsedData: Map<string, Profile> = Map({})
                result.users.forEach((user) => {
                    const userId = Object.keys(user)[0]
                    const userData = user[userId]
                    parsedData = parsedData.set(userId, userData)
                })
                dispatch(addPeopleInfo(parsedData))
            })
                .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))

        }
    }
}

/* _____________ CRUD State _____________ */

/**
 * Add user information
 */
export const addUserInfo = (uid: string, info: Profile) => {
    return {
        type: UserActionType.ADD_USER_INFO,
        payload: {uid, info}
    }
}

/**
 * Add user information
 */
export const addUserIndustry = (uid: string, industry: Industry) => {
    return {
        type: UserActionType.ADD_USER_INDUSTRY,
        payload: {uid, industry}
    }
}
export const addUserEducation = (uid: string, education: Education) => {
    return {
        type: UserActionType.ADD_USER_EDUCATION,
        payload: {uid, education}
    }
}

/**
 * Report Profile
 */
export const reportProfile = (info: ReportUser) => {
    return {
        type: UserActionType.ADD_PROFILE_REPORT,
        payload: {info}
    }
}
/**
 * Block people
 */
export const blockPeople = (info: BlockUser) => {
    return {
        type: UserActionType.ADD_USER_BLOCK,
        payload: {info}
    }
}

/**
 * Add people information
 */
export const addPeopleInfo = (infoList: Map<string, Profile>) => {
    return {
        type: UserActionType.ADD_PEOPLE_INFO,
        payload: infoList
    }
}
/**
 * Add people information
 */
export const addPersonalInfo = (info: PersonalInfo) => {
    return {
        type: UserActionType.ADD_USER_PERSONAL_INFO,
        payload: {info}
    }
}

/**
 * Update users industry information
 */
export const updateUserIndustry = (uid: string, industry: Industry) => {
    return {
        type: UserActionType.UPDATE_USER_INDUSTRY,
        payload: {uid, industry}
    }
}

export const addAccountsettings = (info: AccountSetting) => {
    return {
        type: UserActionType.ADD_USER_ACCOUNT_SETTING,
        payload: {info}
    }
}

/**
 * Update user information
 */
export const updateUserInfo = (uid: string, info: Profile) => {
    return {
        type: UserActionType.UPDATE_USER_INFO,
        payload: {uid, info}
    }
}

/**
 * Update user information
 */
export const updateUserPP = (uid: string, info: Profile) => {
    return {
        type: UserActionType.UPDATE_USER_AVATAR,
        payload: {uid, info}
    }
}

export const clearAllData = () => {
    return {
        type: UserActionType.CLEAR_ALL_DATA_USER
    }
}

/**
 * Open edit profile
 */
export const openEditProfile = () => {
    return {
        type: UserActionType.OPEN_EDIT_PROFILE
    }
}
/**
 * Open change avatar
 */
export const openChangeAvatar = () => {
    return {
        type: UserActionType.OPEN_CHANGE_AVATAR
    }
}

/**
 * Open edit profile
 */
export const openReportProfile = () => {
    return {
        type: UserActionType.OPEN_REPORT_PROFILE
    }
}

/**
 * Close edit profile
 */
export const closeEditProfile = () => {
    return {
        type: UserActionType.CLOSE_EDIT_PROFILE
    }
}
/**
 * Close change avatar
 */
export const closeChangeAvatar = () => {
    return {
        type: UserActionType.CLOSE_CHANGE_AVATAR
    }
}

/**
 * Set profile posts has more data to show
 */
export const hasMoreDataPeople = () => {
    return {
        type: UserActionType.HAS_MORE_DATA_PEOPLE
    }
}

/**
 * Set profile posts has not data any more to show
 */
export const notMoreDataPeople = () => {
    return {
        type: UserActionType.NOT_MORE_DATA_PEOPLE
    }
}

/**
 * Set last page request of profile posts
 */
export const requestPagePeople = (page: number) => {
    return {
        type: UserActionType.REQUEST_PAGE_PEOPLE,
        payload: {page}
    }
}

/**
 * Set last user identification of find people page
 */
export const lastUserPeople = (lastUserId: string) => {
    return {
        type: UserActionType.LAST_USER_PEOPLE,
        payload: {lastUserId}
    }
}
/**
 * Set last user identification of find people page
 */
export const getUserIndutry = (uid: string, industries: { [industryId: string]: Industry }) => {
    return {
        type: UserActionType.GET_USER_INDUSTRY_BY_ID,
        payload: {uid, industries}
    }
}