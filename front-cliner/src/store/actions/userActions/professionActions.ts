// - Import react components
import {provider} from 'src/socialEngine'
import {Map} from 'immutable'

// - Import domain
import {SocialError} from 'src/core/domain/common'

// - Import action types
import {UserActionType} from 'constants/userActionType'

// - Import actions
import * as globalActions from 'store/actions/globalActions'

import {IUserService} from 'src/core/services/users'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {Profession} from 'core/domain/users/profession'

/**
 * Get service providers
 */
const userService: IUserService = provider.get<IUserService>(SocialProviderTypes.UserService)

/* _____________ CRUD DB _____________ */

/**
 * Add user profession
 */
export let dbAddProfession = (newProfession: Profession) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, Profession> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let postProfession: Profession = {
            userId: uid,
            companyName: newProfession.companyName,
            companyBusiness: newProfession.companyBusiness,
            designation: newProfession.designation,
            department: newProfession.department,
            experience: newProfession.experience,
            responsibilities: newProfession.responsibilities,
            companyLocation: newProfession.companyLocation,
            workFrom: newProfession.workFrom,
            workTo: newProfession.workTo,
        }

        return userService.addProfession(uid,postProfession).then((id: string) => {
            dispatch(globalActions.showNotificationSuccess())
            dispatch(addProfession(uid,{
                ...postProfession,
                id: id
            }))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbGetUserProfessionByUserId = (userId: string) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any>  = getState()
        if (userId) {
            return userService.getUserProfession(userId).then((userProfession) => {
                let parsedData: { [professionId: string]: Profession } = {}
                userProfession.forEach((profession) => {
                    const professionId = Object.keys(profession)[0]
                    const professionData = profession[professionId]
                    parsedData = {
                        ...parsedData,
                        [professionId]: {
                            ...professionData
                        }
                    }
                })
                dispatch(getUserProfession(userId,parsedData))
            })
                .catch((error: SocialError) => {
                    dispatch(globalActions.showMessage(error.message))
                })
        }
    }
}

export const dbUpdateProfession = (profession: Profession) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let getProfession = state.getIn(['user','profession',uid,profession.id]).toJS()
        let userProfessionInfo: Profession = {
            id: profession.id,
            companyName: profession.companyName || getProfession.companyName || '',
            companyBusiness: profession.companyBusiness || getProfession.companyBusiness || '',
            designation: profession.designation || getProfession.designation || '',
            department: profession.department || getProfession.department || '',
            experience: profession.experience || getProfession.experience || '',
            responsibilities: profession.responsibilities || getProfession.responsibilities || '',
            companyLocation: profession.companyLocation || getProfession.companyLocation || '',
            workFrom: profession.workFrom || getProfession.workFrom || '',
            workTo: profession.workTo || getProfession.workTo || '',
            userId: uid,
        }
        return userService.updateProfessionalInfo(userProfessionInfo).then(() => {
            dispatch(updateProfessionalInfo(uid,userProfessionInfo))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbDeleteProfessionInfo = (professionId: string) => {
    return (dispatch: any, getState: Function) => {

        if (professionId === undefined || professionId === null) {
            dispatch(globalActions.showMessage('profession id can not be null or undefined'))
        }
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        return userService.deleteProfession(professionId)
            .then(() => {
                dispatch(deleteProfession(uid,professionId))
                dispatch(globalActions.hideTopLoading())

            }, (error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())

            })
    }
}

/* _____________ CRUD State _____________ */

/**
 * Add user Profession
 */
export const addProfession = (uid: string,profession: Profession) => {
    return {
        type: UserActionType.ADD_USER_PROFESSION,
        payload: {uid, profession}
    }
}

export const updateProfessionalInfo = (uid: string,profession: Profession) => {
    return {
        type: UserActionType.UPDATE_USER_PROFESSION,
        payload: {uid, profession}
    }
}

/**
 * Set last user identification of find people page
 */
export const getUserProfession = (uid: string, professions: { [professionId: string]: Profession }) => {
    return {
        type: UserActionType.GET_USER_PROFESSION_BY_ID,
        payload: {uid, professions}
    }
}

/**
 * Delete a comment
 */
export const deleteProfession = (uid: string, id: string) => {
    return {
        type: UserActionType.DELETE_PROFESSION,
        payload: {uid,id}
    }
}
