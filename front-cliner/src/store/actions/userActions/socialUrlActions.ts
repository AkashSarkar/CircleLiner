// - Import react components
import {provider} from 'src/socialEngine'
import {Map} from 'immutable'

// - Import domain
import {SocialError} from 'src/core/domain/common'

// - Import action types
import {UserActionType} from 'constants/userActionType'

// - Import actions
import * as globalActions from 'store/actions/globalActions'

import {IUserService} from 'src/core/services/users'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {SocialUrl} from 'core/domain/users/socialUrl'

/**
 * Get service providers
 */
const userService: IUserService = provider.get<IUserService>(SocialProviderTypes.UserService)

/* _____________ CRUD DB _____________ */

/**
 * Add educational info
 */
export const dbAddSocialUrl = (socialUrl: SocialUrl, callBack: Function) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, SocialUrl> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let parsedData: Map<string, SocialUrl> = Map({})
        let userSocialUrl: SocialUrl = {
            socialType: socialUrl.socialType,
            socialUrl: socialUrl.socialUrl,
            userId: uid,
        }
        return userService.addSocailUrl(uid,userSocialUrl).then((id: string) => {
            dispatch(addSocialUrl(uid,{
                ...userSocialUrl,
                id: id
            }))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbUpdateSocialUrl = (socialUrl: SocialUrl) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        // let getSocailUrl = state.getIn(['user','socialUrl',uid,socialUrl.id]).toJS()
        let getSocail = state.getIn(['user', 'socialUrl', uid, socialUrl.id])
        let getSocailUrl = Map(getSocail).toJS()
        console.log(socialUrl.id,socialUrl.socialUrl)
        let userSocialUrlInfo: SocialUrl = {
            id: socialUrl.id,
            socialUrl: socialUrl.socialUrl || getSocailUrl.socialUrl || '',
            socialType: socialUrl.socialType || getSocailUrl.socialType || '',
            userId: uid,
        }
        return userService.updateSocialUrl(userSocialUrlInfo).then(() => {
            dispatch(updateSocialUrl(uid,userSocialUrlInfo))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbDeleteSocialUrl = (socialUrlId: string) => {
    return (dispatch: any, getState: Function) => {

        if (socialUrlId === undefined || socialUrlId === null) {
            dispatch(globalActions.showMessage('social url id can not be null or undefined'))
        }
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        return userService.deleteSocialUrl(socialUrlId)
            .then(() => {
                dispatch(deleteSocialUrl(uid,socialUrlId))
                dispatch(globalActions.hideTopLoading())

            }, (error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())

            })
    }
}

export const dbGetUserSocialUrlByUserId = (userId: string) => {
    return (dispatch: any, getState: Function) => {
        if (userId) {
            const state: Map<string, SocialUrl> = getState()
            let uid: string = state.getIn(['authorize', 'uid'])
            return userService.getUserSocialUrls(uid).then((userSocialUrl) => {
                const state: Map<string, any> = getState()
                let parsedData: { [socialUrlId: string]: SocialUrl } = {}
                userSocialUrl.forEach((socialUrl) => {
                    const socialUrlId = Object.keys(socialUrl)[0]
                    const socialUrlData = socialUrl[socialUrlId]
                    parsedData = {
                        ...parsedData,
                        [socialUrlId]: {
                            ...socialUrlData
                        }
                    }
                })
                dispatch(getUserSocialUrls(uid,parsedData))
            }).catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
        }
    }
}

/* _____________ CRUD State _____________ */

export const addSocialUrl = (uid: string,social: SocialUrl) => {
    return {
        type: UserActionType.ADD_USER_SOCIAL_URL,
        payload: {uid,social}
    }
}

export const updateSocialUrl = (uid: string,socialUrl: SocialUrl) => {
    return {
        type: UserActionType.UPDATE_USER_SOCIAL_URL,
        payload: {uid,socialUrl}
    }
}

/**
 * Delete a social url
 */
export const deleteSocialUrl = (uid: string, id: string) => {
    return {
        type: UserActionType.DELETE_SOCIAL_URL,
        payload: {uid,id}
    }
}

export const getUserSocialUrls = (uid: string, socialUrls: { [socialUrlId: string]: SocialUrl }) => {
    return {
        type: UserActionType.GET_USER_SOCIAL_URL_BY_ID,
        payload: {uid,socialUrls}
    }
}