// - Import domain
import {Post} from 'src/core/domain/posts'
import {PostPromotion} from 'src/core/domain/posts/postPromotion'
import {PostArticle} from 'src/core/domain/posts/postArticle'
import {PostAward} from 'src/core/domain/posts/postAward'
import {PostEvent} from 'src/core/domain/posts/postEvent'
import {PostProject} from 'src/core/domain/posts/postProject'
import {PostConference} from 'src/core/domain/posts/postConference'
import {SocialError} from 'src/core/domain/common'
import {Map} from 'immutable'

// - Import utility components
import moment from 'moment/moment'

// - Import action types
import {PlatformActionType} from 'constants/platformActionType'
import {
    requestPageProfile,
    lastPostProfile,
    notMoreDataProfile,
    addPosts,
    updatePost,
    deletePost,
    notMoreDataStream,
    lastPostStream
} from 'store/actions/postActions'

// - Import actions
import * as globalActions from 'store/actions/globalActions'
import {IPostService} from 'src/core/services/posts'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {provider} from 'src/socialEngine'

/**
 * Get service providers
 */
const postService: IPostService = provider.get<IPostService>(SocialProviderTypes.PostService)

/* _____________ CRUD DB _____________ */

/**
 * Update a post from database
 */
export const dbUpdatePltPost = (pltid: string, updatedPost: Map<string, any>, callBack: Function) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())

        return postService.updatePost(updatedPost.toJS()).then(() => {

            dispatch(updatePost(updatedPost))
            dispatch(updatePltPost(pltid, updatedPost))
            callBack()
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())
            })
    }

}

/**
 * Delete a post from database
 * @param  {string} id is post identifier
 */
export const dbDeletePltPost = (pltid: string, id: string) => {
    return (dispatch: any, getState: Function) => {

        dispatch(globalActions.showTopLoading())

        const state: Map<string, any> = getState()
        // Get current user id
        let uid: string = state.getIn(['authorize', 'uid'])

        return postService.deletePost(id).then(() => {
            dispatch(deletePost(uid, id))
            dispatch(deletePltPost(pltid, uid, id))
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())
            })
    }

}

/**
 * Get all user posts from data base
 */
export const dbGetPltPosts = (pltid: string, page: number = 0, limit: number = 10) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        const stream: Map<string, any> = state.getIn(['post', 'stream'])
        const lastPageRequest = stream.get('lastPageRequest')
        const lastPostId = stream.get('lastPostId')

        let uid: string = state.getIn(['authorize', 'uid'])
        if (uid && lastPageRequest !== page) {
            return postService.getPosts(uid, lastPostId, page, limit).then((result) => {
                if (!result.posts || !(result.posts.length > 0) || (result.posts.length < limit)) {
                    dispatch(notMoreDataStream())
                    return dispatch(notMoreDataPltStream(pltid))
                }

                // Store last post Id
                dispatch(lastPostStream(result.newLastPostId))
                dispatch(lastPltPostStream(pltid, result.newLastPostId))
                let parsedData: { [userId: string]: { [postId: string]: Post } } = {}
                let parsedPostData: { [postId: string]: Post } = {}
                result.posts.forEach((post) => {
                    const postId = Object.keys(post)[0]
                    const postData = post[postId]
                    const ownerId = postData.ownerUserId!
                    parsedData = {
                        ...parsedData,
                        [ownerId]: {
                            ...parsedData[ownerId],
                            [postId]: {
                                ...postData
                            }
                        }
                    }
                    parsedPostData = {
                        ...parsedPostData,
                        [postId]: {
                            ...postData
                        }
                    }
                })
                dispatch(addPosts(parsedData))
                console.log(pltid)
                dispatch(addPltPosts(pltid, parsedPostData))
                dispatch(requestPageProfile(uid, page))
            })
                .catch((error: SocialError) => {
                    dispatch(globalActions.showMessage(error.message))
                })

        }
    }
}

/**
 * Get all user posts from data base
 */
export const dbGetPltPostsByUserId = (pltid: string, userId: string, page: number = 0, limit: number = 10) => {
    return (dispatch: any, getState: Function) => {
        const state: Map<string, any> = getState()
        const {profile} = state.get('post')
        const lastPageRequest = state.getIn(['post', 'profile', userId, 'lastPageRequest'], -1)
        const lastPostId = state.getIn(['post', 'profile', userId, 'lastPostId'], '')

        let uid: string = state.getIn(['authorize', 'uid'])

        if (uid && lastPageRequest !== page) {

            return postService.getPostsByUserId(userId, lastPostId, page, limit).then((result) => {

                if (!result.posts || !(result.posts.length > 0)) {
                    return dispatch(notMoreDataProfile(userId))
                }
                // Store last post Id
                dispatch(lastPostProfile(userId, result.newLastPostId))
                let parsedData: { [userId: string]: { [postId: string]: Post } } = {}
                result.posts.forEach((post) => {
                    const postId = Object.keys(post)[0]
                    const postData = post[postId]
                    const ownerId = postData.ownerUserId!
                    parsedData = {
                        ...parsedData,
                        [ownerId]: {
                            ...parsedData[ownerId],
                            [postId]: {
                                ...postData
                            }
                        }
                    }
                })
                dispatch(addPosts(parsedData))
                dispatch(addPltPosts(pltid, parsedData))
                dispatch(requestPageProfile(uid, page))
            })
                .catch((error: SocialError) => {
                    dispatch(globalActions.showMessage(error.message))
                })

        }
    }
}

/* _____________ CRUD State _____________ */

/**
 * Update a post
 */
export const updatePltPost = (pltid: string, post: Map<string, any>) => {
    return {
        type: PlatformActionType.UPDATE_POST_PLT,
        payload: {pltid, post}
    }
}

/**
 * Update the comments of post
 */
export const updatePltPostComments = (pltid: string, comments: Map<string, any>) => {
    return {
        type: PlatformActionType.UPDATE_POST_PLT,
        payload: {pltid, comments}
    }
}

/**
 * Update the votes of post
 */
export const updatePltPostVotes = (pltid: string, votes: Map<string, any>) => {
    return {
        type: PlatformActionType.UPDATE_POST_PLT,
        payload: {pltid, votes}
    }
}

/**
 * Delete a post
 */
export const deletePltPost = (pltid: string, uid: string, id: string) => {
    return {
        type: PlatformActionType.DELETE_POST_PLT,
        payload: {pltid, uid, id}
    }
}

/**
 * Add a list of post
 */
export const addPltPosts = (pltid: string, userPosts: { [postId: string]: Post }) => {
    return {
        type: PlatformActionType.ADD_LIST_POST_PLT,
        payload: {pltid, userPosts}
    }
}

/**
 * Clea all data in post store
 */
export const clearAllPltData = () => {
    return {
        type: PlatformActionType.CLEAR_ALL_DATA_POST_PLT
    }
}

/**
 * Set stream has more data to show
 */
export const hasMoreDataPltStream = (pltid: string) => {
    return {
        type: PlatformActionType.HAS_MORE_DATA_STREAM_PLT,
        payload: {pltid}
    }
}

/**
 * Set stream has not data any more to show
 */
export const notMoreDataPltStream = (pltid: string) => {
    return {
        type: PlatformActionType.NOT_MORE_DATA_STREAM_PLT,
        payload: {pltid}
    }

}

/**
 * Set last page request of stream
 */
export const requestPltPageStream = (pltid: string, page: number) => {
    return {
        type: PlatformActionType.REQUEST_PAGE_STREAM_PLT,
        payload: {pltid, page}
    }

}

/**
 * Set last post identification of stream
 */
export const lastPltPostStream = (pltid: string, lastPostId: string) => {
    return {
        type: PlatformActionType.LAST_POST_STREAM_PLT,
        payload: {pltid, lastPostId}
    }

}
