// - Import domain
import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'
import {SocialError} from 'src/core/domain/common'
import {Map} from 'immutable'

// - Import utility components
import moment from 'moment/moment'

// - Import action types
import {JobActionType} from 'constants/jobActionType'
import {JobPreferences} from 'core/domain/jobs/jobPreferences'

// - Import actions
import * as globalActions from 'store/actions/globalActions'
import {IJobpostService} from 'src/core/services/jobpost'
import {SocialProviderTypes} from 'src/core/socialProviderTypes'
import {provider} from 'src/socialEngine'
import {JobAsNonCompany} from 'core/domain/jobs/jobAsNonCompany'
import {EmployerPreferences} from 'core/domain/jobs/employerPreferences'

/**
 * Get service providers
 */
const JobpostService: IJobpostService = provider.get<IJobpostService>(SocialProviderTypes.JobpostService)

/* _____________ CRUD DB _____________ */

export const dbAddJobPreferences = (jobPref: JobPreferences) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let jobPreferences: JobPreferences = {
            jobPreferance: jobPref.jobPreferance,
            wantToWork: jobPref.wantToWork,
            location: jobPref.location,
            role: jobPref.role,
            industry: jobPref.industry,
            preferToWork: jobPref.preferToWork,
            remoteWork: jobPref.remoteWork,
            workFrom: jobPref.workFrom,
            workTo: jobPref.workTo,
            jobType: jobPref.jobType,
            userId: uid,
        }
        return JobpostService.addJobPreferences(jobPreferences).then((Key: string) => {
            dispatch(addJobPreferences(uid, {
                ...jobPreferences,
                id: Key
            }))
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbAddEmployerPreferences = (employerPref: EmployerPreferences) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let employerPreferences: EmployerPreferences = {
            employerPreferance: employerPref.employerPreferance,
            wantForWork: employerPref.wantForWork,
            location: employerPref.location,
            role: employerPref.role,
            industry: employerPref.industry,
            preferToWork: employerPref.preferToWork,
            remoteWork: employerPref.remoteWork,
            workFrom: employerPref.workFrom,
            workTo: employerPref.workTo,
            jobType: employerPref.jobType,
            userId: uid,
        }
        return JobpostService.addEmployerPreferences(employerPreferences).then((Key: string) => {
            dispatch(addEmployerPreferences(uid, {
                ...employerPreferences,
                id: Key
            }))
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbAddCompanyJobpost = (jobPost: JobAsCompany) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        console.log(jobPost)
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let jobposts: JobAsCompany
        if (jobPost.postJobAs === 'Company') {
            jobposts = {
                postType: jobPost.postJobAs,
                companyName: jobPost.companyName,
                industry: jobPost.industry,
                altCompanyName: jobPost.altCompanyName,
                bunsinessDescription: jobPost.bunsinessDescription,
                companyAddress: jobPost.companyAddress,
                phone: jobPost.phone,
                email: jobPost.email,
                website: jobPost.website,
                jobTitle: jobPost.jobTitle,
                skill: jobPost.skill,
                jobType: jobPost.jobType,
                eduRequirement: jobPost.eduRequirement,
                levelOfSkill: jobPost.levelOfSkill,
                salary: jobPost.salary,
                noOfVacancy: jobPost.noOfVacancy,
                responsibilities: jobPost.responsibilities,
                userId: uid,
            }
        } else {
            jobposts = {
                postType: jobPost.postJobAs,
                individualName: jobPost.individualName,
                individualEmail: jobPost.individualEmail,
                individualPhone: jobPost.individualPhone,
                designation: jobPost.designation,
                jobTitle: jobPost.jobTitle,
                skill: jobPost.skill,
                jobType: jobPost.jobType,
                eduRequirement: jobPost.eduRequirement,
                levelOfSkill: jobPost.levelOfSkill,
                salary: jobPost.salary,
                noOfVacancy: jobPost.noOfVacancy,
                responsibilities: jobPost.responsibilities,
                userId: uid,
            }
        }

        return JobpostService.addJobpost(jobposts).then((Key: string) => {
            dispatch(addJobpost(uid, {
                ...jobposts,
                id: Key
            }))
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbAddNonCompanyJobpost = (jobPostNonCompany: JobAsNonCompany) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let jobpostNonCompany: JobAsNonCompany = {
            id: jobPostNonCompany.id,
            postType: 2,
            name: jobPostNonCompany.name,
            designation: jobPostNonCompany.designation,
            phone: jobPostNonCompany.phone,
            email: jobPostNonCompany.email,
            jobTitle: jobPostNonCompany.jobTitle,
            skill: jobPostNonCompany.skill,
            jobType: jobPostNonCompany.jobType,
            location: jobPostNonCompany.location,
            eduRequirement: jobPostNonCompany.eduRequirement,
            industry: jobPostNonCompany.industry,
            levelOfSkill: jobPostNonCompany.levelOfSkill,
            salary: jobPostNonCompany.salary,
            noOfVacancy: jobPostNonCompany.noOfVacancy,
            responsibilities: jobPostNonCompany.responsibilities,
            userId: uid,
        }
        return JobpostService.addJobpostAsNonCompany(jobpostNonCompany).then((Key: string) => {
            console.log('jobasNonCOmpany')
            console.log(Key)
            dispatch(addJobpostAsNonCompany(uid, {
                ...jobpostNonCompany,
                id: Key
            }))
            dispatch(globalActions.hideTopLoading())

        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbGetJobpostAsCompanyById = (userId: string) => {
    return (dispatch: any, getState: Function) => {
        if (userId) {
            return JobpostService.getJobpostAsCompany(userId).then((jobpostAsCompany) => {
                const state: Map<string, any> = getState()
                let parsedData: { [jobAsCompanyId: string]: JobAsCompany } = {}
                jobpostAsCompany.forEach((jobAsCompany) => {
                    const jobAsCompanyId = Object.keys(jobAsCompany)[0]
                    const jobAsCompanyData = jobAsCompany[jobAsCompanyId]
                    parsedData = {
                        ...parsedData,
                        [jobAsCompanyId]: {
                            ...jobAsCompanyData
                        }
                    }
                })
                dispatch(getJobpostAsCompany(userId, parsedData))
            }).catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
        }
    }
}

export const dbGetJobpostAsNonCompanyById = (userId: string) => {
    return (dispatch: any, getState: Function) => {
        console.log(userId)
        if (userId) {
            return JobpostService.getJobpostAsNonCompany(userId).then((jobpostAsNonCompany) => {
                console.log('jobasNonCOmpany')
                console.log(jobpostAsNonCompany)
                const state: Map<string, any> = getState()
                let parsedData: { [jobAsNonCompanyId: string]: JobAsNonCompany } = {}
                jobpostAsNonCompany.forEach((jobAsNonCompany) => {
                    const jobAsNonCompanyId = Object.keys(jobAsNonCompany)[0]
                    const jobAsNonCompanyData = jobAsNonCompany[jobAsNonCompanyId]
                    parsedData = {
                        ...parsedData,
                        [jobAsNonCompanyId]: {
                            ...jobAsNonCompanyData
                        }
                    }
                })
                dispatch(getJobpostAsNonCompany(userId, parsedData))
            }).catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
        }
    }
}

export const dbUpdateJobpostAsCompany = (jobPost: JobAsCompany, postId: string) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let getJobpostAsCompany = state.getIn(['jobpost', 'jobpostAsCompany', uid, postId]).toJS()
        let jobpostAsCompany: JobAsCompany = {
            id: postId,
            companyName: jobPost.companyName ? jobPost.companyName : getJobpostAsCompany.companyName,
            industry: jobPost.industry ? jobPost.industry : getJobpostAsCompany.industry,
            altCompanyName: jobPost.altCompanyName ? jobPost.altCompanyName : getJobpostAsCompany.altCompanyName,
            bunsinessDescription: jobPost.bunsinessDescription ? jobPost.bunsinessDescription : getJobpostAsCompany.bunsinessDescription,
            companyAddress: jobPost.companyAddress ? jobPost.companyAddress : getJobpostAsCompany.companyAddress,
            phone: jobPost.phone ? jobPost.phone : getJobpostAsCompany.phone,
            email: jobPost.email ? jobPost.email : getJobpostAsCompany.email,
            website: jobPost.website ? jobPost.website : getJobpostAsCompany.website,
            jobTitle: jobPost.jobTitle ? jobPost.jobTitle : getJobpostAsCompany.jobTitle,
            skill: jobPost.skill ? jobPost.skill : getJobpostAsCompany.skill,
            jobType: jobPost.jobType ? jobPost.jobType : getJobpostAsCompany.jobType,
            eduRequirement: jobPost.eduRequirement ? jobPost.eduRequirement : getJobpostAsCompany.eduRequirement,
            levelOfSkill: jobPost.levelOfSkill ? jobPost.levelOfSkill : getJobpostAsCompany.levelOfSkill,
            salary: jobPost.salary ? jobPost.salary : getJobpostAsCompany.salary,
            noOfVacancy: jobPost.noOfVacancy ? jobPost.noOfVacancy : getJobpostAsCompany.noOfVacancy,
            responsibilities: jobPost.responsibilities ? jobPost.responsibilities : getJobpostAsCompany.responsibilities,
            userId: uid,
        }
        return JobpostService.updateJobpostAsCompany(jobpostAsCompany).then(() => {
            dispatch(updateJobpostAsCompany(uid, jobpostAsCompany))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbUpdateJobpostAsNonCompany = (jobPost: JobAsNonCompany, postId: string) => {
    return (dispatch: any, getState: Function) => {
        dispatch(globalActions.showTopLoading())
        dispatch(globalActions.showNotificationRequest())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        let getJobpostAsNonCompany = state.getIn(['jobpost', 'jobpostAsNonCompany', uid, postId]).toJS()
        let jobpostAsNonCompany: JobAsNonCompany = {
            id: postId,
            name: jobPost.name ? jobPost.name : getJobpostAsNonCompany.name,
            industry: jobPost.industry ? jobPost.industry : getJobpostAsNonCompany.industry,
            designation: jobPost.designation ? jobPost.designation : getJobpostAsNonCompany.designation,
            phone: jobPost.phone ? jobPost.phone : getJobpostAsNonCompany.phone,
            email: jobPost.email ? jobPost.email : getJobpostAsNonCompany.email,
            location: jobPost.location ? jobPost.location : getJobpostAsNonCompany.location,
            jobTitle: jobPost.jobTitle ? jobPost.jobTitle : getJobpostAsNonCompany.jobTitle,
            skill: jobPost.skill ? jobPost.skill : getJobpostAsNonCompany.skill,
            jobType: jobPost.jobType ? jobPost.jobType : getJobpostAsNonCompany.jobType,
            eduRequirement: jobPost.eduRequirement ? jobPost.eduRequirement : getJobpostAsNonCompany.eduRequirement,
            levelOfSkill: jobPost.levelOfSkill ? jobPost.levelOfSkill : getJobpostAsNonCompany.levelOfSkill,
            salary: jobPost.salary ? jobPost.salary : getJobpostAsNonCompany.salary,
            noOfVacancy: jobPost.noOfVacancy ? jobPost.noOfVacancy : getJobpostAsNonCompany.noOfVacancy,
            responsibilities: jobPost.responsibilities ? jobPost.responsibilities : getJobpostAsNonCompany.responsibilities,
            userId: uid,
        }
        return JobpostService.updateJobpostAsNonCompany(jobpostAsNonCompany).then(() => {
            dispatch(updateJobpostAsNonCompany(uid, jobpostAsNonCompany))
            dispatch(globalActions.showNotificationSuccess())
            dispatch(globalActions.hideTopLoading())
        })
            .catch((error: SocialError) => dispatch(globalActions.showMessage(error.message)))
    }
}

export const dbDeleteJobpostAsCompany = (jobAsCompanyId: string) => {
    return (dispatch: any, getState: Function) => {

        if (jobAsCompanyId === undefined || jobAsCompanyId === null) {
            dispatch(globalActions.showMessage('Job post id can not be null or undefined'))
        }
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        return JobpostService.deleteJobpostAsCompany(jobAsCompanyId)
            .then(() => {
                dispatch(deleteJobpostAsCompany(uid, jobAsCompanyId))
                dispatch(globalActions.hideTopLoading())

            }, (error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())

            })
    }
}

export const dbDeleteJobpostAsNonCompany = (jobAsNonCompanyId: string) => {
    return (dispatch: any, getState: Function) => {

        if (jobAsNonCompanyId === undefined || jobAsNonCompanyId === null) {
            dispatch(globalActions.showMessage('Job post id can not be null or undefined'))
        }
        dispatch(globalActions.showTopLoading())
        const state: Map<string, any> = getState()
        let uid: string = state.getIn(['authorize', 'uid'])
        return JobpostService.deleteJobpostAsNonCompany(jobAsNonCompanyId)
            .then(() => {
                dispatch(deleteJobpostAsNonCompany(uid, jobAsNonCompanyId))
                dispatch(globalActions.hideTopLoading())

            }, (error: SocialError) => {
                dispatch(globalActions.showMessage(error.message))
                dispatch(globalActions.hideTopLoading())

            })
    }
}

/* _____________ CRUD State _____________ */

export const addJobPreferences = (uid: string, jobPref: JobPreferences) => {
    return {
        type: JobActionType.ADD_JOBPREFERENCES,
        payload: {uid, jobPref}
    }
}

export const addEmployerPreferences = (uid: string, employerPref: EmployerPreferences) => {
    return {
        type: JobActionType.ADD_EMPLOYER_PREFERENCES,
        payload: {uid, employerPref}
    }
}

export const addJobpost = (uid: string, jobPost: JobAsCompany) => {
    return {
        type: JobActionType.ADD_JOBPOST_AS_COMPANY,
        payload: {uid, jobPost}
    }
}

export const addJobpostAsNonCompany = (uid: string, jobPost: JobAsNonCompany) => {
    return {
        type: JobActionType.ADD_JOBPOST_AS_NON_COMPANY,
        payload: {uid, jobPost}
    }
}

export const getJobpostAsCompany = (uid: string, jobpostAsCompany: { [jobAsCompanyId: string]: JobAsCompany }) => {
    return {
        type: JobActionType.GET_JOBPOST_BY_COMPANY,
        payload: {uid, jobpostAsCompany}
    }
}

export const getJobpostAsNonCompany = (uid: string, jobpostAsNonCompany: { [jobAsNonCompanyId: string]: JobAsNonCompany }) => {
    return {
        type: JobActionType.GET_JOBPOST_BY_NON_COMPANY,
        payload: {uid, jobpostAsNonCompany}
    }
}

export const updateJobpostAsCompany = (uid: string, jobPost: JobAsCompany) => {
    return {
        type: JobActionType.UPDATE_JOBPOST_AS_COMPANY,
        payload: {uid, jobPost}
    }
}

export const updateJobpostAsNonCompany = (uid: string, jobPost: JobAsNonCompany) => {
    return {
        type: JobActionType.UPDATE_JOBPOST_AS_NON_COMPANY,
        payload: {uid, jobPost}
    }
}

/**
 * Delete a comment
 */
export const deleteJobpostAsCompany = (uid: string, id: string) => {
    return {
        type: JobActionType.DELETE_JOBPOST_AS_COMPANY,
        payload: {uid, id}
    }
}

/**
 * Delete a comment
 */
export const deleteJobpostAsNonCompany = (uid: string, id: string) => {
    return {
        type: JobActionType.DELETE_JOBPOST_AS_NON_COMPANY,
        payload: {uid, id}
    }
}