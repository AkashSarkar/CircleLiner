import { NotifyBox } from 'src/core/domain/notifyBox'
import {Map} from 'immutable'

/**
 * Notification state
 * 
 * @export
 * @class NotifyBoxState
 */
export class NotifyBoxState  {

    /**
     * The list of users notification
     */
    boxNotifies: Map<string, Map<string, any>> = Map({})

    /**
     * If user notifications are loaded {true} or not {false}
     */
    loaded: Boolean = false
  }