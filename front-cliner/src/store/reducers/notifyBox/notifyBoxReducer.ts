// - Import react components
import moment from 'moment/moment'
import _ from 'lodash'
import {Map} from 'immutable'

// - Import domain
import {NotifyBox} from 'src/core/domain/notifyBox'

// - Import action types
import {NotifyBoxActionType} from 'constants/notifyBoxActionType'

import {NotifyBoxState} from './NotificationState'
import {INotifyBoxAction} from './INotifyBoxAction'

/**
 * Notify actions
 * @param {object} state
 * @param {object} action
 */
export let notifyBoxReducer = (state = Map(new NotifyBoxState()), action: INotifyBoxAction) => {
    let {payload} = action
    switch (action.type) {

        /* _____________ CRUD _____________ */
        case NotifyBoxActionType.ADD_NOTIFY_BOX:
            return state

        case NotifyBoxActionType.ADD_NOTIFY_LIST_BOX:
            return state
                .set('boxNotifies', 'BoxNotification')
                .set('loaded', true)

        case NotifyBoxActionType.SEEN_NOTIFY_BOX:
            return state
                .setIn(['boxNotifies', 'BoxNotification', 'isSeen'], true)
                .set('loaded', true)

        case NotifyBoxActionType.DELETE_NOTIFY_BOX:
            return state
                .deleteIn(['boxNotifies', 'BoxNotification'])

        case NotifyBoxActionType.CLEAR_ALL_DATA_NOTIFY_BOX:
            return Map(new NotifyBoxState())
        case NotifyBoxActionType.HAS_MORE_DATA:
            return state
                .setIn(['hasMoreData'], true)
        case NotifyBoxActionType.NO_MORE_DATA:
            return state
                .setIn(['hasMoreData'], false)
        case NotifyBoxActionType.REQUEST_PAGE:
            return state
                .setIn(['lastPageRequested'], payload.page)
        case NotifyBoxActionType.LAST_NOTIFY_ID:
            return state
                .setIn(['lastNotifyId'], payload.lastUserId)
        default:
            return state

    }

}
