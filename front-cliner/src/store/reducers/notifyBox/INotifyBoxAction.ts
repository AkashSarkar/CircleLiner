import {NotifyBoxActionType} from 'constants/notifyBoxActionType'

/**
 *  Notification action interface
 * 
 * @export
 * @interface INotificationAction
 */
export interface INotifyBoxAction  {
    payload: any,
    type: NotifyBoxActionType
  
  }
  