import {SettingActionType} from 'constants/settingActionType'

/**
 *  Server action interface
 *
 * @export
 * @interface ISettingAction
 */
export interface ISettingAction {
    payload: any,
    type: SettingActionType

}
