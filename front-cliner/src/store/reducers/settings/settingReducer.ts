// - Import react components
import _ from 'lodash'
import {Map} from 'immutable'
// import {SettingRequestModel} from 'models/setting/settingRequestModel'
import {ISettingAction} from 'store/reducers/settings/ISettingAction'
import {SettingState} from 'store/reducers/settings/SettingState'
// - Import action types

export let settingReducer = (state = Map(new SettingState()), action: ISettingAction) => {
    let {payload} = action
    switch (action.type) {
        default:
            return state
    }
}