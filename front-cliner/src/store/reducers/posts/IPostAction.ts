import { PostActionType } from 'constants/postActionType'

/**
 * Post action interface
 *
 * @export
 * @interface IJobAction
 */
export interface IPostAction {
  payload: any,
  type: PostActionType

}
