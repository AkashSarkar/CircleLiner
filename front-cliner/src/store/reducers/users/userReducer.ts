// - Import action types
import {UserActionType} from 'constants/userActionType'
import {List, Map} from 'immutable'

// - Import domain
import {User, Profile} from 'src/core/domain/users'

import {UserState} from './UserState'
import {IUserAction} from './IUserAction'
import {CommentActionType} from 'src/constants/commentActionType'

/**
 * User reducer
 */
export let userReducer = (state = Map(new UserState()), action: IUserAction) => {
    const {payload} = action
    console.log(action)
    switch (action.type) {
        case UserActionType.UPDATE_USER_AVATAR:
            console.log(UserActionType.UPDATE_USER_AVATAR)
            return state
                .setIn(['info', payload.uid], payload.info)
        case UserActionType.USER_INFO:
            console.log(UserActionType.USER_INFO)
            return state
                .setIn(['info', payload.uid], payload.info)

        case UserActionType.ADD_USER_INFO:
            console.log(UserActionType.USER_INFO)
            return state
                .setIn(['info', payload.uid], payload.info)
                .set('loaded', true)

        case UserActionType.ADD_PEOPLE_INFO:
            console.log(UserActionType.USER_INFO)
            return state
                .mergeIn(['info'], payload)

        case UserActionType.UPDATE_USER_INFO:
            console.log(UserActionType.USER_INFO)
            return state
                .mergeIn(['info', payload.uid], payload.info)

        case UserActionType.CLEAR_ALL_DATA_USER:
            console.log(UserActionType.LAST_USER_PEOPLE)
            return Map(new UserState())

        case UserActionType.CLOSE_EDIT_PROFILE:
            return state
                .set('openEditProfile', false)

        case UserActionType.OPEN_EDIT_PROFILE:
            console.log(UserActionType.OPEN_EDIT_PROFILE)
            return state
                .set('openEditProfile', true)

        case UserActionType.CLOSE_CHANGE_AVATAR:
            return state
                .set('openMyPhoto', false)

        case UserActionType.OPEN_CHANGE_AVATAR:
            console.log(UserActionType.OPEN_CHANGE_AVATAR)
            return state
                .set('openMyPhoto', true)

        case UserActionType.HAS_MORE_DATA_PEOPLE:
            console.log(UserActionType.HAS_MORE_DATA_PEOPLE)
            return state
                .setIn(['people', 'hasMoreData'], true)

        case UserActionType.NOT_MORE_DATA_PEOPLE:
            console.log(UserActionType.NOT_MORE_DATA_PEOPLE)
            return state
                .setIn(['people', 'hasMoreData'], false)

        case UserActionType.REQUEST_PAGE_PEOPLE:
            console.log(UserActionType.LAST_USER_PEOPLE)
            return state
                .setIn(['people', 'lastPageRequest'], payload.page)

        case UserActionType.LAST_USER_PEOPLE:
            console.log(UserActionType.LAST_USER_PEOPLE)
            return state
                .setIn(['people', 'lastUserId'], payload.lastUserId)

        case UserActionType.ADD_USER_PERSONAL_INFO:
            return state
                .setIn(['info', 'details'], payload)

        case UserActionType.ADD_USER_INDUSTRY:
            return state
                .setIn(['industry', payload.uid, payload.industry.id], payload.industry)

        case UserActionType.UPDATE_USER_INDUSTRY:
            return state
                .setIn(['industry', payload.uid, payload.industry.id], payload.industry)

        case UserActionType.ADD_USER_ACCOUNT_SETTING:
            return state
                .setIn(['info', 'accountId'], payload.uid)

        case UserActionType.ADD_USER_EDUCATION:
            return state
                .setIn(['education', payload.uid, payload.education.id], payload.education)
        case UserActionType.ADD_USER_SOCIAL_URL:
            return state
                .setIn(['socialUrl', payload.uid, payload.social.id], payload.social)

        case UserActionType.GET_USER_INDUSTRY_BY_ID:
            return state
                .mergeDeepIn(['industry', payload.uid], payload.industries)

        case UserActionType.ADD_PROFILE_REPORT:
            return state
                .setIn(['reportProfile'], payload.info)

        case UserActionType.ADD_USER_BLOCK:
            return state
                .setIn(['blockstatus'], payload)

        case UserActionType.ADD_USER_PROFESSION:
            return state
                .setIn(['profession', payload.uid, payload.profession.id], payload.profession)

        case UserActionType.GET_USER_PROFESSION_BY_ID:
            return state
                .mergeDeepIn(['profession', payload.uid], payload.professions)

        case UserActionType.GET_USER_EDUCATION_INFO_BY_ID:
            console.log('GET_USER_EDUCATION_INFO')
            return state
                .mergeDeepIn(['education', payload.uid], payload.educations)
        case UserActionType.GET_USER_SOCIAL_URL_BY_ID:
            console.log('GET_USER_EDUCATION_INFO')
            return state
                .mergeDeepIn(['socialUrl', payload.uid], payload.socialUrls)

        case UserActionType.UPDATE_USER_EDUCATION:
            console.log('UPDATE_USER_EDUCATION')
            return state
                .setIn(['education', payload.uid, payload.education.id], payload.education)
        case UserActionType.UPDATE_USER_SOCIAL_URL:
            console.log('UPDATE_USER_SOCIAL_URL')
            return state
                .setIn(['socialUrl', payload.uid, payload.socialUrl.id], payload.socialUrl)
        case UserActionType.UPDATE_USER_PROFESSION:
            console.log('UPDATE_USER_PROFESSION')
            return state
                .setIn(['profession', payload.uid, payload.profession.id], payload.profession)

        case UserActionType.DELETE_EDUCATION:
            console.log('DELETE_USER_EDUCATION')
            return state.deleteIn(['education', payload.uid, payload.id])
        case UserActionType.DELETE_SOCIAL_URL:
            console.log('DELETE_USER_EDUCATION')
            return state.deleteIn(['socialUrl', payload.uid, payload.id])
        case UserActionType.DELETE_PROFESSION:
            console.log('DELETE_USER_PROFESSION')
            return state.deleteIn(['profession', payload.uid, payload.id])
        default:
            return state
    }

}