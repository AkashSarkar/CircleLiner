import {Profession} from 'src/core/domain/users/profession'
import {User, Profile, Education, Industry} from 'src/core/domain/users'
import {Map, fromJS, List} from 'immutable'
import {SocialUrl} from 'core/domain/users/socialUrl'
/**
 * User state
 *
 * @export
 * @class UserState
 */
export class UserState {
    /**
     * The list of users information
     */
    info: Map<string, Profile> = Map({})
    /**
     * The list of users information
     */
    profession: Map<string, Profession> = Map({})
    /**
     * The list of users education information
     */
    education: Map<string, Education> = Map({})
    /**
     * The list of users social url information
     */
    socialUrl: Map<string, SocialUrl> = Map({})

    /**
     * If users profile are loaded
     */
    loaded: Boolean = false

    /**
     * If edit profile is open {true} or not {false}
     */
    openEditProfile: Boolean = false

    /**
     * If change avatar is in action {true} or not {false}
     */
    openMyPhoto: Boolean = false

    /**
     * People data storage
     */
    people?: Map<string, any> = Map({hasMoreData: true, lastPageRequest: -1, lastUserId: ''})

    industry: Map<string, Industry> = Map({})

}
