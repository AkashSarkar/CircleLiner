import {Post} from 'src/core/domain/posts'
import {Map, fromJS, List} from 'immutable'

/**
 * Post state
 *
 * @export
 * @class JobState
 */
export class JobState {

    /**
     * The list of user posts
     *
     * @type {*}
     * @memberof PostState
     */
    jobs = Map({})
    /**
     * The list of user posts
     *
     * @type {*}
     * @memberof PostState
     */
        // TODO: Move to single variable {jobpostAsCompany,jobpostAsNonCompany} => job
    jobpostAsCompany
        = Map({})

    /**
     * The list of user posts
     *
     * @type {*}
     * @memberof PostState
     */
    jobpostAsNonCompany
        = Map({})

    /**
     * The list of user posts
     *
     * @type {*}
     * @memberof PostState
     */
    jobPreferences
        = Map({})

    /**
     * The list of user posts
     *
     * @type {*}
     * @memberof PostState
     */
    employerPreferences
        = Map({})

    /**
     * If user posts are loaded {true} or not {false}
     *
     * @type {Boolean}
     * @memberof PostState
     */
    loaded: Boolean = false

    /**
     * Stream data storage
     */
    stream?: Map<string, any> = Map({hasMoreData: true, lastPageRequest: -1, lastJobId: ''})

    /**
     * Profile posts data storage
     */
    profile?: Map<string, any> = Map({})

}
