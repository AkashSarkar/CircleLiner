// - Import react components
import moment from 'moment/moment'
import _ from 'lodash'
import {Reducer, Action} from 'redux'
import {Map} from 'immutable'

// - Import action types
import {JobActionType} from 'constants/jobActionType'

import {JobState} from './JobState'
import {IJobAction} from './IJobAction'
import {PostActionType} from 'src/constants/postActionType'

/**
 * Post reducer
 * @param {object} state
 * @param {object} action
 */
export let jobReducer = (state = Map(new JobState()), action: IJobAction) => {
    const {payload} = action
    switch (action.type) {
        case JobActionType.ADD_LIST_JOB:
            return state
                .mergeDeepIn(['jobs'], payload.userPosts)
                .set('loaded', true)
        case JobActionType.ADD_JOBPREFERENCES:
            return state
                .setIn(['jobPreferences', payload.uid], payload.jobPref)

        case JobActionType.ADD_EMPLOYER_PREFERENCES:
            return state
                .setIn(['employerPreferences', payload.uid], payload.employerPref)

        case JobActionType.ADD_JOBPOST_AS_COMPANY:
            return state
                .setIn(['jobpostAsCompany', payload.uid, payload.jobPost.id], payload.jobPost)

        case JobActionType.ADD_JOBPOST_AS_NON_COMPANY:
            return state
                .setIn(['jobpostAsNonCompany', payload.uid, payload.jobPost.id], payload.jobPost)

        case JobActionType.GET_JOBPOST_BY_COMPANY:
            return state
                .mergeDeepIn(['jobpostAsCompany', payload.uid], payload.jobpostAsCompany)

        case JobActionType.GET_JOBPOST_BY_NON_COMPANY:
            return state
                .mergeDeepIn(['jobpostAsNonCompany', payload.uid], payload.jobpostAsNonCompany)

        case JobActionType.UPDATE_JOBPOST_AS_COMPANY:
            return state
                .setIn(['jobpostAsCompany', payload.uid, payload.jobPost.id], payload.jobPost)

        case JobActionType.UPDATE_JOBPOST_AS_NON_COMPANY:
            return state
                .setIn(['jobpostAsNonCompany', payload.uid, payload.jobPost.id], payload.jobPost)

        case JobActionType.DELETE_JOBPOST_AS_COMPANY:
            return state.deleteIn(['jobpostAsCompany', payload.uid, payload.id])

        case JobActionType.DELETE_JOBPOST_AS_NON_COMPANY:
            return state.deleteIn(['jobpostAsNonCompany', payload.uid, payload.id])

        default:
            return state
    }
}
