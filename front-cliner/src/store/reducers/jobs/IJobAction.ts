import {JobActionType} from 'constants/jobActionType'

/**
 * Post action interface
 *
 * @export
 * @interface IJobAction
 */
export interface IJobAction {
    payload: any,
    type: JobActionType

}
