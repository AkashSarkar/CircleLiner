// - Import react components
import moment from 'moment/moment'
import _ from 'lodash'
import {Map} from 'immutable'

// - Import domain
import {Notification} from 'src/core/domain/notifications'

// - Import action types
import {NotifyBoxActionType} from 'constants/notificationActionType'

import {NotificationState} from './NotificationState'
import {INotificationAction} from './INotificationAction'

/**
 * Notify actions
 * @param {object} state
 * @param {object} action
 */
export let notificationReducer = (state = Map(new NotificationState()), action: INotificationAction) => {
    let {payload} = action
    switch (action.type) {

        /* _____________ CRUD _____________ */
        case NotifyBoxActionType.ADD_NOTIFY:
            return state

        case NotifyBoxActionType.ADD_NOTIFY_LIST:
            return state
                .set('userNotifies', payload)
                .set('loaded', true)

        case NotifyBoxActionType.SEEN_NOTIFY:
            return state
                .setIn(['userNotifies', payload, 'isSeen'], true)
                .set('loaded', true)

        case NotifyBoxActionType.DELETE_NOTIFY:
            return state
                .deleteIn(['userNotifies', payload])

        case NotifyBoxActionType.CLEAR_ALL_DATA_NOTIFY:
            return Map(new NotificationState())
        case NotifyBoxActionType.HAS_MORE_DATA:
            return state
                .setIn(['hasMoreData'], true)
        case NotifyBoxActionType.NO_MORE_DATA:
            return state
                .setIn(['hasMoreData'], false)
        case NotifyBoxActionType.REQUEST_PAGE:
            return state
                .setIn(['lastPageRequested'], payload.page)
        case NotifyBoxActionType.LAST_NOTIFY_ID:
            return state
                .setIn(['lastNotifyId'], payload.lastUserId)
        default:
            return state

    }

}
