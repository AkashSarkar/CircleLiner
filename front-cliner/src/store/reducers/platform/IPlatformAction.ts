import {PlatformActionType} from 'constants/platformActionType'

/**
 * Post action interface
 *
 * @export
 * @interface IJobAction
 */
export interface IPlatformAction {
    payload: any,
    type: PlatformActionType

}
