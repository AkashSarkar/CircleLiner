import {Post} from 'src/core/domain/posts'
import {Map, fromJS, List} from 'immutable'

/**
 * Post state
 *
 * @export
 * @class JobState
 */
export class PlatformState {
    /**
     * If user posts are loaded {true} or not {false}
     *
     * @type {Boolean}
     * @memberof PostState
     */
    loaded: Boolean = false
}
