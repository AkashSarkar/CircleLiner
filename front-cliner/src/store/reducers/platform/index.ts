import { platformReducer } from './platformReducer'
import { platformSelector } from './platformSelector'

export {
    platformReducer,
    platformSelector
}