// - Import react components
import moment from 'moment/moment'
import _ from 'lodash'
import {Reducer, Action} from 'redux'
import {Map} from 'immutable'

// - Import action types
import {PlatformActionType} from 'constants/platformActionType'

import {PlatformState} from './PlatformState'
import {IPlatformAction} from './IPlatformAction'
import {Post} from 'src/core/domain/posts/post'
import CommonAPI from 'src/api/CommonAPI'

const updatePost = (state: any, payload: any) => {
    const post: Map<string, any> = payload.post
    const updatePostOwnerId = post.get('ownerUserId')
    const updatePostId = post.get('id')
    return state
        .setIn(['osts', updatePostId], Map(post))
}

const updatePostComments = (state: any, payload: any) => {
    const post: Map<string, any> = payload.post
    const updatePostOwnerId = post.get('ownerUserId')
    const updatePostId = post.get('id')
    return state
        .setIn(['posts', updatePostId, 'comments'], post.get('comments'))
}

const updatePostVotes = (state: any, payload: any) => {
    const post: Map<string, any> = payload.post
    const updatePostOwnerId = post.get('ownerUserId')
    const updatePostId = post.get('id')
    return state
        .setIn(['posts', updatePostId, 'votes'], post.get('votes'))
}

/**
 * Post reducer
 * @param {object} state
 * @param {object} action
 */
export let platformReducer = (state = Map(new PlatformState()), action: IPlatformAction) => {
    const {payload} = action
    switch (action.type) {
        case PlatformActionType.CLEAR_ALL_DATA_POST_PLT:
            return Map(new PlatformState())
        case PlatformActionType.UPDATE_POST_PLT:
            return updatePost(state, payload)
        case PlatformActionType.UPDATE_POST_COMMENTS_PLT:
            return updatePostComments(state, payload)
        case PlatformActionType.UPDATE_POST_VOTES_PLT:
            return updatePostVotes(state, payload)

        case PlatformActionType.DELETE_POST_PLT:
            return state
                .deleteIn([payload.pltid, 'posts', payload.id])

        case PlatformActionType.ADD_LIST_POST_PLT:
            return state
                .mergeDeepIn([payload.pltid, 'posts'], payload.userPosts)
                .set('loaded', true)

        case PlatformActionType.HAS_MORE_DATA_STREAM_PLT:
            return state
                .setIn([payload.pltid, 'stream', 'hasMoreData'], true)

        case PlatformActionType.NOT_MORE_DATA_STREAM_PLT:
            return state
                .setIn([payload.pltid, 'stream', 'hasMoreData'], false)

        case PlatformActionType.REQUEST_PAGE_STREAM_PLT:
            return state
                .setIn([payload.pltid, 'stream', 'lastPageRequest'], payload.page)

        case PlatformActionType.LAST_POST_STREAM_PLT:
            return state
                .setIn([payload.pltid, 'stream', 'lastPostId'], payload.lastPostId)
        default:
            return state

    }
}
