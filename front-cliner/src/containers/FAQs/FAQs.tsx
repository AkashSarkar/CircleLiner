// - Import react components
import React, {Component} from 'react'
import {IFAQsProps} from './IFAQsProps'
import {IFAQsState} from './IFAQsState'
import {IRouterProps} from 'routes/IRouterProps'
import {Map} from 'immutable'
import {HomeRouter} from 'routes/HomeRouter'
import {connect} from 'react-redux'

// - Import components
import HomeView from 'layouts/views/homeView'
// - Import actions

// - Create component class
export class FAQs extends Component<IFAQsProps, IFAQsState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IFAQsProps) {
        super(props)

        // Default state
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <>
                {this.props.isAuthed ? <HomeView>
                <div className='body-bg-white'>
                    <div className='stunning-header bg-primary-opacity'>
                        <div className='header-spacer--standard'/>
                        <div className='stunning-header-content'>
                            <h1 className='stunning-header-title'>Frequently Asked Questions</h1>
                            <ul className='breadcrumbs'>
                                <li className='breadcrumbs-item'>
                                    <a href='#'>Home</a>
                                    <span className='icon breadcrumbs-custom'>/</span>
                                </li>
                                <li className='breadcrumbs-item active'>
                                    <span>FAQs</span>
                                </li>
                            </ul>
                        </div>
                        <div className='content-bg-wrap stunning-header-bg1'/>
                    </div>
                    <section className='mb60'>
                        <div className='container'>
                            <div className='row'>
                                <div className='col col-xl-8 m-auto col-lg-10 col-md-12 col-sm-12 col-12'>
                                    <div id='accordion' role='tablist' aria-multiselectable='true'
                                         className='accordion-faqs'>
                                        <div className='card'>
                                            <div className='card-header' role='tab' id='headingOne'>
                                                <h3 className='mb-0'>
                                                    <a data-toggle='collapse' data-parent='#accordion'
                                                       href='#collapseOne' aria-expanded='true'
                                                       aria-controls='collapseOne'>
                                                        How do I create my Olympus account?
                                                        <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div id='collapseOne' className='collapse show' role='tabpanel'
                                                 aria-labelledby='headingOne'>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                    eiusmod tempor incididunt
                                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation
                                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                    irure dolor in reprehenderit
                                                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                    Excepteur sint occaecat
                                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                    anim id est laborum.
                                                </p>
                                                <p>
                                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque
                                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                    veritatis et quasi
                                                    architecto beatae vitae dicta sunt explicabo.
                                                </p>
                                            </div>
                                            <div className='card-header' role='tab' id='headingOne-1'>
                                                <h3 className='mb-0'>
                                                    <a data-toggle='collapse' data-parent='#accordion'
                                                       href='#collapseOne-1' aria-expanded='true'
                                                       aria-controls='collapseOne' className='collapsed'>
                                                        Is my account free? Do I have to pay anything?
                                                        <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div id='collapseOne-1' className='collapse' role='tabpanel'
                                                 aria-labelledby='headingOne-1'>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                    eiusmod tempor incididunt
                                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation
                                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                    irure dolor in reprehenderit
                                                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                    Excepteur sint occaecat
                                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                    anim id est laborum.
                                                </p>
                                                <p>
                                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque
                                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                    veritatis et quasi
                                                    architecto beatae vitae dicta sunt explicabo.
                                                </p>
                                            </div>
                                            <div className='card-header' role='tab' id='headingOne-2'>
                                                <h3 className='mb-0'>
                                                    <a data-toggle='collapse' data-parent='#accordion'
                                                       href='#collapseOne-2' aria-expanded='true'
                                                       aria-controls='collapseOne' className='collapsed'>
                                                        Can anyone create an account?
                                                        <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div id='collapseOne-2' className='collapse' role='tabpanel'
                                                 aria-labelledby='headingOne-2'>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                    eiusmod tempor incididunt
                                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation
                                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                    irure dolor in reprehenderit
                                                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                    Excepteur sint occaecat
                                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                    anim id est laborum.
                                                </p>
                                                <p>
                                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque
                                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                    veritatis et quasi
                                                    architecto beatae vitae dicta sunt explicabo.
                                                </p>
                                            </div>
                                            <div className='card-header' role='tab' id='headingOne-3'>
                                                <h3 className='mb-0'>
                                                    <a data-toggle='collapse' data-parent='#accordion'
                                                       href='#collapseOne-3' aria-expanded='true'
                                                       aria-controls='collapseOne-3' className='collapsed'>
                                                        Can I invite friends to join Olympus?
                                                        <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div id='collapseOne-3' className='collapse' role='tabpanel'
                                                 aria-labelledby='headingOne'>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                    eiusmod tempor incididunt
                                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation
                                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                    irure dolor in reprehenderit
                                                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                    Excepteur sint occaecat
                                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                    anim id est laborum.
                                                </p>
                                                <p>
                                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque
                                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                    veritatis et quasi
                                                    architecto beatae vitae dicta sunt explicabo.
                                                </p>
                                            </div>
                                            <div className='card-header' role='tab' id='headingOne-4'>
                                                <h3 className='mb-0'>
                                                    <a data-toggle='collapse' data-parent='#accordion'
                                                       href='#collapseOne-4' aria-expanded='true'
                                                       aria-controls='collapseOne' className='collapsed'>
                                                        Is there a referral system?
                                                        <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div id='collapseOne-4' className='collapse' role='tabpanel'
                                                 aria-labelledby='headingOne'>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                    eiusmod tempor incididunt
                                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation
                                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                    irure dolor in reprehenderit
                                                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                    Excepteur sint occaecat
                                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                    anim id est laborum.
                                                </p>
                                                <p>
                                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque
                                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                    veritatis et quasi
                                                    architecto beatae vitae dicta sunt explicabo.
                                                </p>
                                            </div>
                                            <div className='card-header' role='tab' id='headingOne-5'>
                                                <h3 className='mb-0'>
                                                    <a data-toggle='collapse' data-parent='#accordion'
                                                       href='#collapseOne-5' aria-expanded='true'
                                                       aria-controls='collapseOne-5' className='collapsed'>
                                                        How can I close my Olympus account?
                                                        <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div id='collapseOne-5' className='collapse' role='tabpanel'
                                                 aria-labelledby='headingOne-5'>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                    eiusmod tempor incididunt
                                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation
                                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                    irure dolor in reprehenderit
                                                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                    Excepteur sint occaecat
                                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                    anim id est laborum.
                                                </p>
                                                <p>
                                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque
                                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                    veritatis et quasi
                                                    architecto beatae vitae dicta sunt explicabo.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    {/* Footer Full Width */}
                    <div className='footer footer-full-width' id='footer'>
                        <div className='container'>
                            <div className='row'>
                                <div className='col col-lg-4 col-md-4 col-sm-6 col-6'>
                                    {/* Widget About */}
                                    <div className='widget w-about'>
                                        <a href='02-ProfilePage.html' className='logo'>
                                            <div className='img-wrap'>
                                                <img src='/img/logo-colored.png' alt='Olympus'/>
                                            </div>
                                            <div className='title-block'>
                                                <h6 className='logo-title'>olympus</h6>
                                                <div className='sub-title'>SOCIAL NETWORK</div>
                                            </div>
                                        </a>
                                        <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por
                                            incidid ut labore et lorem.</p>
                                        <ul className='socials'>
                                            <li>
                                                <a href='#'>
                                                    <i className='fab fa-facebook-square' aria-hidden='true'/>
                                                </a>
                                            </li>
                                            <li>
                                                <a href='#'>
                                                    <i className='fab fa-twitter' aria-hidden='true'/>
                                                </a>
                                            </li>
                                            <li>
                                                <a href='#'>
                                                    <i className='fab fa-youtube' aria-hidden='true'/>
                                                </a>
                                            </li>
                                            <li>
                                                <a href='#'>
                                                    <i className='fab fa-google-plus-g' aria-hidden='true'/>
                                                </a>
                                            </li>
                                            <li>
                                                <a href='#'>
                                                    <i className='fab fa-instagram' aria-hidden='true'/>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    {/* ... end Widget About */}
                                </div>
                                <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                    {/* Widget List */}
                                    <div className='widget w-list'>
                                        <h6 className='title'>Main Links</h6>
                                        <ul>
                                            <li>
                                                <a href='#'>Landing</a>
                                            </li>
                                            <li>
                                                <a href='#'>Home</a>
                                            </li>
                                            <li>
                                                <a href='#'>About</a>
                                            </li>
                                            <li>
                                                <a href='#'>Events</a>
                                            </li>
                                        </ul>
                                    </div>
                                    {/* ... end Widget List */}
                                </div>
                                <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                    <div className='widget w-list'>
                                        <h6 className='title'>Your Profile</h6>
                                        <ul>
                                            <li>
                                                <a href='#'>Main Page</a>
                                            </li>
                                            <li>
                                                <a href='#'>About</a>
                                            </li>
                                            <li>
                                                <a href='#'>Friends</a>
                                            </li>
                                            <li>
                                                <a href='#'>Photos</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                    <div className='widget w-list'>
                                        <h6 className='title'>Features</h6>
                                        <ul>
                                            <li>
                                                <a href='#'>Newsfeed</a>
                                            </li>
                                            <li>
                                                <a href='#'>Post Versions</a>
                                            </li>
                                            <li>
                                                <a href='#'>Messages</a>
                                            </li>
                                            <li>
                                                <a href='#'>Friend Groups</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                    <div className='widget w-list'>
                                        <h6 className='title'>Olympus</h6>
                                        <ul>
                                            <li>
                                                <a href='#'>Privacy</a>
                                            </li>
                                            <li>
                                                <a href='#'>Terms &amp; Conditions</a>
                                            </li>
                                            <li>
                                                <a href='#'>Forums</a>
                                            </li>
                                            <li>
                                                <a href='#'>Statistics</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                    {/* SUB Footer */}
                                    <div className='sub-footer-copyright'>
            <span>
              Copyright <a href='index-2.html'>Olympus Buddypress + WP</a> All Rights Reserved 2017
            </span>
                                    </div>
                                    {/* ... end SUB Footer */}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* ... end Footer Full Width */}
                    <a className='back-to-top' href='#'>
                        <img src='svg-icons/back-to-top.svg' alt='arrow' className='back-icon'/>
                    </a>
                </div>
            </HomeView> : <div className='body-bg-white'>
                <div className='stunning-header bg-primary-opacity'>
                    <div className='header-spacer--standard'/>
                    <div className='stunning-header-content'>
                        <h1 className='stunning-header-title'>Frequently Asked Questions</h1>
                        <ul className='breadcrumbs'>
                            <li className='breadcrumbs-item'>
                                <a href='#'>Home</a>
                                <span className='icon breadcrumbs-custom'>/</span>
                            </li>
                            <li className='breadcrumbs-item active'>
                                <span>FAQs</span>
                            </li>
                        </ul>
                    </div>
                    <div className='content-bg-wrap stunning-header-bg1'/>
                </div>
                <section className='mb60'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col col-xl-8 m-auto col-lg-10 col-md-12 col-sm-12 col-12'>
                                <div id='accordion' role='tablist' aria-multiselectable='true'
                                     className='accordion-faqs'>
                                    <div className='card'>
                                        <div className='card-header' role='tab' id='headingOne'>
                                            <h3 className='mb-0'>
                                                <a data-toggle='collapse' data-parent='#accordion'
                                                   href='#collapseOne' aria-expanded='true'
                                                   aria-controls='collapseOne'>
                                                    How do I create my Olympus account?
                                                    <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div id='collapseOne' className='collapse show' role='tabpanel'
                                             aria-labelledby='headingOne'>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                eiusmod tempor incididunt
                                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                nostrud exercitation
                                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                irure dolor in reprehenderit
                                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                Excepteur sint occaecat
                                                cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                anim id est laborum.
                                            </p>
                                            <p>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                accusantium doloremque
                                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi
                                                architecto beatae vitae dicta sunt explicabo.
                                            </p>
                                        </div>
                                        <div className='card-header' role='tab' id='headingOne-1'>
                                            <h3 className='mb-0'>
                                                <a data-toggle='collapse' data-parent='#accordion'
                                                   href='#collapseOne-1' aria-expanded='true'
                                                   aria-controls='collapseOne' className='collapsed'>
                                                    Is my account free? Do I have to pay anything?
                                                    <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div id='collapseOne-1' className='collapse' role='tabpanel'
                                             aria-labelledby='headingOne-1'>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                eiusmod tempor incididunt
                                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                nostrud exercitation
                                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                irure dolor in reprehenderit
                                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                Excepteur sint occaecat
                                                cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                anim id est laborum.
                                            </p>
                                            <p>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                accusantium doloremque
                                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi
                                                architecto beatae vitae dicta sunt explicabo.
                                            </p>
                                        </div>
                                        <div className='card-header' role='tab' id='headingOne-2'>
                                            <h3 className='mb-0'>
                                                <a data-toggle='collapse' data-parent='#accordion'
                                                   href='#collapseOne-2' aria-expanded='true'
                                                   aria-controls='collapseOne' className='collapsed'>
                                                    Can anyone create an account?
                                                    <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div id='collapseOne-2' className='collapse' role='tabpanel'
                                             aria-labelledby='headingOne-2'>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                eiusmod tempor incididunt
                                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                nostrud exercitation
                                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                irure dolor in reprehenderit
                                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                Excepteur sint occaecat
                                                cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                anim id est laborum.
                                            </p>
                                            <p>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                accusantium doloremque
                                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi
                                                architecto beatae vitae dicta sunt explicabo.
                                            </p>
                                        </div>
                                        <div className='card-header' role='tab' id='headingOne-3'>
                                            <h3 className='mb-0'>
                                                <a data-toggle='collapse' data-parent='#accordion'
                                                   href='#collapseOne-3' aria-expanded='true'
                                                   aria-controls='collapseOne-3' className='collapsed'>
                                                    Can I invite friends to join Olympus?
                                                    <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div id='collapseOne-3' className='collapse' role='tabpanel'
                                             aria-labelledby='headingOne'>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                eiusmod tempor incididunt
                                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                nostrud exercitation
                                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                irure dolor in reprehenderit
                                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                Excepteur sint occaecat
                                                cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                anim id est laborum.
                                            </p>
                                            <p>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                accusantium doloremque
                                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi
                                                architecto beatae vitae dicta sunt explicabo.
                                            </p>
                                        </div>
                                        <div className='card-header' role='tab' id='headingOne-4'>
                                            <h3 className='mb-0'>
                                                <a data-toggle='collapse' data-parent='#accordion'
                                                   href='#collapseOne-4' aria-expanded='true'
                                                   aria-controls='collapseOne' className='collapsed'>
                                                    Is there a referral system?
                                                    <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div id='collapseOne-4' className='collapse' role='tabpanel'
                                             aria-labelledby='headingOne'>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                eiusmod tempor incididunt
                                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                nostrud exercitation
                                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                irure dolor in reprehenderit
                                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                Excepteur sint occaecat
                                                cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                anim id est laborum.
                                            </p>
                                            <p>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                accusantium doloremque
                                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi
                                                architecto beatae vitae dicta sunt explicabo.
                                            </p>
                                        </div>
                                        <div className='card-header' role='tab' id='headingOne-5'>
                                            <h3 className='mb-0'>
                                                <a data-toggle='collapse' data-parent='#accordion'
                                                   href='#collapseOne-5' aria-expanded='true'
                                                   aria-controls='collapseOne-5' className='collapsed'>
                                                    How can I close my Olympus account?
                                                    <span className='icons-wrap'>
                      <svg className='olymp-plus-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon'/></svg>
                      <svg className='olymp-accordion-close-icon'><use
                          xlinkHref='svg-icons/sprites/icons.svg#olymp-accordion-close-icon'/></svg>
                    </span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div id='collapseOne-5' className='collapse' role='tabpanel'
                                             aria-labelledby='headingOne-5'>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                eiusmod tempor incididunt
                                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                nostrud exercitation
                                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                                irure dolor in reprehenderit
                                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                                Excepteur sint occaecat
                                                cupidatat non proident, sunt in culpa qui officia deserunt mollit
                                                anim id est laborum.
                                            </p>
                                            <p>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                accusantium doloremque
                                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi
                                                architecto beatae vitae dicta sunt explicabo.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {/* Footer Full Width */}
                <div className='footer footer-full-width' id='footer'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col col-lg-4 col-md-4 col-sm-6 col-6'>
                                {/* Widget About */}
                                <div className='widget w-about'>
                                    <a href='02-ProfilePage.html' className='logo'>
                                        <div className='img-wrap'>
                                            <img src='/img/logo-colored.png' alt='Olympus'/>
                                        </div>
                                        <div className='title-block'>
                                            <h6 className='logo-title'>olympus</h6>
                                            <div className='sub-title'>SOCIAL NETWORK</div>
                                        </div>
                                    </a>
                                    <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por
                                        incidid ut labore et lorem.</p>
                                    <ul className='socials'>
                                        <li>
                                            <a href='#'>
                                                <i className='fab fa-facebook-square' aria-hidden='true'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='#'>
                                                <i className='fab fa-twitter' aria-hidden='true'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='#'>
                                                <i className='fab fa-youtube' aria-hidden='true'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='#'>
                                                <i className='fab fa-google-plus-g' aria-hidden='true'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='#'>
                                                <i className='fab fa-instagram' aria-hidden='true'/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                {/* ... end Widget About */}
                            </div>
                            <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                {/* Widget List */}
                                <div className='widget w-list'>
                                    <h6 className='title'>Main Links</h6>
                                    <ul>
                                        <li>
                                            <a href='#'>Landing</a>
                                        </li>
                                        <li>
                                            <a href='#'>Home</a>
                                        </li>
                                        <li>
                                            <a href='#'>About</a>
                                        </li>
                                        <li>
                                            <a href='#'>Events</a>
                                        </li>
                                    </ul>
                                </div>
                                {/* ... end Widget List */}
                            </div>
                            <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                <div className='widget w-list'>
                                    <h6 className='title'>Your Profile</h6>
                                    <ul>
                                        <li>
                                            <a href='#'>Main Page</a>
                                        </li>
                                        <li>
                                            <a href='#'>About</a>
                                        </li>
                                        <li>
                                            <a href='#'>Friends</a>
                                        </li>
                                        <li>
                                            <a href='#'>Photos</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                <div className='widget w-list'>
                                    <h6 className='title'>Features</h6>
                                    <ul>
                                        <li>
                                            <a href='#'>Newsfeed</a>
                                        </li>
                                        <li>
                                            <a href='#'>Post Versions</a>
                                        </li>
                                        <li>
                                            <a href='#'>Messages</a>
                                        </li>
                                        <li>
                                            <a href='#'>Friend Groups</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className='col col-lg-2 col-md-4 col-sm-6 col-6'>
                                <div className='widget w-list'>
                                    <h6 className='title'>Olympus</h6>
                                    <ul>
                                        <li>
                                            <a href='#'>Privacy</a>
                                        </li>
                                        <li>
                                            <a href='#'>Terms &amp; Conditions</a>
                                        </li>
                                        <li>
                                            <a href='#'>Forums</a>
                                        </li>
                                        <li>
                                            <a href='#'>Statistics</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                {/* SUB Footer */}
                                <div className='sub-footer-copyright'>
            <span>
              Copyright <a href='index-2.html'>Olympus Buddypress + WP</a> All Rights Reserved 2017
            </span>
                                </div>
                                {/* ... end SUB Footer */}
                            </div>
                        </div>
                    </div>
                </div>
                {/* ... end Footer Full Width */}
                <a className='back-to-top' href='#'>
                    <img src='svg-icons/back-to-top.svg' alt='arrow' className='back-icon'/>
                </a>
            </div>}
         </>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IFAQsProps) => {
    return {}
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IFAQsProps) => {
    const isAuthed = state.getIn(['authorize', 'authed'], 0)
    return {
        isAuthed: isAuthed,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((FAQs as any) as any)
