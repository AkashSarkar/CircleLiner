// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

// - Import app components
import StreamComponent from 'containers/stream'
import InfoProfile from 'src/components/profile/infoProfile'
import PhotosProfileComponent from 'components/profile/photosProfile/PhotosProfileComponent'

// - Import API

// - Import actions
import * as postActions from 'src/store/actions/postActions'
import * as professionActions from 'src/store/actions/userActions/professionActions'
import * as educationActions from 'src/store/actions/userActions/educationActions'
import {IProfileComponentProps} from './IProfileComponentProps'
import {IProfileComponentState} from './IProfileComponentState'
import {Profile} from 'core/domain/users'
import ProfileViewCenter from 'layouts/views/profileView/center'
import {NotificationBoxComponent} from 'components/sugestionBox/notifications/NotificationBoxComponent'

/**
 * Create component class
 */
export class ProfileComponent extends Component<IProfileComponentProps, IProfileComponentState> {

    static propTypes = {}

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IProfileComponentProps) {
        super(props)

        // Defaul state
        this.state = {}

        // Binding functions to `this`

    }

    componentWillMount() {
        console.log('welcome profile')
        // this.props.loadPosts(),
        this.props.getEducationData(),
            this.props.getProfessionData()
        // this.props.loadUserIndustryInfo()
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        /**
         * Component styles
         */
        const styles = {
            profile: {},
            header: {},
            content: {},
            showcover: {
                height: '450px'
            },
            avatar: {
                border: '2px solid rgb(255, 255, 255)'
            }
        }
        const {loadPosts, hasMorePosts, translate, fullName, avatar} = this.props
        const St = StreamComponent as any
        const posts = Map(this.props.posts)
        return (
            <ProfileViewCenter userId={this.props.userId}>
                <div className='container'>
                    <div className='row'>
                        <div className='col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12'>
                            <NotificationBoxComponent userId={this.props.userId}/>

                            {posts
                                ? (<St
                                        fullName={fullName}
                                        avatar={avatar}
                                        posts={posts}
                                        loadStream={loadPosts}
                                        hasMorePosts={hasMorePosts}
                                        displayWriting={this.props.isAuthedUser}/>
                                )
                                : (<div className='profile__title'>
                                    {translate!('profile.nothingSharedLabel')}
                                </div>)
                            }

                        </div>
                        <div className='col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12'>
                            <InfoProfile
                                userId={this.props.userId}
                                professions={this.props.professions}
                                educations={this.props.educations}
                                isAuthed={this.props.isAuthedUser}
                            />
                        </div>
                        <div className='col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12'>
                            <PhotosProfileComponent userId={this.props.userId}/>
                        </div>
                    </div>
                </div>
            </ProfileViewCenter>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IProfileComponentProps) => {
    const {userId} = ownProps.match.params
    return {
        loadPosts: (page: number, limit: number) => dispatch(postActions.dbGetPostsByUserId(userId, page, limit)),
        getEducationData: () => dispatch(educationActions.dbGetUsersEducationInfoById(userId)),
        // loadUserInfo: () => dispatch(userActions.dbGetUserInfoByUserId(userId, 'header')),
        // loadUserIndustryInfo: () => dispatch(userActions.dbGetUserIndustryByUserId(userId)),
        getProfessionData: () => dispatch(professionActions.dbGetUserProfessionByUserId(userId))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IProfileComponentProps) => {
    const {userId} = ownProps.match.params
    const uid = state.getIn(['authorize', 'uid'], 0)
    const hasMorePosts = state.getIn(['post', 'profile', userId, 'hasMoreData'], true)
    const posts = state.getIn(['post', 'userPosts', userId])
    const userProfile = state.getIn(['user', 'info', userId], {}) as Profile
    const professions = state.getIn(['user', 'profession', userId])
    const education = state.getIn(['user', 'education', userId])
    return {
        translate: getTranslate(state.get('locale')),
        avatar: userProfile.avatar,
        fullName: userProfile.fullName,
        banner: userProfile.banner,
        tagLine: userProfile.tagLine,
        isAuthedUser: userId === uid,
        userId: userId,
        posts,
        hasMorePosts,
        professions: professions ? professions : [],
        educations: education ? education : [],

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(ProfileComponent as any)
