// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {NavLink, withRouter} from 'react-router-dom'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import Button from '@material-ui/core/Button'

// - Import actions
import * as authorizeActions from 'src/store/actions/authorizeActions'
import * as globalActions from 'src/store/actions/globalActions'

// - Import app API
import StringAPI from 'src/api/StringAPI'

import {ISignupComponentProps} from './ISignupComponentProps'
import {ISignupComponentState} from './ISignupComponentState'
import {UserRegisterModel} from 'src/models/users/userRegisterModel'

// - Create Signup component class
export class SignupComponent extends Component<ISignupComponentProps, ISignupComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISignupComponentProps) {
        super(props)

        this.state = {
            // fullNameInput: '',
            // fullNameInputError: '',
            firstNameInput: '',
            firstNameInputError: '',
            lastNameInput: '',
            lastNameInputError: '',
            emailInput: '',
            emailInputError: '',
            passwordInput: '',
            passwordInputError: '',
            confirmInput: '',
            confirmInputError: '',
            disabledSignupButton: true
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)

    }

    componentWillMount() {
        document.body.classList.add('landing-page')
    }

    componentWillUnmount() {
        // document.body.classList.remove('landing-page')
    }

    /**
     * Handle data on input change
     * @param  {event} evt is an event of inputs of element on change
     */
    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            // case 'fullNameInput':
            //     this.setState({
            //         fullNameInputError: ''
            //     })
            //     break
            case 'firstNameInput':
                this.setState({
                    firstNameInputError: '',
                })
                break
            case 'lastNameInput':
                this.setState({
                    lastNameInputError: ''
                })
                break
            case 'emailInput':
                this.setState({
                    emailInputError: ''
                })
                break
            case 'passwordInput':
                this.setState({
                    confirmInputError: '',
                    passwordInputError: ''
                })
                break
            case 'confirmInput':
                this.setState({
                    confirmInputError: '',
                    passwordInputError: ''
                })
                break
            case 'checkInput':
                if (value) {
                    this.setState({
                        disabledSignupButton: false
                    })
                } else {
                    this.setState({
                        disabledSignupButton: true
                    })
                }
                break
            default:
        }
    }

    /**
     * Handle register form
     */
    handleForm = () => {

        const {firstNameInput, lastNameInput, emailInput, passwordInput, confirmInput} = this.state
        const {register, translate} = this.props

        let error = false

        // // Validate full name
        // Validate first name
        let firstNameCheck = firstNameInput.trim().toLowerCase()

        if (firstNameCheck.indexOf('test') > -1
            || firstNameCheck.indexOf('demo') > -1
            || firstNameCheck.indexOf('asd') > -1
            || firstNameCheck.length < 4) {
            this.setState({
                firstNameInputError: translate!('signup.validNameError')
            })
            error = true
        }
        // Validate last name
        let lastNameCheck = lastNameInput.trim().toLowerCase()

        if (lastNameCheck.indexOf('test') > -1
            || lastNameCheck.indexOf('demo') > -1
            || lastNameCheck.indexOf('asd') > -1
            || lastNameCheck.length < 4) {
            this.setState({
                lastNameInputError: translate!('signup.validNameError')
            })
            error = true
        }

        /* Validate email*/
        if (!StringAPI.isValidEmail(emailInput)) {
            this.setState({
                emailInputError: translate!('signup.validEmailError')
            })
            error = true

        }

        /* Check password */
        if (passwordInput === '') {
            this.setState({
                passwordInputError: translate!('signup.passwordRequiredError')
            })
            error = true

        }
        if (confirmInput === '') {
            this.setState({
                confirmInputError: translate!('signup.confirmRequiredError')
            })
            error = true

        } else if (confirmInput !== passwordInput) {
            this.setState({
                passwordInputError: translate!('signup.passwordEqualConfirmError'),
                confirmInputError: translate!('signup.confirmEqualPasswordError')
            })
            error = true

        }

        if (!error) {
            register!({
                email: emailInput,
                password: passwordInput,
                fullName: firstNameInput + lastNameInput,
                firstName: firstNameInput,
                lastName: lastNameInput
            })
        }

    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        const {classes, translate} = this.props

        return (

            <form className='col col-12'>
                <div className='form-group'>
                    <input
                        style={{backgroundColor: '#fff',lineHeight: '0px'}}
                        className='form-control'
                        autoFocus
                        onChange={this.handleInputChange}
                        name='firstNameInput'
                        type='text'
                        placeholder={'First Name'}
                    />
                    <span style={{color: 'red'}}>
                                                            {this.state.firstNameInputError.trim() ? this.state.firstNameInputError : ''}
                                                        </span>
                </div>
                <div className='form-group'>
                    <input
                        style={{backgroundColor: '#fff',lineHeight: '0px'}}
                        className='form-control'
                        onChange={this.handleInputChange}
                        name='lastNameInput'
                        type='text'
                        placeholder={'Last Name'}
                    />
                    <span style={{color: 'red'}}>
                                                            {this.state.lastNameInputError.trim() ? this.state.lastNameInputError : ''}
                                                        </span>
                </div>
                <div className='form-group'>
                    <input
                        style={{backgroundColor: '#fff',lineHeight: '0px'}}
                        className='form-control'
                        onChange={this.handleInputChange}
                        name='emailInput'
                        type='email'
                        placeholder={'Email'}
                    />
                    <span style={{color: 'red'}}>
                                                            {this.state.emailInputError.trim() ? this.state.emailInputError : ''}
                                                        </span>
                </div>
                <div className='form-group'>
                    <input
                        style={{backgroundColor: '#fff',lineHeight: '0px'}}
                        className='form-control'
                        onChange={this.handleInputChange}
                        name='passwordInput'
                        type='password'
                        placeholder={'Password'}
                    />
                    <span style={{color: 'red'}}>
                                                            {this.state.passwordInputError.trim() ? this.state.passwordInputError : ''}
                                                        </span>
                </div>
                <div className='form-group'>
                    <input
                        style={{backgroundColor: '#fff',lineHeight: '0px'}}
                        className='form-control'
                        onChange={this.handleInputChange}
                        name='confirmInput'
                        type='password'
                        placeholder={'Confirm Password'}
                    />
                    <span style={{color: 'red'}}>
                                                            {this.state.confirmInputError.trim() ? this.state.confirmInputError : ''}
                                                        </span>
                </div>
                {/*<div className='form-group'>*/}
                {/*<input type='txt' className='form-control' id='birthday'*/}
                {/*placeholder='Date of Birth'*/}
                {/*required/>*/}
                {/*</div>*/}
                {/*<div className='form-row'>*/}
                {/*<div className='input-group col-md-6'>*/}
                {/*<select className='custom-select' id='inputGroupSelect04' required>*/}
                {/*<option selected>Gender</option>*/}
                {/*<option value={1}>Male</option>*/}
                {/*<option value={2}>Female</option>*/}
                {/*</select>*/}
                {/*</div>*/}
                {/*<div className='input-group col-md-6'>*/}
                {/*<select className='custom-select' id='inputGroupSelect04' required>*/}
                {/*<option selected>Field of Study</option>*/}
                {/*<option value={1}>HSC</option>*/}
                {/*<option value={2}>BBA</option>*/}
                {/*<option value={3}>MBA</option>*/}
                {/*</select>*/}
                {/*</div>*/}
                {/*</div>*/}
                <div className='checkbox'>
                    <label>
                        <input name='checkInput'
                               type='checkbox'
                               onChange={this.handleInputChange}/><span
                        className='checkbox-material'><span className='check'/></span>
                        I accept the <NavLink to={'/terms-and-conditions'}>Terms and Conditions</NavLink> of the
                        website
                    </label>
                </div>
                <br/>
                <Button className='btn btn-primary'
                        disabled={this.state.disabledSignupButton}
                        onClick={this.handleForm}>{translate!('signup.createButton')}</Button>
                {/*<button type='submit' className='btn btn-primary'>Sign Up</button>*/}
            </form>

        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISignupComponentProps) => {
    return {
        showError: (message: string) => {
            dispatch(globalActions.showMessage(message))
        },
        register: (userRegister: UserRegisterModel) => {
            dispatch(authorizeActions.dbSignup(userRegister))
        },
        loginPage: () => {
            dispatch(push('/login'))
        }
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: ISignupComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),
    }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)((SignupComponent as any) as any) as any)
