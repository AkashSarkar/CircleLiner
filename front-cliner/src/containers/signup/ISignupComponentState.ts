export interface ISignupComponentState {

    // /**
    //  * Full name input value
    //  *
    //  * @type {string}
    //  * @memberof ISignupComponentState
    //  */
    // fullNameInput: string
    //
    // /**
    //  * Full name input error text
    //  *
    //  * @type {string}
    //  * @memberof ISignupComponentState
    //  */
    // fullNameInputError: string
    /**
     * First name input value
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    firstNameInput: string

    /**
     * First name input error text
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    firstNameInputError: string

    /**
     * Last name input value
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    lastNameInput: string

    /**
     * Last name input error text
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    lastNameInputError: string

    /**
     * Email input value
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    emailInput: string

    /**
     * Email input error text
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    emailInputError: string

    /**
     * Password input value
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    passwordInput: string

    /**
     * Passwor input error text
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    passwordInputError: string

    /**
     * Confirm input value
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    confirmInput: string

    /**
     * Confirm input error text
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    confirmInputError: string

    /**
     * Checkbox input error text
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    checkInputError?: string

    /**
     * Signup button enable/disable
     *
     * @type {boolean}
     * @memberof ISignupComponentState
     */
    disabledSignupButton: boolean
}
