// - Import external components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink, withRouter} from 'react-router-dom'
import {push} from 'react-router-redux'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import RaisedButton from '@material-ui/core/Button'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Divider from '@material-ui/core/Divider'
import ActionAndroid from '@material-ui/icons/Android'
import {withStyles} from '@material-ui/core/styles'
import config from 'src/config'
import {localize} from 'react-localize-redux'

// - Import actions
import * as authorizeActions from 'src/store/actions/authorizeActions'
import {ILoginComponentProps} from './ILoginComponentProps'
import {ILoginComponentState} from './ILoginComponentState'
import {OAuthType} from 'src/core/domain/authorize'
import Grid from '@material-ui/core/Grid/Grid'
import CommonAPI from 'api/CommonAPI'
import SignupComponent from 'containers/signup'

// - Create Login component class
export class LoginComponent extends Component<ILoginComponentProps, ILoginComponentState> {

    styles = {
        logo: {
            width: '250px',
            marginTop: '70px',
            marginLeft: '45%',
        },
        singinOptions: {
            paddingBottom: 10,
            justifyContent: 'space-around',
            display: 'flex'
        },
        divider: {
            marginBottom: 10,
            marginTop: 15
        }
    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ILoginComponentProps) {
        super(props)
        this.state = {
            emailInput: '',
            emailInputError: '',
            passwordInput: '',
            passwordInputError: '',
            confirmInputError: '',
            authorized: ''
        }

        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)

    }

    componentWillMount() {
        document.body.classList.add('landing-page')
    }

    componentWillUnmount() {
        // document.body.classList.remove('landing-page')
    }

    /**
     * Handle data on input change
     * @param  {event} evt is an event of inputs of element on change
     */
    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'emailInput':
                this.setState({
                    emailInputError: ''
                })
                break
            case 'passwordInput':
                this.setState({
                    confirmInputError: '',
                    passwordInputError: ''
                })

                break
            default:
        }
    }

    /**
     * Handle register form
     */
    handleForm = () => {
        const {translate} = this.props
        let error = false
        console.log('login button clicked')
        if (this.state.emailInput === '') {
            this.setState({
                emailInputError: translate!('login.emailRequiredError')
            })
            error = true
        }
        if (this.state.passwordInput === '') {
            this.setState({
                passwordInputError: translate!('login.passwordRequiredError')
            })
            error = true
        }
        if (!error) {
            this.props.login!(
                this.state.emailInput,
                this.state.passwordInput
            )
        }

    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {classes, loginWithOAuth, translate, loginAttemptError} = this.props

        const OAuthLogin = (
            <div style={this.styles.singinOptions as any}>
                <IconButton
                    onClick={() => loginWithOAuth!(OAuthType.FACEBOOK)}
                >
                    <div className='icon-fb icon'></div>
                </IconButton>
                <IconButton
                    onClick={() => loginWithOAuth!(OAuthType.GOOGLE)}
                >
                    <div className='icon-google icon'></div>
                </IconButton>
                <IconButton
                    onClick={() => loginWithOAuth!(OAuthType.GITHUB)}
                >
                    <div className='icon-github icon'></div>
                </IconButton>

            </div>
        )
        return (
            <>
                <div className='content-bg-wrap'/>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col col-4'>
                            <div style={this.styles.logo}>
                                <img src='./img/logo.png'/>
                            </div>
                            <div className='homepage-text'>
                                <div className='quote-content'>
                                    <h2>Social Network</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                                </div>
                            </div>
                        </div>
                        <div className='col col-4'>
                            <div className='homepage-text2'>
                                <div className='quote-content2'>
                                    <h2>Social Network</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                </div>
                            </div>
                        </div>
                        <div className='col col-4'>
                            <div className='login-input'>
                                <form>
                                    <div className='form-row'>
                                        <div className='col-5'>
                                            <input
                                                style={{backgroundColor: '#fff',lineHeight: '0px'}}
                                                name='emailInput'
                                                onChange={this.handleInputChange}
                                                className='form-control'
                                                type='email'
                                            />
                                            <span style={{color: 'red'}}>
                                                            {this.state.emailInputError.trim() ? this.state.emailInputError : ''}
                                                        </span>
                                            {/*<input type='email' className='form-control' id='exampleInputEmail1'*/}
                                            {/*aria-describedby='emailHelp' placeholder='Enter email' required/>*/}
                                        </div>
                                        <div className='col-5'>
                                            <input
                                                style={{backgroundColor: '#fff',lineHeight: '0px'}}
                                                onChange={this.handleInputChange}
                                                className='form-control'
                                                type='password'
                                                name='passwordInput'
                                            />
                                            <span style={{color: 'red'}}>
                                                            {this.state.passwordInputError.trim() ? this.state.passwordInputError : '' ||
                                                            loginAttemptError ? 'Incorrect Email or Password.' : ''
                                                            }
                                                        </span>
                                            {/*<input type='password' className='form-control' id='exampleInputPassword1'*/}
                                            {/*placeholder='Password' required/>*/}
                                            <div className='forgot-pass'>
                                                <NavLink to='/resetPassword'>Forgot my Password</NavLink>
                                            </div>
                                        </div>
                                        <div className='col-2'>
                                            <a className='btn btn-primary'
                                               href={'javascript:(0)'}
                                               onClick={this.handleForm}>{translate!('login.loginButton')}</a>
                                            {/*<button type='submit' className='btn btn-primary'>Submit</button>*/}
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div className='signup-form'>
                                {config.settings.enabledOAuthLogin ? OAuthLogin : ''}
                                <div className='or' />
                                <h2>Sign up for free</h2>
                                <SignupComponent/>
                            </div>
                            <div className='terms'>
                                <p>By clicking Sign Up, you agree to our Terms, Data Policy and Cookie Policy. You may
                                    revieve SMS notifications from us and can opt out at any time.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ILoginComponentProps) => {
    return {
        login: (email: string, password: string) => {
            dispatch(authorizeActions.dbLogin(email, password))
        },
        loginWithOAuth: (type: OAuthType) => dispatch(authorizeActions.dbLoginWithOAuth(type)),
        signupPage: () => {
            dispatch(push('/signup'))
        }
    }
}

/**
 * Map state to props
 */
const mapStateToProps = (state: any, ownProps: ILoginComponentProps) => {
    // TODO:: solve undefined problem
    let authorized = state.getIn(['authorize', 'loginAttemptError'])
    return {
        loginAttemptError: authorized ? authorized : ''
    }
}

// - Connect component to redux store
export default withRouter<any>(connect(mapStateToProps, mapDispatchToProps)((localize(LoginComponent, 'locale', CommonAPI.getStateSlice) as any) as any)) as typeof LoginComponent
