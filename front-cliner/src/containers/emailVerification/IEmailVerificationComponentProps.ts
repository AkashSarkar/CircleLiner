export interface IEmailVerificationComponentProps {

    /**
     * Send email for verification
     */
    sendEmailVerification: (email: string) => any

    /**
     * Redirect to home page
     */
    homePage: () => any
    isVerified: boolean

    /**
     * Styles
     */
    classes?: any
    match?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any, param?: {}) => any
}
