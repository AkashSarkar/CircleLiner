// - Import external components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink, withRouter} from 'react-router-dom'
import {push} from 'react-router-redux'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import RaisedButton from '@material-ui/core/Button'
import Button from '@material-ui/core/Button'
import config from 'src/config'
import {withStyles} from '@material-ui/core/styles'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import actions
import * as authorizeActions from 'src/store/actions/authorizeActions'
import {IEmailVerificationComponentProps} from './IEmailVerificationComponentProps'
import {IEmailVerificationComponentState} from './IEmailVerificationComponentState'
import {Grid} from '@material-ui/core'

const styles = (theme: any) => ({
    textField: {
        minWidth: 280,
        marginTop: 20

    },
    contain: {
        margin: '0 auto'
    },
    paper: {
        minHeight: 370,
        maxWidth: 450,
        minWidth: 337,
        textAlign: 'center',
        display: 'block',
        margin: 'auto'
    }
})

/**
 * Create component class
 *
 * @export
 * @class EmailVerificationComponent
 * @extends {Component}
 */
export class EmailVerificationComponent extends Component<IEmailVerificationComponentProps, IEmailVerificationComponentState> {

    styles = {
        message: {
            fontWeight: 400
        },
        buttons: {
            marginTop: 60
        },
        buttontextblack: {
            fontColor: 'black'
        },
        homeButton: {
            marginRight: 10
        },
        contain: {
            margin: '0 auto'
        },
        paper: {
            minHeight: 370,
            maxWidth: 450,
            minWidth: 337,
            textAlign: 'center',
            display: 'block',
            margin: 'auto'
        }

    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IEmailVerificationComponentProps) {
        super(props)

        // Binding function to `this`

    }

    sendMail = () => {
        const {email} = this.props.match.params
        console.log(this.props.match.search)
        this.props.sendEmailVerification(email)

    }

    componentWillMount() {
        const {isVerified} = this.props
        if (isVerified) {
            this.props.homePage()
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate, classes} = this.props
        return (
            <>
                {/* Header Standard Landing  */}
                <div className='header--standard header--standard-landing' id='header--standard'>
                    <div className='container'>
                        <div className='header--standard-wrap'>
                            <a href={'javascript:(0)'} className='logo'>
                                <div className='img-wrap'>
                                    <img src='/img/logo.png' alt='Olympus'/>
                                    <img src='/img/logo-colored-small.png' alt='Olympus' className='logo-colored'/>
                                </div>
                                <div className='title-block'>
                                    <h6 className='logo-title'>{config.settings.appName}</h6>
                                    <div className='sub-title'>Professional Network</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                {/* ... end Header Standard Landing  */}
                <div className='main-header-post'>
                    <img src='/img/top-header5.png' alt='author'/>
                </div>
                <div className='container negative-margin-top150 mb60'>
                    <div className='row'>
                        <div className='col col-xl-8 m-auto col-lg-12 col-md-12 col-sm-12 col-12'>
                            <div className='ui-block'>
                                {/* Single Post */}
                                <article className='hentry blog-post single-post single-post-v1'>
                                    <div style={{
                                        paddingLeft: '40px',
                                        paddingRight: '40px'
                                    }}>
                                        <h2 className='zoomOutLCorner animated g__paper-title'>{translate!('emailVerification.title')}</h2>
                                    </div>
                                    <p style={this.styles.message as any}>
                                        {translate!('emailVerification.description')}
                                    </p>
                                    <div style={this.styles.buttons}>
                                        <input type='email' placeholder='email'/>
                                        <br/>
                                        {/*<Button variant='raised' style={this.styles.homeButton} color='primary' onClick={() => this.props.homePage()}> {translate!('emailVerification.homeButton')} </Button>*/}
                                        <Button variant='raised' color='secondary'
                                                onClick={this.sendMail}> {translate!('emailVerification.sendButton')} </Button>
                                    </div>
                                </article>
                                {/* ... end Single Post */}
                            </div>
                        </div>
                    </div>
                </div>
            </>

        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: Function, ownProps: IEmailVerificationComponentProps) => {
    return {
        homePage: () => {
            dispatch(push('/'))
        },
        sendEmailVerification: (email: string) => dispatch(authorizeActions.dbSendEmailVerfication(email, ''))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: IEmailVerificationComponentProps) => {
    let isVerifired = state.getIn(['authorize', 'isVerified'])
    return {
        translate: getTranslate(state.get('locale')),
        isVerified: isVerifired
    }
}

// - Connect component to redux store
export default withRouter<any>(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(EmailVerificationComponent as any) as any)) as typeof EmailVerificationComponent
