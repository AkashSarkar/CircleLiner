export interface IPeopleComponentProps {

    /**
     * Router match
     *
     * @type {*}
     * @memberof IPeopleComponentProps
     */
    match?: any

    /**
     * Circles loaded {true} or not {false}
     *
     * @type {boolean}
     * @memberof IPeopleComponentProps
     */
    circlesLoaded?: boolean

    /**
     * Rediret to another route
     *
     * @memberof IPeopleComponentProps
     */
    goTo?: (url: string) => any

    /**
     * Set title of top bar
     *
     * @memberof IPeopleComponentProps
     */
    setHeaderTitle?: (title: string) => any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    name: string

    /**
     * User tag line
     *
     * @type {string}
     * @memberof IProfileComponentProps
     */
    tagLine: string

    /**
     * User's avatar address
     *
     * @type {string}
     * @memberof IProfileComponentProps
     */
    avatar: string

    /**
     * It's current user profile {true} or not {false}
     *
     * @type {boolean}
     * @memberof IProfileComponentProps
     */
    isAuthedUser: boolean

    /**
     * User's banner
     *
     * @type {string}
     * @memberof IProfileComponentProps
     */
    banner: string

    /**
     * User identifier
     *
     * @type {string}
     * @memberof IProfileComponentProps
     */
    userId: string
}
