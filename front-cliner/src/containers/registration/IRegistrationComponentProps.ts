import {Industry} from 'core/domain/users/industry'
import {Map} from 'immutable'
import {ConcentratedIndustry} from 'core/domain/users'

export interface IRegistrationComponentProps {
    /**
     * show industry
     *
     * @memberof IPostWriteComponentProps
     */
    industryName?: string
    /**
     * show industry
     *
     * @memberof IPostWriteComponentProps
     */
    concentratedIndustryName?: string
    /**
     * Save industry
     *
     * @memberof IPostWriteComponentProps
     */
    addIndustry?: (add: Industry) => any

    /**
     * Save ConcentratedIndustry
     *
     * @memberof IPostWriteComponentProps
     */
    /**
     * Styles
     */
    classes?: any
    /**
     * Router match
     *
     * @type {*}
     * @memberof IProfileComponentProps
     */
    match: any

    /**
     * Translate to locale string
     */
    translate?: (state: any, param?: {}) => any

    userinfo?: () => any

    authed?: boolean

    onRequestClose: () => void
    goTo?: (url: string) => any
}
