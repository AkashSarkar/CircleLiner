export interface IRegistrationComponentState {

    /**
     * industry input value
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    industryName?: string
    /**
     * concentratedindustry input value
     *
     * @type {string}
     * @memberof ISignupComponentState
     */
    concentratedIndustryName?: string

    /**
     * If it's true post button will be disabled
     */
    disabledButton: boolean
}
