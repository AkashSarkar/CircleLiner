// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {NavLink, withRouter} from 'react-router-dom'

import {withStyles} from '@material-ui/core/styles'

import {IEducationComponentProps} from './IEducationComponentProps'
import {IEducationComponentState} from './IEducationComponentState'
import {getTranslate} from 'react-localize-redux'
import * as userActions from 'store/actions/userActions/userActions'
import {Education} from 'core/domain/users'
import config from 'src/config'
import Button from '@material-ui/core/Button'

const styles = (theme: any) => ({
    textField: {
        minWidth: 280,
        marginTop: 20

    },
    contain: {
        margin: '0 auto'
    },
    paper: {
        minHeight: 370,
        maxWidth: 450,
        minWidth: 337,
        textAlign: 'center',
        display: 'block',
        margin: 'auto'
    }
})

// - Create Signup component class
export class EducationComponent extends Component<IEducationComponentProps, IEducationComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IEducationComponentProps) {
        super(props)
        this.state = {
            instituteName: '',
            result: '',
            passingYear: ''
        }
        // Binding function to `this`
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleForm = this.handleForm.bind(this)
        this.redirect = this.redirect.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })

        switch (name) {
            case 'fieldOfStudy':
                this.setState({
                    fieldOfStudy: value,
                })
                break
            case 'instituteName':
                this.setState({
                    instituteName: value,
                })
                break
            case 'degreeTitle':
                this.setState({
                    degreeTitle: value,
                })
                break
            case 'result':
                this.setState({
                    result: value
                })
                break
            case 'passingYear':
                this.setState({
                    passingYear: value,
                })
                break
            case 'currentlyStudying':
                this.setState({
                    currentlyStudying: 'continuing'
                })
                break
            default:

        }

    }

    /**
     * Handle register form
     */
    handleForm = (event: any) => {
        const {
            fieldOfStudy,
            degreeTitle,
            instituteName,
            result,
            passingYear,
            currentlyStudying
        } = this.state
        const {addEducation, classes, translate, onRequestClose} = this.props

        let error = false

        if (!error) {
            addEducation!({
                fieldOfStudy: fieldOfStudy,
                instituteName: instituteName,
                degreeTitle: degreeTitle,
                result: result,
                passingYear: passingYear,
                currentlyStudying: currentlyStudying,
            })
        }
    }
    redirect = () => {
        this.props.goTo!('/')
    }

    skip = () => {
        this.props.goTo!('/registration/location')
    }

    componentWillMount() {
        const {userinfo} = this.props
        userinfo!()
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        const {classes, translate} = this.props

        return (
            <>
                <div className='header--standard header--standard-landing' id='header--standard'>
                    <div className='container'>
                        <div className='header--standard-wrap'>
                            <a href={'javascript:(0)'} className='logo'>
                                <div className='img-wrap'>
                                    {/*<img src='/img/logo.png' alt='Olympus'/>*/}
                                    {/*<img src='/img/logo-colored-small.png' alt='Olympus' className='logo-colored'/>*/}
                                </div>
                                <div className='title-block'>
                                    <h6 className='logo-title'>{config.settings.appName}</h6>
                                    <div className='sub-title'>Professional Network</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                {/* ... end Header Standard Landing  */}
                <div className='main-header-post'>
                    <img src='/img/top-header3.png' alt='author'/>
                </div>
                <div className='container negative-margin-top150 mb60'>
                    <div className='row'>
                        <div className='col col-xl-8 m-auto col-lg-12 col-md-12 col-sm-12 col-12'>
                            <div className='ui-block'>
                                <article className='hentry blog-post single-post single-post-v1'>
                                    <div style={{
                                        paddingLeft: '40px',
                                        paddingRight: '40px'
                                    }}>
                                        <h2 className='zoomOutLCorner animated g__paper-title'>Few more steps to
                                            go..</h2>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Field of study*</label>
                                        <input className='form-control'
                                               type='text'
                                               onChange={this.handleInputChange}
                                               name='fieldOfStudy'
                                               defaultValue=''/>
                                        <span className='material-input'/>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Institute Name*</label>
                                        <input className='form-control'
                                               name={'instituteName'}
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue={''}/>
                                        <span className='material-input'/>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Exam/Degree Title (Optional)</label>
                                        <input className='form-control'
                                               name={'degreeTitle'}
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue={''}/>
                                        <span className='material-input'/>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Result (Optional)</label>
                                        <input className='form-control'
                                               name={'result'}
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue={''}/>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Year of Passing</label>
                                        <input className='form-control'
                                               name={'passingYear'}
                                               onChange={this.handleInputChange}
                                               defaultValue={''}
                                               type='text'/>
                                        <span className='material-input'/>
                                    </div>
                                    <div className='checkbox ml-3'>
                                        <label>
                                            <input
                                                onChange={this.handleInputChange}
                                                name='currentlyStudying'
                                                type='checkbox'/>
                                            <span className='checkbox-material'>
                                        <span className='check'/></span>
                                            I currently study here.
                                        </label>
                                    </div>

                                    <Button variant='raised'
                                            style={{float: 'left'}}
                                            color='secondary'
                                            onClick={this.redirect}> -> Go to home</Button>
                                    <Button variant='raised'
                                            style={{float: 'right'}}
                                            color='secondary'
                                            onClick={this.handleForm}>Next</Button>
                                    <Button variant='raised'
                                            className='mr-2'
                                            style={{float: 'right'}}
                                            color='secondary'
                                            onClick={this.skip}>Skip</Button>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </>
    )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IEducationComponentProps) => {
    const {userId} = ownProps.match.params
    return {
        addEducation: (add: Education) => dispatch(userActions.dbAddUserEducation(add)),
        userinfo: () => dispatch(userActions.dbGetUserInfo()),
        goTo: (url: string) => dispatch(push(url)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: IEducationComponentProps) => {
    const {userId} = ownProps.match.params
    const uid = state.getIn(['authorize', 'uid'])
    const user = state.getIn(['user', 'info', uid], 0)
    return {
        translate: getTranslate(state.get('locale')),
        ownerAvatar: user.avatar || '',
        ownerDisplayName: user.fullName || ''
    }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(EducationComponent as any) as any) as any)
