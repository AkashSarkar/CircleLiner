export interface IEducationComponentState {
    fieldOfStudy?: string | null
    instituteName: string | null
    degreeTitle?: string | null
    result: string | null
    passingYear: string | null
    currentlyStudying?: string | null

}
