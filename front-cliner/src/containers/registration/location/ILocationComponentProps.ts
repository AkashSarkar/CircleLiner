import {Industry} from 'core/domain/users/industry'
import {Map} from 'immutable'
import {Education} from 'core/domain/users'

export interface ILocationComponentProps {
    location?: string | null
    userId?: string
    addEducation?: (add: Education) => any

    classes?: any

    match: any

    translate?: (state: any, param?: {}) => any

    userinfo?: () => any

    authed?: boolean
    onRequestClose: () => void
    goTo?: (url: string) => any
}