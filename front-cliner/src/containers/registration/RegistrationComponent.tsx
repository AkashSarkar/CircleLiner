// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {NavLink, withRouter} from 'react-router-dom'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import RaisedButton from '@material-ui/core/Button'
import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles'
import config from 'src/config'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import actions
import * as userActions from 'src/store/actions/userActions/userActions'
import * as globalActions from 'src/store/actions/globalActions'

// - Import app API
import StringAPI from 'src/api/StringAPI'

import {IRegistrationComponentProps} from './IRegistrationComponentProps'
import {IRegistrationComponentState} from './IRegistrationComponentState'
import {UserRegisterModel} from 'src/models/users/userRegisterModel'
import {Grid} from '@material-ui/core'
import {Industry} from 'core/domain/users/industry'
import * as postActions from 'store/actions/postActions'

const styles = (theme: any) => ({
    textField: {
        minWidth: 280,
        marginTop: 20

    },
    contain: {
        margin: '0 auto'
    },
    paper: {
        minHeight: 370,
        maxWidth: 450,
        minWidth: 337,
        textAlign: 'center',
        display: 'block',
        margin: 'auto'
    }
})

// - Create Signup component class
export class SignupComponent extends Component<IRegistrationComponentProps, IRegistrationComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IRegistrationComponentProps) {
        super(props)

        this.state = {
            industryName: props.industryName ? props.industryName : '',
            concentratedIndustryName: props.concentratedIndustryName ? props.concentratedIndustryName : '',
            disabledButton: true
        }
        // Binding function to `this`
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleForm = this.handleForm.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
        if (name === 'industryName') {
            if (value.length === 0 || value.trim() === '') {
                this.setState({
                    industryName: value,
                    disabledButton: true
                })
            } else {
                switch (name) {
                    case 'industryName':
                        this.setState({
                            industryName: value,
                            disabledButton: false
                        })
                        break
                    case 'concentratedIndustryName':
                        this.setState({
                            concentratedIndustryName: value,
                            disabledButton: false
                        })
                        break
                    default:
                        this.setState({
                            disabledButton: true
                        })
                }
            }
        }

    }

    /**
     * Handle register form
     */
    handleForm = (event: any) => {

        const {industryName, concentratedIndustryName} = this.state
        const {addIndustry, classes, translate, onRequestClose} = this.props

        let error = false

        if (!error) {
            addIndustry!({
                'industryName': industryName!,
                'concentratedIndustryName': concentratedIndustryName!,
            })
        }
    }

    componentWillMount() {
        const {userinfo} = this.props
        userinfo!()
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        const {classes, translate} = this.props

        return (
            <>
                <div className='header--standard header--standard-landing' id='header--standard'>
                    <div className='container'>
                        <div className='header--standard-wrap'>
                            <a href={'javascript:(0)'} className='logo'>
                                <div className='img-wrap'>
                                    <img src='/img/logo.png' alt='Olympus'/>
                                    <img src='/img/logo-colored-small.png' alt='Olympus' className='logo-colored'/>
                                </div>
                                <div className='title-block'>
                                    <h6 className='logo-title'>{config.settings.appName}</h6>
                                    <div className='sub-title'>Professional Network</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                {/* ... end Header Standard Landing  */}
                <div className='main-header-post'>
                    <img src='/img/top-header3.png' alt='author'/>
                </div>
                <div className='container negative-margin-top150 mb60'>
                    <div className='row'>
                        <div className='col col-xl-8 m-auto col-lg-12 col-md-12 col-sm-12 col-12'>
                            <div className='ui-block'>
                                <article className='hentry blog-post single-post single-post-v1'>
                                    <div style={{
                                        paddingLeft: '40px',
                                        paddingRight: '40px'
                                    }}>
                                    <h2 className='zoomOutLCorner animated g__paper-title'>Step 1</h2>
                                    </div>
                                    <div className='form-group label-floating is-empty'>
                                        <label className='control-label'>Choose you profession*</label>
                                        <input
                                            className='form-control'
                                            autoFocus
                                            onChange={this.handleInputChange}
                                            name='industryName'
                                            type='text'
                                            value={this.state.industryName}
                                        />
                                    </div>
                                    <div className='form-group label-floating is-empty'>
                                        <label className='control-label'>I'm specialized in...</label>
                                        <input
                                            className='form-control'
                                            onChange={this.handleInputChange}
                                            name='concentratedIndustryName'
                                            type='text'
                                            value={this.state.concentratedIndustryName}
                                        />
                                    </div>
                                    <Button style={{ float: 'right'}}
                                            variant='raised'
                                            color='secondary'
                                            disabled={this.state.disabledButton}
                                            onClick={this.handleForm}>Next</Button>
                                    {/*<a  onClick={this.handleForm} className='btn btn-secondary'>Complete Registration</a>*/}
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IRegistrationComponentProps) => {
    const {userId} = ownProps.match.params
    return {
        addIndustry: (add: Industry) => dispatch(userActions.dbAddUserIndustry(add)),
        userinfo: () => dispatch(userActions.dbGetUserInfo()),
        // goTo: (url: string) => dispatch(push(url)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: IRegistrationComponentProps) => {
    const {userId} = ownProps.match.params
    const uid = state.getIn(['authorize', 'uid'])
    const user = state.getIn(['user', 'info', uid], 0)
    return {
        translate: getTranslate(state.get('locale')),
        ownerAvatar: user.avatar || '',
        ownerDisplayName: user.fullName || '',
        industryName: user.industryName,
        concentratedIndustryName: user.concentratedIndustryName,
    }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(SignupComponent as any) as any) as any)
