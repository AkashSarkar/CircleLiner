import {Post} from 'src/core/domain/posts'

export interface IPhotosComponetProps {
    /**
     * fullname
     */
     fullName: string
    /**
     * Router match
     *
     * @type {*}
     * @memberof IProfileComponentProps
     */
     match: any
    /**
     * User identifier
     *
     * @type {string}
     * @memberof IProfileComponentProps
     */
     userId: string
    /**
     * user photo lists
     */
     images: any
    /**
     * Translate to locale string
     */
     translate?: (state: any, params?: {}) => any
}
