// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

// - Import app components
import ProfileHeader from 'layouts/profileHeader'
import CreateAlbumBar from 'src/components/createAlbumBar'
import Album from 'src/components/profile/album'
import ProfileViewCenter from 'layouts/views/profileView/center'
import PhotoListComponent from 'components/photoList'

// - Import API

// - Import actions

import {IPhotosComponetProps} from './IPhotosComponetProps'
import {IPhotosComponentState} from './IPhotosComponentState'
import {Profile} from 'core/domain/users'

/**
 * Create component class
 */
export class PhotosComponent extends Component<IPhotosComponetProps, IPhotosComponentState> {

    constructor(props: IPhotosComponetProps) {
        super(props)
    }

    photoList = () => {
        const item: any[] = []
        this.props.images.map((image: any) => {
            item.push(
                <PhotoListComponent
                    key={image.id}
                    URL={image.URL}
                    CreationTime={image.creationDate}
                    Score={image.score}
                />
            )
            // console.log(image)

        })
        return item
    }
    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <ProfileViewCenter userId={this.props.userId}>
                <CreateAlbumBar fullName={this.props.fullName}/>
                <div className='container'>
                    <div className='row'>
                        <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                            {/* Tab panes */}
                            <div className='tab-content'>
                                <div className='tab-pane active' id='photo-page' role='tabpanel'>
                                    <div className='photo-album-wrapper'>
                                        {/*<div className='photo-item half-width'>*/}
                                        {/*<img src='https://cdn.pixabay.com/photo/2017/05/09/21/49/gecko-2299365_960_720.jpg' alt='photo'/>*/}
                                        {/*<div className='overlay overlay-dark'/>*/}
                                        {/*<a href={'javascript:(0)'} className='more'>*/}
                                        {/*<svg className='olymp-three-dots-icon'>*/}
                                        {/*<use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>*/}
                                        {/*</svg>*/}
                                        {/*</a>*/}
                                        {/*<a href={'javascript:(0)'} className='post-add-icon inline-items'>*/}
                                        {/*<svg className='olymp-heart-icon'>*/}
                                        {/*<use xlinkHref='/svg-icons/sprites/icons.svg#olymp-heart-icon'/>*/}
                                        {/*</svg>*/}
                                        {/*<span>15</span>*/}
                                        {/*</a>*/}
                                        {/*<a href={'javascript:(0)'} data-toggle='modal' data-target='#open-photo-popup-v1'*/}
                                        {/*className='  full-block'/>*/}
                                        {/*<div className='content'>*/}
                                        {/*<a href={'javascript:(0)'} className='h6 title'>Header Photos</a>*/}
                                        {/*<time className='published' dateTime='2017-03-24T18:18'>1 week ago*/}
                                        {/*</time>*/}
                                        {/*</div>*/}
                                        {/*</div>*/}
                                        {this.photoList()}
                                        <a href={'javascript:(0)'} className='btn btn-control btn-more'>
                                            <svg className='olymp-three-dots-icon'>
                                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                                <div className='tab-pane' id='album-page' role='tabpanel'>
                                <div className='photo-album-wrapper'>
                                <div className='photo-album-item-wrap col-4-width'>
                                <div className='photo-album-item create-album' data-mh='album-item'>
                                <a href={'javascript:(0)'} data-toggle='modal' data-target='#create-photo-album'
                                className='  full-block'/>
                                <div className='content'>
                                <a href={'javascript:(0)'} className='btn btn-control bg-primary'
                                data-toggle='modal' data-target='#create-photo-album'>
                                <svg className='olymp-plus-icon'>
                                <use
                                xlinkHref='/svg-icons/sprites/icons.svg#olymp-plus-icon'/>
                                </svg>
                                </a>
                                <a href={'javascript:(0)'} className='title h5' data-toggle='modal'
                                data-target='#create-photo-album'>Create an Album</a>
                                <span className='sub-title'>It only takes a few minutes!</span>
                                </div>
                                </div>
                                </div>
                                {/*<Album />*/}
                                </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </ProfileViewCenter>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPhotosComponetProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPhotosComponetProps) => {
    const {userId} = ownProps.match.params
    const user = state.getIn(['user', 'info', userId])
    const images = state.getIn(['imageGallery', 'images'])
    // console.log('images', images)
    return {
        userId,
        fullName: user.fullName,
        images: images ? images : []
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(PhotosComponent as any)
