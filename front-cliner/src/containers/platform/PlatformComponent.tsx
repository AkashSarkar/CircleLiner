// - Import react components
import {HomeRouter} from 'routes'
import {Map} from 'immutable'
import React, {Component} from 'react'
import {Route, Switch, withRouter, Redirect, NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import app components
import HomeView from 'layouts/views/homeView'

// - Import actions

import {IPlatformComponentProps} from './IPlatformComponentProps'
import {IPlatformComponentState} from './IPlatformComponentState'
import StreamComponent from 'containers/stream'
import * as platformActions from 'store/actions/platformActions'

// - Create Home component class
export class PlatformComponent extends Component<IPlatformComponentProps, IPlatformComponentState> {

    // Constructor
    constructor(props: IPlatformComponentProps) {
        super(props)

        // Default state
        this.state = {
            drawerOpen: false
        }
        // Binding function to `this`
    }

    /**
     * Handle drawer toggle
     */
    handleDrawerToggle = () => {
        this.setState({drawerOpen: !this.state.drawerOpen})
    }

    componentWillMount() {
    }

    /**
     * Render DOM component
     *
     * @returns DOM
     *
     * @memberof Home
     */
    render() {
        const {loadPosts, hasMorePosts, translate, fullName, avatar} = this.props
        const St = StreamComponent as any
        const posts = Map(this.props.posts)
        return (
            <HomeView>
                <div className='container'>
                    <div className='row'>
                        <div
                            className='col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12'>
                            <St
                                fullName={fullName}
                                avatar={avatar}
                                posts={posts}
                                loadStream={loadPosts}
                                hasMorePosts={hasMorePosts}
                                displayWriting={false}/>
                        </div>
                        <div className='col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12'>
                        </div>
                        <div className='col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12'>
                        </div>
                    </div>
                </div>
            </HomeView>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IPlatformComponentProps) => {
    const {pltId} = ownProps.match.params
    return {
        loadPosts: (page: number, limit: number) => dispatch(platformActions.dbGetPltPosts(pltId, page, limit)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPlatformComponentProps) => {
    const {pltId} = ownProps.match.params
    const uid = state.getIn(['authorize', 'uid'], {})
    const global = state.get('global', {})
    const hasMorePosts = state.getIn(['platform', pltId, 'stream', 'hasMoreData'], true)
    const posts = state.getIn(['platform', pltId, 'posts'])
    return {
        authed: state.getIn(['authorize', 'authed'], false),
        isVerified: state.getIn(['authorize', 'isVerified'], false),
        translate: getTranslate(state.get('locale')),
        currentLanguage: getActiveLanguage(state.get('locale')).code,
        posts,
        hasMorePosts,
    }
}

// - Connect component to redux store
export default withRouter<any>(connect(mapStateToProps, mapDispatchToProps)((PlatformComponent as any) as any)) as typeof PlatformComponent
