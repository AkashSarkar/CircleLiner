export interface IPlatformComponentState {

    /**
     * Whether drawer is open
     */
    drawerOpen: boolean
}
