import {Post} from 'core/domain/posts'

export interface IPlatformComponentProps {
    /**
     * If there is more posts to show in profile
     */
    hasMorePosts: boolean
    /**
     * Load user's post
     *
     * @memberof IProfileComponentProps
     */
    loadPosts: () => any
    /**
     * User's avatar address
     *
     * @type {string}
     * @memberof IProfileComponentProps
     */
    avatar: string
    /**
     * String user full name
     *
     * @type {string}
     * @memberof IProfileComponentProps
     */
    fullName: string
    /**
     * It's current user profile {true} or not {false}
     *
     * @type {boolean}
     * @memberof IProfileComponentProps
     */
    isAuthedUser: boolean

    /**
     * If user email is verifide {true} or not {false}
     *
     * @type {boolean}
     * @memberof IPlatformComponentProps
     */
    isVerified?: boolean

    /**
     * User identifier
     *
     * @type {string}
     * @memberof IPlatformComponentProps
     */
    pltId?: string

    /**
     * User identifier
     *
     * @type {string}
     * @memberof IPlatformComponentProps
     */
    uid?: string

    /**
     * Global state
     *
     * @type {*}
     * @memberof IPlatformComponentProps
     */
    global?: any

    /**
     * Translate locale to string
     */
    translate?: (state: any) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Theme
     */
    theme?: any

    /**
     * Router match
     *
     * @type {*}
     * @memberof IPlatformComponentProps
     */
    match?: any
    /**
     * User's post
     *
     * @type {{[postId: string]: Post}}
     * @memberof IProfileComponentProps
     */
    posts: { [postId: string]: Post }

}
