export interface IAboutComponentState {
    /**
     * Whether drawer is open
     */
    drawerOpen: boolean
    
}