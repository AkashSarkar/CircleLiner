// - Import react components
import {HomeRouter} from 'routes'
import {Map} from 'immutable'
import React, {Component} from 'react'
import {Route, Switch, withRouter, Redirect, NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import app components
import HomeView from 'layouts/views/homeView'

import {IJobComponentProps} from './IJobComponentProps'
import {IJobComponentState} from './IJobComponentState'
import JobLeftMenuComponent from 'components/job/leftMenu'
import JobStremComponent from 'components/job/stream'

const Container = (props: any) => {
    return (
        props.children
    )
}

// - Create Home component class
export class JobComponent extends Component<IJobComponentProps, IJobComponentState> {

    // Constructor
    constructor(props: IJobComponentProps) {
        super(props)
        const {type} = this.props.match.params
        // Default state
        this.state = {
            typeIndex: this.getTypeIndexByNav(type)
        }
        // Binding function to `this`
    }

    /**
     * Render DOM component
     *
     * @returns DOM
     *
     * @memberof Home
     */
    render() {
        const {jobsLoaded, data, translate} = this.props
        const {typeIndex} = this.state
        return (
            <HomeView>
                <div className='container'>
                    <div className='row'>
                        <div
                            className='col col-xl-9 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12'>
                            {/* Search form */}
                            <div className='input-group md-form form-sm form-1 pl-0'
                                 style={{backgroundColor: 'white'}}>
                                <div className='input-group-prepend'>
                                    <span className='input-group-text cyan lighten-2' id='basic-text1'><i
                                        className='fa fa-search text-white' aria-hidden='true'/></span>
                                </div>
                                <input className='form-control my-0 py-1' type='text' placeholder='Search'
                                       aria-label='Search'/>
                            </div>
                            <br/>
                            <JobStremComponent jobs={data.mergedPosts} hasMorePosts={data.hasMorePosts}
                                               loadStream={data.loadDataStream}/>
                            {/*{typeIndex === '' &&*/}
                            {/*<Container>*/}
                            {/*{jobsLoaded ? <JobStremComponent jobs={data.jobs}*/}
                            {/*loadStream={data.loadDataStream}/> : ''}*/}
                            {/*</Container>}*/}

                            {/*{typeIndex === '' && <Container>*/}
                            {/*{jobsLoaded ?*/}
                            {/*<JobStremComponent jobs={data.jobs} loadStream={data.loadDataStream}/> : ''}*/}
                            {/*{jobsLoaded ?*/}
                            {/*<JobStremComponent jobs={data.jobs} loadStream={data.loadDataStream}/> : ''}*/}
                            {/*</Container>}*/}
                            {/*{typeIndex === '' &&*/}
                            {/*<Container>{jobsLoaded ? <JobStremComponent jobs={data.jobs}*/}
                            {/*loadStream={data.loadDataStream}/> : ''}</Container>}*/}
                        </div>
                        <div
                            className='col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12'>
                            <div className='crumina-sticky-sidebar'>
                                <div className='sidebar__inner'>
                                    <JobLeftMenuComponent/>
                                </div>
                            </div>
                        </div>
                        <div
                            className='col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12'>

                        </div>
                    </div>
                </div>
            </HomeView>
        )
    }

    /**
     * Get tab index by navigation name
     */
    private getTypeIndexByNav: (navName: string) => string = (navName: string) => {
        switch (navName) {
            case 'recommended':
                return 'recommended'
            case 'approaches':
                return 'approaches'
            case 'saved-jobs':
                return 'saved'
            case 'posted-jobs':
                return 'posted'
            case 'searched-jobs':
                return 'searched'
            default:
                return ''
        }
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IJobComponentProps) => {
    const uid = ownProps.uid
    return {}

}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], {})
    let jobs = Map({})
    return {
        authed: state.getIn(['authorize', 'authed'], false),
        isVerified: state.getIn(['authorize', 'isVerified'], false),
        translate: getTranslate(state.get('locale')),
        jobsLoaded: state.getIn(['job', 'loaded']),
        jobs
    }
}

// - Connect component to redux store
export default withRouter<any>(connect(mapStateToProps, mapDispatchToProps)((JobComponent as any) as any)) as typeof JobComponent
