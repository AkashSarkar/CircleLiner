export interface IJobComponentState {
    /**
     * Tab index
     */
    typeIndex: string
}
