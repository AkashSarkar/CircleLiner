// - Import react components
import React, {Component} from 'react'
import {IBrokenPageProps} from './IBrokenPageProps'
import {IBrokenPageState} from './IBrokenPageState'
import {IRouterProps} from 'routes/IRouterProps'
import {Map} from 'immutable'
import {HomeRouter} from 'routes/HomeRouter'
import {connect} from 'react-redux'

// - Import components
import HomeView from 'layouts/views/homeView'
import config from 'src/config'
import {NavLink} from 'react-router-dom'
// - Import actions

// - Create component class
export class BrokenPage extends Component<IBrokenPageProps, IBrokenPageState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IBrokenPageProps) {
        super(props)

        // Default state
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <>
                {/* Stunning header */}
                <div className='stunning-header bg-primary-opacity'>
                    {/* Header Standard Landing  */}
                    <div className='header--standard header--standard-landing position-sticky' id='header--standard'>
                        <div className='container'>
                            <div className='header--standard-wrap'>
                                <a href='#' className='logo'>
                                    <div className='img-wrap'>
                                        <img src='/img/logo.png' alt='Olympus' />
                                        <img src='/img/logo-colored-small.png' alt='Olympus' className='logo-colored' />
                                    </div>
                                    <div className='title-block'>
                                        <h6 className='logo-title'>Circleliner</h6>
                                        <div className='sub-title'>PROFESSIONAL NETWORK</div>
                                    </div>
                                </a>
                                <a href='#' className='open-responsive-menu js-open-responsive-menu'>
                                    <svg className='olymp-menu-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-menu-icon' /></svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    {/* ... end Header Standard Landing  */}
                    <div className='header-spacer--standard' />
                    <div className='stunning-header-content'>
                        <h1 className='stunning-header-title'>404 Error Page</h1>
                    </div>
                    <div className='content-bg-wrap stunning-header-bg1' />
                </div>
                {/* End Stunning header */}
                <section className='medium-padding120'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col col-xl-6 m-auto col-lg-6 col-md-12 col-sm-12 col-12'>
                                <div className='page-404-content'>
                                    <img src='/img/404.png' alt='photo'/>
                                    <div className='crumina-module crumina-heading align-center'>
                                        <h2 className='h1 heading-title'>A <span
                                            className='c-primary'>wild ghost</span> appears! Sadly, not what you were
                                            looking for...</h2>
                                        <p className='heading-text'>Sorry! The page you were looking for has been moved
                                            or doesn’t exist.
                                            If you like, you can return to our homepage, or if the problem persists,
                                            send us an email to: <a href='#'>support@circleliner.com</a>
                                        </p>
                                    </div>
                                    {this.props.isAuthed ?
                                        <NavLink to='/'>
                                            <a href='' className='btn btn-primary btn-lg'>Go to Homepage</a>
                                        </NavLink> : <NavLink to='/login'>
                                            <a href='' className='btn btn-primary btn-lg'>Login</a>
                                        </NavLink>}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <a className='back-to-top' href='#'>
                    <img src='svg-icons/back-to-top.svg' alt='arrow' className='back-icon' />
                </a>
            </>
            )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IBrokenPageProps) => {
    return {}
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IBrokenPageProps) => {
    const isAuthed = state.getIn(['authorize', 'authed'], 0)
    return {
        isAuthed: isAuthed,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((BrokenPage as any) as any)
