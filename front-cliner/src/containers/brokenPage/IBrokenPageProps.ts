import {Profile} from 'core/domain/users'

export interface IBrokenPageProps {

    isAuthed?: boolean
    /**
     * User full name
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    fullName: string

    /**
     * User identifier
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    userId?: string

    /**
     * Translate to locale strting
     */
    translate?: (state: any) => any
}
