import {Profile} from 'core/domain/users'

export interface ITermsAndConditionsProps {

    isAuthed?: boolean
    /**
     * User full name
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    fullName: string

    /**
     * User tag line
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    tagLine: string

    /**
     * User tag line
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    banner: string
    /**
     * User's avatar address
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    avatar: string

    /**
     * User identifier
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    userId?: string

    /**
     * Router data for the components in routing
     *
     * @type {*}
     * @memberof IRouterProps
     */
    data: any
    /**
     * Translate to locale strting
     */
    translate?: (state: any) => any
}
