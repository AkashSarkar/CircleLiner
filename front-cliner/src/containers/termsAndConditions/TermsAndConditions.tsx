// - Import react components
import React, {Component} from 'react'
import {ITermsAndConditionsProps} from './ITermsAndConditionsProps'
import {ITermsAndConditionsState} from './ITermsAndConditionsState'
import {IRouterProps} from 'routes/IRouterProps'
import {Map} from 'immutable'
import {HomeRouter} from 'routes/HomeRouter'
import {connect} from 'react-redux'

// - Import components
import HomeView from 'layouts/views/homeView'
import config from 'src/config'
// - Import actions

// - Create component class
export class TermsAndConditions extends Component<ITermsAndConditionsProps, ITermsAndConditionsState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ITermsAndConditionsProps) {
        super(props)

        // Default state
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <>{this.props.isAuthed ?  <HomeView>
                <div>
                    {/*<div className='header--standard header--standard-landing position-sticky' id='header--standard'>*/}
                        {/*<div className='container'>*/}
                            {/*<div className='header--standard-wrap'>*/}
                                {/*<a href={'javascript:(0)'} className='logo'>*/}
                                    {/*<div className='img-wrap'>*/}
                                        {/*<img src='/img/logo.png' alt='Olympus' />*/}
                                        {/*<img src='/img/logo-colored-small.png' alt='Olympus' className='logo-colored' />*/}
                                    {/*</div>*/}
                                    {/*<div className='title-block'>*/}
                                        {/*<h6 className='logo-title'>{config.settings.appName}</h6>*/}
                                        {/*<div className='sub-title'>Professional Network</div>*/}
                                    {/*</div>*/}
                                {/*</a>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                    {/* ... end Header Standard Landing  */}
                    <div className='main-header-post'>
                        <img src='/img/top-header3.png' alt='author' />
                    </div>
                    <div className='container negative-margin-top150 mb60'>
                        <div className='row'>
                            <div className='col col-xl-8 m-auto col-lg-12 col-md-12 col-sm-12 col-12'>
                                <div className='ui-block p-5'>
                                    <div className='body-bg-white'>
                                        <h5>Privacy policy</h5>
                                        <p>Connecting and collaborating all the people of same academic or professional field and build a virtual society of each field of study through extending hand by one another towards the professional betterment and development.</p>
                                        <p>This agreement was written in English (UK).</p>
                                        <p><strong>Effective on March 1, 2018</strong></p>
                                        <p>By using or accessing the services, members shall agree to our use of their data under this policy., as updated from time to time.</p>
                                        <h5>Privacy and Safety</h5>
                                        <p>To strictly maintain and protect the privacy of the members, our terms and agreement should help you decide the information you can share.</p>
                                        <p>
                                            •	Based on the services you use, the information collected and stored by us shall be different.<br />
                                            •   Use of our services, including when you sign up for an account; also, create or share,<br />
                                            •	Exchanging message or communicate with others.<br />
                                            •	Resume or portfolio submitted.<br />
                                            •	Photos, their locations and the date of any file created.<br />
                                            •	Other member shares a photo of you, sends a message to you or upload or sync your information.<br />
                                            •	If you register for a Service, for payment (e.g., credit card) and billing information shall be needed. </p>
                                        <p>To secure your account the best protection shall be taken, but we cannot guarantee, unless you help us with your careful and reasonable actions to preserve your safety. The members must avoid following activities-</p>
                                        <p>
                                            •	Facilitate or encourage any violations of our policies.<br />
                                            •	Doing anything which can impair the proper or regular function of the Netexpro .<br />
                                            •	Sharing content which contains threat or pornographic elements or contains nudity or graphic or incites violence.<br />
                                            •	Bully, intimidate, or harass any member.<br />
                                            •	Access an account belonging to someone else.<br />
                                            •	Spreading spams or unauthorized contents.<br />
                                            •	Upload viruses or other malicious code.<br />
                                            •	Violating any legal provision which is in force.<br />
                                        </p><p>
                                        •	All the information and communication shall be guarded, secured and protected where confidential according to Information and Communication Technology Act (ICT), 2006. </p>
                                        <h5>Account and Information</h5>
                                        <p>
                                            •	Members cannot create more than one personal account; if created we disable your account.<br />
                                            •	For registration members academic field should be mentioned specifically.<br />
                                            •	To prove your age limit and field of study and relevant information, a form has to be filled up which shall be sent to your email address. <br />
                                            •	Your professional and academic information and achievements should be accurate and up-to-date. <br />
                                            •	Members are not allowed to transfer their account without our written permission and authorization. <br />
                                            •	You must not provide any false personal information. </p>
                                        <h5>Notice</h5>
                                        <p>A notice shall be provided on the site page, if we make changes to policies, guidelines or other terms referenced in or incorporated by this agreement. Before any changes to these terms, we shall notify you and give you the opportunity to review and comment on the revised terms before continuing to our services. </p>
                                        <h5>Termination</h5>
                                        <p>
                                            •	Any violation of the terms and policy, or otherwise legal provision shall be stopped by freezing account and services to you. <br />
                                            •	We will notify you by email or at the next time you attempt to access your account. <br />
                                            •	You can delete your account or disable your application at any time you want. <br />
                                            •	According to circumstances, it is the discretion of the Netxpro to disable, terminate or simply remove content if the content is found inappropriate. </p>
                                        <h5>Legal Protection</h5>
                                        <p>
                                            •	All the information of the users must be valid, accurate and authentic. <br />
                                            •	Any complain relating to the information or the violation of the rights of third party can be submitted following the procedure by the members. <br />
                                            •	Violate the terms and agreement or creating any possible risk or infringement of any legal provision can lead to disable or termination of account. <br />
                                            •	Any changes or steps based on the complaint shall be notified to the member complained against through email and be dealt with good faith. <br />
                                            •	Submission of complain must not be false or vexatious, which can arise complainant’s personal liability. <br />
                                            •	The information preserved and protected might be disclosed if in good faith it is believed that law and law enforcing agencies of Bangladesh or of any other jurisdiction requires it to be disclosed following international standard. <br />
                                            •	 Shared information may also be disclosed in good faith, if believed that it is necessary to: detect, prevent and address fraud and other illegal activity or to protect life and property. <br />
                                            •	Any notice of infringement of copy right or any claim regarding copyright shall be dealt under Copyright Act 2000 (amended in 2005). </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </HomeView> : <div>
                {/*<div className='header--standard header--standard-landing position-sticky' id='header--standard'>*/}
                    {/*<div className='container'>*/}
                        {/*<div className='header--standard-wrap'>*/}
                            {/*<a href={'javascript:(0)'} className='logo'>*/}
                                {/*<div className='img-wrap'>*/}
                                    {/*<img src='/img/logo.png' alt='Olympus' />*/}
                                    {/*<img src='/img/logo-colored-small.png' alt='Olympus' className='logo-colored' />*/}
                                {/*</div>*/}
                                {/*<div className='title-block'>*/}
                                    {/*<h6 className='logo-title'>{config.settings.appName}</h6>*/}
                                    {/*<div className='sub-title'>Professional Network</div>*/}
                                {/*</div>*/}
                            {/*</a>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                {/* ... end Header Standard Landing  */}
                <div className='main-header-post'>
                    <img src='/img/top-header3.png' alt='author' />
                </div>
                <div className='container negative-margin-top150 mb60'>
                    <div className='row'>
                        <div className='col col-xl-8 m-auto col-lg-12 col-md-12 col-sm-12 col-12'>
                            <div className='ui-block p-5'>
                                <div className='body-bg-white'>
                                    <h5>Terms and Agreement</h5>
                                    <p>Connecting and collaborating all the people of same academic or professional field and build a virtual society of each field of study through extending hand by one another towards the professional betterment and development.</p>
                                    <p>This agreement was written in English (UK).</p>
                                    <p><strong>Effective on March 1, 2018</strong></p>
                                    <p>By using or accessing the services, members shall agree to our use of their data under this policy., as updated from time to time.</p>
                                    <h5>Privacy and Safety</h5>
                                    <p>To strictly maintain and protect the privacy of the members, our terms and agreement should help you decide the information you can share.</p>
                                    <p>
                                        •	Based on the services you use, the information collected and stored by us shall be different.<br />
                                        •   Use of our services, including when you sign up for an account; also, create or share,<br />
                                        •	Exchanging message or communicate with others.<br />
                                        •	Resume or portfolio submitted.<br />
                                        •	Photos, their locations and the date of any file created.<br />
                                        •	Other member shares a photo of you, sends a message to you or upload or sync your information.<br />
                                        •	If you register for a Service, for payment (e.g., credit card) and billing information shall be needed. </p>
                                    <p>To secure your account the best protection shall be taken, but we cannot guarantee, unless you help us with your careful and reasonable actions to preserve your safety. The members must avoid following activities-</p>
                                    <p>
                                        •	Facilitate or encourage any violations of our policies.<br />
                                        •	Doing anything which can impair the proper or regular function of the Netexpro .<br />
                                        •	Sharing content which contains threat or pornographic elements or contains nudity or graphic or incites violence.<br />
                                        •	Bully, intimidate, or harass any member.<br />
                                        •	Access an account belonging to someone else.<br />
                                        •	Spreading spams or unauthorized contents.<br />
                                        •	Upload viruses or other malicious code.<br />
                                        •	Violating any legal provision which is in force.<br />
                                    </p><p>
                                    •	All the information and communication shall be guarded, secured and protected where confidential according to Information and Communication Technology Act (ICT), 2006. </p>
                                    <h5>Account and Information</h5>
                                    <p>
                                        •	Members cannot create more than one personal account; if created we disable your account.<br />
                                        •	For registration members academic field should be mentioned specifically.<br />
                                        •	To prove your age limit and field of study and relevant information, a form has to be filled up which shall be sent to your email address. <br />
                                        •	Your professional and academic information and achievements should be accurate and up-to-date. <br />
                                        •	Members are not allowed to transfer their account without our written permission and authorization. <br />
                                        •	You must not provide any false personal information. </p>
                                    <h5>Notice</h5>
                                    <p>A notice shall be provided on the site page, if we make changes to policies, guidelines or other terms referenced in or incorporated by this agreement. Before any changes to these terms, we shall notify you and give you the opportunity to review and comment on the revised terms before continuing to our services. </p>
                                    <h5>Termination</h5>
                                    <p>
                                        •	Any violation of the terms and policy, or otherwise legal provision shall be stopped by freezing account and services to you. <br />
                                        •	We will notify you by email or at the next time you attempt to access your account. <br />
                                        •	You can delete your account or disable your application at any time you want. <br />
                                        •	According to circumstances, it is the discretion of the Netxpro to disable, terminate or simply remove content if the content is found inappropriate. </p>
                                    <h5>Legal Protection</h5>
                                    <p>
                                        •	All the information of the users must be valid, accurate and authentic. <br />
                                        •	Any complain relating to the information or the violation of the rights of third party can be submitted following the procedure by the members. <br />
                                        •	Violate the terms and agreement or creating any possible risk or infringement of any legal provision can lead to disable or termination of account. <br />
                                        •	Any changes or steps based on the complaint shall be notified to the member complained against through email and be dealt with good faith. <br />
                                        •	Submission of complain must not be false or vexatious, which can arise complainant’s personal liability. <br />
                                        •	The information preserved and protected might be disclosed if in good faith it is believed that law and law enforcing agencies of Bangladesh or of any other jurisdiction requires it to be disclosed following international standard. <br />
                                        •	 Shared information may also be disclosed in good faith, if believed that it is necessary to: detect, prevent and address fraud and other illegal activity or to protect life and property. <br />
                                        •	Any notice of infringement of copy right or any claim regarding copyright shall be dealt under Copyright Act 2000 (amended in 2005). </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>}</>
            )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: ITermsAndConditionsProps) => {
    return {}
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ITermsAndConditionsProps) => {
    const isAuthed = state.getIn(['authorize', 'authed'], 0)
    return {
        isAuthed: isAuthed,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((TermsAndConditions as any) as any)
