// - Import react components
import {HomeRouter} from 'routes'
import {Map} from 'immutable'
import React, {Component} from 'react'
import {Route, Switch, withRouter, Redirect, NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import app components
import HomeView from 'layouts/views/homeView'

// - Import actions
import {
    authorizeActions,
    imageGalleryActions,
    postActions,
    commentActions,
    voteActions,
    globalActions,
    circleActions,
    notifyActions,
} from 'src/store/actions'
import * as userActions from 'store/actions/userActions/userActions'
import * as notifyBoxAction from 'store/actions/notifyActions/notifyBoxActions'
import * as educationActions from 'store/actions/userActions/educationActions'

import {IHomeComponentProps} from './IHomeComponentProps'
import {IHomeComponentState} from './IHomeComponentState'

// - Create Home component class
export class HomeComponent extends Component<IHomeComponentProps, IHomeComponentState> {

    // Constructor
    constructor(props: IHomeComponentProps) {
        super(props)

        // Default state
        this.state = {
            drawerOpen: false
        }
        // Binding function to `this`
    }

    /**
     * Handle drawer toggle
     */
    handleDrawerToggle = () => {
        this.setState({drawerOpen: !this.state.drawerOpen})
    }

    componentWillMount() {
        const {global, clearData, loadData, authed, defaultDataEnable, isVerified, goTo} = this.props
        if (!authed) {
            goTo!('/login')
            return
        }
        if (!isVerified) {
            goTo!('/emailVerification')

        } else if (!global.get('defaultLoadDataStatus')) {

            clearData!()
            loadData!()
            defaultDataEnable!()
        }
    }

    /**
     * Render DOM component
     *
     * @returns DOM
     *
     * @memberof Home
     */
    render() {
        const HR = HomeRouter
        const {loaded, authed, loadDataStream, mergedPosts, hasMorePosts, showSendFeedback, translate, classes, theme} = this.props
        return (
            <HR enabled={loaded!} data={{mergedPosts, loadDataStream, hasMorePosts}}/>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IHomeComponentProps) => {
    const uid = ownProps.uid
    return {
        loadDataStream:
            (page: number, limit: number) => dispatch(postActions.dbGetPosts(page, limit)),
        loadData: () => {
            dispatch(postActions.dbGetPosts())
            dispatch(imageGalleryActions.dbGetImageGallery())
            dispatch(userActions.dbGetUserInfo())
            dispatch(notifyActions.dbGetNotifications())
            dispatch(circleActions.dbGetCircles())
            dispatch(circleActions.dbGetUserTies())
            dispatch(circleActions.dbGetFollowers())
            // dispatch(userActions.dbGetUserProfessionByUserId(uid!))
            dispatch(userActions.dbGetUserIndustryByUserId(uid!))
            dispatch(notifyBoxAction.dbGetNotifications())

        },
        clearData: () => {
            dispatch(imageGalleryActions.clearAllData())
            dispatch(postActions.clearAllData())
            dispatch(userActions.clearAllData())
            dispatch(notifyActions.clearAllNotifications())
            dispatch(circleActions.clearAllCircles())
            dispatch(globalActions.clearTemp())

        },
        defaultDataDisable: () => {
            dispatch(globalActions.defaultDataDisable())
        },
        defaultDataEnable: () => {
            dispatch(globalActions.defaultDataEnable())
        },
        goTo: (url: string) => dispatch(push(url)),
        showSendFeedback: () => dispatch(globalActions.showSendFeedback()),
        hideSendFeedback: () => dispatch(globalActions.hideSendFeedback())

    }

}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IHomeComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], {})
    const global = state.get('global', {})
    let mergedPosts = Map({})
    const circles = state.getIn(['circle', 'circleList'], {})
    const followingUsers: Map<string, any> = state.getIn(['circle', 'userTies'], {})
    const posts = state.getIn(['post', 'userPosts', uid], {})
    const hasMorePosts = state.getIn(['post', 'stream', 'hasMoreData'], true)
    followingUsers.forEach((user, userId) => {
        let newPosts = state.getIn(['post', 'userPosts', userId], {})
        mergedPosts = mergedPosts.merge(newPosts)
    })
    mergedPosts = mergedPosts.merge(posts)
    return {
        authed: state.getIn(['authorize', 'authed'], false),
        isVerified: state.getIn(['authorize', 'isVerified'], false),
        translate: getTranslate(state.get('locale')),
        currentLanguage: getActiveLanguage(state.get('locale')).code,
        mergedPosts,
        global,
        hasMorePosts,
        loaded: state.getIn(['user', 'loaded']) && state.getIn(['imageGallery', 'loaded']) && state.getIn(['notify', 'loaded']) && state.getIn(['circle', 'loaded'])
    }
}

// - Connect component to redux store
export default withRouter<any>(connect(mapStateToProps, mapDispatchToProps)((HomeComponent as any) as any)) as typeof HomeComponent
