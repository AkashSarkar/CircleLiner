export interface ICompleteRegistrationProps {

    /**
     * Send email for verification
     */
    sendEmailVerification: () => any

    /**
     * Send email for verification
     */
    completeEmailVerification: (token: string) => any

    /**
     * Redirect to home page
     */
    homePage: () => any
    industryPage: () => any

    emailConfirmationStatus?: string

    /**
     * Styles
     */
    classes?: any

    /**
     * Routing match
     *
     * @type {*}
     * @memberof ICompleteRegistrationProps
     */
    match?: any

    goTo?: (url: string) => any

    /**
     * Translate to locale string
     */
    translate?: (state: any, param?: {}) => any
}
