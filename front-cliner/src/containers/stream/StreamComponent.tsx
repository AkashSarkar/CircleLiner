// - Import react components
import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import {grey, teal} from '@material-ui/core/colors'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import Paper from '@material-ui/core/Paper'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import InfiniteScroll from 'react-infinite-scroller'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map, List as ImuList} from 'immutable'

// - Import app components
import PostComponent from 'src/components/post'
import PostWriteComponent from 'src/components/post/postWrite'
import UserAvatarComponent from 'src/components/userAvatar'
import LoadMoreProgressComponent from 'layouts/loadMoreProgress'
import StatusWriteComponent from 'src/components/post/postType/statusPostWrite'
import ArticleWriteComponent from 'src/components/post/postType/articlePostWrite'
import AwardWriteComponent from 'src/components/post/postType/awardPostWrite'
import ConferenceWriteComponent from 'src/components/post/postType/conferencePostWrite'
import EventWriteComponent from 'src/components/post/postType/eventPostWrite'
import ProjectWriteComponent from 'src/components/post/postType/projectPostWrite'
import PromotionWriteComponent from 'src/components/post/postType/promotionPostWrite'

// - Import API
import * as PostAPI from 'src/api/PostAPI'

// - Import actions
import * as globalActions from 'src/store/actions/globalActions'

import {IStreamComponentProps} from './IStreamComponentProps'
import {IStreamComponentState} from './IStreamComponentState'
import {Post} from 'src/core/domain/posts'

// - Create StreamComponent component class
export class StreamComponent extends Component<IStreamComponentProps, IStreamComponentState> {

    static propTypes = {
        /**
         * If it's true , writing post block will be visible
         */
        displayWriting: PropTypes.bool.isRequired,
        /**
         * A list of post
         */
        posts: PropTypes.object,

        /**
         * The title of home header
         */
        homeTitle: PropTypes.string

    }

    styles = {
        postWritePrimaryText: {
            color: grey[400],
            cursor: 'text'
        },
        postWtireItem: {
            fontWeight: '200'
        }
    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IStreamComponentProps) {
        super(props)

        this.state = {
            /**
             * It's true if we want to have two column of posts
             */
            divided: false,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: this.props.disableComments!,
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: this.props.disableSharing!,
            /**
             * If it's true, post write will be open
             */
            openPostWrite: false,
            /**
             * If it's true, status write will be open
             */
            openStatusPostWrite: false,
            /**
             * If it's true, Article write will be open
             */
            openArticlePostWrite: false,
            /**
             * If it's true, Award write will be open
             */
            openAwardPostWrite: false,
            /**
             * If it's true, Conference write will be open
             */
            openConferencePostWrite: false,
            /**
             * If it's true, status write will be open
             */
            openEventPostWrite: false,
            /**
             * If it's true, Article write will be open
             */
            openProjectPostWrite: false,
            /**
             * If it's true, Award write will be open
             */
            openPromotionPostWrite: false,
            /**
             * The title of home header
             */
            homeTitle: props.homeTitle!,

            /**
             * If there is more post to show {true} or not {false}
             */
            hasMorePosts: true
        }

        // Binding functions to `this`
        this.postLoad = this.postLoad.bind(this)
        this.handleOpenPostWrite = this.handleOpenPostWrite.bind(this)
        this.handleOpenStatusPostWrite = this.handleOpenStatusPostWrite.bind(this)
        this.handleOpenArticlePostWrite = this.handleOpenArticlePostWrite.bind(this)
        this.handleOpenAwardPostWrite = this.handleOpenAwardPostWrite.bind(this)
        this.handleOpenConferencePostWrite = this.handleOpenConferencePostWrite.bind(this)
        this.handleOpenEventPostWrite = this.handleOpenEventPostWrite.bind(this)
        this.handleOpenProjectPostWrite = this.handleOpenProjectPostWrite.bind(this)
        this.handleOpenPromotionPostWrite = this.handleOpenPromotionPostWrite.bind(this)
        this.handleClosePostWrite = this.handleClosePostWrite.bind(this)
    }

    /**
     * Open post write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenPostWrite = () => {
        this.setState({
            openPostWrite: true
        })
    }
    /**
     * Open status write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenStatusPostWrite = () => {
        this.setState({
            openStatusPostWrite: true
        })
    }
    /**
     * Open article write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenArticlePostWrite = () => {
        this.setState({
            openArticlePostWrite: true
        })
    }
    /**
     * Open award write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenAwardPostWrite = () => {
        this.setState({
            openAwardPostWrite: true
        })
    }
    /**
     * Open conference write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenConferencePostWrite = () => {
        this.setState({
            openConferencePostWrite: true
        })
    }
    /**
     * Open article write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenEventPostWrite = () => {
        this.setState({
            openEventPostWrite: true
        })
    }
    /**
     * Open award write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenProjectPostWrite = () => {
        this.setState({
            openProjectPostWrite: true
        })
    }
    /**
     * Open award write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenPromotionPostWrite = () => {
        this.setState({
            openPromotionPostWrite: true
        })
    }
    // Close handlers
    /**
     * Close post write
     *
     *
     * @memberof StreamComponent
     */
    handleClosePostWrite = () => {
        this.setState({
            openPostWrite: false
        })
    }
    /**
     * Close Status post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseStatusPostWrite = () => {
        this.setState({
            openStatusPostWrite: false
        })
    }
    /**
     * Close Article post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseArticlePostWrite = () => {
        this.setState({
            openArticlePostWrite: false
        })
    }
    /**
     * Close Award post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseAwardPostWrite = () => {
        this.setState({
            openAwardPostWrite: false
        })
    }
    /**
     * Close post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseConferencePostWrite = () => {
        this.setState({
            openConferencePostWrite: false
        })
    }
    /**
     * Close Status post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseEventPostWrite = () => {
        this.setState({
            openEventPostWrite: false
        })
    }
    /**
     * Close Article post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseProjectPostWrite = () => {
        this.setState({
            openProjectPostWrite: false
        })
    }
    /**
     * Close Award post write
     *
     *
     * @memberof StreamComponent
     */
    handleClosePromotionPostWrite = () => {
        this.setState({
            openPromotionPostWrite: false
        })
    }
    /**
     * Create a list of posts
     * @return {DOM} posts
     */
    postLoad = () => {

        let {match} = this.props
        let posts: Map<string, Map<string, any>> = this.props.posts
        let {tag} = match.params
        if (posts === undefined || !(posts.keySeq().count() > 0)) {

            return (

                <h1>
                    'Nothing has been shared.'
                </h1>

            )
        } else {
            let postBack = {divided: false, oddPostList: [], evenPostList: []}
            let parsedPosts: ImuList<any> = ImuList()
            posts.forEach((post: Map<string, any>) => {
                if (tag) {
                    let regex = new RegExp('#' + tag, 'g')
                    let postMatch = String(post.get('body', '')).match(regex)
                    if (postMatch !== null) {
                        parsedPosts = parsedPosts.push(post)
                    }
                } else {
                    parsedPosts = parsedPosts.push(post)
                }
            })
            const sortedPosts = PostAPI.sortImuObjectsDate(parsedPosts)
            if (sortedPosts.count() > 6) {
                postBack.divided = true

            } else {
                postBack.divided = false
            }
            let index = 0
            sortedPosts.forEach((post) => {

                let newPost: any = (
                    <PostComponent key={`${post!.get('id')}-stream-div-post`} post={post! as any}/>
                )

                if ((index % 2) === 1 && postBack.divided) {
                    postBack.oddPostList.push(newPost as never)
                } else {
                    postBack.evenPostList.push(newPost as never)
                }
                ++index
            })
            return postBack
        }
    }

    /**
     * Scroll loader
     */
    scrollLoad = (page: number) => {
        console.log('===========================scrollLoad=================================')
        const {loadStream} = this.props
        loadStream!(page, 10)
    }

    componentWillMount() {
        const {setHomeTitle} = this.props
        setHomeTitle!()
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {tag, displayWriting, hasMorePosts, translate} = this.props
        const postList = this.postLoad() as { evenPostList: Post[], oddPostList: Post[], divided: boolean } | any

        return (
            <InfiniteScroll
                pageStart={0}
                loadMore={this.scrollLoad}
                hasMore={hasMorePosts}
                useWindow={true}
                loader={<LoadMoreProgressComponent key='stream-load-more-progress'/>}
            >

                {displayWriting && !tag
                    ? (
                        <div className='ui-block'>
                            {/* News Feed Form  */}
                            <div className='news-feed-form'>
                                {/* Nav tabs */}
                                <ul className='nav nav-tabs' role='tablist'>
                                    <li className='nav-item'>
                                        <a className='nav-link active inline-items' data-toggle='tab' href='#home-1'
                                           role='tab' aria-expanded='true'>
                                            <svg className='olymp-status-icon'>
                                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-status-icon'/>
                                            </svg>
                                            <span>Status</span>
                                        </a>
                                    </li>
                                    <li className='nav-item' onClick={this.handleOpenProjectPostWrite}>
                                        <a className='nav-link inline-items' data-toggle='tab' href='#project'
                                           role='tab' aria-expanded='false'>
                                            <svg className='olymp-multimedia-icon'>
                                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-multimedia-icon'/>
                                            </svg>
                                            <span>Project</span>
                                        </a>
                                    </li>
                                    <li className='nav-item' onClick={this.handleOpenArticlePostWrite}>
                                        <a className='nav-link inline-items' data-toggle='tab' href='#profile-1'
                                           role='tab' aria-expanded='false'>
                                            <svg className='olymp-multimedia-icon'>
                                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-multimedia-icon'/>
                                            </svg>
                                            <span>Article</span>
                                        </a>
                                    </li>
                                    <li className='nav-item'>
                                        <div className='more'>
                                            <div className='container pt-3'>
                                                <svg className='olymp-three-dots-icon'>
                                                    <use
                                                        xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                                                </svg>
                                            </div>
                                            <ul className='more-dropdown'>
                                                <li className='nav-item' onClick={this.handleOpenAwardPostWrite}>
                                                    <a data-toggle='tab' href='#award'
                                                       role='tab' aria-expanded='false'>
                                                        <span>Award</span>
                                                    </a>
                                                </li>
                                                <li className='nav-item' onClick={this.handleOpenConferencePostWrite}>
                                                    <a data-toggle='tab' href='#conference'
                                                       role='tab' aria-expanded='false'>
                                                        <span>Conference</span>
                                                    </a>
                                                </li>
                                                <li className='nav-item' onClick={this.handleOpenEventPostWrite}>
                                                    <a data-toggle='tab' href='#event'
                                                       role='tab' aria-expanded='false'>
                                                        <span>Event</span>
                                                    </a>
                                                </li>
                                                <li className='nav-item' onClick={this.handleOpenPromotionPostWrite}>
                                                    <a data-toggle='tab' href='#promotion'
                                                       role='tab' aria-expanded='false'>
                                                        <span>Promotion</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                                {/* Tab panes */}
                                <div className='tab-content'>
                                    {/*Status Post*/}
                                    <div className='tab-pane active' id='home-1' role='tabpanel'
                                         aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share what you are thinking
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onMouseUp={this.handleOpenStatusPostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                    {/*Article Post*/}
                                    <div className='tab-pane' id='profile-1' role='tabpanel' aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share your article
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onClick={this.handleOpenArticlePostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                    {/*Blog Post*/}
                                    <div className='tab-pane' id='blog' role='tabpanel' aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share your blog
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onClick={this.handleOpenPostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                    {/*Award Post*/}
                                    <div className='tab-pane' id='award' role='tabpanel' aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share your award winning experience
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onClick={this.handleOpenAwardPostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                    {/*Conference Post*/}
                                    <div className='tab-pane' id='conference' role='tabpanel' aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share conference details
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onClick={this.handleOpenConferencePostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                    {/*Event Post*/}
                                    <div className='tab-pane' id='event' role='tabpanel' aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share an event
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onClick={this.handleOpenEventPostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                    {/*Project Post*/}
                                    <div className='tab-pane' id='project' role='tabpanel' aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share your project details
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onClick={this.handleOpenProjectPostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                    {/*Promotion Post*/}
                                    <div className='tab-pane' id='promotion' role='tabpanel' aria-expanded='true'>
                                        <form>
                                            <UserAvatarComponent fileName={this.props.avatar!}
                                                                 fullName={this.props.fullName!} size={36}/>
                                            <div className='form-group with-icon label-floating is-empty'>
                                                <label className='control-label'>Share about you promotion
                                                    here...</label>
                                                <textarea className='form-control' defaultValue={''}
                                                          onClick={this.handleOpenPromotionPostWrite}/>
                                                <span className='material-input'/></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {/* ... end News Feed Form  */}
                            <StatusWriteComponent open={this.state.openStatusPostWrite}
                                                  onRequestClose={this.handleCloseStatusPostWrite} edit={false}>
                            </StatusWriteComponent>
                            <ArticleWriteComponent open={this.state.openArticlePostWrite}
                                                   onRequestClose={this.handleCloseArticlePostWrite} edit={false}>
                            </ArticleWriteComponent>
                            <AwardWriteComponent open={this.state.openAwardPostWrite}
                                                 onRequestClose={this.handleCloseAwardPostWrite} edit={false}>
                            </AwardWriteComponent>
                            <ConferenceWriteComponent open={this.state.openConferencePostWrite}
                                                      onRequestClose={this.handleCloseConferencePostWrite} edit={false}>
                            </ConferenceWriteComponent>
                            <EventWriteComponent open={this.state.openEventPostWrite}
                                                 onRequestClose={this.handleCloseEventPostWrite} edit={false}>
                            </EventWriteComponent>
                            <ProjectWriteComponent open={this.state.openProjectPostWrite}
                                                   onRequestClose={this.handleCloseProjectPostWrite} edit={false}>
                            </ProjectWriteComponent>
                            <PromotionWriteComponent open={this.state.openPromotionPostWrite}
                                                     onRequestClose={this.handleClosePromotionPostWrite} edit={false}>
                            </PromotionWriteComponent>
                            <PostWriteComponent open={this.state.openPostWrite}
                                                onRequestClose={this.handleClosePostWrite} edit={false}>
                            </PostWriteComponent>
                        </div>
                    )
                    : ''}
                <div id='newsfeed-items-grid'>
                    {postList.evenPostList}
                    {postList.divided
                        ? postList.oddPostList
                        : ''}
                </div>
            </InfiniteScroll>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IStreamComponentProps) => {
    return {
        setHomeTitle: () => dispatch(globalActions.setHeaderTitle(ownProps.homeTitle || '')),
        showTopLoading: () => dispatch(globalActions.showTopLoading()),
        hideTopLoading: () => dispatch(globalActions.hideTopLoading())

    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IStreamComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const user = state.getIn(['user', 'info', uid])
    return {
        translate: getTranslate(state.get('locale')),
        // avatar: user.avatar ? user.avatar : '',
        // fullName: user.fullName ? user.fullName : ''
    }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(StreamComponent as any) as any) as typeof StreamComponent
