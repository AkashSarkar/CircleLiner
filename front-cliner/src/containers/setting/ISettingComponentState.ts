export interface ISettingComponentState {

    /**
     * Is change password page
     *
     * @type {boolean}
     * @memberof ISettingComponentState
     */
    isChangePassword?: boolean

    /**
     * Password input value
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    passwordInput: string

    /**
     * Password input error text
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    passwordInputError: string

    /**
     * Confirm input value
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    confirmInput: string

    /**
     * Confirm input error
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    confirmInputError: string
    /**
     * Whether drawer is open
     */
    drawerOpen: boolean
}
