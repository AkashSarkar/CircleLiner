// - Import external components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import RaisedButton from '@material-ui/core/Button'
import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles'
import config from 'src/config'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - import external components
import HomeView from 'src/layouts/views/homeView'
import ChangePassword from 'src/components/settings/changePassword'
import PersonalInfo from 'src/components/settings/personalInfo'
import AccountSettings from 'src/components/settings/account'
import Sidebar from 'src/components/settings/sidebar'

// - Import actions
import * as authorizeActions from 'src/store/actions/authorizeActions'
import {ISettingComponentProps} from './ISettingComponentProps'
import {ISettingComponentState} from './ISettingComponentState'
import {Grid} from '@material-ui/core'

// - Import layouts
import Setting from 'layouts/setting'

const styles = (theme: any) => ({
    textField: {
        minWidth: 280,
        marginTop: 20

    },
    contain: {
        margin: '0 auto'
    },
    paper: {
        minHeight: 370,
        maxWidth: 450,
        minWidth: 337,
        textAlign: 'center',
        display: 'block',
        margin: 'auto'
    },
})

/**
 * Create component class
 *
 * @export
 * @class AboutComponent
 * @extends {Component}
 */
export class SettingComponent extends Component<ISettingComponentProps, ISettingComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISettingComponentProps) {
        super(props)
        // Default state
        this.state = {
            passwordInput: '',
            passwordInputError: '',
            confirmInput: '',
            confirmInputError: '',
            drawerOpen: false,
        }
    }

    /**
     * Handle drawer toggle
     */
    handleDrawerToggle = () => {
        this.setState({drawerOpen: !this.state.drawerOpen})
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        const {classes, translate} = this.props

        return (
            <HomeView>
                <div className='container'>
                    <div className='row'>
                        <PersonalInfo/>
                    </div>
                </div>
            </HomeView>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISettingComponentProps) => {
    return {
        // login: (password: string) => {
        //     dispatch(authorizeActions.dbUpdatePassword(password))
        // },
        homePage: () => {
            dispatch(push('/'))
        }
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: ISettingComponentProps) => {
    return {
        translate: getTranslate(state.get('locale'))
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(SettingComponent as any) as any)
