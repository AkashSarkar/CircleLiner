// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {Map} from 'immutable'

// - Import app components
import Stream from 'containers/stream'
import HomeHeader from 'layouts/homeHeader'

// - Import API

// - Import actions
import * as postActions from 'src/store/actions/postActions'
import * as userActions from 'src/store/actions/userActions/userActions'

import {IPostPageComponentProps} from './IPostPageComponentProps'
import {IPostPageComponentState} from './IPostPageComponentState'

/**
 * Create component class
 */
export class PostPageComponent extends Component<IPostPageComponentProps, IPostPageComponentState> {

    static propTypes = {}

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IPostPageComponentProps) {
        super(props)
        const {userId} = props
        // Defaul state
        this.state = {
            uid: userId,
        }
        // Binding functions to `this`
    }

    componentWillMount() {
        this.props.loadPost!()
        this.props.loadUserInfo!()
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const St = Stream as any
        return (
            <>
                <HomeHeader userId={this.state.uid}/>
                <div className='header-spacer'/>
                <div className='row'>
                    <div className='col col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12'></div>
                    <div className='col col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                        <St posts={this.props.posts} />
                    </div>
                </div>
            </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPostPageComponentProps) => {
    const {userId, postId} = ownProps.match.params
    return {
        loadPost: () => dispatch(postActions.dbGetPostById(userId, postId)),
        loadUserInfo: () => dispatch(userActions.dbGetUserInfoByUserId(userId, 'header'))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPostPageComponentProps) => {
    const {userId, postId} = ownProps.match.params
    const uid = state.getIn(['authorize', 'uid'])
    const userInfo = state.getIn(['state', 'user', 'info', userId])
    let posts: Map<string, Map<string, any>> = Map({})
    posts = posts.set(postId, state.getIn(['post', 'userPosts', userId, postId], Map({})))
    // console.log('----------posts in postPageComponent------------')
    // console.log(posts)
    return {
        avatar: userInfo ? userInfo.avatar : '',
        name: userInfo ? userInfo.fullName : '',
        posts,
        userId: uid
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(PostPageComponent as any)
