import {BaseDomain} from 'core/domain/common'

export class RegisterUserResult extends BaseDomain {

    private _uid: string
    private _email: string
    private _token: string

    constructor(uid: string, email: string, token: string) {
        super()

        this._uid = uid
        this._email = email
        this._token = token
    }

    /**
     * User identifier
     *
     * @type {string}
     * @memberof LoginUser
     */

    public get uid(): string {
        return this._uid
    }
    public get email(): string {
        return this._email
    }
    public get token(): string {
        return this._token
    }
}
