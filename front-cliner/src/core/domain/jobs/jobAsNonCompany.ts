import { BaseDomain } from 'core/domain/common'
export class JobAsNonCompany extends BaseDomain {

    /**
     * Post identifier
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public id?: string | null

    /**
     * The identifier of post type
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public postType?: number

    /**
     * employer name
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public name: string
    /**
     * employer phone
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public phone: string

    /**
     * employer designation
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public designation: string

    /**
     * employer email
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public email: string

    /**
     * Avatar address of post owner
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public jobTitle: string
    /**
     * Post view count
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public location: string

    /**
     * Last post edit date
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public skill: string

    /**
     * Post tags
     *
     * @type {string[]}
     * @memberof JobAsCompany
     */
    public jobType: string

    /**
     * Numeber of comment on the post
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public eduRequirement: string

    /**
     * The post creation date
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public industry: string

    /**
     * The address of image on the post
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public levelOfSkill?: string

    /**
     * Post image full path
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public salary?: string

    /**
     * The adress of video on the post
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public noOfVacancy?: string

    /**
     * If writing comment is disabled {true} or not {false}
     *
     * @type {Boolean}
     * @memberof JobAsCompany
     */
    public responsibilities: string
    /**
     * User identifier
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public userId?: string
}