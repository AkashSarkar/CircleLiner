import {BaseDomain} from 'core/domain/common'

export class Job extends BaseDomain {

    /**
     * Post identifier
     *
     * @type {string}
     * @memberof Job
     */
    public id?: string | null
    /**
     * The post creation date
     *
     * @type {number}
     * @memberof Job
     */
    public creationDate?: number
    /**
     * The text of post
     *
     * @type {string}
     * @memberof Job
     */
    public description?: string
    /**
     * The identifier of post owner
     *
     * @type {string}
     * @memberof Post
     */
    public ownerUserId?: string
}