import {BaseDomain} from 'core/domain/common'

export class EmployerPreferences extends BaseDomain {
    public id?: string | null
    employerPreferance: string
    wantForWork: string
    location: string
    role: string
    industry: string
    preferToWork: string
    remoteWork: string
    workFrom: string
    workTo: string
    jobType?: any
    userId?: string
}
