import { BaseDomain } from 'core/domain/common'
export class JobAsCompany extends BaseDomain {

    /**
     * Post identifier
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public id?: string | null

    /**
     * The identifier of post type
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public postType?: string
    /**
     * The identifier of post type
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public companyName?: string

    /**
     * The post creation date
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public industry?: string

    /**
     * The post delete date
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public altCompanyName?: string

    /**
     * The score of post
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public bunsinessDescription?: string

    /**
     * Post view count
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public companyAddress?: string

    /**
     * The text of post
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public phone?: string

    /**
     * The identifier of post owner
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public email?: string

    /**
     * Full name of post owner
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public website?: string

    /**
     * Avatar address of post owner
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public jobTitle?: string

    /**
     * Last post edit date
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public skill?: string

    /**
     * Post tags
     *
     * @type {string[]}
     * @memberof JobAsCompany
     */
    public jobType?: string

    /**
     * Numeber of comment on the post
     *
     * @type {number}
     * @memberof JobAsCompany
     */
    public eduRequirement?: string

    /**
     * The address of image on the post
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public levelOfSkill?: string

    /**
     * Post image full path
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public salary?: string

    /**
     * The adress of video on the post
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public noOfVacancy?: string

    /**
     * If writing comment is disabled {true} or not {false}
     *
     * @type {Boolean}
     * @memberof JobAsCompany
     */
    public responsibilities?: string
    /**
     * User identifier
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public userId?: string

    public postJobAs?: string

    public individualName?: string
    /**
     * employer phone
     *
     * @type {string}
     * @memberof JobAsCompany
     */

    public designation?: string

    /**
     * employer email
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    public individualEmail?: string
    public individualPhone?: string
}
