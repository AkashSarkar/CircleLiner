import {BaseDomain} from 'core/domain/common'

export class JobPreferences extends BaseDomain {
    public id?: string | null
    jobPreferance: string
    wantToWork: string
    location: string
    role: string
    industry: string
    preferToWork: string
    remoteWork: string
    workFrom: string
    workTo: string
    jobType?: any
    userId?: string
}
