import { BaseDomain } from 'core/domain/common'

export class PersonalInfo extends BaseDomain {
    public firstName: string | null
    public lastName: string | null
    public fullName: string | null
    public email: string | null
    public dob: string | null
    public website: string | null
    public phone: string | null
    public birthPlace: string | null
    public gender: string | null
    public status: string | null
    public ownerUserId?: string
}