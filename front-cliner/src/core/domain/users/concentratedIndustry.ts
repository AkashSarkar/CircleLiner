import {BaseDomain} from 'core/domain/common'

export class ConcentratedIndustry extends BaseDomain {

    public id?: string | null
    /**
     * Full name of user
     *
     * @type {string}
     * @memberof User
     */
    public concentratedIndustryName: string
    /**
     * User identifier
     *
     * @type {string}
     * @memberof User
     */
    public userId?: string | null
}
