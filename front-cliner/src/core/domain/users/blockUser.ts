import { BaseDomain } from 'core/domain/common'

export class BlockUser extends BaseDomain {
    public blockedBy?: string | null
    public blockedUserId: string | null
}