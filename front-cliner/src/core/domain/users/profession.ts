import {BaseDomain} from 'core/domain/common'

export class Profession extends BaseDomain {

    public id?: string
    /**
     * company name
     *
     * @type {string}
     * @memberof User
     */
    public companyName?: string
    /**
     * company business
     *
     * @type {string}
     * @memberof User
     */
    public companyBusiness?: string

    /**
     * user designation
     *
     * @type {string}
     * @memberof User
     */
    public designation?: string

    /**
     * department
     *
     * @type {string}
     * @memberof User
     */
    public department?: string
    /**
     * Full work experience of user
     *
     * @type {string}
     * @memberof User
     */
    public experience?: string
    /**
     * Full work responsibilities of user
     *
     * @type {string}
     * @memberof User
     */
    public responsibilities?: string
    /**
     * companyLocation
     *
     * @type {string}
     * @memberof User
     */
    public companyLocation?: string
    /**
     * workFrom
     *
     * @type {string}
     * @memberof User
     */
    public workFrom?: string
    /**
     * workTo
     *
     * @type {string}
     * @memberof User
     */
    public workTo?: string
    /**
     * User identifier
     *
     * @type {string}
     * @memberof User
     */
    public userId?: string
}
