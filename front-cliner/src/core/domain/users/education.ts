import { BaseDomain } from 'core/domain/common'

export class Education extends BaseDomain {
    /**
     * education post id
     */
     public id?: string
    /**
     * education title
     */
     public fieldOfStudy?: string | null
    /**
     * education duration
     */
     public degreeTitle?: string | null
    /**
     * education description
     */
    public instituteName?: string | null
    /**
     * education description
     */
    public result?: string | null
    /**
     * education description
     */
    public passingYear?: string | null
    /**
     * education description
     */
    public currentlyStudying?: string | null
    /**
     * education description
     */
    public location?: string | null
    /**
     * education description
     */
    public group?: string | null
    /**
     * education description
     */
    public educationLevel?: string | null
    /**
     * education description
     */
    public achievement?: string | null
    /**
     * education description
     */
    public duration?: string | null
    /**
     * education user id
     */
     public userId?: string
}