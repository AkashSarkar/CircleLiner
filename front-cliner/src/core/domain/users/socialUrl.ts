import {BaseDomain} from 'core/domain/common'

export class SocialUrl extends BaseDomain {
    /**
     * post Id
     *
     * @type {string}
     * @memberof SocialUrl
     */
     public id?: string | null
    /**
     * social type name
     *
     * @type {string}
     * @memberof SocialUrl
     */
     public socialType: string | null
    /**
     * social url
     *
     * @type {string}
     * @memberof SocialUrl
     */
     public socialUrl: string | null
    /**
     * User identifier
     *
     * @type {string}
     * @memberof SocialUrl
     */
     public userId?: string
}