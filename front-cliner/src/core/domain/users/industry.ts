import {BaseDomain} from 'core/domain/common'

export class Industry extends BaseDomain {

    public id?: string | null
    /**
     * Full name of user
     *
     * @type {string}
     * @memberof User
     */
    public industryName: string

    public concentratedIndustryName?: string
    /**
     * User identifier
     *
     * @type {string}
     * @memberof User
     */
    public userId?: string | null
}
