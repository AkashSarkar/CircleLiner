import {BaseDomain} from 'core/domain/common'

export class User extends BaseDomain {

    /**
     * First name of user
     *
     * @type {string}
     * @memberof User
     */
    public fullName: string
    /**
     * First name of user
     *
     * @type {string}
     * @memberof User
     */
    public firstName: string
    /**
     * last name of user
     *
     * @type {string}
     * @memberof User
     */
    public lastName: string

    /**
     * User avatar address
     *
     * @type {string}
     * @memberof User
     */
    public avatar: string

    /**
     * Email of the user
     *
     * @type {string}
     * @memberof User
     */
    public email?: string | null

    /**
     * Password of the user
     *
     * @type {string}
     * @memberof User
     */
    public password?: string | null

    /**
     * User identifier
     *
     * @type {string}
     * @memberof User
     */
    public userId?: string | null

    /**
     * User creation date
     *
     * @type {number}
     * @memberof User
     */
    public creationDate: number

    // public banner: string
    // public birthday: number
    // public companyName: string
    // public concentratedIndustry: string
    // public industry: string

}
