import { BaseDomain } from 'core/domain/common'

export class AccountSetting extends BaseDomain {
  public followPrivacy: string
  public postPrivacy: string
  public notificationSound: boolean
  public emailNotificationSound: boolean
  public followerBirthdayNotificationSound: boolean
  public chatSound: boolean
  public accountDeactivate: boolean
  public ownerUserId?: string
}