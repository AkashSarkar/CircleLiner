import { BaseDomain } from 'core/domain/common'

export class ReportUser extends BaseDomain {
    /**
     * blockedBy user's id
     *
     * @type {string}
     * @memberof User
     */
    public reportedBy?: string | null
    /**
     * blocked user's id
     *
     * @type {string}
     * @memberof User
     */
    public reportedUserId: string | null
    /**
     * block report
     *
     * @type {string}
     * @memberof User
     */
    public reportText?: string | null

}