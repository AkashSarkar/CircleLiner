import {User, Profile} from 'core/domain/users'
import {PersonalInfo} from 'core/domain/users/personalInfo'
import {Industry} from 'core/domain/users/industry'
import {Education} from 'core/domain/users/education'
import {Profession} from 'core/domain/users/profession'
import {AccountSetting} from 'core/domain/users/accountSettings'
import {BlockUser} from 'core/domain/users/blockUser'
import {ReportUser} from 'core/domain/users/reportUser'
import {SocialUrl} from 'core/domain/users/socialUrl'
/**
 * User service interface
 *
 * @export
 * @interface IUserService
 */
export interface IUserService {
    addUserIndustry: (userId: string, addIndustry: Industry) => Promise<string>
    addEducationalInfo: (userId: string, education: Education) => Promise<string>
    addSocailUrl: (userId: string,socialUrl: SocialUrl) => Promise<string>
    updateEducationalInfo: (personalInfo: Education) => Promise<void>
    updateSocialUrl: (socialUrl: SocialUrl) => Promise<void>
    updateProfessionalInfo: (personalInfo: Profession) => Promise<void>
    deleteEducation: (educationId: string) => Promise<void>
    deleteSocialUrl: (socialUrlId: string) => Promise<void>
    deleteProfession: (professionId: string) => Promise<void>
    addPersonalInfo: (userId: string, personalInfo: PersonalInfo) => Promise<string>
    updateUserIndustry: (industry: Industry) => Promise<void>
    addAccountsettings: (userId: string, personalInfo: AccountSetting) => Promise<string>
    addProfession: (userId: string, addProfession: Profession) => Promise<string>
    blockPeople: (blockPeople: BlockUser) => Promise<string>
    reportProfile: (reportProfile: ReportUser) => Promise<string>
    updateProfession: (updateProfession: Profession) => Promise<void>
    getUserProfile: (userId: string) => Promise<Profile>
    getUserIndustry: (userId: string) => Promise<{ [industryId: string]: Industry }[]>
    getUserProfession: (userId: string) => Promise<{ [professionId: string]: Profession }[]>
    getUserEducation: (userId: string) => Promise<{ [educationId: string]: Education }[]>
    getUserSocialUrls: (userId: string) => Promise<{ [socialUrlId: string]: SocialUrl }[]>
    updateUserProfile: (userId: string, profile: Profile) => Promise<void>
    getUsersProfile: (userId: string, lastUserId?: string, page?: number, limit?: number)
        => Promise<{ users: { [userId: string]: Profile }[], newLastUserId: string }>
    updateUserPP: (userId: string, avatar: string) => Promise<void>
    updateUserCP: (userId: string, coverPhoto: string) => Promise<void>
}
