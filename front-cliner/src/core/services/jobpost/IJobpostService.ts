import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'
import {JobAsNonCompany} from 'core/domain/jobs/jobAsNonCompany'
import {JobPreferences} from 'core/domain/jobs/jobPreferences'
import {EmployerPreferences} from 'core/domain/jobs/employerPreferences'
/**
 * Post service interface
 *
 * @export
 * @interface IJobpostService
 */
export interface IJobpostService {
    addJobPreferences: (jobPreferences: JobPreferences) => Promise<string>
    addEmployerPreferences: (employerPreferences: EmployerPreferences) => Promise<string>
    addJobpost: (jobPost: JobAsCompany) => Promise<string>
    addJobpostAsNonCompany: (jobPost: JobAsNonCompany) => Promise<string>
    getJobpostAsCompany: (userId: string) => Promise<{ [jobAsCompanyId: string]: JobAsCompany }[]>
    getJobpostAsNonCompany: (userId: string) => Promise<{ [jobAsNonCompanyId: string]: JobAsNonCompany }[]>
    updateJobpostAsCompany: (jobPost: JobAsCompany) => Promise<void>
    updateJobpostAsNonCompany: (jobPost: JobAsNonCompany) => Promise<void>
    deleteJobpostAsCompany: (jobAsCompanyId: string) => Promise<void>
    deleteJobpostAsNonCompany: (jobAsNonCompanyId: string) => Promise<void>
}
