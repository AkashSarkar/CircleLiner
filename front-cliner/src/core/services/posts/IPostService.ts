import { User } from 'core/domain/users'
import { Post} from 'core/domain/posts'
import { PostPromotion } from 'core/domain/posts/postPromotion'
import { PostArticle } from 'core/domain/posts/postArticle'
import { PostAward } from 'core/domain/posts/postAward'
import { PostEvent } from 'core/domain/posts/postEvent'
import { PostProject } from 'core/domain/posts/postProject'
import { PostConference } from 'core/domain/posts/postConference'

/**
 * Post service interface
 *
 * @export
 * @interface IJobpostService
 */
export interface IPostService {
  addPost: (post: Post) => Promise<string>
  addPostPromotion: (post: PostPromotion) => Promise<string>
  addPostArticle: (post: PostArticle) => Promise<string>
  addPostAward: (post: PostAward) => Promise<string>
  addPostEvent: (post: PostEvent) => Promise<string>
  addPostProject: (post: PostProject) => Promise<string>
  addPostConference: (post: PostConference) => Promise<string>
  updatePost: (post: Post) => Promise<void>
  deletePost: (postId: string) => Promise<void>
  getPosts: (currentUserId: string,lastPostId: string, page: number, limit: number)
  => Promise<{posts: {[postId: string]: Post }[], newLastPostId: string}>

  /**
   * Get list of post by user identifier
   */
  getPostsByUserId: (userId: string, lastPostId?: string, page?: number, limit?: number)
    => Promise<{ posts: { [postId: string]: Post }[], newLastPostId: string }>

    /**
     * Get post by the post identifier
     */
  getPostById: (postId: string) => Promise<Post>
}
