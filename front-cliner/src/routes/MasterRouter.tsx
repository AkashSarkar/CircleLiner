// - Import react components
import PublicRoute from './PublicRoute'
import PrivateRoute from './PrivateRoute'
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Route, Switch, withRouter, Redirect, NavLink} from 'react-router-dom'
import Loadable from 'react-loadable'

import {IRouterProps} from './IRouterProps'
import MasterLoadingComponent from 'components/masterLoading/MasterLoadingComponent'

// - Async Components
const AsyncHome: any = Loadable({
    loader: () => import('containers/home'),
    loading: MasterLoadingComponent,
})
const AsyncSignup = Loadable({
    loader: () => import('containers/signup'),
    loading: MasterLoadingComponent,
})
const AsyncEmailVerification = Loadable({
    loader: () => import('containers/emailVerification'),
    loading: MasterLoadingComponent,
})
const AsyncCompleteVerification = Loadable({
    loader: () => import('containers/completeRegistration'),
    loading: MasterLoadingComponent,
})
const AsyncResetPassword = Loadable({
    loader: () => import('containers/resetPassword'),
    loading: MasterLoadingComponent,
})
const AsyncLogin = Loadable({
    loader: () => import('containers/login'),
    loading: MasterLoadingComponent,
})

const AsyncTermsAndConditions = Loadable({
    loader: () => import('containers/termsAndConditions'),
    loading: MasterLoadingComponent,
})

const AsyncFAQs = Loadable({
    loader: () => import('containers/FAQs'),
    loading: MasterLoadingComponent,
})

const AsyncBrokenPage = Loadable({
    loader: () => import('containers/brokenPage'),
    loading: MasterLoadingComponent,
})
const AsyncMessageBox = Loadable({
    loader: () => import('components/messageComponent'),
    loading: MasterLoadingComponent,
})

/**
 * Master router
 */
export class MasterRouter extends Component<IRouterProps, any> {
    render() {
        const {enabled, match, data} = this.props
        return (
            enabled ? (
                    <Switch>
                        <Route path='/signup' component={AsyncSignup}/>
                        <Route path='/complete-registration/:token' component={AsyncCompleteVerification}/>
                        <Route path='/emailVerification' component={AsyncEmailVerification}/>
                        <Route path='/resetPassword' component={AsyncResetPassword}/>
                        <Route path='/chatBox' component={AsyncMessageBox}/>
                        <PublicRoute path='/Page404Error' component={<AsyncBrokenPage/>}/>
                        <PublicRoute path='/terms-and-conditions' component={<AsyncTermsAndConditions/>}/>
                        <PublicRoute path='/FAQs' component={<AsyncFAQs/>}/>
                        <PublicRoute path='/login' component={<AsyncLogin/>}/>
                        <Route render={() => <AsyncHome uid={data.uid}/>}/>
                    </Switch>)
                : ''

        )
    }
}

export default withRouter<any>(connect(null, null)(MasterRouter as any)) as typeof MasterRouter
