// - Import react components
import PrivateRoute from './PrivateRoute'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Route, Switch, withRouter, Redirect, NavLink} from 'react-router-dom'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import Loadable from 'react-loadable'
import {Map} from 'immutable'
import {IRouterProps} from './IRouterProps'
import MasterLoadingComponent from 'components/masterLoading/MasterLoadingComponent'

// - Async Components
const AsyncStream = Loadable({
    loader: () => import('containers/stream'),
    loading: MasterLoadingComponent,
})
const AsyncProfile = Loadable({
    loader: () => import('containers/profile'),
    loading: MasterLoadingComponent,
})
const AsyncPostPage = Loadable({
    loader: () => import('containers/postPage'),
    loading: MasterLoadingComponent,
})

const AsyncJob = Loadable({
    loader: () => import('containers/job'),
    loading: MasterLoadingComponent,
})

const AsyncJobSeeker = Loadable({
    loader: () => import('components/job/jobSeeker'),
    loading: MasterLoadingComponent,
})

const AsyncPeople = Loadable({
    loader: () => import('containers/people'),
    loading: MasterLoadingComponent,
})

const AsyncPhotos = Loadable({
    loader: () => import('containers/photos'),
    loading: MasterLoadingComponent,
})
const AsyncAbout = Loadable({
    loader: () => import('containers/about'),
    loading: MasterLoadingComponent,
})
const AsyncChangePassword = Loadable({
    loader: () => import('components/settings/changePassword'),
    loading: MasterLoadingComponent,
})
const AsyncAccountSetting = Loadable({
    loader: () => import('components/settings/account'),
    loading: MasterLoadingComponent,
})
const AsyncEducation = Loadable({
    loader: () => import('components/profile/about/education'),
    loading: MasterLoadingComponent,
})
const AsyncIndustry = Loadable({
    loader: () => import('components/profile/about/industry'),
    loading: MasterLoadingComponent,
})
const AsyncInterests = Loadable({
    loader: () => import('components/profile/about/interest'),
    loading: MasterLoadingComponent,
})
const AsyncProfessions = Loadable({
    loader: () => import('components/profile/about/profession'),
    loading: MasterLoadingComponent,
})
const AsyncPersonalInformation = Loadable({
    loader: () => import('components/settings/personalInfo'),
    loading: MasterLoadingComponent,
})
const AsyncRegistrationStep = Loadable({
    loader: () => import('containers/registration'),
    loading: MasterLoadingComponent,
})
const AsyncRegistrationEducation = Loadable({
    loader: () => import('containers/registration/education'),
    loading: MasterLoadingComponent,
})
const AsyncRegistrationLocation = Loadable({
    loader: () => import('containers/registration/location'),
    loading: MasterLoadingComponent,
})
const AsyncPlatform = Loadable({
    loader: () => import('containers/platform'),
    loading: MasterLoadingComponent,
})
const AsyncSetting = Loadable({
    loader: () => import('components/settings/personalInfo'),
    loading: MasterLoadingComponent,
})
const AsyncJobSetting = Loadable({
    loader: () => import('components/job/jobPreferences'),
    loading: MasterLoadingComponent,
})
const AsyncEmployeeSetting = Loadable({
    loader: () => import('components/job/employerPreferences'),
    loading: MasterLoadingComponent,
})
const AsyncJobAsCompany = Loadable({
    loader: () => import('components/job/jobAsCompany'),
    loading: MasterLoadingComponent,
})
const AsyncJobAsNonCompany = Loadable({
    loader: () => import('components/job/jobAsNonCompany'),
    loading: MasterLoadingComponent,
})
const AsyncHomePage = Loadable({
    loader: () => import('components/homePage'),
    loading: MasterLoadingComponent,
})
const AsyncNotification = Loadable({
    loader: () => import('components/sugestionBox/notifications/notificationList'),
    loading: MasterLoadingComponent,
})
const AsyncEmailNotification = Loadable({
    loader: () => import('components/sugestionBox/notifications/notificationList/emailNotification'),
    loading: MasterLoadingComponent,
})
const AsyncBlockList = Loadable({
    loader: () => import('components/profile/blockPeople/blockPeopleList'),
    loading: MasterLoadingComponent,
})

const AsyncJobItem = Loadable({
    loader: () => import('components/job/jobItem'),
    loading: MasterLoadingComponent,
})

const AsyncApplyForJob = Loadable({
    loader: () => import('components/job/applyForJob'),
    loading: MasterLoadingComponent,
})

/**
 * Home Router
 */
export class HomeRouter extends Component<IRouterProps, any> {
    render() {
        const {enabled, match, data, translate} = this.props
        const St = AsyncStream
        return (
            enabled ? (
                    <Switch>
                        <Route path='/:userId/about/interests' component={AsyncInterests}/>
                        <Route path='/:userId/about/education' component={AsyncEducation}/>
                        <Route path='/:userId/about/industry' component={AsyncIndustry}/>
                        <Route path='/:userId/about' component={AsyncProfessions}/>
                        <PrivateRoute path='/feed/:pltId' component={<AsyncPlatform/>}/>
                        <Route path='/job-preferences' component={AsyncJobSetting}/>
                        <Route path='/employer-preferences' component={AsyncEmployeeSetting}/>
                        <Route path='/post-job' component={AsyncJobAsCompany}/>
                        <Route path='/job-as-non-company' component={AsyncJobAsNonCompany}/>
                        <Route path='/job/:jobId' component={AsyncJobItem}/>
                        <Route path='/job-submit-proposal' component={AsyncApplyForJob}/>
                        <PrivateRoute path='/notification' component={<AsyncNotification/>}/>
                        <PrivateRoute path='/email-notification' component={<AsyncEmailNotification/>}/>
                        <PrivateRoute path='/block-list' component={<AsyncBlockList/>}/>
                        <PrivateRoute path='/settings/account' component={<AsyncAccountSetting/>}/>
                        <PrivateRoute path='/settings/changepassword' component={<AsyncChangePassword/>}/>
                        <PrivateRoute path='/registration/education' component={<AsyncRegistrationEducation/>}/>
                        <PrivateRoute path='/registration/location' component={<AsyncRegistrationLocation/>}/>
                        <PrivateRoute path='/registration' component={<AsyncRegistrationStep/>}/>
                        <PrivateRoute path='/settings' component={<AsyncSetting/>}/>
                        {/*<PrivateRoute path='/job-preferences' component={<AsyncJobSetting/>}/>*/}
                        <PrivateRoute path='/people/:tab?' component={<AsyncPeople/>}/>
                        <PrivateRoute path='/tag/:tag' component={(
                            <St displayWriting={false} homeTitle={`#${match.params.tag}`} posts={data.mergedPosts}/>
                        )}/>
                        <Route path='/:userId/timeline' component={AsyncProfile}/>
                        <Route path='/:userId/people' component={AsyncPeople}/>
                        <Route path='/:userId/photos' component={AsyncPhotos}/>
                        <Route path='/:userId/posts/:postId/:tag?' component={AsyncPostPage}/>
                        <PrivateRoute path='/find-job/:type' component={<AsyncJob data={data}/>}/>
                        <PrivateRoute path='/find-job'
                                      component={<AsyncJob data={data}/>}/>
                        <PrivateRoute path='/find-job-seeker'
                                      component={<AsyncJobSeeker data={data}/>}/>
                        <Route path='/:userId' component={AsyncProfile}/>

                        <PrivateRoute path='/' component={<AsyncHomePage data={data}/>}/>
                    </Switch>
                )
                : ''
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IRouterProps) => {
    return {}
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IRouterProps) => {
    return {
        translate: getTranslate(state.get('locale')),
        currentLanguage: getActiveLanguage(state.get('locale')).code,
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeRouter as any) as any) as typeof HomeRouter
