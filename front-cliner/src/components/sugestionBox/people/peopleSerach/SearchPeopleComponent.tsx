// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import InfiniteScroll from 'react-infinite-scroller'
import { getTranslate, getActiveLanguage } from 'react-localize-redux'
import {Map} from 'immutable'

// - Import app components
import UserBoxList from 'components/userBoxList/index'
import LoadMoreProgressComponent from 'layouts/loadMoreProgress/index'

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import { ISearchPeopleComponentProps } from './ISearchPeopleComponentProps'
import { ISearchPeopleComponentState } from './ISearchPeopleComponentState'
import { UserTie } from 'core/domain/circles/userTie'

/**
 * Create component class
 */
export class SearchPeopleComponent extends Component<ISearchPeopleComponentProps, ISearchPeopleComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
  constructor (props: ISearchPeopleComponentProps) {
    super(props)
        // Defaul state
    this.state = {
    }

  }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
  render () {
    const {translate} = this.props

    return (
        <div className='container'>
            <div className='row'>
                <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                    <div className='ui-block responsive-flex'>
                        <div className='ui-block-title'>
                            <div className='h6 title'></div>
                            <form className='w-search'>
                                <div className='form-group with-button'>
                                    <input className='form-control' type='text' placeholder='Search Friends...'/>
                                    <button>
                                        <svg className='olymp-magnifying-glass-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon'/>
                                        </svg>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISearchPeopleComponentProps) => {
  return {
  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: ISearchPeopleComponentProps) => {
  return {
    translate: getTranslate(state.get('locale')),
  }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(SearchPeopleComponent as any)
