import {Profile} from 'core/domain/users/profile'
import {UserTie} from 'core/domain/circles/index'

export interface ISearchPeopleComponentProps {
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}
