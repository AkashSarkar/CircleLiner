import {Profile} from 'core/domain/users/index'
import {UserTie} from 'core/domain/circles/index'
import {Map} from 'immutable'

export interface IPeopleSugessionComponentProps {
    /**
     * uid
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    uid?: string

    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    users?: Map<string, UserTie>

    /**
     * Load users' profile
     *
     * @memberof IFindPeopleComponentProps
     */
    loadPeople?: (page: number, limit: number) => any

    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     */
    currentLanguage?: string
}
