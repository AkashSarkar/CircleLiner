// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
// - Import app components

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import {IPeopleSugessionComponentProps} from './IPeopleSugessionComponentProps'
import {IPeopleSuggestionComponentState} from './IPeopleSuggestionComponentState'

import PeopleSuggestionItemComponent from 'components/sugestionBox/people/peopleSuggestionItem/index'
import {UserTie} from 'core/domain/circles/userTie'

/**
 * Create component class
 */
export class PeopleSugessionComponent extends Component<IPeopleSugessionComponentProps, IPeopleSuggestionComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IPeopleSugessionComponentProps) {
        super(props)
        this.state = {}
    }

    componentWillMount() {
        const {loadPeople} = this.props
        loadPeople!(0, 4)
    }

    peopleSuggestion = () => {

        let uid = this.props.uid!
        const users = Map<string, UserTie>(this.props.users!)
        const item: any[] = []
        if (users) {
            users.forEach((user: UserTie, key: string) => {
                if (uid !== key) {
                    item.push(
                        <PeopleSuggestionItemComponent key={key} userId={key} fullName={user.fullName}
                                                       avatar={user.avatar}/>
                    )
                }
            })
        }
        return item
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <div className='ui-block'>
                <div className='ui-block-title'>
                    <h6 className='title'>People Suggestions</h6>
                </div>
                {/* W-Action */}
                <ul className='widget w-friend-pages-added notification-list friend-requests'>
                    {this.peopleSuggestion()}
                </ul>
                {/* ... end W-Action */}
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPeopleSugessionComponentProps) => {
    return {
        loadPeople: (page: number, limit: number) => dispatch(userActions.dbGetPeopleInfo(page, limit))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPeopleSugessionComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const info: Map<string, any> = state.getIn(['user', 'info'])
    return {
        translate: getTranslate(state.get('locale')),
        uid,
        users: info,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PeopleSugessionComponent as any) as any)
