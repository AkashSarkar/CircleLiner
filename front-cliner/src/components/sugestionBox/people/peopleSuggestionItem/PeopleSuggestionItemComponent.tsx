// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {NavLink} from 'react-router-dom'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import ImgCover from 'components/profile/imgCover/index'
import UserAvatar from 'components/userAvatar/index'
import ImageGallery from 'components/imageGallery/index'
import AppDialogTitle from 'layouts/dialogTitle/index'
import AppInput from 'layouts/appInput/index'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import * as globalActions from 'store/actions/globalActions'
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import {IPeopleSuggestionItemComponentProps} from './IPeopleSuggestionItemComponentProps'
import {IPeopleSuggestionItemComponentState} from './IPeopleSuggestionItemComponentState'
import {Profile} from 'core/domain/users/index'
import {push} from 'react-router-redux'

/**
 * Create component class
 */
export class PeopleSuggestionItemComponent extends Component<IPeopleSuggestionItemComponentProps, IPeopleSuggestionItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: IPeopleSuggestionItemComponentProps) {
        super(props)
        this.state = {}
    }

    render() {
        const {userId,translate, avatar, fullName} = this.props
        return (
            <li className='inline-items'>
                <div className='author-thumb'
                     style={{cursor: 'pointer',}}>
                    <UserAvatar fileName={avatar} fullName={fullName} size={36}/>
                </div>
                <div className='notification-event'>
                    <NavLink to={`/${userId}`} className='h6 notification-friend'>
                        <span>{fullName!}</span>
                    </NavLink>
                    <span className='chat-message-item'>10 Friends in Common</span>
                </div>
                <span className='notification-icon'>
                <a href={'javascript:(0)'} className='accept-request'>
                    <span className='icon-add without-text'>
                        <svg className='olymp-happy-face-icon'><use
                            xlinkHref='/svg-icons/sprites/icons.svg#olymp-happy-face-icon'/></svg>
                    </span>
                </a>
            </span>
            </li>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPeopleSuggestionItemComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPeopleSuggestionItemComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale'))
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PeopleSuggestionItemComponent as any) as any)
