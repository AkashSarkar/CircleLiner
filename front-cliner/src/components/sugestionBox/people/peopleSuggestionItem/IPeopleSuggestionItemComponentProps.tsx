import {Profile} from 'core/domain/users/index'
import {UserTie} from 'core/domain/circles/index'
import {Map} from 'immutable'

export interface IPeopleSuggestionItemComponentProps {

    /**
     * uid
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    userId?: string

    /**
     * User's banner URL
     *
     * @type {string}
     * @memberof IEditProfileComponentState
     */
    /**
     * User full name
     */
    fullName?: string

    /**
     * User avatar address
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    avatar?: string
    /**
     * Redirect page to [url]
     */
    goTo?: (url: string) => any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

}
