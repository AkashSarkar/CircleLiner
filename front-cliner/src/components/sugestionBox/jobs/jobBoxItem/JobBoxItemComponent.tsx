// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import {push} from 'react-router-redux'
import PropTypes from 'prop-types'
import moment from 'moment/moment'
import Linkify from 'react-linkify'
import copy from 'copy-to-clipboard'
import SvgClose from '@material-ui/icons/Close'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
import {IJobBoxItemComponentProps} from './IJobBoxItemComponentProps'
import {IJobBoxItemComponentState} from './IJobBoxItemComponentState'
import {Industry} from 'core/domain/users/index'

// - Create component class
export class JobBoxItemComponent extends Component<IJobBoxItemComponentProps, IJobBoxItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    styles = {
        logo: {
            marginTop: '20%',
        }
    }

    constructor(props: IJobBoxItemComponentProps) {
        super(props)
        this.state = {
            isOpen: props.isOpen!
        }
        // Binding functions to this

    }

    onClose = () => {
        this.setState({
            isOpen: false
        })
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {hasIndustry} = this.props
        // Define variables
        return (this.state.isOpen ?
            (
                <li style={{ padding: 8}}>
                    <div className='row' >
                        <div className='col-3' style={{paddingRight: 0}}><img style={this.styles.logo} src='./img/hiring.png'/></div>
                        <div className='col-9'>
                            <div className='more' style={{float: 'right'}}>
                                <svg onClick={this.onClose} className='olymp-little-delete'>
                                    <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-little-delete'/>
                                </svg>
                            </div>
                            <a href='#' className='h6 title'>Software Developer </a>
                            <time className='entry-date updated' dateTime='2017-06-24T18:18'>5 days left
                            </time>
                            <div className='forums'>Netexpro</div>
                        </div>
                    </div>
                </li>
            ) : '')
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobBoxItemComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobBoxItemComponentProps) => {

    const uid = state.getIn(['authorize', 'uid'])
    const industries: Map<string, Industry> = state.getIn(['user', 'industry', uid])
    let notifyBox = state.getIn(['notifyBox', 'boxNotifies'])
    return {
        translate: getTranslate(state.get('locale')),
        hasIndustry: industries ? industries : '',
        userId: uid,
        isOpen: !!notifyBox
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(JobBoxItemComponent as any)