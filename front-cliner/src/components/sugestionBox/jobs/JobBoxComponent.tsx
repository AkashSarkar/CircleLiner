// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import {push} from 'react-router-redux'
import PropTypes from 'prop-types'
import moment from 'moment/moment'
import Linkify from 'react-linkify'
import copy from 'copy-to-clipboard'
import SvgClose from '@material-ui/icons/Close'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import JobSugestionItem from './jobBoxItem'
import {Map} from 'immutable'
import {IJobBoxComponentProps} from './IJobBoxComponentProps'
import {IJobBoxComponentState} from './IJobBoxComponentState'
import {Industry} from 'core/domain/users/index'

// - Create component class
export class JobBoxComponent extends Component<IJobBoxComponentProps, IJobBoxComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobBoxComponentProps) {
        super(props)
        this.state = {
            isOpen: props.isOpen!
        }
        // Binding functions to this

    }

    onClose = () => {
        this.setState({
            isOpen: false
        })
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {hasIndustry} = this.props
        // Define variables
        return (this.state.isOpen ?
            (
                <div className='ui-block'>
                    <div className='h6 ui-block-title'>
                        <div className='more' style={{float: 'right'}}>
                            <svg onClick={this.onClose} className='olymp-little-delete'><use xlinkHref='/svg-icons/sprites/icons.svg#olymp-little-delete' /></svg>
                        </div>
                        Job Sugestionns
                    </div>
                    <div className='ui-content m-2'>
                        {/* Job Sugession*/}
                        <ul className='w-featured-topics table-careers' >
                            <JobSugestionItem userId={this.props.userId!} isOpen={true}/>
                            <JobSugestionItem userId={this.props.userId!} isOpen={true}/>
                            <JobSugestionItem userId={this.props.userId!} isOpen={true}/>
                            <JobSugestionItem userId={this.props.userId!} isOpen={true}/>
                        </ul>
                        {/* ... end WJob Sugession */}
                    </div>
                </div>) : '')
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobBoxComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobBoxComponentProps) => {

    const uid = state.getIn(['authorize', 'uid'])
    const industries: Map<string, Industry> = state.getIn(['user', 'industry', uid])
    let notifyBox = state.getIn(['notifyBox', 'boxNotifies'])
    return {
        translate: getTranslate(state.get('locale')),
        hasIndustry: industries ? industries : '',
        userId: uid,
        isOpen: !!notifyBox
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(JobBoxComponent as any)