import {Comment} from 'core/domain/comments/index'
import {Post} from 'core/domain/posts/post'
import {Map} from 'immutable'

export interface IAddContactComponentProps {

    userId: string
    hasIndustry?: string
    isOpen?: boolean
    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}
