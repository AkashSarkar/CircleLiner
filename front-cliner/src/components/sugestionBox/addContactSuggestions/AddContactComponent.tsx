// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import {push} from 'react-router-redux'
import PropTypes from 'prop-types'
import moment from 'moment/moment'
import Linkify from 'react-linkify'
import copy from 'copy-to-clipboard'

// - Malarial - UI
import SvgClose from '@material-ui/icons/Close'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

// - Import domains
import {Industry} from 'core/domain/users/index'

// - Import Interfaces
import {IAddContactComponentProps} from './IAddContactComponentProps'
import {IAddContactComponentState} from './IAddContactComponentState'

// - Create component class
export class AddContactComponent extends Component<IAddContactComponentProps, IAddContactComponentState> {

    styles = {
        formGroup : {
            marginBottom: 10,
        },
        button : {
            marginTop: 2,
            fontColor: 'black'
        },
        input: {
            padding: 0,
            height: 30,
            borderRadius: 'none'
        },
    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IAddContactComponentProps) {
        super(props)
        this.state = {
            isOpen: props.isOpen!
        }
        // Binding functions to this

    }

    onClose = () => {
        this.setState({
            isOpen: false
        })
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {hasIndustry} = this.props
        // Define variables
        return (this.state.isOpen ?
            (
                <div className='ui-block'>
                    <div className='h6 ui-block-title'>
                        <div className='more' style={{float: 'right'}}>
                            <svg onClick={this.onClose} className='olymp-little-delete'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-little-delete'/>
                            </svg>
                        </div>
                        Add Personal Contacts
                    </div>
                    <div className='ui-content'>
                    <div className='align-center'>
                        <ul className='inline-block'>
                            <li className='float-left m-1'>
                                <a href='#'>
                                    <img src='/img/gmail-24.png' alt='icon' data-toggle='tooltip' data-placement='top'
                                         data-original-title='LIKE'/>
                                </a>
                            </li>
                            <li className='float-left m-1'>
                                <a href='#'>
                                    <img src='/img/yahoo.png' alt='icon' data-toggle='tooltip' data-placement='top'
                                         data-original-title='LIKE'/>
                                </a>
                            </li>
                            <li className='float-left m-1'>
                                <a href='#'>
                                    <img src='/img/aol.png' alt='icon' data-toggle='tooltip' data-placement='top'
                                         data-original-title='COOL'/>
                                </a>
                            </li>
                            <li className='float-left m-1'>
                                <a href='#'>
                                    <img src='/img/outlook.png' alt='icon' data-toggle='tooltip' data-placement='top'
                                         data-original-title='LIKE'/>
                                </a>
                            </li>
                            <li className='float-left m-1'>
                                <a href='#'>
                                    <img src='/img/e-mail-envelope.png' alt='icon' data-toggle='tooltip' data-placement='top'
                                         data-original-title='LIKE'/>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <p className='align-center p-2'>You control who you connect to, and you can manage your contacts anytime</p>
                     <form className='m-2'>
                        <div style={this.styles.formGroup as any}>
                            <input style={this.styles.input} type='text'/>
                            <span className='material-input'/>
                        </div>
                         <button className='btn-primary' style={this.styles.button}>Find friends!</button>
                    </form>
                    </div>
                </div>) : '')
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IAddContactComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IAddContactComponentProps) => {

    const uid = state.getIn(['authorize', 'uid'])
    const industries: Map<string, Industry> = state.getIn(['user', 'industry', uid])
    let notifyBox = state.getIn(['notifyBox', 'boxNotifies'])
    return {
        translate: getTranslate(state.get('locale')),
        hasIndustry: industries ? industries : '',
        userId: uid,
        isOpen: !!notifyBox
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(AddContactComponent as any)