// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
import {IEmailNotificationListComponentProps} from './IEmailNotificationListComponentProps'
import {IEmailNotificationListComponentState} from './IEmailNotificationListComponentState'
import InfiniteScroll from 'react-infinite-scroller'
import Setting from 'layouts/setting/index'
import {NotificationItemComponent} from 'components/sugestionBox/notifications/notificationItem/NotificationItemComponent'

import LoadMoreProgressComponent from 'layouts/loadMoreProgress/index'
import {notifyActions} from 'store/actions/index'

export class EmailNotificationListComponent extends Component<IEmailNotificationListComponentProps, IEmailNotificationListComponentState> {

    constructor(props: IEmailNotificationListComponentProps) {
        super(props)

    }

    notifyItemList = () => {
        let {info, onRequestClose} = this.props
        let notifications: Map<string, Map<string, any>> = this.props.notifications!
        let parsedDOM: any[] = []
        if (notifications) {
            notifications.forEach((notification, key) => {
                const notifierUserId = notification!.get('notifierUserId')
                const userInfo = info!.get(notifierUserId)
                parsedDOM.push(
                    <NotificationItemComponent
                        key={key}
                        description={notification!.get('description', '')}
                        fullName={(userInfo ? userInfo.fullName || '' : '')}
                        avatar={(userInfo ? userInfo.avatar || '' : '')}
                        id={key!}
                        isSeen={notification!.get('isSeen', false)}
                        url={notification!.get('url')}
                        notifierUserId={notifierUserId}
                        closeNotify={onRequestClose}
                    />
                )
            })
        }
        return parsedDOM
    }

    scrollLoad = (page: number) => {
        const {loadNotification} = this.props
        loadNotification!(page, 5)
    }

    render() {
        const {hasMoreNotification} = this.props
        return (
            <Setting userId={this.props.userId}>
                <div className='container'>
                    <div className='row'>
                        <div
                            className='col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12'>
                            <div className='ui-block'>
                                <div className='ui-block-title'>
                                    <h6 className='title'>Notifications</h6>
                                    <a href='#' className='more'>
                                        <svg className='olymp-three-dots-icon'>
                                            <use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                                        </svg>
                                    </a>
                                </div>
                                {/* Notification List */}
                                <ul className='notification-list'>
                                    <InfiniteScroll
                                        pageStart={0}
                                        loadMore={this.scrollLoad}
                                        hasMore={hasMoreNotification}
                                        useWindow={true}
                                        loader={<LoadMoreProgressComponent key='find-people-load-more-progress'/>}
                                    >
                                        {this.notifyItemList()}
                                    </InfiniteScroll>
                                </ul>
                                {/* ... end Notification List */}
                            </div>
                        </div>
                    </div>
                </div>
            </Setting>

        )
    }
}

const mapDispatchToProps = (dispatch: any, ownProps: IEmailNotificationListComponentProps) => {
    return {
        loadNotification: (page: number, limit: number) => dispatch(notifyActions.dbGetPaginatedNotifications(page, limit))
    }
}

const mapStateToProps = (state: Map<string, any>, ownProps: IEmailNotificationListComponentProps) => {
    const {userId} = state.getIn(['authorize', 'uid'])
    const notifications = state.getIn(['notify', 'userNotifies'])
    let hasMoreNotification = state.getIn(['notify', 'hasMoreData'])
    return {
        translate: getTranslate(state.get('locale')),
        userId: userId,
        notifications: notifications,
        hasMoreNotification: hasMoreNotification,
        info: state.getIn(['user', 'info'])
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((EmailNotificationListComponent as any) as any)
