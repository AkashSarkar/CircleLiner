// - Import react components
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { push } from 'react-router-redux'
import SvgClose from '@material-ui/icons/Close'
import { grey } from '@material-ui/core/colors'
import { withStyles } from '@material-ui/core/styles'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'

// - Import app components
import UserAvatar from 'components/userAvatar/index'

// - Import API

// - Import actions
import * as notifyActions from 'store/actions/notifyActions/notifyActions'

import { INotificationItemComponentProps } from './INotificationItemComponentProps'
import { INotificationItemComponentState } from './INotificationItemComponentState'

const styles = (theme: any) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  closeButton: {color: 'black'},
  closeIcon: {width: 12, height: 12},
  listItem: {
    backgroundColor: 'white',
    marginBottom: '6px'
  }
})

/**
 * Create component class
 */
export class NotificationItemComponent extends Component<INotificationItemComponentProps,INotificationItemComponentState> {

  static propTypes = {
        /**
         * Notification description
         */
    description: PropTypes.string,
        /**
         * Which user relates to the notification item
         */
    fullName: PropTypes.string,
        /**
         * Avatar of the user who relate to the notification item
         */
    avatar: PropTypes.string,
        /**
         * Notification identifier
         */
    id: PropTypes.string,
        /**
         * If user's seen the notification or not (true/false)
         */
    isSeen: PropTypes.bool,
        /**
         * Which address notification refers
         */
    url: PropTypes.string,
        /**
         * The notifier user identifier
         */
    notifierUserId: PropTypes.string,
        /**
         * Close notification popover
         */
    closeNotify: PropTypes.func
  }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
  constructor (props: INotificationItemComponentProps) {
    super(props)

        // Defaul state
    this.state = {
    }

        // Binding functions to `this`
    this.handleSeenNotify = this.handleSeenNotify.bind(this)
  }

  handleSeenNotify = (event: any) => {
    const { seenNotify, id, url, goTo, isSeen } = this.props
    if (id) {
      if (!isSeen) {
        seenNotify!(id)
      }
      goTo!(url)
    }
  }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
  render () {
    let { description, fullName, avatar, isSeen, id, goTo, notifierUserId, url, deleteNotiy, classes } = this.props

    return (
        <li className={isSeen ?  'un-read' : ''} style={isSeen ? { opacity: 0.6 } : {}} key={notifierUserId} >
            <div className='author-thumb'>
                <UserAvatar fullName={fullName} fileName={avatar} />
            </div>
        
            <div className='notification-event' onClick={this.handleSeenNotify}>
                <div>
                    <NavLink className='h6 notification-friend'
                             to={`/${notifierUserId}`}
                             onClick={(evt) => {
                                 goTo!(`/${notifierUserId}`)
                             }}
                    >
                        {fullName}
                    </NavLink>
                    <NavLink className='notification-link'
                             to={url}
                             onClick={this.handleSeenNotify}
                    >
                        {description}
                    </NavLink>.
                </div>
                <span className='notification-date'>
                    <time className='entry-date updated' dateTime='2004-07-24T18:18'>4 hours ago</time>
                </span>
            </div>
            <div className='more'>
                <svg className='olymp-three-dots-icon'><use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon' /></svg>
                <svg onClick={() => deleteNotiy!(id)} className='olymp-little-delete'><use xlinkHref='/svg-icons/sprites/icons.svg#olymp-little-delete' /></svg>
            </div>
        </li>
    )
  }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: INotificationItemComponentProps) => {
  return {
    goTo: (url: string) => dispatch(push(url)),
    seenNotify: (id: string) => dispatch(notifyActions.dbSeenNotification(id)),
    deleteNotiy: (id: string) => dispatch(notifyActions.dbDeleteNotification(id))
  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: INotificationItemComponentProps) => {
  return {

  }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(NotificationItemComponent as any) as any )
