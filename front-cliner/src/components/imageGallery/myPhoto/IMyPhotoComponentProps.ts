import {Image} from 'core/domain/imageGallery'
import {Map, Collection, List} from 'immutable'

export interface IMyPhotoComponentProps {

    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * Recieve request close function
     */
    onRequestClose: () => void
    /**
     * List of image in image gallery
     */
    images?: List<Image>
    /**
     * Select image from image gallery
     *
     * @type {(URL: string,fullPath: string)}
     * @memberof IImageGalleryComponentProps
     */
    set: (URL: string, fullPath: string) => void

    /**
     * Delete an image
     *
     * @memberof IImageGalleryComponentProps
     */
    deleteImage?: (imageId: string) => void

    /**
     * Upload image to the server
     *
     * @memberof IImageGalleryComponentProps
     */
    uploadImage?: (image: any, imageName: string) => any
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * Open photo gallary dialog
     *
     * @memberof IProfileHeaderComponentProps
     */
    closeMyPhoto?: () => void
}
