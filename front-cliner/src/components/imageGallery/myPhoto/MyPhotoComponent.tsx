// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
import PropTypes, {any} from 'prop-types'
import SvgAddImage from '@material-ui/icons/AddAPhoto'
import SvgDelete from '@material-ui/icons/Delete'

import {Card, CardActions, CardHeader, CardMedia, CardContent} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import {grey} from '@material-ui/core/colors'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import {Manager, Target, Popper} from 'react-popper'

// - Import app components
// - Import API
// - Import actions
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import * as globalActions from 'store/actions/globalActions'

import {IMyPhotoComponentProps} from './IMyPhotoComponentProps'
import {IMyPhotoComponentState} from './IMyPhotoComponentState'
import {Post} from 'core/domain/posts'
import Grid from '@material-ui/core/Grid/Grid'

// - Import Layouts
import LModal from 'layouts/modal'
import {Image} from 'core/domain/imageGallery'
import * as userActions from 'store/actions/userActions/userActions'

// - Create PostWrite component class
export class MyPhotoComponent extends Component<IMyPhotoComponentProps, IMyPhotoComponentState> {
    styles = {
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around'
        },
        gridList: {
            width: 500,
            height: 450,
            overflowY: 'auto'
        },
        uploadButton: {
            verticalAlign: 'middle',
            fontWeight: 400
        },
        uploadInput: {
            cursor: 'pointer',
            position: 'absolute',
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            width: '100%',
            opacity: 0
        },
        deleteImage: {
            marginLeft: '5px',
            cursor: 'pointer',
            color: 'white'
        },
        addImage: {
            marginRight: '5px',
            cursor: 'pointer',
            color: 'white'
        }
    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IMyPhotoComponentProps) {
        super(props)
        // Default state
        // Binding functions to `this`
        this.close = this.close.bind(this)
        this.handleSetImage = this.handleSetImage.bind(this)
        this.handleDeleteImage = this.handleDeleteImage.bind(this)
        this.imageList = this.imageList.bind(this)
    }

    /**
     * Hide image gallery
     */
    close = () => {
        this.props.onRequestClose!()
    }
    /**
     * Handle set image
     * @param event
     * @param URL
     * @param fullPath
     */
    handleSetImage = (event: any, URL: string, fullPath: string) => {
        this.props.set!(URL, fullPath)
        this.close()
    }

    /**
     * Handle delete image
     * @param event
     * @param  {integer} id is the image identifier which selected to delete
     */
    handleDeleteImage = (event: any, id: string) => {
        this.props.deleteImage!(id)
    }

    imageList = () => {
        return this.props.images!.map((image: Image, index) => {

            return (
                <div className='choose-photo-item' data-mh='choose-item' key={image.id!}>
                    <div className='radio'>
                        <label className='custom-radio'>
                            <img src={image.URL} alt='photo'/>
                            <span onClick={evt => this.handleDeleteImage(evt, image.id!)} style={{
                                position: 'absolute', width: '28px', backgroundColor: 'rgba(255, 255, 255, 0.22)',
                                height: '28px', right: 12, top: 4, cursor: 'pointer', borderRadius: '50%',
                                display: 'flex', alignItems: 'center', justifyContent: 'center'
                            }}>
                                <SvgDelete style={this.styles.deleteImage as any}/>
                            </span>
                            <span onClick={evt => this.handleSetImage(evt, image.URL, image.fullPath)} style={{
                                position: 'absolute', width: '28px', backgroundColor: 'rgba(255, 255, 255, 0.22)',
                                height: '28px', right: 44, top: 4, cursor: 'pointer', borderRadius: '50%',
                                display: 'flex', alignItems: 'center', justifyContent: 'center'
                            }}>
                                <SvgAddImage style={this.styles.addImage as any}/>
                            </span>
                        </label>
                    </div>
                </div>
            )
        })
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate} = this.props
        const myPhotoModalAction = (
            <div>
                <a href={'javascript:(0)'} className='btn btn-secondary btn-lg btn--half-width'>Cancel</a>
                <a href={'javascript:(0)'} className='btn btn-primary btn-lg btn--half-width'>Confirm Photo</a>
            </div>
        )
        return (
            <LModal open={this.props.open} onClose={this.props.closeMyPhoto} modalClassName={'choose-from-my-photo'}
                    modalTitle={'Choose from My Photos'} modalFooterAction={myPhotoModalAction}>
                {this.imageList()}
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IMyPhotoComponentProps) => {
    return {
        closeMyPhoto: () => dispatch(userActions.closeChangeAvatar()),
        uploadImage: (image: any, imageName: string) => dispatch(imageGalleryActions.dbUploadImage(image, imageName)),
        deleteImage: (id: string) => dispatch(imageGalleryActions.dbDeleteImage(id)),
        progressChange: (percent: number, status: boolean) => dispatch(globalActions.progressChange(percent, status))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IMyPhotoComponentProps) => {
    return {
        images: state.getIn(['imageGallery', 'images']) || [],
        translate: getTranslate(state.get('locale')),

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(MyPhotoComponent as any)
