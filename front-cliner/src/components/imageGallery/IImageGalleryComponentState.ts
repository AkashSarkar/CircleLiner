import {float} from 'aws-sdk/clients/lightsail'

export interface IImageGalleryComponentState {
    /**
     * Width of cropping image
     */
    width?: number
    /**
     * Height of cropping image
     */
    height?: number
    /**
     * Height of cropping image
     */
    position?: { x: number, y: number }

    image: string,
    /**
     * If it's true post writing page will be open
     */
    openChangeCover: boolean
    /**
     * If it's true post writing page will be open
     */
    openChangeAvatar: boolean
    /**
     * If it's true post writing page will be open
     */
    closeMyPhoto?: boolean
    /**
     * If it's true post writing page will be open
     * @memberof IImageGalleryComponentState
     */
    openCropPhoto: boolean
    /**
     * If it's true post writing page will be open
     * @memberof IImageGalleryComponentState
     */
    closeCropPhoto?: boolean
    /**
     * If it's true post writing page will be open
     * @memberof IImageGalleryComponentState
     */
    openAddPostImage?: boolean

    /**
     * User's avatar URL address
     *
     * @type {string}
     * @memberof IImageGalleryComponentState
     */
    avatar: string
    /**
     * User's cover photo URL address
     *
     * @type {string}
     * @memberof IImageGalleryComponentState
     */
    coverPhoto: string
}
