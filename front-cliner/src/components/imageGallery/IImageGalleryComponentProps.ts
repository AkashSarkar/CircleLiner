import {Profile} from 'core/domain/users'

export interface IImageGalleryComponentProps {
    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * Close image gallery
     *
     * @memberof IImageGalleryComponentProps
     */
    close?: () => void
    /**
     * If it's true image upload will be crop for profile picture
     */
    cropProfilePic?: boolean
    /**
     * If it's true image upload will be crop for cover picture
     */
    cropCoverPic?: boolean
    /**
     * Update user profile
     *
     * @memberof IEditProfileComponentProps
     */
    updateAvatar?: (avatar: string) => void
    /**
     * Update user profile
     *
     * @memberof IEditProfileComponentProps
     */
    updateCoverPhoto?: (coverPhoto: string) => void
    /**
     * User profile banner addresss
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    coverPhoto?: string

    /**
     * User avatar address
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    avatar?: string
    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * Open photo gallary dialog
     *
     * @memberof IProfileHeaderComponentProps
     */
    openMyPhoto?: () => void
    /**
     * Whether my photo is open
     */
    myPhotoOpen?: boolean
    /**
     * Select image from image gallery
     *
     * @type {(URL: string,fullPath: string)}
     * @memberof IImageGalleryComponentProps
     */
    set?: (URL: string, fullPath: string) => void
}