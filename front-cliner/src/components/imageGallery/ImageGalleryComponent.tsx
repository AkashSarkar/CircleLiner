// - Impoer react components
import React, {Component} from 'react'
import PropTypes, {any} from 'prop-types'
import {connect} from 'react-redux'
import GridList from '@material-ui/core/GridList'
import GridListTileBar from '@material-ui/core/GridListTileBar'
import GridListTile from '@material-ui/core/GridListTile'
import IconButton from '@material-ui/core/IconButton'
import StarBorder from '@material-ui/icons/StarBorder'
import Button from '@material-ui/core/Button'
import SvgUpload from '@material-ui/icons/CloudUpload'
import SvgAddImage from '@material-ui/icons/AddAPhoto'
import SvgDelete from '@material-ui/icons/Delete'
import {grey} from '@material-ui/core/colors'
import {withStyles} from '@material-ui/core/styles'
import uuid from 'uuid'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import Dropzone from 'react-dropzone'
import AvatarEditor from 'react-avatar-editor'
import {Map} from 'immutable'

// - Import actions
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import * as globalActions from 'store/actions/globalActions'
import * as userActions from 'store/actions/userActions/userActions'

// - Import app components
import Img from 'components/img'
import MyPhotoComponent from 'components/imageGallery/myPhoto'
import CropPhotoComponent from 'components/imageGallery/cropPhoto'

// - Import API
import FileAPI from 'api/FileAPI'
import {IImageGalleryComponentProps} from './IImageGalleryComponentProps'
import {IImageGalleryComponentState} from './IImageGalleryComponentState'
import {Profile} from 'core/domain/users'

// - Import Layouts
import LModal from 'layouts/modal'

const styles = (theme: any) => ({
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0,
            overflowY: 'auto'
        }
    }
})

/**
 * Create ImageGallery component class
 */
export class ImageGalleryComponent extends Component<IImageGalleryComponentProps, IImageGalleryComponentState> {

    static propTypes = {
        /**
         * User avatar address
         */
        avatar: PropTypes.string,
        /**
         * User avatar address
         */
        banner: PropTypes.string,
    }

    styles = {
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around'
        },
        gridList: {
            width: 500,
            height: 450,
            overflowY: 'auto'
        },
        uploadButton: {
            verticalAlign: 'middle',
            fontWeight: 400
        },
        uploadInput: {
            cursor: 'pointer',
            position: 'absolute',
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            width: '100%',
            opacity: 0
        },
        deleteImage: {
            marginLeft: '5px',
            cursor: 'pointer',
            color: 'white'
        },
        addImage: {
            marginRight: '5px',
            cursor: 'pointer',
            color: 'white'
        }
    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IImageGalleryComponentProps) {
        super(props)

        this.state = {
            image: 'avatar.jpg',
            /**
             * User avatar address
             */
            avatar: props.avatar || '',
            coverPhoto: props.avatar || '',
            openChangeCover: false,
            openChangeAvatar: false,
            openCropPhoto: false,
            width: 320,
            height: 320,
            position: {x: 200, y: 50}
        }
        // Binding function to `this`
        this.handleRequestSetAvatar = this.handleRequestSetAvatar.bind(this)
        this.handleRequestSetCover = this.handleRequestSetCover.bind(this)
        this.handleCloseCropPhoto = this.handleCloseCropPhoto.bind(this)
        this.handleCloseMyPhoto = this.handleCloseMyPhoto.bind(this)
        this.handleDropCover = this.handleDropCover.bind(this)
        this.handleDropAvatar = this.handleDropAvatar.bind(this)
        this.handleDropPost = this.handleDropPost.bind(this)
        this.showChangeCover = this.showChangeCover.bind(this)
        this.showChangeAvatar = this.showChangeAvatar.bind(this)
        this.showAddPostImage = this.showAddPostImage.bind(this)
        this.options = this.options.bind(this)
    }

    /**
     * Set avatar image url
     */
    handleRequestSetAvatar = (fileName: string) => {
        console.log('=====================================')
        console.log(fileName)
        console.log('=====================================')
        const {updateAvatar} = this.props
        if (fileName !== '' && fileName !== this.state.avatar) {
            this.setState({
                avatar: fileName
            })
            updateAvatar!(fileName)
        } else {

        }
    }
    /**
     * Set post image url
     */
    handleRequestSetPostImage = (fileName: string) => {
        this.props.set!(fileName, fileName)
    }
    /**
     * Set avatar image url
     */
    handleRequestSetCover = (fileName: string) => {
        console.log('=====================================')
        console.log(fileName)
        console.log('=====================================')
        const {updateCoverPhoto} = this.props
        if (fileName !== '' && fileName !== this.state.coverPhoto) {
            this.setState({
                coverPhoto: fileName
            })
            updateCoverPhoto!(fileName)
        } else {

        }
    }
    /**
     * Close Status post write
     *
     *
     * @memberof CropImage Component
     */
    handleCloseCropPhoto = () => {
        this.setState({
            openCropPhoto: false
        })
    }
    /**
     * Close Status post write
     *
     *
     * @memberof MyPhotoComponent
     */
    handleCloseMyPhoto = () => {
        this.props.close!()
    }
    /*
    DROPBOX
    */
    handleDropCover = (acceptedFiles: any) => {
        this.setState({image: acceptedFiles[0]})
        this.setState({width: 851})
        this.setState({height: 350})
        this.setState({position: {x: 1, y: 50}})
        this.setState({openCropPhoto: true})
    }
    handleDropAvatar = (acceptedFiles: any) => {
        this.setState({image: acceptedFiles[0]})
        this.setState({width: 320})
        this.setState({height: 320})
        this.setState({position: {x: 200, y: 50}})
        this.setState({openCropPhoto: true})
    }
    handleDropPost = (acceptedFiles: any) => {
        this.setState({image: acceptedFiles[0]})
    }

    /*
    * Dropbox end
    * */
    showChangeCover = (event: any) => {
        this.setState({openChangeCover: true})
        this.props.openMyPhoto!()
    }
    showChangeAvatar = (event: any) => {
        this.setState({openChangeAvatar: true})
        this.props.openMyPhoto!()
    }
    showAddPostImage = (event: any) => {
        this.setState({openAddPostImage: true})
        this.props.openMyPhoto!()
    }

    options = () => {
        const {cropCoverPic, cropProfilePic} = this.props
        if (cropCoverPic) {
            return (
                <div><Dropzone
                    onDrop={this.handleDropCover}
                    multiple={true}
                    className='upload-photo-item'
                >
                    <svg className='olymp-computer-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-computer-icon'/>
                    </svg>
                    <h6>Upload Photo</h6>
                    <span>Browse your computer.</span>
                </Dropzone>
                    <a href={'javascript:(0)'} className='upload-photo-item' data-toggle='modal'
                       onClick={this.showChangeCover}
                       data-target='#choose-from-my-photo'>
                        <svg className='olymp-photos-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-photos-icon'/>
                        </svg>
                        <h6>Choose from My Photos</h6>
                        <span>Choose from your uploaded photos</span>
                    </a></div>)
        } else if (cropProfilePic) {
            return (
                <div><Dropzone
                    onDrop={this.handleDropAvatar}
                    multiple={true}
                    className='upload-photo-item'
                >
                    <svg className='olymp-computer-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-computer-icon'/>
                    </svg>
                    <h6>Upload Photo</h6>
                    <span>Browse your computer.</span>
                </Dropzone>
                    <a href={'javascript:(0)'} className='upload-photo-item' data-toggle='modal'
                       onClick={this.showChangeAvatar}
                       data-target='#choose-from-my-photo'>
                        <svg className='olymp-photos-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-photos-icon'/>
                        </svg>
                        <h6>Choose from My Photos</h6>
                        <span>Choose from your uploaded photos</span>
                    </a></div>
            )
        } else {
            return (
                <div><Dropzone
                    onDrop={this.handleDropPost}
                    multiple={true}
                    className='upload-photo-item'
                >
                    <svg className='olymp-computer-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-computer-icon'/>
                    </svg>
                    <h6>Upload Photo</h6>
                    <span>Browse your computer.</span>
                </Dropzone>
                    <a href={'javascript:(0)'} className='upload-photo-item' data-toggle='modal'
                       onClick={this.showAddPostImage}
                       data-target='#choose-from-my-photo'>
                        <svg className='olymp-photos-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-photos-icon'/>
                        </svg>
                        <h6>Choose from My Photos</h6>
                        <span>Choose from your uploaded photos</span>
                    </a></div>
            )
        }
    }

    render() {

        const {translate, cropCoverPic, cropProfilePic} = this.props
        /**
         * Component styles
         * @type {Object}
         */

        return (
            <LModal open={this.props.open} onClose={this.props.close} modalClassName={'update-header-photo'}
                    modalTitle={'Update Header Photo'}>
                {this.options()}
                {/*Comment Out*/}
                <CropPhotoComponent open={this.state.openCropPhoto} image={this.state.image}
                                    onRequestClose={this.handleCloseCropPhoto} height={this.state.height}
                                    width={this.state.width}
                                    position={this.state.position}/>
                {this.props.myPhotoOpen && this.state.openChangeCover ? (
                    <MyPhotoComponent set={this.handleRequestSetCover} open={true}
                                      onRequestClose={this.handleCloseMyPhoto}/>) : ''}
                {this.props.myPhotoOpen && this.state.openChangeAvatar ? (
                    <MyPhotoComponent set={this.handleRequestSetAvatar} open={true}
                                      onRequestClose={this.handleCloseMyPhoto}/>) : ''}
                {this.props.myPhotoOpen && this.state.openAddPostImage ? (
                    <MyPhotoComponent set={this.handleRequestSetPostImage} open={true}
                                      onRequestClose={this.handleCloseMyPhoto}/>) : ''}
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IImageGalleryComponentProps) => {
    return {
        openMyPhoto: () => dispatch(userActions.openChangeAvatar()),
        updateAvatar: (avatar: string) => dispatch(userActions.dbUpdateUserAvatar(avatar)),
        updateCoverPhoto: (coverPhoto: string) => dispatch(userActions.dbUpdateUserCover(coverPhoto)),
        uploadImage: (image: any, imageName: string) => dispatch(imageGalleryActions.dbUploadImage(image, imageName)),
        deleteImage: (id: string) => dispatch(imageGalleryActions.dbDeleteImage(id)),
        progressChange: (percent: number, status: boolean) => dispatch(globalActions.progressChange(percent, status))

    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IImageGalleryComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const currentUser = state.getIn(['user', 'info', uid])
    return {
        translate: getTranslate(state.get('locale')),
        avatar: currentUser ? currentUser.avatar : '',
        coverPhoto: currentUser ? currentUser.banner : '',
        myPhotoOpen: state.getIn(['user', 'openMyPhoto']),

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(ImageGalleryComponent as any) as any)
