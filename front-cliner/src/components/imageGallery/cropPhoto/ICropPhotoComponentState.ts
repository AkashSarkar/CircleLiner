export interface ICropPhotoComponentState {
    image: string,
    allowZoomOut: boolean,
    position: { x: number, y: number },
    scale: number,
    rotate: number,
    borderRadius: number,
    preview: {},
    width: number,
    height: number,
}