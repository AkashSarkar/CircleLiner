import {Image} from 'core/domain/imageGallery'
import {Map, Collection, List} from 'immutable'

export interface ICropPhotoComponentProps {
    /**
     * Width of cropping image
     */
    width?: number
    /**
     * Height of cropping image
     */
    height?: number
    /**
     * Height of cropping image
     */
    position?: { x: number, y: number }
    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * Recieve request close function
     */
    onRequestClose: () => void
    /**
     * Upload image to the server
     *
     * @memberof IImageGalleryComponentProps
     */
    uploadCropImage?: (image: any, imageName: string) => any

    image: string | File
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}