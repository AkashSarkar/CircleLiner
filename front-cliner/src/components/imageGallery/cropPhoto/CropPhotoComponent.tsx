// - Impoer react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import AvatarEditor from 'react-avatar-editor'
import {withStyles} from '@material-ui/core/styles'
import {getTranslate} from 'react-localize-redux'
import uuid from 'uuid'
import {Map} from 'immutable'

// - Import actions
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import * as globalActions from 'store/actions/globalActions'

// - Import Layout
import LModal from 'layouts/modal'
import FileAPI from 'api/FileAPI'
import {ICropPhotoComponentProps} from './ICropPhotoComponentProps'
import {ICropPhotoComponentState} from './ICropPhotoComponentState'

export class CropPhotoComponent extends Component<ICropPhotoComponentProps, ICropPhotoComponentState> {

    public editor: any
    styles = {
        canvas: {
            outline: 'none',
            cursor: 'move',
        },
        canvasParent: {
            width: '775px',
            height: '440px',
        }

    }

    constructor(props: ICropPhotoComponentProps) {
        super(props)
        const {width, height, position} = this.props
        this.state = {
            image: 'avatar.jpg',
            allowZoomOut: false,
            position: position ? position : {x: 200, y: 50},
            scale: 1,
            rotate: 0,
            borderRadius: 0,
            preview: {},
            width: width ? width : 320,
            height: height ? height : 320,
        }
        // Binding function to `this`

    }

    setEditorRef = (editor: any) => this.editor = editor

    handleSave = (data: any) => {
        const {
            onRequestClose
        } = this.props
        if (this.editor) {
            const img = this.editor.getImageScaledToCanvas().toDataURL()
            const rect = this.editor.getCroppingRect()
            let imageBLOB = FileAPI.dataURLToBlob(img)
            const extension = 'jpg'
            let fileName = (`${uuid()}.${extension}`)
            this.setState({image: fileName})
            const {uploadCropImage} = this.props
            uploadCropImage!(imageBLOB, fileName)
        }

    }

    render() {
        console.log(this.state.position)
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose} modalClassName={'choose-from-my-photo'}>
                <div style={this.styles.canvasParent}>
                    <AvatarEditor
                        ref={this.setEditorRef}
                        image={this.props.image}
                        width={this.state.width}
                        height={this.state.height}
                        border={[this.state.position.x, this.state.position.y]}
                        scale={1}
                        color={[242, 242, 242, 0.5]}
                        style={this.styles.canvas}
                    />
                </div>
                <button className='btn btn-blue' onClick={this.handleSave}>Save</button>
            </LModal>
        )
    }

}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ICropPhotoComponentProps) => {
    return {
        uploadCropImage: (image: any, imageName: string) => dispatch(imageGalleryActions.dbUploadImage(image, imageName)),
        deleteImage: (id: string) => dispatch(imageGalleryActions.dbDeleteImage(id)),
        progressChange: (percent: number, status: boolean) => dispatch(globalActions.progressChange(percent, status))

    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ICropPhotoComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const currentUser = state.getIn(['user', 'info', uid])
    return {
        translate: getTranslate(state.get('locale')),
        images: state.getIn(['imageGallery', 'images']) || [],
        avatar: currentUser ? currentUser.avatar : '',
        openMyPhoto: false

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CropPhotoComponent as any)
