import { UserTie } from 'core/domain/circles'
import {Map} from 'immutable'

export interface IFollowingComponentProps {
  /**
   * User following info
   */
   followingUsers?: Map<string, UserTie>

  /**
   * Translate to locale string
   */
   translate?: (state: any) => any
}
