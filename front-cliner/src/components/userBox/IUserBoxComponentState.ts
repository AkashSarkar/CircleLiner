
export interface IUserBoxComponentState {

  /**
   * Create new circle button is disabled {true} or not {false}
   *
   * @type {boolean}
   * @memberof IUserBoxComponentState
   */
  disabledCreateCircle: boolean

  /**
   * The button of add user in a circle is disabled {true} or not {false}
   *
   * @type {boolean}
   * @memberof IUserBoxComponentState
   */
  disabledAddToCircle: boolean

  /**
   * Circle name
   *
   * @type {string}
   * @memberof IUserBoxComponentState
   */
  circleName: string

  /**
   * Keep element
   *
   * @type {*}
   * @memberof IUserBoxComponentState
   */
  anchorEl?: any

  /**
   * Whether current user changed the selected circles for referer user
   *
   * @type {boolean}
   * @memberof IUserBoxComponentState
   */
  disabledDoneCircles: boolean

  /**
   * Whether the block modal is open or not
   *
   * @type {boolean}
   * @memberof IUserBoxComponentState
   */
  openBlockModal: boolean

  /**
   * Whether the report modal is open or not
   *
   * @type {boolean}
   * @memberof IUserBoxComponentState
   */
  openReportEditor: boolean

}
