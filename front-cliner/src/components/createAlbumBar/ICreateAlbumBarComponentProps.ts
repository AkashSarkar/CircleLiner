export interface ICreateAlbumBarComponentProps {
    /**
     * fullname
     */
    fullName: string
    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     */
    currentLanguage?: string
}