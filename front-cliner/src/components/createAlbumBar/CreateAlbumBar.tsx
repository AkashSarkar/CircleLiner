// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getTranslate, getActiveLanguage } from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import { grey } from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, { withOptions } from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import  FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import { withStyles } from '@material-ui/core/styles'

// - Import app components
import ImgCover from 'components/profile/imgCover'
import UserAvatarComponent from 'components/userAvatar'
import ImageGallery from 'components/imageGallery'
import AppDialogTitle from 'layouts/dialogTitle'
import AppInput from 'layouts/appInput'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import {ICreateAlbumBarComponentProps} from './ICreateAlbumBarComponentProps'
import { ICreateAlbumBarComponentState } from './ICreateAlbumBarComponentState'
/**
 * Create component class
 */
export class CreateAlbumBar extends Component<ICreateAlbumBarComponentProps, ICreateAlbumBarComponentState> {
  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props: ICreateAlbumBarComponentProps) {
      super(props)
  }

  /**
   * Reneder component DOM
   * @return {react element} return the DOM which rendered by component
   */
  render() {
    return (
        <div className='container'>
            <div className='row'>
                <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                    <div className='ui-block responsive-flex'>
                        <div className='ui-block-title'>
                            <div className='h6 title'>{this.props.fullName}'s Photo Gallery</div>
                            <div className='block-btn align-right'>
                                <a href={'javascript:(0)'} data-toggle='modal' data-target='#create-photo-album'
                                   className='btn btn-primary btn-md-2'>Create Album +</a>
                                <a href={'javascript:(0)'} data-toggle='modal' data-target='#update-header-photo'
                                   className='btn btn-md-2 btn-border-think custom-color c-grey'>Add Photos</a>
                            </div>
                            <ul className='nav nav-tabs photo-gallery' role='tablist'>
                                <li className='nav-item'>
                                    <a className='nav-link active' data-toggle='tab' href='#photo-page' role='tab'>
                                        <svg className='olymp-photos-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-photos-icon'/>
                                        </svg>
                                    </a>
                                </li>
                                <li className='nav-item'>
                                    <a className='nav-link' data-toggle='tab' href='#album-page' role='tab'>
                                        <svg className='olymp-albums-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-albums-icon'/>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                            <a href={'javascript:(0)'} className='more'>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ICreateAlbumBarComponentProps) => {
  return {
   }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ICreateAlbumBarComponentProps) => {
  return {
  }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((CreateAlbumBar as any) as any)
