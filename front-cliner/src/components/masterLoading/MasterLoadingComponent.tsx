// - Import react components
import React, {Component} from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import Dialog from '@material-ui/core/Dialog'
import red from '@material-ui/core/colors/red'
import {IMasterLoadingComponentProps} from './IMasterLoadingComponentProps'
import {IMasterLoadingComponentState} from './IMasterLoadingComponentState'
import Grid from '@material-ui/core/Grid/Grid'
import {Typography} from '@material-ui/core'

// - Import app components

// - Create MasterLoading component class
export default class MasterLoadingComponent extends Component<IMasterLoadingComponentProps, IMasterLoadingComponentState> {

    // Constructor
    constructor(props: IMasterLoadingComponentProps) {
        super(props)
        // Binding functions to `this`

    }

    loadProgress() {
        const {error, timedOut, pastDelay} = this.props
        if (error) {
            return (
                <Typography variant='title' color='primary' style={{marginLeft: '15px'}}>
                    Unexpected Error Happened ...
                </Typography>
            )
        } else if (timedOut) {
            return (
                <Typography variant='title' color='primary' style={{marginLeft: '15px'}}>
                    It takes long time ...
                </Typography>

            )
        } else if (pastDelay) {
            return (
                <Typography variant='title' color='primary' style={{marginLeft: '15px'}}>
                    Loading...
                </Typography>
            )
        } else {
            return (
                'Loading'
            )
        }
    }

    // Render app DOM component
    render() {
        return (
            <a id='load-more-button' href={'javascript:(0)'} className='btn btn-control btn-more'
               data-load-link='items-to-load.html'
               data-container='newsfeed-items-grid'>
                {
                    this.loadProgress()
                }
                Loading
            </a>
        )
    }

}
