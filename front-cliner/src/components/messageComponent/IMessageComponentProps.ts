import {Profile} from 'core/domain/users'

export interface IMessageComponentProps {

    /**
     * Chat owner
     */
    chatOwner?: Profile
    /**
     * Open profile editor
     *
     * @type {Function}
     * @memberof IMessageComponentProps
     */
    openChatEditor?: Function
    /**
     * Close Chat editor
     *
     * @type {Function}
     * @memberof IMessageComponentProps
     */
    closeChatEditor?: () => any
    /**
     * Current user is Chat owner {true} or not {false}
     *
     * @type {boolean}
     * @memberof IMessageComponentProps
     */
    isChatOwner?: boolean
    /**
     * User full name
     *
     * @type {string}
     * @memberof IMessageComponentProps
     */
    fullName: string
    /**
     * User avatar address
     *
     * @type {string}
     * @memberof Chat
     */
    avatar: string
    /**
     * Whether Chat edit is open
     */
    chatBoxStatus: boolean
    /**
     * Styles
     */
    classes?: any
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}