// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

// - Import API

// - Import actions

import {IMessageComponentProps} from './IMessageComponentProps'
import {IMessageComponentState} from './IMessageComponentState'

/**
 * Create component class
 */
export class InfoProfileComponent extends Component<IMessageComponentProps, IMessageComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IMessageComponentProps) {
        super(props)
        // Defaul state
        this.state = {

            }

    }

    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        const {fullName} = this.props
        const {} = this.state
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col col-xl-12'>
                        <h2 className='presentation-margin'>Chat</h2>
                    </div>
                    <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                        {/* Popup Chat */}
                        <div className='ui-block popup-chat'>
                            <div className='ui-block-title'>
                                <span className='icon-status online' />
                                <h6 className='title'>Mathilda Brinker</h6>
                                <div className='more'>
                                    <svg className='olymp-three-dots-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon' /></svg>
                                    <svg className='olymp-little-delete'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-little-delete' /></svg>
                                </div>
                            </div>
                            <div className='mCustomScrollbar ps ps--theme_default ps--active-y' data-mcs-theme='dark' data-ps-id='34609e62-42c7-6362-3342-3f0102eb1de6'>
                                <ul className='notification-list chat-message chat-message-field'>
                                    <li>
                                        <div className='author-thumb'>
                                            <img src='/img/avatar14-sm.jpg' alt='author' />
                                        </div>
                                        <div className='notification-event'>
                                            <span className='chat-message-item'>Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                                            <span className='notification-date'><time className='entry-date updated' dateTime='2004-07-24T18:18'>Yesterday at 8:10pm</time></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='author-thumb'>
                                            <img src='/img/author-page.jpg' alt='author' />
                                        </div>
                                        <div className='notification-event'>
                                            <span className='chat-message-item'>Don’t worry Mathilda!</span>
                                            <span className='chat-message-item'>I already bought everything</span>
                                            <span className='notification-date'><time className='entry-date updated' dateTime='2004-07-24T18:18'>Yesterday at 8:29pm</time></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='author-thumb'>
                                            <img src='/img/avatar14-sm.jpg' alt='author' />
                                        </div>
                                        <div className='notification-event'>
                                            <span className='chat-message-item'>Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                                            <span className='notification-date'><time className='entry-date updated' dateTime='2004-07-24T18:18'>Yesterday at 8:10pm</time></span>
                                        </div>
                                    </li>
                                </ul>
                                <div className='ps__scrollbar-x-rail' style={{left: 0, bottom: 0}}><div className='ps__scrollbar-x' tabIndex={0} style={{left: 0, width: 0}} /></div><div className='ps__scrollbar-y-rail' style={{top: 0, height: 350, right: 0}}><div className='ps__scrollbar-y' tabIndex={0} style={{top: 0, height: 255}} /></div></div>
                            <form>
                                <div className='form-group label-floating is-empty'>
                                    <label className='control-label'>Press enter to post...</label>
                                    <textarea className='form-control' defaultValue={''} />
                                    <div className='add-options-message'>
                                        <a href='#' className='options-message'>
                                            <svg className='olymp-computer-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-computer-icon' /></svg>
                                        </a>
                                        <div className='options-message smile-block'>
                                            <svg className='olymp-happy-sticker-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-happy-sticker-icon' /></svg>
                                            <ul className='more-dropdown more-with-triangle triangle-bottom-right'>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat1.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat2.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat3.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat4.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat5.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat6.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat7.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat8.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat9.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat10.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat11.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat12.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat13.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat14.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat15.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat16.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat17.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat18.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat19.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat20.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat21.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat22.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat23.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat24.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat25.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat26.png' alt='icon' />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#'>
                                                        <img src='/img/icon-chat27.png' alt='icon' />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span className='material-input' /></div>
                            </form>
                        </div>

                        {/* ... end Popup Chat */}
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IMessageComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IMessageComponentProps) => {
    return {
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((InfoProfileComponent as any) as any)