// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import {push} from 'react-router-redux'
import PropTypes from 'prop-types'
import moment from 'moment/moment'
import Linkify from 'react-linkify'
import copy from 'copy-to-clipboard'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

// - Material UI
import LinearProgress from '@material-ui/core/LinearProgress'
import {Manager, Target, Popper} from 'react-popper'
import Grow from '@material-ui/core/Grow'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import classNames from 'classnames'

import reactStringReplace from 'react-string-replace'

// - Import app components
import PostWrite from 'components/post/postWrite'
import StatusPostWrite from 'components/post/postType/statusPostWrite'
import PostResponseComponent from 'components/post/respose'
import Img from 'components/img'
import IconButtonElement from 'layouts/iconButtonElement'
import UserAvatar from 'components/userAvatar'
import ImageModalDown from 'src/components/imageModal/imageModalDown'
import {UserAvatarComponent} from 'components/userAvatar/UserAvatarComponent'

// - Import actions
import * as voteActions from 'store/actions/voteActions'
import * as postActions from 'store/actions/postActions'
import * as commentActions from 'store/actions/commentActions'
import * as globalActions from 'store/actions/globalActions'

// - Import Interfaces
import {IPostComponentProps} from './IPostComponentProps'
import {IPostComponentState} from './IPostComponentState'

// - Create component class
export class PostComponent extends Component<IPostComponentProps, IPostComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IPostComponentProps) {
        super(props)
        const {post} = props
        this.state = {
            /**
             * Post text
             */
            text: post.get('body', ''),
            /**
             * It's true if whole the text post is visible
             */
            readMoreState: false,
            /**
             * Handle open comment from parent component
             */
            openComments: false,
            /**
             * If it's true, share dialog will be open
             */
            shareOpen: false,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: post.get('disableComments', false),
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: post.get('disableSharing', false),
            /**
             * Title of share post
             */
            shareTitle: 'Share On',
            /**
             * If it's true, post link will be visible in share post dialog
             */
            openCopyLink: false,
            /**
             * If it's true, post write will be open
             */
            openPostWrite: false,
            /**
             * If it's true, post write will be open
             */
            openStatusPostWrite: false,
            /**
             * Post menu anchor element
             */
            postMenuAnchorEl: null,
            /**
             * Whether post menu open
             */
            isPostMenuOpen: false,
            /**
             * Whether image modal is open
             */
            openImageModal: false,
        }

        // Binding functions to this
        this.handleReadMore = this.handleReadMore.bind(this)
        this.handleOpenImageModal = this.handleOpenImageModal.bind(this)
        this.handleCloseImageModal = this.handleCloseImageModal.bind(this)
        this.getOpenCommentGroup = this.getOpenCommentGroup.bind(this)
        this.handleVote = this.handleVote.bind(this)
        this.handleOpenShare = this.handleOpenShare.bind(this)
        this.handleCloseShare = this.handleCloseShare.bind(this)
        this.handleCopyLink = this.handleCopyLink.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
        this.handleOpenPostWrite = this.handleOpenPostWrite.bind(this)
        this.handleClosePostWrite = this.handleClosePostWrite.bind(this)
        this.handleOpenStatusPostWrite = this.handleOpenStatusPostWrite.bind(this)
        this.handleCloseStatusPostWrite = this.handleCloseStatusPostWrite.bind(this)
        this.handleOpenComments = this.handleOpenComments.bind(this)
    }

    /**
     * Toggle on show/hide comment
     * @param  {event} evt passed by clicking on comment slide show
     */
    handleOpenComments = () => {
        const {getPostComments, commentList, post} = this.props
        const id = post.get('id')
        const ownerUserId = post.get('ownerUserId')
        if (!commentList) {
            getPostComments!(ownerUserId!, id!)
        }
        this.setState({
            openComments: !this.state.openComments
        })
    }

    /**
     * Open image modal
     *
     *
     * @memberof StreamComponent
     */
    handleOpenImageModal = () => {
        this.setState({
            openImageModal: true,
        })
    }

    /**
     * Close image modal
     *
     *
     * @memberof StreamComponent
     */
    handleCloseImageModal = () => {
        this.setState({
            openImageModal: false,
        })
    }

    /**
     * Open status post write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenPostWrite = () => {
        this.setState({
            openPostWrite: true
        })
    }

    /**
     * Open status post write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenStatusPostWrite = () => {
        this.setState({
            openStatusPostWrite: true
        })
    }

    /**
     * Close post write
     *
     *
     * @memberof StreamComponent
     */
    handleClosePostWrite = () => {
        this.setState({
            openPostWrite: false
        })
    }
    /**
     * Close status post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseStatusPostWrite = () => {
        this.setState({
            openStatusPostWrite: false
        })
    }

    /**
     * Delete a post
     *
     *
     * @memberof Post
     */
    handleDelete = () => {
        const {post} = this.props
        this.props.delete!(post.get('id'))
    }

    /**
     * Open post menu
     */
    openPostMenu = (event: any) => {
        this.setState({
            postMenuAnchorEl: event.currentTarget,
            isPostMenuOpen: true
        })
    }

    /**
     * Close post menu
     */
    closePostMenu = (event: any) => {
        this.setState({
            postMenuAnchorEl: event.currentTarget,
            isPostMenuOpen: false
        })
    }

    /**
     * Show copy link
     *
     *
     * @memberof Post
     */
    handleCopyLink = () => {
        const {translate} = this.props
        this.setState({
            openCopyLink: true,
            shareTitle: translate!('post.copyLinkButton')
        })
    }

    /**
     * Open share post
     *
     *
     * @memberof Post
     */
    handleOpenShare = () => {
        const {post} = this.props
        copy(`${location.origin}/${post.get('ownerUserId')}/posts/${post.get('id')}`)
        this.setState({
            shareOpen: true
        })
    }

    /**
     * Close share post
     *
     *
     * @memberof Post
     */
    handleCloseShare = () => {
        this.setState({
            shareOpen: false,
            shareTitle: 'Share On',
            openCopyLink: false
        })
    }

    /**
     * Handle vote on a post
     *
     *
     * @memberof Post
     */
    handleVote = () => {
        if (this.props.currentUserVote) {
            this.props.unvote!()
        } else {
            this.props.vote!()
        }
    }

    /**
     * Set open comment group function on state which passed by CommentGroup component
     * @param  {function} open the function to open comment list
     */
    getOpenCommentGroup = (open: () => void) => {
        this.setState({
            openCommentGroup: open
        })
    }

    /**
     * Handle read more event
     * @param  {event} evt  is the event passed by click on read more
     */
    handleReadMore(event: any) {
        this.setState({
            readMoreState: !this.state.readMoreState

        })
    }

    imageBox = (images: any) => {
        let imageData = []
        let lastImage = ''
        let counter = 0
        for (let key in images) {
            if (images.hasOwnProperty(key)) {
                if (images.length === 1) {
                    return (
                        <div className='post-thumb' key={key}>
                            <img src={images[key].url} alt='photo' onClick={this.handleOpenImageModal}/>
                        </div>
                    )
                } else {
                    counter++
                    lastImage = images[key].url
                    imageData.push(
                        <a key={key} className='col col-3-width'><img src={images[key].url} onClick={this.handleOpenImageModal} alt='photo'/></a>)
                    if (counter === 2) {
                        break
                    }
                }

            }

        }
        return (
            <div className='post-block-photo js-zoom-gallery'>
                {imageData}
                <a href={lastImage} className='more-photos col-3-width'>
                    <img src={lastImage} alt='photo'/>
                    <span className='h2'>+{images.length - 2}</span>
                </a>
            </div>

        )
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {post, setHomeTitle, goTo, fullName, isPostOwner, commentList, avatar, classes, translate} = this.props
        const {postMenuAnchorEl, isPostMenuOpen} = this.state
        const rightIconMenu = (
            <div className='more'>
                <svg className='olymp-three-dots-icon' onClick={this.openPostMenu.bind(this)}
                     aria-owns={isPostMenuOpen! ? 'post-menu' : ''}>
                    <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                </svg>
                <ClickAwayListener onClickAway={this.closePostMenu}>
                    <ul role='menu' className='more-dropdown'>
                        {/*<li onClick={this.handleOpenPostWrite}> {translate!('post.edit')} </li>*/}
                        <li onClick={this.handleOpenStatusPostWrite}> {translate!('post.edit')} </li>
                        <li onClick={this.handleDelete}> {translate!('post.delete')} </li>
                        <li
                            onClick={() => this.props.toggleDisableComments!(!post.get('disableComments'))}>
                            {post.get('disableComments') ? translate!('post.enableComments') : translate!('post.disableComments')}
                        </li>
                        <li
                            onClick={() => this.props.toggleSharingComments!(!post.get('disableSharing'))}>
                            {post.get('disableSharing') ? translate!('post.enableSharing') : translate!('post.disableSharing')}
                        </li>
                    </ul>
                </ClickAwayListener>
            </div>
        )

        const {
            ownerUserId,
            ownerDisplayName,
            creationDate,
            image,
            postData,
            description,
            id,
            disableComments,
            commentCounter,
            disableSharing,
        } = post.toJS()
        let body = ''
        if (postData) {
            body = postData.description
        } else if (description) {
            body = description
        }
        // Define variables
        return (
            <div className='ui-block'>
                <article className='hentry post'>
                    <div className='post__author author vcard inline-items'>
                        <UserAvatarComponent fullName={fullName!} fileName={avatar!} size={36}/>
                        <div className='author-date'>
                            <NavLink to={`/${ownerUserId}`}
                                     className='h6 post__author-name fn'>{fullName}</NavLink>
                            {creationDate ? <div className='post__date'>
                                <time className='published'
                                      dateTime='2017-03-24T18:18'>{moment.unix(creationDate!).fromNow() + ' | ' + translate!('post.public')}</time>
                            </div> : <LinearProgress color='primary'/>}
                        </div>
                        {isPostOwner ? rightIconMenu : ''}
                    </div>
                    <p>
                        <Linkify properties={{target: '_blank', style: {color: 'blue'}}}>
                            {reactStringReplace(body, /#(\w+)/g, (match: string, i: string) => (
                                <NavLink
                                    style={{color: 'green'}}
                                    key={match + i}
                                    to={`/tag/${match}`}
                                    onClick={evt => {
                                        evt.preventDefault()
                                        goTo!(`/tag/${match}`)
                                        setHomeTitle!(`#${match}`)
                                    }}
                                >
                                    #{match}

                                </NavLink>

                            ))}
                        </Linkify>
                    </p>
                    {image ? (this.imageBox(image)) : ''}
                    <PostResponseComponent post={this.props.post}/>
                    <PostWrite
                        open={this.state.openPostWrite}
                        onRequestClose={this.handleClosePostWrite}
                        edit={true}
                        postModel={post}
                    />
                    <StatusPostWrite
                        open={this.state.openStatusPostWrite}
                        onRequestClose={this.handleCloseStatusPostWrite}
                        edit={true}
                        postModel={post}
                    />
                    <ImageModalDown
                        open={this.state.openImageModal}
                        onRequestClose={this.handleCloseImageModal}
                        userId={this.props.userId!}/>
                </article>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPostComponentProps) => {
    const {post} = ownProps
    return {
        vote: () => dispatch(voteActions.dbAddVote(post.get('id'), post.get('ownerUserId'))),
        unvote: () => dispatch(voteActions.dbDeleteVote(post.get('id'), post.get('ownerUserId'))),
        delete: (id: string) => dispatch(postActions.dbDeletePost(id)),
        toggleDisableComments: (status: boolean) => {
            dispatch(postActions.dbUpdatePost(post.set('disableComments', status), (x: any) => x))
        },
        toggleSharingComments: (status: boolean) => {
            dispatch(postActions.dbUpdatePost(post.set('disableSharing', status), (x: any) => x))
        },
        goTo: (url: string) => dispatch(push(url)),
        setHomeTitle: (title: string) => dispatch(globalActions.setHeaderTitle(title || '')),
        getPostComments: (ownerUserId: string, postId: string) => dispatch(commentActions.dbFetchComments(ownerUserId, postId))

    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPostComponentProps) => {

    const uid = state.getIn(['authorize', 'uid'])
    let currentUserVote = ownProps.post.getIn(['votes', uid], false)
    const voteCount = state.getIn(['post', 'userPosts', ownProps.post.get('ownerUserId'), ownProps.post.get('id'), 'score'], 0)
    const commentList: { [commentId: string]: Comment } = state.getIn(['comment', 'postComments', ownProps.post.get('id')])
    const user = state.getIn(['user', 'info', ownProps.post.get('ownerUserId')])
    return {
        translate: getTranslate(state.get('locale')),
        commentList,
        avatar: user ? user.avatar : '',
        fullName: user ? user.fullName : '',
        voteCount,
        currentUserVote,
        isPostOwner: uid === ownProps.post.get('ownerUserId')
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(PostComponent as any)
