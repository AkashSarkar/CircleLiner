// - Import react components
import React, {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import PropTypes from 'prop-types'
import moment from 'moment/moment'
import Linkify from 'react-linkify'
import Popover from '@material-ui/core/Popover'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

import {Comment} from 'core/domain/comments/index'

// - Import material UI libraries
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'
import {withStyles} from '@material-ui/core/styles'
import {Manager, Target, Popper} from 'react-popper'
import {Card, CardActions, CardHeader, CardMedia, CardContent} from '@material-ui/core'
import Grow from '@material-ui/core/Grow'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import classNames from 'classnames'

// - Import app components
import UserAvatar from 'components/userAvatar/index'

// - Import API

// - Import actions
import * as commentActions from 'store/actions/commentActions'
import * as userActions from 'store/actions/userActions/userActions'

import {ICommentComponentProps} from './ICommentComponentProps'
import {ICommentComponentState} from './ICommentComponentState'
import {CommentListComponent} from 'components/post/comment/commentList/CommentListComponent'

const styles = (theme: any) => ({
    textField: {
        fontWeight: 400,
        fontSize: '14px'
    },
    header: {
        padding: '2px 3px 3px 10px'
    },
    popperOpen: {
        zIndex: 11
    },
    popperClose: {
        pointerEvents: 'none',
        zIndex: 0
    },
    iconButton: {
        top: 0,
        display: 'flex',
        right: 4,
        flexDirection: 'row-reverse',
        position: 'absolute'

    },
    commentBody: {
        color: 'black',
        fontWeight: 400,
        fontSize: '12px',
        height: '100%',
        border: 'none',
        width: '100%',
        outline: 'none',
        resize: 'none'
    },
    rightIconMenuItem: {
        fontSize: 12,
        fontWeight: 300,
        paddingLeft: 6,
        paddingRight: 6,
        paddingTop: 0,
        paddingBottom: 0

    },
    moreIcon: {
        width: '0.6em',
        height: '0.6em'
    }
})

/**
 * Create component class
 */
export class CommentComponent extends Component<ICommentComponentProps, ICommentComponentState> {
    static propTypes = {
        /**
         * Comment object
         */
        comment: PropTypes.object,
        /**
         * If it's true the post owner is the logged in user which this post be long to the comment
         */
        isPostOwner: PropTypes.bool,
        /**
         * If it's true the comment is disable to write
         */
        disableComments: PropTypes.bool
    }

    /**
     * Fields
     */
    buttonMenu = null

    /**
     * DOM styles
     *
     *
     * @memberof CommentComponent
     */
    styles = {
        author: {
            fontSize: '10px',
            paddingRight: '10px',
            fontWeight: 400,
            color: 'rgba(0,0,0,0.87)',
            textOverflow: 'ellipsis',
            overflow: 'hidden'

        },
        textarea: {
            fontWeight: 400,
            fontSize: '14px',
            border: 'none',
            width: '100%',
            outline: 'none',
            resize: 'none'
        },
        cancel: {
            float: 'right',
            clear: 'both',
            zIndex: 5,
            margin: '0px 5px 5px 0px',
            fontWeight: 400
        }
    }

    /**
     * Fields
     *
     * @type {*}
     * @memberof CommentComponent
     */
    textareaRef: any
    divCommentRef: any
    inputText: any
    divComment: any

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ICommentComponentProps) {
        super(props)

        this.textareaRef = (i: any) => {
            this.inputText = i
        }
        this.divCommentRef = (i: any) => {
            this.divComment = i
        }

        // Defaul state
        this.state = {
            /**
             * Comment text
             */
            text: this.props.comment.text!,
            /**
             * Comment text to match edit with new comment that is edited
             */
            initialText: this.props.comment.text!,
            /**
             * If comment text dosn't take any change it will be true
             */
            editDisabled: true,
            /**
             * If it's true the post owner is the logged in user which this post be long to the comment
             */
            isPostOwner: false,
            /**
             * The anchor of comment menu element
             */
            openMenu: false,
            /**
             * Anchor element
             */
            anchorEl: null

        }

        // Binding functions to `this`
        this.handleDelete = this.handleDelete.bind(this)
        this.handleUpdateComment = this.handleUpdateComment.bind(this)
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleCancelEdit = this.handleCancelEdit.bind(this)
        this.handleEditComment = this.handleEditComment.bind(this)

    }

    /**
     * Handle show edit comment
     * @param  {event} evt is an event passed by clicking on edit button
     */
    handleEditComment = (evt: any) => {
        this.setState({openMenu: false})
        this.props.openEditor!()
    }

    /**
     * Handle cancel edit
     * @param  {event} evt is an event passed by clicking on cancel button
     */
    handleCancelEdit = (evt: any) => {

        this.setState({
            text: this.state.initialText!
        })
        this.props.closeEditor!()
    }

    /**
     * Handle edit comment
     * @param  {event} evt is an event passed by clicking on post button
     */
    handleUpdateComment = (evt: any) => {
        const {comment} = this.props
        comment.text = this.state.text
        this.props.update!(comment)
        this.setState({
            initialText: this.state.text
        })

    }

    /**
     * When comment text changed
     * @param  {event} evt is an event passed by change comment text callback funciton
     * @param  {string} data is the comment text which user writes
     */
    handleOnChange = (evt: any) => {
        const data = evt.target.value || ''
        if (data.length === 0 || data.trim() === '' || data.trim() === this.state.initialText) {
            this.setState({
                text: data,
                editDisabled: true
            })
        } else {
            this.setState({
                text: data,
                editDisabled: false
            })
        }

    }

    handleOnKeyDown = (e: any) => {
        // if (['Enter', 'Escape'].includes(e.key)) {
        console.log(e.key)
        console.log(e.keyCode)
        if (e.key === 'Enter') {
            this.handleUpdateComment(e)
        } else if (e.key === 'Escape') {
            this.handleCancelEdit(e)
            console.log('Escape Clicked')
        }
    }

    /**
     * Delete a comment
     * @param  {event} evt    an event passed by click on delete comment
     * @param  {string} id     comment identifire
     * @param  {string} postId post identifier which comment belong to
     */
    handleDelete = (evt: any, id?: string | null, postId?: string) => {
        this.props.delete!(id, postId)
    }

    /**
     * Handle comment menu
     */
    handleCommentMenu = (event: any) => {
        this.setState({openMenu: true, anchorEl: findDOMNode(this.buttonMenu!),})
    }

    /**
     * Handle close request for comment menu
     */
    handleCloseCommentMenu = () => {
        this.setState({openMenu: false})
    }

    componentWillMount() {
        const {commentOwner} = this.props
        if (!this.props.isCommentOwner && !commentOwner) {
            this.props.getUserInfo!()
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        /**
         * Comment object from props
         */
        const {comment, classes, fullName, avatar, translate, editorStatus} = this.props

        const {openMenu, anchorEl} = this.state

        const rightIconMenu = (
            <>
                <IconButton
                    className={'more'}
                    component={'a'}
                    buttonRef={(node: any) => {
                        this.buttonMenu = node
                    }}
                    aria-owns={openMenu! ? 'comment-menu' : ''}
                    aria-haspopup='true'
                    onClick={this.handleCommentMenu}
                >
                    <MoreHorizIcon className={classes.moreIcon}/>
                </IconButton>
                <Menu
                    open={openMenu!}
                    anchorEl={anchorEl}
                    anchorReference={'anchorEl'}
                    anchorPosition={{top: 0, left: 0}}
                    onClose={this.handleCloseCommentMenu}
                >
                    <MenuItem
                        className={classes.rightIconMenuItem}>{translate!('comment.replyButton')}</MenuItem>
                    {this.props.isCommentOwner ? (<MenuItem className={classes.rightIconMenuItem}
                                                            onClick={this.handleEditComment}>{translate!('comment.editButton')}</MenuItem>) : ''}
                    {(this.props.isCommentOwner || this.props.isPostOwner) ? (
                        <MenuItem className={classes.rightIconMenuItem}
                                  onClick={(evt: any) => this.handleDelete(evt, comment.id, comment.postId)}>{translate!('comment.deleteButton')}</MenuItem>) : ''}
                </Menu>

            </>
        )
        const {userId} = comment
        const author = (
            <>
                <UserAvatar fullName={comment.userDisplayName!} fileName={comment.userAvatar!} size={28}/>
                {/*<img src={comment.userAvatar}/>*/}
                <div className='author-date'>
                    <NavLink className='h6 post__author-name fn' to={`/${userId}`}> {comment.userDisplayName}</NavLink>
                    <div className='post__date'>
                        <time className='published' dateTime='2017-03-24T18:18'>
                            {moment.unix(comment.creationDate!).fromNow()}
                        </time>
                    </div>
                </div>
            </>
        )
        // const reply: Map<string, Comment> = comment.reply || Map({})
        return (
            <li className={comment.hasChild ? 'comment-item has-children' : 'comment-item'} key={comment.id!}>
                <div className='post__author author vcard inline-items'>
                    {author}
                    {rightIconMenu}
                    {/*<a href='#' className='more'>*/}
                    {/*<svg className='olymp-three-dots-icon'>*/}
                    {/*<use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>*/}
                    {/*</svg>*/}
                    {/*</a>*/}
                </div>
                {editorStatus ? <TextField
                    placeholder={translate!('comment.updateCommentPlaceholder')}
                    value={this.state.text}
                    onChange={this.handleOnChange}
                    onKeyDown={this.handleOnKeyDown}
                    className={classes.textField}
                    fullWidth={true}

                /> : <p>{this.state.text}</p>}

                <a href='#' className='post-add-icon inline-items'>
                    <svg className='olymp-heart-icon'>
                        <use xlinkHref='svg-icons/sprites/icons.svg#olymp-heart-icon'/>
                    </svg>
                    <span>{comment.replyCounter}</span>
                </a>
                <a href='#' className='reply'>Reply</a>
                {comment.hasChild ? (<ul className={'children'}
                                         style={open ? {display: 'block'} : {display: 'none'}}>
                    <CommentListComponent comments={comment.reply!} isPostOwner={this.props.isPostOwner || false}
                                          disableComments={this.props.disableComments || false}
                                          postId={comment.postId}/>
                </ul>) : ''}

                {/*<Paper elevation={0} className='animate2-top10'*/}
                {/*style={{position: 'relative', padding: '', display: (!this.state.display ? 'block' : 'none')}}>*/}
                {/*<Card elevation={0} className={'post__author author vcard inline-items'}>*/}
                {/*<CardHeader*/}
                {/*className={classes.header}*/}
                {/*title={editorStatus ? '' : <Author/>}*/}
                {/*// subheader={commentBody}*/}
                {/*avatar={<NavLink to={`/${userId}`}><UserAvatar fullName={fullName!} fileName={avatar!}*/}
                {/*size={24}/></NavLink>}*/}
                {/*action={(!this.props.isCommentOwner && !this.props.isPostOwner && this.props.disableComments) || editorStatus ? '' : rightIconMenu}*/}
                {/*>*/}
                {/*</CardHeader>*/}
                {/*<CardContent>*/}
                {/*</CardContent>*/}
                {/*<CardActions>*/}

                {/*</CardActions>*/}
                {/*</Card>*/}
                {/*</Paper>*/}
            </li>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ICommentComponentProps) => {
    return {
        delete: (id: string | null, postId: string) => dispatch(commentActions.dbDeleteComment(id, postId)),
        update: (comment: Comment) => {
            dispatch(commentActions.dbUpdateComment(comment))
        },
        openEditor: () => dispatch(commentActions.openCommentEditor({
            id: ownProps.comment.id,
            postId: ownProps.comment.postId
        })),
        closeEditor: () => dispatch(commentActions.closeCommentEditor({
            id: ownProps.comment.id,
            postId: ownProps.comment.postId
        })),
        getUserInfo: () => dispatch(userActions.dbGetUserInfoByUserId(ownProps.comment.userId!, ''))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: ICommentComponentProps) => {
    const commentOwnerId = ownProps.comment.userId
    const uid = state.getIn(['authorize', 'uid'])
    const avatar = ownProps.comment.userAvatar
    const fullName = ownProps.comment.userDisplayName
    return {
        translate: getTranslate(state.get('locale')),
        uid: uid,
        isCommentOwner: (uid === commentOwnerId),
        commentOwner: state.getIn(['user', 'info', commentOwnerId]),
        avatar,
        fullName
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(CommentComponent as any) as any)
