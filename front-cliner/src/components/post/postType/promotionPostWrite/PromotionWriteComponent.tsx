// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

import {Card, CardActions, CardHeader, CardMedia, CardContent} from '@material-ui/core'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Paper from '@material-ui/core/Paper'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import TextField from '@material-ui/core/TextField'
import Tooltip from '@material-ui/core/Tooltip'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import SvgRemoveImage from '@material-ui/icons/RemoveCircle'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import {withStyles} from '@material-ui/core/styles'
import {Manager, Target, Popper} from 'react-popper'
import Grow from '@material-ui/core/Grow'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import classNames from 'classnames'

// - Import app components
import ImageGallery from 'components/imageGallery/index'
import Img from 'components/img/index'
import UserAvatarComponent from 'components/userAvatar/index'

// - Import API
import * as PostAPI from 'api/PostAPI'

// - Import actions
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import * as postActions from 'store/actions/postActions'
import {IPromotionWriteComponentProps} from './IPromotionWriteComponentProps'
import {IPromotionWriteComponentState} from './IPromotionWriteComponentState'
import {Post, PostPromotion} from 'core/domain/posts/index'

// - Import Layouts
import LModal from 'layouts/modal/index'

const styles = (theme: any) => ({
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0,
            overflowY: 'auto'
        }
    },
    backdrop: {
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: '-1',
        position: 'fixed',
        willChange: 'opacity',
        backgroundColor: 'rgba(251, 249, 249, 0.5)',
        WebkitTapHighlightColor: 'transparent'
    },
    content: {
        padding: 0,
        paddingTop: 0
    },
    dialogRoot: {
        paddingTop: 0
    },
    popperOpen: {
        zIndex: 10
    },
    popperClose: {
        pointerEvents: 'none',
        zIndex: 0
    },
    author: {
        paddingRight: 70
    }
})

// - Create PostWrite component class
export class PromotionWriteComponent extends Component<IPromotionWriteComponentProps, IPromotionWriteComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IPromotionWriteComponentProps) {
        super(props)
        const {postPromotionModel} = props

        // Default state
        this.state = {
            jobPositionName: this.props.edit && postPromotionModel ? postPromotionModel.get('jobPositionName', '') : '',
            companyName: this.props.edit && postPromotionModel ? postPromotionModel.get('companyName', '') : '',
            departmentName:  this.props.edit && postPromotionModel ? postPromotionModel.get('departmentName', '') : '',
            description: this.props.edit && postPromotionModel ? postPromotionModel.get('description', '') : '',
            /**
             * Post text
             */
            postText: this.props.edit && postPromotionModel ? postPromotionModel.get('body', '') : '',
            /**
             * The URL image of the post
             */
            image: this.props.edit && postPromotionModel ? postPromotionModel.get('image', '') : '',
            /**
             * The path identifier of image on the server
             */
            imageFullPath: this.props.edit && postPromotionModel ? postPromotionModel.get('imageFullPath', '') : '',
            /**
             * If it's true gallery will be open
             */
            galleryOpen: false,
            /**
             * Whether menu is open
             */
            menuOpen: false,
            /**
             * If it's true post button will be disabled
             */
            disabledPost: true,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: this.props.edit && postPromotionModel ? postPromotionModel.get('disableComments') : false,
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: this.props.edit && postPromotionModel ? postPromotionModel.get('disableSharing') : false
        }

        // Binding functions to `this`
        this.handlePromotionPost = this.handlePromotionPost.bind(this)
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleCloseGallery = this.handleCloseGallery.bind(this)
        this.handleOpenGallery = this.handleOpenGallery.bind(this)
        this.onRequestSetImage = this.onRequestSetImage.bind(this)
        this.handleRemoveImage = this.handleRemoveImage.bind(this)
        this.handleToggleComments = this.handleToggleComments.bind(this)
        this.handleToggleSharing = this.handleToggleSharing.bind(this)
    }

    /**
     * Toggle comments of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleComments = () => {
        this.setState({
            disableComments: !this.state.disableComments,
            disabledPost: false
        })
    }

    /**
     * Toggle sharing of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleSharing = () => {
        this.setState({
            disableSharing: !this.state.disableSharing,
            disabledPost: false
        })
    }

    /**
     * Romove the image of post
     *
     *
     * @memberof PostWrite
     */
    handleRemoveImage = () => {
        this.setState({
            image: '',
            imageFullPath: '',
            disabledPost: this.state.postText.trim() === ''
        })
    }

    /**
     * Set post image url
     */
    onRequestSetImage = (url: string, fullPath: string) => {
        this.setState({
            image: url,
            imageFullPath: fullPath,
            disabledPost: false
        })
    }

    /**
     * When the post text changed
     * @param  {event} evt is an event passed by change post text callback funciton
     * @param  {string} data is the post content which user writes
     */
    handleOnChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        // console.log(value)
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'jobPositionName':
                this.setState({
                    jobPositionName: value,
                    disabledPost: false
                })
                break
            case 'companyName':
                this.setState({
                    companyName: value,
                    disabledPost: false
                })
                break
            case 'departmentName':
                this.setState({
                    departmentName: value,
                    disabledPost: false
                })
                break
            case 'description':
                this.setState({
                    description: value,
                    disabledPost: false
                })
                break
            default:
        }
    }
    /**
     * Handle send status post to the server
     * @param  {event} evt passed by clicking on the post button
     */
    handlePromotionPost = () => {
        const {
            image,
            imageFullPath,
            disableComments,
            disableSharing,
            postText,
            jobPositionName,
            companyName,
            departmentName,
            description
        } = this.state

        const {
            id,
            ownerAvatar,
            ownerDisplayName,
            edit,
            onRequestClose,
            post,
            update,
            postPromotionModel
        } = this.props

        if (image === '' && jobPositionName.trim() === '' && companyName.trim() === '' && departmentName.trim() === '' && description.trim() === '') {
            this.setState({
                image: '',
                disabledPost: false
            })
            // return
        }

        let tags = PostAPI.getContentTags(description!)

        // In edit status we should fire update if not we should fire post function
        if (!edit) {
            if (image !== '') {
                post!({
                    jobPositionName: jobPositionName,
                    companyName: companyName,
                    departmentName: departmentName,
                    description: description,
                    // body: postText,
                    tags: tags,
                    image: image,
                    imageFullPath: imageFullPath,
                    ownerAvatar: ownerAvatar,
                    ownerDisplayName: ownerDisplayName,
                    disableComments: disableComments,
                    disableSharing: disableSharing,
                    postTypeId: 1,
                    score: 0,
                    viewCount: 0
                }, onRequestClose)
            } else {
                post!({
                    // body: postText,
                    jobPositionName: jobPositionName,
                    companyName: companyName,
                    departmentName: departmentName,
                    description: description,
                    tags: tags,
                    ownerAvatar: ownerAvatar,
                    ownerDisplayName: ownerDisplayName,
                    disableComments: disableComments,
                    disableSharing: disableSharing,
                    postTypeId: 0,
                    score: 0,
                    viewCount: 0
                }, onRequestClose)
            }
        } else { // In edit status we pass post to update functions
            const updatedPost = postPromotionModel!.set('jobPositionName', jobPositionName)
                .set('companyName', companyName)
                .set('departmentName', departmentName)
                .set('description', description)
                .set('tags', tags)
                .set('image', image)
                .set('imageFullPath', imageFullPath)
                .set('disableComments', disableComments)
                .set('disableSharing', disableSharing)

            update!(updatedPost, onRequestClose)
        }
    }

    /**
     * Close image gallery
     */
    handleCloseGallery = () => {
        this.setState({
            galleryOpen: false
        })
    }

    /**
     * Open image gallery
     */
    handleOpenGallery = () => {
        this.setState({
            galleryOpen: true
        })
    }

    /**
     * Handle open more menu
     */
    handleOpenMenu = () => {
        this.setState({
            menuOpen: true
        })
    }

    /**
     * Handle close more menu
     */
    handleCloseMenu = () => {
        this.setState({
            menuOpen: false
        })
    }

    componentWillReceiveProps(nextProps: IPromotionWriteComponentProps) {
        if (!nextProps.open) {
            const {postPromotionModel} = this.props
            this.setState({
                jobPositionName: this.props.edit && postPromotionModel ? postPromotionModel.get('jobPositionName', '') : '',
                companyName: this.props.edit && postPromotionModel ? postPromotionModel.get('companyName', '') : '',
                departmentName:  this.props.edit && postPromotionModel ? postPromotionModel.get('departmentName', '') : '',
                description: this.props.edit && postPromotionModel ? postPromotionModel.get('description', '') : '',
                /**
                 * Post text
                 */
                postText: this.props.edit && postPromotionModel ? postPromotionModel.get('body', '') : '',
                /**
                 * The URL image of the post
                 */
                image: this.props.edit && postPromotionModel ? postPromotionModel.get('image', '') : '',
                /**
                 * The path identifier of image on the server
                 */
                imageFullPath: this.props.edit && postPromotionModel ? postPromotionModel.get('imageFullPath', '') : '',
                /**
                 * If it's true gallery will be open
                 */
                galleryOpen: false,
                /**
                 * Whether menu is open
                 */
                menuOpen: false,
                /**
                 * If it's true post button will be disabled
                 */
                disabledPost: true,
                /**
                 * If it's true comment will be disabled on post
                 */
                disableComments: this.props.edit && postPromotionModel ? postPromotionModel.get('disableComments') : false,
                /**
                 * If it's true share will be disabled on post
                 */
                disableSharing: this.props.edit && postPromotionModel ? postPromotionModel.get('disableSharing') : false
            })
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {classes, translate} = this.props
        const {menuOpen} = this.state

        const rightIconMenu = (
            <div className='more'>
                <div className='container pt-3'>
                    <svg className='olymp-three-dots-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                    </svg>
                </div>
                <ul className='more-dropdown'>
                    <li className='nav-item' onClick={this.handleToggleComments}>
                        <a data-toggle='tab' href='' role='tab' aria-expanded='false'>
                            <span>{!this.state.disableComments ? 'Disable comments' : 'Enable comments'}</span>
                        </a>
                    </li>
                    <li className='nav-item' onClick={this.handleToggleSharing}>
                        <a data-toggle='tab' href=''
                           role='tab' aria-expanded='false'>
                            <span>{!this.state.disableSharing ? 'Disable sharing' : 'Enable sharing'}</span>
                        </a>
                    </li>
                </ul>
            </div>
        )
        let postAvatar = (<UserAvatarComponent fullName={this.props.ownerDisplayName!} fileName={this.props.ownerAvatar!} size={36}/>)
        let author = (
            <div className={classes.author}>
        <span style={{
            fontSize: '14px',
            paddingRight: '10px',
            fontWeight: 400,
            color: 'rgba(0,0,0,0.87)',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            lineHeight: '25px'
        }}>{this.props.ownerDisplayName}</span><span style={{
                fontWeight: 400,
                fontSize: '10px'
            }}> | {translate!('post.public')}</span>
            </div>
        )

        /**
         * Provide post image
         */
        const loadImage = (this.state.image && this.state.image !== '')
            ? (
                <div>
                    <div style={{position: 'relative', overflowY: 'hidden', overflowX: 'auto'}}>
                        <ul style={{
                            position: 'relative',
                            whiteSpace: 'nowrap',
                            padding: '0 0 0 16px',
                            margin: '8px 0 0 0',
                            paddingRight: '16px',
                            verticalAlign: 'bottom',
                            flexShrink: 0,
                            listStyleType: 'none'
                        }}>
                            <div style={{display: 'flex', position: 'relative'}}>
                <span onClick={this.handleRemoveImage} style={{
                    position: 'absolute', width: '28px', backgroundColor: 'rgba(255, 255, 255, 0.22)',
                    height: '28px', right: 12, top: 4, cursor: 'pointer', borderRadius: '50%',
                    display: 'flex', alignItems: 'center', justifyContent: 'center'
                }}>
                  <SvgRemoveImage style={{color: 'rgba(0, 0, 0, 0.53)'}}/>
                </span>

                                <div style={{
                                    display: 'inline-block',
                                    width: '100%',
                                    marginRight: '8px',
                                    transition: 'transform .25s'
                                }}>
                                    <li style={{width: '100%', margin: 0, verticalAlign: 'bottom', position: 'static'}}>
                                        <Img fileName={this.state.image} style={{width: '100%', height: 'auto'}}/>
                                    </li>
                                </div>
                            </div>

                        </ul>
                    </div>
                </div>
            ) : ''

        const footerAction = (
            <div className={'add-options-message'}>
                <a href={'javascript:(0)'} className='options-message' data-toggle='tooltip' data-placement='top'
                   data-original-title='ADD PHOTOS'>
                    <svg className='olymp-camera-icon' data-toggle='modal' data-target='#update-header-photo'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-camera-icon'/>
                    </svg>
                </a>
                <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handlePromotionPost}
                    disabled={this.state.disabledPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
            </div>
        )
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose} modalTitle={'Promotion'}
                    modalFooterAction={footerAction} action={rightIconMenu} modalClassName={''}>
                <CardHeader
                    title={author}
                    avatar={postAvatar}
                >
                </CardHeader>
                <div className='modal-body'>
                    <div className='form-group label-floating'>
                        <label className='control-label'>New job position</label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               name='jobPositionName'
                               type='text'
                        />
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Company Name</label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               name='companyName'
                               type='text'/>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Department Name</label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               name='departmentName'
                               type='text'/>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>description</label>
                        <textarea className='form-control'
                                  onChange={this.handleOnChange}
                                  name='description'
                                  defaultValue={''}/>
                    </div>
                    <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handlePromotionPost}
                    disabled={this.state.disabledPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
                    {/*<a href={'javascript:(0)'} className='btn btn-blue btn-lg full-width'>Post</a>*/}
                </div>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPromotionWriteComponentProps) => {
    return {
        post: (post: PostPromotion, callBack: Function) => dispatch(postActions.dbAddPromotionPostWithImage(post, callBack)),
        update: (post: Map<string, any>, callBack: Function) => dispatch(postActions.dbUpdatePost(post, callBack))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPromotionWriteComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        postImageState: state.getIn(['imageGallery', 'status']),
        ownerAvatar: user.avatar || '',
        ownerDisplayName: user.fullName || ''
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(PromotionWriteComponent as any) as any)
