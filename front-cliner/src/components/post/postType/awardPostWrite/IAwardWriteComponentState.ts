export interface IAwardWriteComponentState {
    /**
     * Post text
     */
    postText: string
    /**
     * The URL image of the post
     */
    image?: string
    /**
     * The path identifier of image on the server
     */
    imageFullPath: string
    /**
     * If it's true gallery will be open
     */
    galleryOpen: boolean
    /**
     * If it's true post button will be disabled
     */
    disabledPost: boolean
    /**
     * If it's true comment will be disabled on post
     */
    disableComments: boolean
    /**
     * If it's true share will be disabled on post
     */
    disableSharing: boolean,

    /**
     * Whether menu is open
     */
    menuOpen: boolean

    /**
     * Award name input field
     */
    title: string

    /**
     * Award name error message
     */
    nameError: string

    /**
     * Award receiving place input field
     */
    receivingPlace: string

    /**
     * Award receiving place error message
     */
    receivingPlaceError: string

    /**
     * Award publishing date input field
     */
   publishingDate: string

    /**
     * Award publishing date error message
     */
    publishingDateError: string

    /**
     * award description input field
     */
    description: string
}
