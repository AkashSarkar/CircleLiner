export interface IEventWriteComponentState {

    /**
     * Post text
     */
    postText: string
    /**
     * The URL image of the post
     */
    image?: string
    /**
     * The path identifier of image on the server
     */
    imageFullPath: string
    /**
     * If it's true gallery will be open
     */
    galleryOpen: boolean
    /**
     * If it's true post button will be disabled
     */
    disableComments: boolean
    /**
     * If it's true share will be disabled on post
     */
    disableSharing: boolean,
    /**
     * If it's true share will be disabled on post
     */
    disabledEventPost: boolean
    /**
     * Whether menu is open
     */
    menuOpen: boolean

    title: string
    startDate: string
    endDate: string
    description: string
}
