// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

import {Card, CardActions, CardHeader, CardMedia, CardContent} from '@material-ui/core'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Paper from '@material-ui/core/Paper'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import TextField from '@material-ui/core/TextField'
import Tooltip from '@material-ui/core/Tooltip'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import SvgRemoveImage from '@material-ui/icons/RemoveCircle'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import {withStyles} from '@material-ui/core/styles'
import {Manager, Target, Popper} from 'react-popper'
import Grow from '@material-ui/core/Grow'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import classNames from 'classnames'

// - Import app components
import ImageGallery from 'components/imageGallery/index'
import Img from 'components/img/index'
import UserAvatarComponent from 'components/userAvatar/index'

// - Import API
import * as PostAPI from 'api/PostAPI'

// - Import actions
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import * as postActions from 'store/actions/postActions'
import {IEventWriteComponentProps} from './IEventWriteComponentProps'
import {IEventWriteComponentState} from './IEventWriteComponentState'
import Grid from '@material-ui/core/Grid/Grid'
// - Import Layouts
import LModal from 'layouts/modal/index'
import {PostEvent} from 'core/domain/posts/postEvent'

const styles = (theme: any) => ({
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0,
            overflowY: 'auto'
        }
    },
    backdrop: {
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: '-1',
        position: 'fixed',
        willChange: 'opacity',
        backgroundColor: 'rgba(251, 249, 249, 0.5)',
        WebkitTapHighlightColor: 'transparent'
    },
    content: {
        padding: 0,
        paddingTop: 0
    },
    dialogRoot: {
        paddingTop: 0
    },
    popperOpen: {
        zIndex: 10
    },
    popperClose: {
        pointerEvents: 'none',
        zIndex: 0
    },
    author: {
        paddingRight: 70
    }
})
// - Create PostWrite component class
export class EventWriteComponent extends Component<IEventWriteComponentProps, IEventWriteComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IEventWriteComponentProps) {

        super(props)

        const { postEventModel } = props
        // Default state
        this.state = {
            title : this.props.edit && postEventModel ? postEventModel.get('title', '') : '',
            startDate : this.props.edit && postEventModel ? postEventModel.get('startDate', '') : '',
            endDate:  this.props.edit && postEventModel ? postEventModel.get('endDate', '') : '',
            description: this.props.edit && postEventModel ? postEventModel.get('description', '') : '',
            /**
             * Post text
             */
            postText: this.props.edit && postEventModel ? postEventModel.get('body', '') : '',
            /**
             * The URL image oFf the post
             */
            image: this.props.edit && postEventModel ? postEventModel.get('image', '') : '',
            /**
             * The path identifier of image on the server
             */
            imageFullPath: this.props.edit && postEventModel ? postEventModel.get('imageFullPath', '') : '',
            /**
             * If it's true gallery will be open
             */
            galleryOpen: false,
            /**
             * Whether menu is open
             */
            menuOpen: false,
            /**
             * If it's true post button will be disabled
             */
            disabledEventPost: true,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: this.props.edit && postEventModel ? postEventModel.get('disableComments') : false,
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: this.props.edit && postEventModel ? postEventModel.get('disableSharing') : false
        }

        // Binding functions to `this`
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleCloseGallery = this.handleCloseGallery.bind(this)
        this.handleOpenGallery = this.handleOpenGallery.bind(this)
        this.onRequestSetImage = this.onRequestSetImage.bind(this)
        this.handleEventPost = this.handleEventPost.bind(this)
        this.handleRemoveImage = this.handleRemoveImage.bind(this)
        this.handleToggleComments = this.handleToggleComments.bind(this)
        this.handleToggleSharing = this.handleToggleSharing.bind(this)
    }
    /**
     * Toggle comments of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleComments = () => {
        this.setState({
            disableComments: !this.state.disableComments,
            disabledEventPost: false
        })
    }

    /**
     * Toggle sharing of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleSharing = () => {
        this.setState({
            disableSharing: !this.state.disableSharing,
            disabledEventPost: false
        })
    }

    /**
     * Romove the image of post
     *
     *
     * @memberof PostWrite
     */
    handleRemoveImage = () => {
        this.setState({
            image: '',
            imageFullPath: '',
            disabledEventPost: this.state.postText.trim() === ''
        })
    }
    /**
     * When the post text changed
     * @param  {event} evt is an event passed by change post text callback funciton
     * @param  {string} data is the post content which user writes
     */
    handleOnChange = (event: any) => {
        const target = event.target
        const value = target.value
        const title = target.title
        // console.log(value)
        this.setState({
            [title]: value
        })

        switch (title) {
            case 'title':
                this.setState({
                    title: value,
                    disabledEventPost: false
                })
                break
            case 'startDate':
                this.setState({
                    startDate: value,
                    disabledEventPost: false
                })
                break
            case 'endDate':
                this.setState({
                    endDate: value,
                    disabledEventPost: false
                })
                break
            case 'description':
                this.setState({
                    description: value,
                    disabledEventPost: false
                })
                break
            default:
        }
    }

    /**
     * Handle send post to the server
     * @param  {event} evt passed by clicking on the post button
     */
    handleEventPost = () => {
        const {
            image,
            imageFullPath,
            disableComments,
            disableSharing,
            title,
            startDate,
            endDate,
            description } = this.state

        const {
            id,
            ownerAvatar,
            ownerDisplayName,
            edit,
            onRequestClose,
            post,
            update,
            postEventModel
        } = this.props
        if (image === '' && title.trim() === '' && startDate.trim() === ''  && endDate.trim() === ''  && description.trim() === '') {
            this.setState({
                image: '',
                disabledEventPost: false
            })
            return
        }

        let tags = PostAPI.getContentTags(description!)

        // In edit status we should fire update if not we should fire post function
        if (!edit) {
            if (image !== '') {
                post!({
                    title: title,
                    startDate: startDate,
                    endDate: endDate,
                    description: description,
                    tags: tags,
                    image: image,
                    imageFullPath: imageFullPath,
                    ownerAvatar: ownerAvatar,
                    ownerDisplayName: ownerDisplayName,
                    disableComments: disableComments,
                    disableSharing: disableSharing,
                    postTypeId: 1,
                    score: 0,
                    viewCount: 0
                }, onRequestClose)
            } else {
                post!({
                    title: title,
                    startDate: startDate,
                    endDate: endDate,
                    description: description,
                    tags: tags,
                    ownerAvatar: ownerAvatar,
                    ownerDisplayName: ownerDisplayName,
                    disableComments: disableComments,
                    disableSharing: disableSharing,
                    postTypeId: 0,
                    score: 0,
                    viewCount: 0
                }, onRequestClose)
            }
        } else { // In edit status we pass post to update functions
            const updatedPost =  postEventModel!.set('title', title)
                .set('startDate', startDate)
                .set('endDate', endDate)
                .set('description', description)
                .set('tags', tags)
                .set('image', image)
                .set('imageFullPath', imageFullPath)
                .set('disableComments', disableComments)
                .set('disableSharing', disableSharing)

            update!(updatedPost, onRequestClose)
        }
    }

    /**
     * Set post image url
     */
    onRequestSetImage = (url: string, fullPath: string) => {
        this.setState({
            image: url,
            imageFullPath: fullPath,
            disabledEventPost: false
        })
    }

    /**
     * Close image gallery
     */
    handleCloseGallery = () => {
        this.setState({
            galleryOpen: false
        })
    }

    /**
     * Open image gallery
     */
    handleOpenGallery = () => {
        this.setState({
            galleryOpen: true
        })
    }

    /**
     * Handle open more menu
     */
    handleOpenMenu = () => {
        this.setState({
            menuOpen: true
        })
    }

    /**
     * Handle close more menu
     */
    handleCloseMenu = () => {
        this.setState({
            menuOpen: false
        })
    }
    componentWillReceiveProps(nextProps: IEventWriteComponentProps) {
        if (!nextProps.open) {
            const { postEventModel } = this.props
            this.setState({
                title : this.props.edit && postEventModel ? postEventModel.get('articleName', '') : '',
                startDate : this.props.edit && postEventModel ? postEventModel.get('referenceLink', '') : '',
                endDate:  this.props.edit && postEventModel ? postEventModel.get('publishingDate', '') : '',
                description: this.props.edit && postEventModel ? postEventModel.get('articleDescription', '') : '',

                /**
                 * Post text
                 */
                postText: this.props.edit && postEventModel ? postEventModel.get('body', '') : '',
                /**
                 * The URL image of the post
                 */
                image: this.props.edit && postEventModel ? postEventModel.get('image', '') : '',
                /**
                 * The path identifier of image on the server
                 */
                imageFullPath: this.props.edit && postEventModel ? postEventModel.get('imageFullPath', '') : '',
                /**
                 * If it's true gallery will be open
                 */
                galleryOpen: false,
                /**
                 * Whether menu is open
                 */
                menuOpen: false,
                /**
                 * If it's true post button will be disabled
                 */
                disabledEventPost: true,
                /**
                 * If it's true comment will be disabled on post
                 */
                disableComments: this.props.edit && postEventModel ? postEventModel.get('disableComments') : false,
                /**
                 * If it's true share will be disabled on post
                 */
                disableSharing: this.props.edit && postEventModel ? postEventModel.get('disableSharing') : false

            })
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const { classes, translate } = this.props
        const { menuOpen } = this.state

        const rightIconMenu = (
            <div className='more'>
                <div className='container pt-3'>
                    <svg className='olymp-three-dots-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                    </svg>
                </div>
                <ul className='more-dropdown'>
                    <li className='nav-item' onClick={this.handleToggleComments}>
                        <a data-toggle='tab' href='' role='tab' aria-expanded='false'>
                            <span>{!this.state.disableComments ? 'Disable comments' : 'Enable comments'}</span>
                        </a>
                    </li>
                    <li className='nav-item' onClick={this.handleToggleSharing}>
                        <a data-toggle='tab' href=''
                           role='tab' aria-expanded='false'>
                            <span>{!this.state.disableSharing ? 'Disable sharing' : 'Enable sharing'}</span>
                        </a>
                    </li>
                </ul>
            </div>
        )

        let postAvatar = <UserAvatarComponent fullName={this.props.ownerDisplayName!} fileName={this.props.ownerAvatar!} size={36} />

        let author = (
            <div className={classes.author}>
        <span style={{
            fontSize: '14px',
            paddingRight: '10px',
            fontWeight: 400,
            color: 'rgba(0,0,0,0.87)',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            lineHeight: '25px'
        }}>{this.props.ownerDisplayName}</span><span style={{
                fontWeight: 400,
                fontSize: '10px'
            }}> | {translate!('post.public')}</span>
            </div>
        )
        const footerAction = (
            <div className={'add-options-message'}>
                <a href={'javascript:(0)'} className='options-message' data-toggle='tooltip' data-placement='top'
                   data-original-title='ADD PHOTOS'>
                    <svg className='olymp-camera-icon' data-toggle='modal' data-target='#update-header-photo'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-camera-icon'/>
                    </svg>
                </a>

                <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handleEventPost}
                    disabled={this.state.disabledEventPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
            </div>
        )
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose}  modalTitle={'Event'}
                    modalFooterAction={footerAction} action={rightIconMenu} modalClassName={''}>
                <CardHeader
                    title={author}
                    avatar={postAvatar}
                >
                </CardHeader>
                <div className='modal-body'>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Event title</label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               title='title'
                               type='text'/>
                    </div>
                    <div className='row'>
                        <div className='col order-1 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className='form-group date-time-picker label-floating is-focused'>
                                <label className='control-label'>Event Start Date</label>
                                <input className='datetimepicker'
                                       onChange={this.handleOnChange}
                                       type='date'
                                       title='startDate'
                                       defaultValue='26/03/2016'/>
                            </div>
                        </div>
                        <div className='col order-2 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className='form-group date-time-picker label-floating is-focused'>
                                <label className='control-label'>Event End Date</label>
                                <input className='datetimepicker'
                                       type='date'
                                       onChange={this.handleOnChange}
                                       title='endDate'
                                       defaultValue='26/03/2016'/>
                            </div>
                        </div>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Description</label>
                        <textarea className='form-control'
                                  onChange={this.handleOnChange}
                                  title='description'
                                  defaultValue={''}/>
                    </div>
                    <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handleEventPost}
                    disabled={this.state.disabledEventPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
                </div>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IEventWriteComponentProps) => {
    return {
        post: (post: PostEvent, callBack: Function) => dispatch(postActions.dbAddEventPost(post, callBack)),
        update: (post: Map<string, any>, callBack: Function) => dispatch(postActions.dbUpdatePost(post, callBack))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IEventWriteComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        postImageState: state.getIn(['imageGallery', 'status']),
        ownerAvatar: user.avatar || '',
        ownerDisplayName: user.fullName || ''
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(EventWriteComponent as any) as any)
