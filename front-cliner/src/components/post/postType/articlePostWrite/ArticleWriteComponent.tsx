// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
import {Card, CardActions, CardHeader, CardMedia, CardContent} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles'
import {Manager, Target, Popper} from 'react-popper'

// - Import app components
import UserAvatarComponent from 'components/userAvatar/index'

// - Import API
import * as PostAPI from 'api/PostAPI'

// - Import actions
import * as postActions from 'store/actions/postActions'
import {IArticleWriteComponentProps} from './IArticleWriteComponentProps'
import {IArticleWriteComponentState} from './IArticleWriteComponentState'
import {PostArticle} from 'core/domain/posts/postArticle'

// - Import Layouts
import LModal from 'layouts/modal/index'

const styles = (theme: any) => ({
    author: {
        paddingRight: 70
    }
})

// - Create PostWrite component class
export class ArticleWriteComponent extends Component<IArticleWriteComponentProps, IArticleWriteComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IArticleWriteComponentProps) {

        super(props)

        const {postArticleModel} = props

        // Default state
        this.state = {
            /**
             * Article title input
             */
            title: this.props.edit && postArticleModel ? postArticleModel.get('articleName', '') : '',

            /**
             * Article title input error
             */
            nameError: '',

            /**
             * Article reference linl input
             */
            referenceLink: this.props.edit && postArticleModel ? postArticleModel.get('referenceLink', '') : '',
            /**
             * Article publishing date input
             */
            publishingDate: this.props.edit && postArticleModel ? postArticleModel.get('publishingDate', '') : '',
            /**
             * Article description input
             */
            description: this.props.edit && postArticleModel ? postArticleModel.get('articleDescription', '') : '',

            /**
             * Article title input error
             */
            descriptionError: '',
            /**
             * Post text
             */
            postText: this.props.edit && postArticleModel ? postArticleModel.get('body', '') : '',
            /**
             * The URL image of the post
             */
            image: this.props.edit && postArticleModel ? postArticleModel.get('image', '') : '',
            /**
             * The path identifier of image on the server
             */
            imageFullPath: this.props.edit && postArticleModel ? postArticleModel.get('imageFullPath', '') : '',
            /**
             * If it's true gallery will be open
             */
            galleryOpen: false,
            /**
             * Whether menu is open
             */
            menuOpen: false,
            /**
             * If it's true post button will be disabled
             */
            disabledPost: true,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: this.props.edit && postArticleModel ? postArticleModel.get('disableComments') : false,
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: this.props.edit && postArticleModel ? postArticleModel.get('disableSharing') : false
        }

        // Binding functions to `this`
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleCloseGallery = this.handleCloseGallery.bind(this)
        this.handleOpenGallery = this.handleOpenGallery.bind(this)
        this.onRequestSetImage = this.onRequestSetImage.bind(this)
        this.handleArticlePost = this.handleArticlePost.bind(this)
        this.handleRemoveImage = this.handleRemoveImage.bind(this)
        this.handleToggleComments = this.handleToggleComments.bind(this)
        this.handleToggleSharing = this.handleToggleSharing.bind(this)
    }

    /**
     * Toggle comments of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleComments = () => {
        this.setState({
            disableComments: !this.state.disableComments,
            disabledPost: false
        })
    }

    /**
     * Toggle sharing of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleSharing = () => {
        this.setState({
            disableSharing: !this.state.disableSharing,
            disabledPost: false
        })
    }

    /**
     * Romove the image of post
     *
     *
     * @memberof PostWrite
     */
    handleRemoveImage = () => {
        this.setState({
            image: '',
            imageFullPath: '',
            disabledPost: this.state.postText.trim() === ''
        })
    }
    /**
     * When the post text changed
     * @param  {event} evt is an event passed by change post text callback funciton
     * @param  {string} data is the post content which user writes
     */
    handleOnChange = (event: any) => {
        const target = event.target
        const value = target.value
        const title = target.title
        // console.log(value)
        this.setState({
            [title]: value
        })

        switch (title) {
            case 'articleName':
                this.setState({
                    title: value,
                    nameError: '',
                    disabledPost: false
                })
                break
            case 'referenceLink':
                this.setState({
                    referenceLink: value,
                    disabledPost: false
                })
                break
            case 'publishingDate':
                this.setState({
                    publishingDate: value,
                    disabledPost: false
                })
                break
            case 'articleDescription':
                this.setState({
                    description: value,
                    descriptionError: '',
                    disabledPost: false
                })
                break
            default:
        }
    }

    /**
     * Handle send post to the server
     * @param  {event} evt passed by clicking on the post button
     */
    handleArticlePost = () => {
        const {translate} = this.props
        let error = false
        const {
            image,
            imageFullPath,
            disableComments,
            disableSharing,
            title,
            referenceLink,
            publishingDate,
            description
        } = this.state

        const {
            id,
            ownerAvatar,
            ownerDisplayName,
            edit,
            onRequestClose,
            post,
            update,
            postArticleModel
        } = this.props

        if (title.trim() === '') {
            this.setState({
                nameError: translate!('post.articleName'),
                disabledPost: true
            })
            error = true

        } else if (description.trim() === '') {
            this.setState({
                descriptionError: translate!('post.descriptionInputError'),
                disabledPost: true
            })
            error = true

        }
        // else if (image === '') {
        //     this.setState({
        //         image: '',
        //         disabledArticlePost: true
        //     })
        //     return
        // }

        let tags = PostAPI.getContentTags(description!)

        // In edit status we should fire update if not we should fire post function
        if (!error) {
            if (!edit) {
                if (image !== '') {
                    post!({
                        title: title,
                        referenceLink: referenceLink,
                        publishingDate: publishingDate,
                        description: description,
                        tags: tags,
                        image: image,
                        imageFullPath: imageFullPath,
                        ownerAvatar: ownerAvatar,
                        ownerDisplayName: ownerDisplayName,
                        disableComments: disableComments,
                        disableSharing: disableSharing,
                        postTypeId: 1,
                        score: 0,
                        viewCount: 0
                    }, onRequestClose)
                } else {
                    post!({
                        title: title,
                        referenceLink: referenceLink,
                        publishingDate: publishingDate,
                        description: description,
                        tags: tags,
                        ownerAvatar: ownerAvatar,
                        ownerDisplayName: ownerDisplayName,
                        disableComments: disableComments,
                        disableSharing: disableSharing,
                        postTypeId: 0,
                        score: 0,
                        viewCount: 0
                    }, onRequestClose)
                }
            } else { // In edit status we pass post to update functions
                const updatedPost = postArticleModel!.set('title', title)
                    .set('referenceLink', referenceLink)
                    .set('publishingDate', publishingDate)
                    .set('description', description)
                    .set('tags', tags)
                    .set('image', image)
                    .set('imageFullPath', imageFullPath)
                    .set('disableComments', disableComments)
                    .set('disableSharing', disableSharing)

                update!(updatedPost, onRequestClose)
            }
        }
    }

    /**
     * Set post image url
     */
    onRequestSetImage = (url: string, fullPath: string) => {
        this.setState({
            image: url,
            imageFullPath: fullPath,
            disabledPost: false
        })
    }

    /**
     * Close image gallery
     */
    handleCloseGallery = () => {
        this.setState({
            galleryOpen: false
        })
    }

    /**
     * Open image gallery
     */
    handleOpenGallery = () => {
        this.setState({
            galleryOpen: true
        })
    }

    /**
     * Handle open more menu
     */
    handleOpenMenu = () => {
        this.setState({
            menuOpen: true
        })
    }

    /**
     * Handle close more menu
     */
    handleCloseMenu = () => {
        this.setState({
            menuOpen: false
        })
    }

    componentWillReceiveProps(nextProps: IArticleWriteComponentProps) {
        if (!nextProps.open) {
            const { postArticleModel } = this.props
            this.setState({
                /**
                 * Article title input
                 */
                title : this.props.edit && postArticleModel ? postArticleModel.get('articleName', '') : '',
                /**
                 * Article reference linl input
                 */
                referenceLink : this.props.edit && postArticleModel ? postArticleModel.get('referenceLink', '') : '',
                /**
                 * Article publishing date input
                 */
                publishingDate:  this.props.edit && postArticleModel ? postArticleModel.get('publishingDate', '') : '',
                /**
                 * Article description input
                 */
                description: this.props.edit && postArticleModel ? postArticleModel.get('articleDescription', '') : '',
                /**
                 * Post text
                 */
                postText: this.props.edit && postArticleModel ? postArticleModel.get('body', '') : '',
                /**
                 * The URL image of the post
                 */
                image: this.props.edit && postArticleModel ? postArticleModel.get('image', '') : '',
                /**
                 * The path identifier of image on the server
                 */
                imageFullPath: this.props.edit && postArticleModel ? postArticleModel.get('imageFullPath', '') : '',
                /**
                 * If it's true gallery will be open
                 */
                galleryOpen: false,
                /**
                 * Whether menu is open
                 */
                menuOpen: false,
                /**
                 * If it's true post button will be disabled
                 */
                disabledPost: true,
                /**
                 * If it's true comment will be disabled on post
                 */
                disableComments: this.props.edit && postArticleModel ? postArticleModel.get('disableComments') : false,
                /**
                 * If it's true share will be disabled on post
                 */
                disableSharing: this.props.edit && postArticleModel ? postArticleModel.get('disableSharing') : false

            })
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const { classes, translate } = this.props
        const { menuOpen } = this.state

        const rightIconMenu = (
            <div className='more'>
                <div className='container pt-3'>
                    <svg className='olymp-three-dots-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                    </svg>
                </div>
                <ul className='more-dropdown'>
                    <li className='nav-item' onClick={this.handleToggleComments}>
                        <a data-toggle='tab' href='' role='tab' aria-expanded='false'>
                            <span>{!this.state.disableComments ? 'Disable comments' : 'Enable comments'}</span>
                        </a>
                    </li>
                    <li className='nav-item' onClick={this.handleToggleSharing}>
                        <a data-toggle='tab' href=''
                           role='tab' aria-expanded='false'>
                            <span>{!this.state.disableSharing ? 'Disable sharing' : 'Enable sharing'}</span>
                        </a>
                    </li>
                </ul>
            </div>
        )

        let postAvatar = <UserAvatarComponent fullName={this.props.ownerDisplayName!} fileName={this.props.ownerAvatar!} size={36} />

        let author = (
        <div className={classes.author}>
        <span style={{
            fontSize: '14px',
            paddingRight: '10px',
            fontWeight: 400,
            color: 'rgba(0,0,0,0.87)',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            lineHeight: '25px'
        }}>{this.props.ownerDisplayName}</span><span style={{
                fontWeight: 400,
                fontSize: '10px'
            }}> | {translate!('post.public')}</span>
        </div>
        )

        const footerAction = (
            <div className={'add-options-message'}>
                <a href={'javascript:(0)'} className='options-message' data-toggle='tooltip' data-placement='top'
                   data-original-title='ADD PHOTOS'>
                    <svg className='olymp-camera-icon' data-toggle='modal' data-target='#update-header-photo'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-camera-icon'/>
                    </svg>
                </a>

                <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handleArticlePost}
                    disabled={this.state.disabledPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
            </div>
        )

        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose} modalTitle={'Article'}
                    modalFooterAction={footerAction} action={rightIconMenu} modalClassName={''}>
                <CardHeader
                    title={author}
                    avatar={postAvatar}>
                </CardHeader>
                <div className='modal-body'>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Article Name<span>*</span></label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               title='articleName'
                               type='text'
                        />
                        <span style={{color: 'red'}}>
                                            {this.state.nameError.trim() ? this.state.nameError : ''}
                                        </span>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Reference Link</label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               title='referenceLink'
                               type='text'
                        />
                    </div>
                    <div className='form-group date-time-picker label-floating'>
                        <label className='control-label'>Publishing Date</label>
                        <input title='publishingDate'
                               type='date'
                               onChange={this.handleOnChange}
                               defaultValue='' />
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Description<span>*</span></label>
                        <textarea className='form-control'
                                  onChange={this.handleOnChange}
                                  title='articleDescription'
                                  defaultValue={''}
                        />
                        <span style={{color: 'red'}}>
                                            {this.state.descriptionError.trim() ? this.state.descriptionError : ''}
                                        </span>
                    </div>
                    <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handleArticlePost}
                    disabled={this.state.disabledPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
                </div>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IArticleWriteComponentProps) => {
    return {
        post: (post: PostArticle, callBack: Function) => dispatch(postActions.dbAddArticlePost(post, callBack)),
        update: (post: Map<string, any>, callBack: Function) => dispatch(postActions.dbUpdatePost(post, callBack))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IArticleWriteComponentProps) => {
        const uid = state.getIn(['authorize', 'uid'])
        const user = state.getIn(['user', 'info', uid], {})
        return {
            translate: getTranslate(state.get('locale')),
            postImageState: state.getIn(['imageGallery', 'status']),
            ownerAvatar: user.avatar || '',
            ownerDisplayName: user.fullName || ''
        }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(ArticleWriteComponent as any) as any)
