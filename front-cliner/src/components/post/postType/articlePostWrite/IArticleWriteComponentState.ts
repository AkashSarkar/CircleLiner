export interface IArticleWriteComponentState {

    /**
     * Post text
     */
    postText: string
    /**
     * The URL image of the post
     */
    image?: string
    /**
     * The path identifier of image on the server
     */
    imageFullPath: string
    /**
     * If it's true gallery will be open
     */
    galleryOpen: boolean
    /**
     * If it's true post button will be disabled
     */
    disabledPost: boolean
    /**
     * If it's true comment will be disabled on post
     */
    disableComments: boolean
    /**
     * If it's true share will be disabled on post
     */
    disableSharing: boolean,

    /**
     * Whether menu is open
     */
    menuOpen: boolean

    /**
     * article name input
     *
     * @type {string}
     * @memberof IArticleWriteComponentState
     */
    title: string

    /**
     * article name input error
     *
     * @type {string}
     * @memberof IArticleWriteComponentState
     */
    nameError: string

    /**
     * article reference input
     *
     * @type {string}
     * @memberof IArticleWriteComponentState
     */
    referenceLink: string

    /**
     * article publishing date input
     *
     * @type {string}
     * @memberof IArticleWriteComponentState
     */
    publishingDate: string

    /**
     * article description input
     *
     * @type {string}
     * @memberof IArticleWriteComponentState
     */
    description: string

    /**
     * article description error input
     *
     * @type {string}
     * @memberof IArticleWriteComponentState
     */
    descriptionError: string
}
