// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles'
import {Manager, Target, Popper} from 'react-popper'

// - Import app components
import UserAvatarComponent from 'components/userAvatar/index'

// - Import API
import * as PostAPI from 'api/PostAPI'

// - Import actions
import * as postActions from 'store/actions/postActions'
import {IStatusWriteComponentProps} from './IStatusWriteComponentProps'
import {IStatusWriteComponentState} from './IStatusWriteComponentState'
import {Post} from 'core/domain/posts/index'

// - Import Layouts
import LModal from 'layouts/modal/index'
import {label} from 'aws-sdk/clients/sns'
import ImageGalleryComponent from 'components/imageGallery'

// - Import Matarial UI
import SvgRemoveImage from '@material-ui/icons/RemoveCircle'
import ImgComponent from 'components/img'

const styles = (theme: any) => ({
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0,
            overflowY: 'auto'
        }
    },
    backdrop: {
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: '-1',
        position: 'fixed',
        willChange: 'opacity',
        backgroundColor: 'rgba(251, 249, 249, 0.5)',
        WebkitTapHighlightColor: 'transparent'
    },
    content: {
        padding: 0,
        paddingTop: 0
    },
    dialogRoot: {
        paddingTop: 0
    },
    popperOpen: {
        zIndex: 10
    },
    popperClose: {
        pointerEvents: 'none',
        zIndex: 0
    },
    author: {
        paddingRight: 70
    }
})

// - Create PostWrite component class
export class StatusWriteComponent extends Component<IStatusWriteComponentProps, IStatusWriteComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IStatusWriteComponentProps) {

        super(props)

        const {postModel} = props

        // Default state
        this.state = {
            /**
             * Post text
             */
            postText: this.props.edit && postModel ? postModel.get('body', '') : '',
            /**
             * description input field required error
             */
            descriptionInputError: '',
            /**
             * The URL image of the post
             */
            image: this.props.edit && postModel ? postModel.get('image', '') : '',
            /**
             * The path identifier of image on the server
             */
            imageFullPath: this.props.edit && postModel ? postModel.get('imageFullPath', '') : '',
            /**
             * If it's true gallery will be open
             */
            galleryOpen: false,
            /**
             * Whether menu is open
             */
            menuOpen: false,
            /**
             * If it's true post button will be disabled
             */
            disabledPost: true,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: this.props.edit && postModel ? postModel.get('disableComments') : false,
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: this.props.edit && postModel ? postModel.get('disableSharing') : false,
            /**
             * Pick date
             */
            // startDate: moment()
        }

        // Binding functions to `this`
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleCloseGallery = this.handleCloseGallery.bind(this)
        this.handleOpenGallery = this.handleOpenGallery.bind(this)
        this.onRequestSetImage = this.onRequestSetImage.bind(this)
        this.handleStatusPost = this.handleStatusPost.bind(this)
        this.handleRemoveImage = this.handleRemoveImage.bind(this)
        this.handleToggleComments = this.handleToggleComments.bind(this)
        this.handleToggleSharing = this.handleToggleSharing.bind(this)
        // this.handleTimeChange = this.handleTimeChange.bind(this);
    }

    // handleTimeChange(date) {
    //     this.setState({
    //         startDate: date
    //     });
    // }
    /**
     * Toggle comments of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleComments = () => {
        this.setState({
            disableComments: !this.state.disableComments,
            disabledPost: false
        })
    }

    /**
     * Toggle sharing of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleSharing = () => {
        this.setState({
            disableSharing: !this.state.disableSharing,
            disabledPost: false
        })
    }

    /**
     * Romove the image of post
     *
     *
     * @memberof PostWrite
     */
    handleRemoveImage = () => {
        this.setState({
            image: '',
            imageFullPath: '',
            disabledPost: this.state.postText.trim() === ''
        })
    }

    /**
     * Handle send status post to the server
     * @param  {event} evt passed by clicking on the post button
     */
    handleStatusPost = () => {
        const {
            image,
            imageFullPath,
            disableComments,
            disableSharing,
            postText
        } = this.state

        const {
            id,
            ownerAvatar,
            ownerDisplayName,
            edit,
            onRequestClose,
            post,
            update,
            postModel,
            translate
        } = this.props

        let error = false

        if (image === '' && postText.trim() === '') {
            this.setState({
                disabledPost: false
            })
            error = true
        } else if (this.state.postText === '') {
            this.setState({
                descriptionInputError: translate!('post.descriptionInputError')
            })
            error = true
        }

        let tags = PostAPI.getContentTags(postText!)

        // In edit status we should fire update if not we should fire post function
        if (!error) {
            if (!edit) {
                if (image !== '') {
                    post!({
                        description: postText,
                        tags: tags,
                        image: image,
                        imageFullPath: imageFullPath,
                        ownerAvatar: ownerAvatar,
                        ownerDisplayName: ownerDisplayName,
                        disableComments: disableComments,
                        disableSharing: disableSharing,
                        postTypeId: 1,
                        score: 0,
                        viewCount: 0
                    }, onRequestClose)
                } else {
                    post!({
                        description: postText,
                        tags: tags,
                        ownerAvatar: ownerAvatar,
                        ownerDisplayName: ownerDisplayName,
                        disableComments: disableComments,
                        disableSharing: disableSharing,
                        postTypeId: 0,
                        score: 0,
                        viewCount: 0
                    }, onRequestClose)
                }
            } else { // In edit status we pass post to update functions
                const updatedPost = postModel!.set('body', postText)
                    .set('tags', tags)
                    .set('image', image)
                    .set('imageFullPath', imageFullPath)
                    .set('disableComments', disableComments)
                    .set('disableSharing', disableSharing)

                update!(updatedPost, onRequestClose)
            }
        }
    }

    /**
     * Set post image url
     */
    onRequestSetImage = (url: string, fullPath: string) => {
        this.setState({
            image: url,
            imageFullPath: fullPath,
            disabledPost: false
        })
    }

    /**
     * When the post text changed
     * @param  {event} evt is an event passed by change post text callback funciton
     * @param  {string} data is the post content which user writes
     */
    handleOnChange = (event: any) => {
        const data = event.target.value
        // console.log(data)
        this.setState({postText: data})
        if (data.length === 0 || data.trim() === '' || (this.props.edit && data.trim() === this.props.text)) {
            this.setState({
                postText: data,
                disabledPost: true,
                descriptionInputError: '',
            })
        } else {
            this.setState({
                postText: data,
                disabledPost: false,
                descriptionInputError: ''
            })
        }
    }

    /**
     * Close image gallery
     */
    handleCloseGallery = () => {
        this.setState({
            galleryOpen: false
        })
    }

    /**
     * Open image gallery
     */
    handleOpenGallery = () => {
        this.setState({
            galleryOpen: true
        })
    }

    /**
     * Handle open more menu
     */
    handleOpenMenu = () => {
        this.setState({
            menuOpen: true
        })
    }

    /**
     * Handle close more menu
     */
    handleCloseMenu = () => {
        this.setState({
            menuOpen: false
        })
    }

    componentWillReceiveProps(nextProps: IStatusWriteComponentProps) {
        if (!nextProps.open) {
            const {postModel} = this.props
            this.setState({
                /**
                 * Post text
                 */
                postText: this.props.edit && postModel ? postModel.get('body', '') : '',
                /**
                 * description input field required error
                 */
                descriptionInputError: '',
                /**
                 * The URL image of the post
                 */
                image: this.props.edit && postModel ? postModel.get('image', '') : '',
                /**
                 * The path identifier of image on the server
                 */
                imageFullPath: this.props.edit && postModel ? postModel.get('imageFullPath', '') : '',
                /**
                 * If it's true gallery will be open
                 */
                galleryOpen: false,
                /**
                 * Whether menu is open
                 */
                menuOpen: false,
                /**
                 * If it's true post button will be disabled
                 */
                disabledPost: true,
                /**
                 * If it's true comment will be disabled on post
                 */
                disableComments: this.props.edit && postModel ? postModel.get('disableComments') : false,
                /**
                 * If it's true share will be disabled on post
                 */
                disableSharing: this.props.edit && postModel ? postModel.get('disableSharing') : false
            })
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {classes, translate} = this.props
        const {menuOpen} = this.state

        const rightIconMenu = (
            <div className='more'>
                <div className='container pt-3'>
                    <svg className='olymp-three-dots-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                    </svg>
                </div>
                <ul className='more-dropdown'>
                    <li className='nav-item' onClick={this.handleToggleComments}>
                        <a data-toggle='tab' href='' role='tab' aria-expanded='false'>
                            <span>{!this.state.disableComments ? 'Disable comments' : 'Enable comments'}</span>
                        </a>
                    </li>
                    <li className='nav-item' onClick={this.handleToggleSharing}>
                        <a data-toggle='tab' href=''
                           role='tab' aria-expanded='false'>
                            <span>{!this.state.disableSharing ? 'Disable sharing' : 'Enable sharing'}</span>
                        </a>
                    </li>
                </ul>
            </div>
        )
        // let postAvatar = (
        //     <UserAvatarComponent fullName={this.props.ownerDisplayName!} fileName={this.props.ownerAvatar!}
        //                          size={36}/>)

        let author = (
            <UserAvatarComponent fullName={this.props.ownerDisplayName!} fileName={this.props.ownerAvatar!}
                                 size={36}/>

        )

        /*         const footerAction = (
                    <div className={'add-options-message'}>
                        <a href={'javascript:(0)'} className='options-message' data-toggle='tooltip' data-placement='top'
                           data-original-title='ADD PHOTOS'>
                            <svg className='olymp-camera-icon' data-toggle='modal' data-target='#update-header-photo'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-camera-icon'/>
                            </svg>
                        </a>

                        <Button
                            className={'btn btn-indigo btn-md-2'}
                            disableFocusRipple={true}
                            disableRipple={true}
                            onClick={this.handleStatusPost}
                            disabled={this.state.disabledPost}
                        >
                            {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                        </Button>
                    </div>
                )
                const headerAction = (
                        <Button
                            className={'btn btn-indigo btn-md-2'}
                            disableFocusRipple={true}
                            disableRipple={true}
                            onClick={this.handleStatusPost}
                            disabled={this.state.disabledPost}
                        >
                            {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                        </Button>
                )*/
        /**
         * Provide post image
         */
        const loadImage = (this.state.image && this.state.image !== '')
            ? (
                <div>
                    <div style={{position: 'relative', overflowY: 'hidden', overflowX: 'auto'}}>
                        <ul style={{
                            position: 'relative',
                            whiteSpace: 'nowrap',
                            padding: '0 0 0 16px',
                            margin: '8px 0 0 0',
                            paddingRight: '16px',
                            verticalAlign: 'bottom',
                            flexShrink: 0,
                            listStyleType: 'none'
                        }}>
                            <div style={{display: 'flex', position: 'relative'}}>
                <span onClick={this.handleRemoveImage} style={{
                    position: 'absolute', width: '28px', backgroundColor: 'rgba(255, 255, 255, 0.22)',
                    height: '28px', right: 12, top: 4, cursor: 'pointer', borderRadius: '50%',
                    display: 'flex', alignItems: 'center', justifyContent: 'center'
                }}>
                  <SvgRemoveImage style={{color: 'rgba(0, 0, 0, 0.53)'}}/>
                </span>

                                <div style={{
                                    display: 'inline-block',
                                    width: '100%',
                                    marginRight: '8px',
                                    transition: 'transform .25s'
                                }}>
                                    <li style={{width: '100%', margin: 0, verticalAlign: 'bottom', position: 'static'}}>
                                        <ImgComponent fileName={this.state.image} style={{width: '100%', height: 'auto'}}/>
                                    </li>
                                </div>
                            </div>

                        </ul>
                    </div>
                </div>
            ) : ''

        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose} modalTitle={'Status'}
                    action={rightIconMenu} modalClassName={''}>
                <div className={'news-feed-form'}>
                    {author}
                    <div className='form-group with-icon label-floating is-empty'>
                        <label className='control-label'>{translate!('post.textareaPlaceholder')}</label>
                        <textarea className='form-control'
                                  autoFocus
                                  onChange={this.handleOnChange}
                                  value={this.state.postText}
                                  placeholder=''
                        />
                        {loadImage}
                        <div className='add-options-message'>
                            <a href={'javascript:(0)'} className='options-message' data-toggle='modal'
                               data-original-title='ADD PHOTOS'
                               onClick={this.handleOpenGallery}
                            >
                                <svg className='olymp-camera-icon'>
                                    <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-camera-icon'/>
                                </svg>
                            </a>
                            <Button
                                className={'btn btn-primary btn-sm-2'}
                                onClick={this.handleStatusPost}
                                disabled={this.state.disabledPost}
                            >
                                {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                            </Button>
                        </div>
                        <span style={{color: 'red'}}>
                        {this.state.descriptionInputError.trim() ? this.state.descriptionInputError : ''}
                    </span>
                    </div>
                </div>
                <ImageGalleryComponent open={this.state.galleryOpen}
                                       set={this.onRequestSetImage}
                                       close={this.handleCloseGallery}/>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IStatusWriteComponentProps) => {
    return {
        post: (post: Post, callBack: Function) => dispatch(postActions.dbAddImagePost(post, callBack)),
        update: (post: Map<string, any>, callBack: Function) => dispatch(postActions.dbUpdatePost(post, callBack))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IStatusWriteComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        postImageState: state.getIn(['imageGallery', 'status']),
        ownerAvatar: user.avatar || '',
        ownerDisplayName: user.fullName || ''
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(StatusWriteComponent as any) as any)
