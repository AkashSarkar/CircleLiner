// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
import {Card, CardActions, CardHeader, CardMedia, CardContent} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import SvgRemoveImage from '@material-ui/icons/RemoveCircle'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import {withStyles} from '@material-ui/core/styles'
import {Manager, Target, Popper} from 'react-popper'
import Grow from '@material-ui/core/Grow'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import classNames from 'classnames'

// - Import app components
import ImageGallery from 'components/imageGallery/index'
import Img from 'components/img/index'
import UserAvatarComponent from 'components/userAvatar/index'

// - Import API
import * as PostAPI from 'api/PostAPI'

// - Import actions
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import * as postActions from 'store/actions/postActions'
import {IConferenceWriteComponentProps} from './IConferenceWriteComponentProps'
import {IConferenceWriteComponentState} from './IConferenceWriteComponentState'

// - Import domain
import { PostConference } from 'core/domain/posts/postConference'

// - Import Layouts
import LModal from 'layouts/modal/index'

const styles = (theme: any) => ({
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0,
            overflowY: 'auto'
        }
    },
    backdrop: {
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: '-1',
        position: 'fixed',
        willChange: 'opacity',
        backgroundColor: 'rgba(251, 249, 249, 0.5)',
        WebkitTapHighlightColor: 'transparent'
    },
    content: {
        padding: 0,
        paddingTop: 0
    },
    dialogRoot: {
        paddingTop: 0
    },
    popperOpen: {
        zIndex: 10
    },
    popperClose: {
        pointerEvents: 'none',
        zIndex: 0
    },
    author: {
        paddingRight: 70
    }
})

// - Create PostWrite component class
export class ConferenceWriteComponent extends Component<IConferenceWriteComponentProps, IConferenceWriteComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IConferenceWriteComponentProps) {

        super(props)
        const { postConferenceModel } = props
        // Default state
        this.state = {
            /**
             * conference title input
             */
            title : this.props.edit && postConferenceModel ? postConferenceModel.get('title', '') : '',
            /**
             * Conference title error message
             */
            nameError: '',
            /**
             * conference topic input
             */
            topic : this.props.edit && postConferenceModel ? postConferenceModel.get('topic', '') : '',
            /**
             * conference topic error message
             */
            topicError: '',
            /**
             * conference url input
             */
            url:  this.props.edit && postConferenceModel ? postConferenceModel.get('url', '') : '',
            /**
             * conference role input
             */
            role: this.props.edit && postConferenceModel ? postConferenceModel.get('role', '') : '',
            /**
             * conference role error message
             */
            roleError: '',
            /**
             * conference description input
             */
            description: this.props.edit && postConferenceModel ? postConferenceModel.get('description', '') : '',
            /**
             * Post conference text
             */
            postText: this.props.edit && postConferenceModel ? postConferenceModel.get('body', '') : '',
            /**
             * The URL image of the post
             */
            image: this.props.edit && postConferenceModel ? postConferenceModel.get('image', '') : '',
            /**
             * The path identifier of image on the server
             */
            imageFullPath: this.props.edit && postConferenceModel ? postConferenceModel.get('imageFullPath', '') : '',
            /**
             * If it's true gallery will be open
             */
            galleryOpen: false,
            /**
             * Whether menu is open
             */
            menuOpen: false,
            /**
             * If it's true post button will be disabled
             */
            disabledPost: true,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: this.props.edit && postConferenceModel ? postConferenceModel.get('disableComments') : false,
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: this.props.edit && postConferenceModel ? postConferenceModel.get('disableSharing') : false
        }

        // Binding functions to `this`
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleCloseGallery = this.handleCloseGallery.bind(this)
        this.handleOpenGallery = this.handleOpenGallery.bind(this)
        this.onRequestSetImage = this.onRequestSetImage.bind(this)
        this.handleConferencePost = this.handleConferencePost.bind(this)
        this.handleRemoveImage = this.handleRemoveImage.bind(this)
        this.handleToggleComments = this.handleToggleComments.bind(this)
        this.handleToggleSharing = this.handleToggleSharing.bind(this)
    }

    /**
     * Toggle comments of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleComments = () => {
        this.setState({
            disableComments: !this.state.disableComments,
            disabledPost: false
        })
    }

    /**
     * Toggle sharing of the post to disable/enable
     *
     *
     * @memberof PostWrite
     */
    handleToggleSharing = () => {
        this.setState({
            disableSharing: !this.state.disableSharing,
            disabledPost: false
        })
    }

    /**
     * Romove the image of post
     *
     *
     * @memberof PostWrite
     */
    handleRemoveImage = () => {
        this.setState({
            image: '',
            imageFullPath: '',
            disabledPost: this.state.postText.trim() === ''
        })
    }
    /**
     * When the post text changed
     * @param  {event} evt is an event passed by change post text callback funciton
     * @param  {string} data is the post content which user writes
     */
    handleOnChange = (event: any) => {
        const target = event.target
        const value = target.value
        const title = target.title
        // console.log(value)
        this.setState({
            [title]: value
        })

        switch (title) {
            case 'title':
                this.setState({
                    title: value,
                    nameError: '',
                    disabledPost: false
                })
                break
            case 'topic':
                this.setState({
                    topic: value,
                    topicError: '',
                    disabledPost: false
                })
                break
            case 'url':
                this.setState({
                    url: value,
                    disabledPost: false
                })
                break
            case 'role':
                this.setState({
                    role: value,
                    roleError: '',
                    disabledPost: false
                })
                break
            case 'description':
                this.setState({
                    role: value,
                    disabledPost: false
                })
                break
            default:
        }
    }

    /**
     * Handle send post to the server
     * @param  {event} evt passed by clicking on the post button
     */
    handleConferencePost = () => {
        const {translate} = this.props
        let error = false

        const {
            image,
            imageFullPath,
            disableComments,
            disableSharing,
            title,
            topic,
            url,
            role,
            description
        } = this.state

        const {
            id,
            ownerAvatar,
            ownerDisplayName,
            edit,
            onRequestClose,
            post,
            update,
            postConferenceModel

        } = this.props

        if (title.trim() === '') {
            this.setState({
                nameError: translate!('post.nameError'),
                disabledPost: true
            })
            error = true

        } else if (topic.trim() === '') {
            this.setState({
                topicError: translate!('post.topicError'),
                disabledPost: true
            })
            error = true

        } else if (role.trim() === '') {
            this.setState({
                roleError: translate!('post.roleError'),
                disabledPost: true
            })
            error = true

        }

        // if (image === '' && title.trim() === ''
        //     && topic.trim() === ''
        //     && url.trim() === ''
        //     && role.trim() === ''
        //     && description.trim() === '') {
        //     this.setState({
        //         image: '',
        //         disabledPost: false
        //     })
        //     return
        // }

        let tags = PostAPI.getContentTags(description!)

        // In edit status we should fire update if not we should fire post function
        if (!error) {
            if (!edit) {
                if (image !== '') {
                    post!({
                        title: title,
                        topic: topic,
                        url: url,
                        role: role,
                        description: description,
                        tags: tags,
                        image: image,
                        imageFullPath: imageFullPath,
                        ownerAvatar: ownerAvatar,
                        ownerDisplayName: ownerDisplayName,
                        disableComments: disableComments,
                        disableSharing: disableSharing,
                        postTypeId: 1,
                        score: 0,
                        viewCount: 0
                    }, onRequestClose)
                } else {
                    post!({
                        title: title,
                        topic: topic,
                        url: url,
                        role: role,
                        description: description,
                        tags: tags,
                        ownerAvatar: ownerAvatar,
                        ownerDisplayName: ownerDisplayName,
                        disableComments: disableComments,
                        disableSharing: disableSharing,
                        postTypeId: 0,
                        score: 0,
                        viewCount: 0
                    }, onRequestClose)
                }
            } else { // In edit status we pass post to update functions
                const updatedPost = postConferenceModel!.set('title', title)
                    .set('topic', topic)
                    .set('url', url)
                    .set('role', role)
                    .set('description', description)
                    .set('tags', tags)
                    .set('image', image)
                    .set('imageFullPath', imageFullPath)
                    .set('disableComments', disableComments)
                    .set('disableSharing', disableSharing)

                update!(updatedPost, onRequestClose)
            }
        }
    }
    /**
     * Set post image url
     */
    onRequestSetImage = (url: string, fullPath: string) => {
        this.setState({
            image: url,
            imageFullPath: fullPath,
            disabledPost: false
        })
    }

    /**
     * Close image gallery
     */
    handleCloseGallery = () => {
        this.setState({
            galleryOpen: false
        })
    }

    /**
     * Open image gallery
     */
    handleOpenGallery = () => {
        this.setState({
            galleryOpen: true
        })
    }

    /**
     * Handle open more menu
     */
    handleOpenMenu = () => {
        this.setState({
            menuOpen: true
        })
    }

    /**
     * Handle close more menu
     */
    handleCloseMenu = () => {
        this.setState({
            menuOpen: false
        })
    }
    
    componentWillReceiveProps(nextProps: IConferenceWriteComponentProps) {
        if (!nextProps.open) {
            const { postConferenceModel
 } = this.props
            this.setState({
                /**
                 * conference title input
                 */
                title : this.props.edit && postConferenceModel ? postConferenceModel.get('title', '') : '',
                /**
                 * Conference title error message
                 */
                nameError: '',
                /**
                 * conference topic input
                 */
                topic : this.props.edit && postConferenceModel ? postConferenceModel.get('topic', '') : '',
                /**
                 * conference topic error message
                 */
                topicError: '',
                /**
                 * conference url input
                 */
                url:  this.props.edit && postConferenceModel ? postConferenceModel.get('url', '') : '',
                /**
                 * conference role input
                 */
                role: this.props.edit && postConferenceModel ? postConferenceModel.get('role', '') : '',
                /**
                 * conference role error message
                 */
                roleError: '',
                /**
                 * conference description input
                 */
                description: this.props.edit && postConferenceModel ? postConferenceModel.get('description', '') : '',
                /**
                 * Post conference text
                 */
                postText: this.props.edit && postConferenceModel ? postConferenceModel.get('body', '') : '',
                /**
                 * The URL image of the post
                 */
                image: this.props.edit && postConferenceModel ? postConferenceModel.get('image', '') : '',
                /**
                 * The path identifier of image on the server
                 */
                imageFullPath: this.props.edit && postConferenceModel ? postConferenceModel.get('imageFullPath', '') : '',
                /**
                 * If it's true gallery will be open
                 */
                galleryOpen: false,
                /**
                 * Whether menu is open
                 */
                menuOpen: false,
                /**
                 * If it's true post button will be disabled
                 */
                disabledPost: true,
                /**
                 * If it's true comment will be disabled on post
                 */
                disableComments: this.props.edit && postConferenceModel ? postConferenceModel.get('disableComments') : false,
                /**
                 * If it's true share will be disabled on post
                 */
                disableSharing: this.props.edit && postConferenceModel ? postConferenceModel.get('disableSharing') : false

            })
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const { classes, translate } = this.props
        const { menuOpen } = this.state

        const rightIconMenu = (
            <div className='more'>
                <div className='container pt-3'>
                    <svg className='olymp-three-dots-icon'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                    </svg>
                </div>
                <ul className='more-dropdown'>
                    <li className='nav-item' onClick={this.handleToggleComments}>
                        <a data-toggle='tab' href='' role='tab' aria-expanded='false'>
                            <span>{!this.state.disableComments ? 'Disable comments' : 'Enable comments'}</span>
                        </a>
                    </li>
                    <li className='nav-item' onClick={this.handleToggleSharing}>
                        <a data-toggle='tab' href=''
                           role='tab' aria-expanded='false'>
                            <span>{!this.state.disableSharing ? 'Disable sharing' : 'Enable sharing'}</span>
                        </a>
                    </li>
                </ul>
            </div>
        )

        let postAvatar = <UserAvatarComponent fullName={this.props.ownerDisplayName!} fileName={this.props.ownerAvatar!} size={36} />

        let author = (
            <div className={classes.author}>
        <span style={{
            fontSize: '14px',
            paddingRight: '10px',
            fontWeight: 400,
            color: 'rgba(0,0,0,0.87)',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            lineHeight: '25px'
        }}>{this.props.ownerDisplayName}</span><span style={{
                fontWeight: 400,
                fontSize: '10px'
            }}> | {translate!('post.public')}</span>
            </div>
        )
        const footerAction = (
            <div className={'add-options-message'}>
                <a href={'javascript:(0)'} className='options-message' data-toggle='tooltip' data-placement='top'
                   data-original-title='ADD PHOTOS'>
                    <svg className='olymp-camera-icon' data-toggle='modal' data-target='#update-header-photo'>
                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-camera-icon'/>
                    </svg>
                </a>

                <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handleConferencePost}
                    disabled={this.state.disabledPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
            </div>
        )
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose} modalTitle={'Conference'}
                    modalFooterAction={footerAction} action={rightIconMenu} modalClassName={''}>
                <CardHeader
                    title={author}
                    avatar={postAvatar}>
                </CardHeader>
                <div className='modal-body'>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Conference title<span>*</span></label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               title='title'
                               type='text'/>
                        <span style={{color: 'red'}}>
                            {this.state.nameError.trim() ? this.state.nameError : ''}
                        </span>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Conference topic<span>*</span></label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               title='topic'
                               type='text'/>
                        <span style={{color: 'red'}}>
                            {this.state.topicError.trim() ? this.state.topicError : ''}
                        </span>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>URL</label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               title='url'
                               type='text'/>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Role in the Conference<span>*</span></label>
                        <input className='form-control'
                               onChange={this.handleOnChange}
                               title='role'
                               type='text'/>
                        <span style={{color: 'red'}}>
                            {this.state.roleError.trim() ? this.state.roleError : ''}
                        </span>
                    </div>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Description</label>
                        <textarea className='form-control'
                                  onChange={this.handleOnChange}
                                  title='description'
                                  defaultValue={''}/>
                    </div>
                    <Button
                    className={'btn btn-indigo btn-md-2'}
                    disableFocusRipple={true}
                    disableRipple={true}
                    onClick={this.handleConferencePost}
                    disabled={this.state.disabledPost}
                >
                    {this.props.edit ? translate!('post.updateButton') : translate!('post.postButton')}
                </Button>
              </div>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object} props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IConferenceWriteComponentProps) => {
    return {
        post: (post: PostConference, callBack: Function) => dispatch(postActions.dbAddConferencePost(post, callBack)),
        update: (post: Map<string, any>, callBack: Function) => dispatch(postActions.dbUpdatePost(post, callBack))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IConferenceWriteComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        postImageState: state.getIn(['imageGallery', 'status']),
        ownerAvatar: user.avatar || '',
        ownerDisplayName: user.fullName || ''
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(ConferenceWriteComponent as any) as any)
