export interface IConferenceWriteComponentState {

    /**
     * Post conference text
     */
    postText: string

    /**
     * The URL image of the post
     */
    image?: string

    /**
     * The path identifier of image on the server
     */
    imageFullPath: string

    /**
     * If it's true gallery will be open
     */
    galleryOpen: boolean

    /**
     * If it's true post button will be disabled
     */
    disabledPost: boolean

    /**
     * If it's true comment will be disabled on post
     */
    disableComments: boolean

    /**
     * If it's true share will be disabled on post
     */
    disableSharing: boolean,

    /**
     * Whether menu is open
     */
    menuOpen: boolean

    /**
     * Conference name input
     */
    title: string

    /**
     * Conference name input error
     */
    nameError: string

    /**
     *  Conference Topic input
     */
    topic: string

    /**
     *  Conference Topic input error
     */
    topicError: string

    /**
     *  Conference Role input
     */
    role: string

    /**
     *  Conference Role input error
     */
    roleError: string

    /**
     *  Conference Url input
     */
    url: string

    /**
     *  Conference Description input
     */
    description: string
}
