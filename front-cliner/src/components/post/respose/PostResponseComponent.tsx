// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import {push} from 'react-router-redux'
import PropTypes from 'prop-types'
import moment from 'moment/moment'
import Linkify from 'react-linkify'
import copy from 'copy-to-clipboard'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

// - Material UI
import {Manager, Target, Popper} from 'react-popper'

import reactStringReplace from 'react-string-replace'

// - Import app components
import CommentGroup from 'components/post/comment/commentGroup/index'
import ShareDialog from 'components/shareDialog/index'

// - Import actions
import * as voteActions from 'store/actions/voteActions'
import * as postActions from 'store/actions/postActions'
import * as commentActions from 'store/actions/commentActions'
import * as globalActions from 'store/actions/globalActions'
import {IPostResponseComponentProps} from './IPostResponseComponentProps'
import {IPostResponseComponentState} from './IPostResponseComponentState'

// - Create component class
export class PostResponseComponent extends Component<IPostResponseComponentProps, IPostResponseComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IPostResponseComponentProps) {
        super(props)
        const {post} = props
        this.state = {

            readMoreState: false,
            /**
             * Handle open comment from parent component
             */
            openComments: false,
            /**
             * If it's true, share dialog will be open
             */
            shareOpen: false,
            /**
             * If it's true comment will be disabled on post
             */
            disableComments: post.get('disableComments', false),
            /**
             * If it's true share will be disabled on post
             */
            disableSharing: post.get('disableSharing', false),
            /**
             * Title of share post
             */
            shareTitle: 'Share On',
            /**
             * If it's true, post link will be visible in share post dialog
             */
            openCopyLink: false,
            /**
             * Whether post menu open
             */
            isPostMenuOpen: false
        }
        // Binding functions to this
        this.handleReadMore = this.handleReadMore.bind(this)
        this.getOpenCommentGroup = this.getOpenCommentGroup.bind(this)
        this.handleVote = this.handleVote.bind(this)
        this.handleOpenShare = this.handleOpenShare.bind(this)
        this.handleCloseShare = this.handleCloseShare.bind(this)
        this.handleCopyLink = this.handleCopyLink.bind(this)
        this.handleOpenComments = this.handleOpenComments.bind(this)
    }

    /**
     * Toggle on show/hide comment
     * @param  {event} evt passed by clicking on comment slide show
     */
    handleOpenComments = () => {
        const {getPostComments, commentList, post} = this.props
        const id = post.get('id')
        const ownerUserId = post.get('ownerUserId')
        if (!commentList) {
            getPostComments!(ownerUserId!, id!)
        }
        this.setState({
            openComments: !this.state.openComments
        })
    }

    /**
     * Show copy link
     *
     *
     * @memberof Post
     */
    handleCopyLink = () => {
        const {translate} = this.props
        this.setState({
            openCopyLink: true,
            shareTitle: translate!('post.copyLinkButton')
        })
    }

    /**
     * Open share post
     *
     *
     * @memberof Post
     */
    handleOpenShare = () => {
        const {post} = this.props
        copy(`${location.origin}/${post.get('ownerUserId')}/posts/${post.get('id')}`)
        this.setState({
            shareOpen: true
        })
    }

    /**
     * Close share post
     *
     *
     * @memberof Post
     */
    handleCloseShare = () => {
        this.setState({
            shareOpen: false,
            shareTitle: 'Share On',
            openCopyLink: false
        })
    }

    /**
     * Handle vote on a post
     *
     *
     * @memberof Post
     */
    handleVote = () => {
        if (this.props.currentUserVote) {
            this.props.unvote!()
        } else {
            this.props.vote!()
        }
    }

    /**
     * Set open comment group function on state which passed by CommentGroup component
     * @param  {function} open the function to open comment list
     */
    getOpenCommentGroup = (open: () => void) => {
        this.setState({
            openCommentGroup: open
        })
    }

    /**
     * Handle read more event
     * @param  {event} evt  is the event passed by click on read more
     */
    handleReadMore(event: any) {
        this.setState({
            readMoreState: !this.state.readMoreState

        })
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {post, setHomeTitle, goTo, isPostOwner, commentList, classes, translate} = this.props

        const {
            ownerUserId,
            id,
            disableComments,
            commentCounter,
            disableSharing,
        } = post.toJS()
        // Define variables
        return (
            <>
                <div className='post-additional-info inline-items pb-2'>
                    <a href={'javascript:(0)'} className='post-add-icon inline-items' onClick={this.handleVote}>
                        {this.props.currentUserVote ? (<svg className='olymp-heart-icon' style={{fill: '#ff5e3a'}}>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-heart-icon'/>
                        </svg>) : (<svg className='olymp-heart-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-heart-icon'/>
                        </svg>)}
                        <span>{this.props.voteCount! > 0 ? this.props.voteCount : ''} Like</span>
                    </a>
                    <div className='comments-shared'>
                        {!disableComments ?
                            (<a href={'javascript:(0)'} className='post-add-icon inline-items '
                                onClick={this.handleOpenComments}>
                                <svg className='olymp-speech-balloon-icon'>
                                    <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon'/>
                                </svg>
                                <span>{commentCounter! > 0 ? commentCounter : ''} Comment</span>
                            </a>) : ''}
                        {!disableSharing ? (
                            <a href={'javascript:(0)'} className='post-add-icon inline-items pl-2'
                               onClick={this.handleOpenShare}>
                                <svg className='olymp-share-icon'>
                                    <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-share-icon'/>
                                </svg>
                                <span>Share</span>
                            </a>) : ''}
                    </div>
                </div>
                <div className='control-block-button post-control-button'>
                    <a href={'javascript:(0)'} className='btn btn-control' onClick={this.handleVote}>
                        {this.props.currentUserVote ? (
                            <svg className='olymp-like-post-icon' style={{fill: '#ff5e3a'}}>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-like-post-icon'/>
                            </svg>) : (<svg className='olymp-like-post-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-like-post-icon'/>
                        </svg>)}
                    </a>
                    {!disableComments ?
                        (<a href={'javascript:(0)'} className='btn btn-control' onClick={this.handleOpenComments}>
                            <svg className='olymp-comments-post-icon'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-comments-post-icon'/>
                            </svg>
                        </a>) : ''}
                    {!disableSharing ? (
                        <a href={'javascript:(0)'} className='btn btn-control' onClick={this.handleOpenShare}>
                            <svg className='olymp-share-icon'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-share-icon'/>
                            </svg>
                        </a>) : ''}
                </div>
                <CommentGroup open={this.state.openComments} comments={commentList} ownerPostUserId={ownerUserId!}
                              onToggleRequest={this.handleOpenComments} isPostOwner={this.props.isPostOwner!}
                              disableComments={disableComments!} postId={id}/>
                <ShareDialog
                    onClose={this.handleCloseShare}
                    shareOpen={this.state.shareOpen}
                    onCopyLink={this.handleCopyLink}
                    openCopyLink={this.state.openCopyLink}
                    post={post}

                />
            </>

        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPostResponseComponentProps) => {
    const {post} = ownProps
    return {
        vote: () => dispatch(voteActions.dbAddVote(post.get('id'), post.get('ownerUserId'))),
        unvote: () => dispatch(voteActions.dbDeleteVote(post.get('id'), post.get('ownerUserId'))),
        delete: (id: string) => dispatch(postActions.dbDeletePost(id)),
        toggleDisableComments: (status: boolean) => {
            dispatch(postActions.dbUpdatePost(post.set('disableComments', status), (x: any) => x))
        },
        toggleSharingComments: (status: boolean) => {
            dispatch(postActions.dbUpdatePost(post.set('disableSharing', status), (x: any) => x))
        },
        goTo: (url: string) => dispatch(push(url)),
        setHomeTitle: (title: string) => dispatch(globalActions.setHeaderTitle(title || '')),
        getPostComments: (ownerUserId: string, postId: string) => dispatch(commentActions.dbFetchComments(ownerUserId, postId))

    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPostResponseComponentProps) => {

    const uid = state.getIn(['authorize', 'uid'])
    let currentUserVote = ownProps.post.getIn(['votes', uid], false)
    const voteCount = state.getIn(['post', 'userPosts', ownProps.post.get('ownerUserId'), ownProps.post.get('id'), 'score'], 0)
    const commentList: { [commentId: string]: Comment } = state.getIn(['comment', 'postComments', ownProps.post.get('id')])
    const user = state.getIn(['user', 'info', ownProps.post.get('ownerUserId')])
    return {
        translate: getTranslate(state.get('locale')),
        commentList,
        avatar: user ? user.avatar : '',
        fullName: user ? user.fullName : '',
        voteCount,
        currentUserVote,
        isPostOwner: uid === ownProps.post.get('ownerUserId')
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(PostResponseComponent as any)
