// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
// import ImageModalDown from 'src/components/imageModal/imageModalDown'
import ImageModalSide from 'src/components/imageModal/imageModalSide'

// - Import API

// - Import actions

import {IPhotosItemComponentProps} from './IPhotosItemComponentProps'
import {IPhotosItemComponentState} from './IPhotosItemComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import {Image} from 'core/domain/imageGallery/index'

/**
 * Create component class
 */
export class PhotosItemComponent extends Component<IPhotosItemComponentProps, IPhotosItemComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IPhotosItemComponentProps) {
        super(props)
        // Defaul state
        this.state = {
            isModalOpen: false,
            /**
             * If it's true, Article write will be open
             */
            openImageModal: false,
        }

        this.toggleModal = this.toggleModal.bind(this)
        this.handleOpenImageModal = this.handleOpenImageModal.bind(this)
        this.handleCloseImageModal = this.handleCloseImageModal.bind(this)
        // this.imageBoxs = this.imageBoxs.bind(this)
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    }
    /**
     * Open image modal
     *
     *
     * @memberof StreamComponent
     */
    handleOpenImageModal = () => {
        this.setState({
            openImageModal: true,
        })
    }

    /**
     * Close image modal
     *
     *
     * @memberof StreamComponent
     */
    handleCloseImageModal = () => {
        this.setState({
            openImageModal: false,
        })
    }
    componentDidMount() {
    }
    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <li key={this.props.id} onClick={this.handleOpenImageModal}>
                <img src={this.props.URL} alt='photo'/>
                <ImageModalSide
                    open={this.state.openImageModal}
                    onRequestClose={this.handleCloseImageModal}
                    userId={this.props.userId}
                    URL={this.props.URL}/>
            </li>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPhotosItemComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPhotosItemComponentProps) => {
    const uid = ownProps.userId
    return {

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotosItemComponent as any) as any)