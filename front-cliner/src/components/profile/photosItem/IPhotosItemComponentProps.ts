export interface IPhotosItemComponentProps {

    /**
     * User Id
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    userId: string
    /**
     * Image url
     */
    URL: string
    /**
     * Id
     */
    id: any
}
