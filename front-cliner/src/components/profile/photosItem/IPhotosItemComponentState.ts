export interface IPhotosItemComponentState {
    /**
     * checking modal is open or not
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    isModalOpen: boolean

    /**
     * Image modal is open {true} or not false
     *
     * @type {boolean}
     * @memberof IStreamComponentState
     */
    openImageModal: boolean
}