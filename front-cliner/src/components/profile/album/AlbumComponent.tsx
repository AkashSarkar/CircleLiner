// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getTranslate, getActiveLanguage } from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import { grey } from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, { withOptions } from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import  FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import { withStyles } from '@material-ui/core/styles'

// - Import app components
import ImgCover from 'components/profile/imgCover/index'
import UserAvatarComponent from 'components/userAvatar/index'
import ImageGallery from 'components/imageGallery/index'
import AppDialogTitle from 'layouts/dialogTitle/index'
import AppInput from 'layouts/appInput/index'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import {IAlbumComponentProps} from './IAlbumComponentProps'
import { IAlbumComponentState } from './IAlbumComponentState'
/**
 * Create component class
 */
export class AlbumComponent extends Component<IAlbumComponentProps, IAlbumComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IAlbumComponentProps) {
        super(props)
    }
    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <div className='photo-album-item-wrap col-4-width'>
                <div className='photo-album-item' data-mh='album-item'>
                    <div className='photo-item'>
                        <img src='/img/photo-item2.jpg' alt='photo'/>
                        <div className='overlay overlay-dark'/>
                        <a href={'javascript:(0)'} className='more'>
                            <svg className='olymp-three-dots-icon'>
                                <use
                                    xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                            </svg>
                        </a>
                        <a href={'javascript:(0)'} className='post-add-icon'>
                            <svg className='olymp-heart-icon'>
                                <use
                                    xlinkHref='/svg-icons/sprites/icons.svg#olymp-heart-icon'/>
                            </svg>
                            <span>324</span>
                        </a>
                        <a href={'javascript:(0)'} data-toggle='modal' data-target='#open-photo-popup-v2'
                           className='  full-block'/>
                    </div>
                    <div className='content'>
                        <a href={'javascript:(0)'} className='title h5'>South America Vacations</a>
                        <span className='sub-title'>Last Added: 2 hours ago</span>
                        <div className='swiper-container'>
                            <div className='swiper-wrapper'>
                                <div className='swiper-slide'>
                                    <ul className='friends-harmonic'>
                                        <li>
                                            <a href={'javascript:(0)'}>
                                                <img src='/img/friend-harmonic5.jpg'
                                                     alt='friend'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href={'javascript:(0)'}>
                                                <img src='/img/friend-harmonic10.jpg'
                                                     alt='friend'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href={'javascript:(0)'}>
                                                <img src='/img/friend-harmonic7.jpg'
                                                     alt='friend'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href={'javascript:(0)'}>
                                                <img src='/img/friend-harmonic8.jpg'
                                                     alt='friend'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href={'javascript:(0)'}>
                                                <img src='/img/friend-harmonic2.jpg'
                                                     alt='friend'/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className='swiper-slide'>
                                    <div className='friend-count'
                                         data-swiper-parallax={-500}>
                                        <a href={'javascript:(0)'} className='friend-count-item'>
                                            <div className='h6'>24</div>
                                            <div className='title'>Photos</div>
                                        </a>
                                        <a href={'javascript:(0)'} className='friend-count-item'>
                                            <div className='h6'>86</div>
                                            <div className='title'>Comments</div>
                                        </a>
                                        <a href={'javascript:(0)'} className='friend-count-item'>
                                            <div className='h6'>16</div>
                                            <div className='title'>Share</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            {/* If we need pagination */}
                            <div className='swiper-pagination'/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IAlbumComponentProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IAlbumComponentProps) => {
    return {

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((AlbumComponent as any) as any)
