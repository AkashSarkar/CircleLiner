import {Profile} from 'core/domain/users/index'
import {Image} from 'core/domain/imageGallery/index'
import {List} from 'immutable'

export interface IPhotosProfileComponentProps {

    /**
     * User Id
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    userId: string
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IInfoProfileComponentProps
     */
    info?: Profile
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     */
    currentLanguage?: string

    images?: List<Image>
}
