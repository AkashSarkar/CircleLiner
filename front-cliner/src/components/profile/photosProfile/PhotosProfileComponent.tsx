// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
// import ImageModalDown from 'src/components/imageModal/imageModalDown'
import ImageModalSide from 'src/components/imageModal/imageModalSide'
import PhotosItem from 'src/components/profile/photosItem'

// - Import API

// - Import actions

import {IPhotosProfileComponentProps} from './IPhotosProfileComponentProps'
import {IPhotosProfileComponentState} from './IPhotosProfileComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import {Image} from 'core/domain/imageGallery/index'
import {UserTie} from 'core/domain/circles/userTie'

/**
 * Create component class
 */
export class PhotoProfileComponent extends Component<IPhotosProfileComponentProps, IPhotosProfileComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IPhotosProfileComponentProps) {
        super(props)
        // Defaul state
        this.imageBoxs = this.imageBoxs.bind(this)
    }
    componentDidMount() {
    }

    imageBoxs = () => {
        const photoBoxList: any[] = []
        this.props.images!.map((image: Image, index: any) => {
            console.log('im',image)
            photoBoxList.push(<PhotosItem key={index} id={image.id} userId={image.ownerUserId} URL={image.URL}/>)
        })
        return photoBoxList
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {images, translate, currentLanguage} = this.props
        return (
            <div className='ui-block'>
                <div className='ui-block-title'>
                    <h6 className='title'>Last Photos</h6>
                </div>
                <div className='ui-block-content'>
                    {/* W-Latest-Photo */}
                    <ul className='widget w-last-photo js-zoom-gallery'>
                        {this.imageBoxs()}
                    </ul>
                    {/* .. end W-Latest-Photo */}
                </div>
            </div>

        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPhotosProfileComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPhotosProfileComponentProps) => {
    const uid = ownProps.userId
    return {
        currentLanguage: getActiveLanguage(state.get('locale')).code,
        translate: getTranslate(state.get('locale')),
        info: state.getIn(['user', 'info', uid]),
        images: state.getIn(['imageGallery', 'images']) || [],
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotoProfileComponent as any) as any)