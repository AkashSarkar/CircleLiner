// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

// - Import API

// - Import actions
import * as educationActions from 'store/actions/userActions/educationActions'
import {IEducationItemComponentProps} from './IEducationItemComponentProps'
import {IEducationItemComponentState} from './IEducationItemComponentState'
import {push} from 'react-router-redux'
import {Education} from 'core/domain/users/education'

/**
 * Create component class
 */
export class EducationItemComponent extends Component<IEducationItemComponentProps, IEducationItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: IEducationItemComponentProps) {
        super(props)
        const {userEducation} = props
        this.state = {
            isEdit: false,
            /**
             * education level
             */
            educationLevel: this.props.edit && userEducation ? userEducation.get('educationLevel') : '',
            /**
             * education degree name
             */
            degreeTitle: this.props.edit && userEducation ? userEducation.get('degreeTitle') : '',
            /**
             * education major subject
             */
            group: this.props.edit && userEducation ? userEducation.get('group') : '',
            /**
             * education major subject
             */
            instituteName: this.props.edit && userEducation ? userEducation.get('instituteName') : '',
            /**
             * education major subject
             */
            result: this.props.edit && userEducation ? userEducation.get('result') : '',
            /**
             * education major subject
             */
            passingYear: this.props.edit && userEducation ? userEducation.get('passingYear') : '',
            /**
             * education major subject
             */
            achievement: this.props.edit && userEducation ? userEducation.get('achievement') : '',
            /**
             * education major subject
             */
            duration: this.props.edit && userEducation ? userEducation.get('duration') : '',
        }
    }

    render() {
        const {translate, educationLevel, degreeTitle, group, instituteName, result, passingYear, achievement, duration} = this.props
        return (
                <div className='row'>
                    <div className='col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'>
                        <li>
                            <span className='text'>{instituteName}</span>
                            <span className='title'>{educationLevel}</span>
                            <span className='date'>{degreeTitle}</span>
                        </li>
                    </div>
                </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IEducationItemComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IEducationItemComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((EducationItemComponent as any) as any)
