import {Profession} from 'core/domain/users/profession'
import {UserTie} from 'core/domain/circles/index'
import {Map} from 'immutable'
import {Profile} from 'core/domain/users/index'
import {Education} from 'core/domain/users/education'

export interface IEducationItemComponentProps {
    /**
     * User education post id
     */
    id?: string
    /**
     * education level
     */
    educationLevel?: string
    /**
     * education degree
     */
    degreeTitle?: string
    /**
     * education major subject
     */
    group?: string
    /**
     * institute name
     */
    instituteName?: string
    /**
     * education result
     */
    result?: string
    /**
     * passing year
     */
    passingYear?: string
    /**
     * educational achievements
     */
    achievement?: string
    /**
     * education period
     */
    duration?: string
    /**
     * education id
     */
    educationId: string
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * Post model
     */
    userEducation?: Map<string, any>
    /**
     * Save a post
     *
     * @memberof IPostWriteComponentProps
     */
    education?: (education: Education) => any
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    edit?: boolean
    /**
     * User's banner URL
     *
     * @type {string}
     * @memberof IEditProfileComponentState
     */
    open?: boolean

    /**
     * Update user profile
     *
     * @memberof IEditProfileComponentProps
     */
    update?: (education: Map<string, any>) => any
    /**
     * Styles
     */
    classes?: any
    /**
     * Current locale language
     */
    currentLanguage?: string
    /**
     * match
     */
    match?: any
    /**
     * user id
     */
    userId?: string
    /**
     * delete education by education id
     */
    delete?: (educationId?: string) => any
    /**
     * user authentication
     */
    isAuthed: boolean
}
