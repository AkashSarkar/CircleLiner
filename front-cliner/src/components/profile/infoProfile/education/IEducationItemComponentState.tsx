
export interface IEducationItemComponentState {

    isEdit: boolean
    /**
     * education level
     */
    educationLevel: string
    /**
     * education degree
     */
    degreeTitle: string
    /**
     * education major subject
     */
    group: string
    /**
     * institute name
     */
    instituteName: string
    /**
     * education result
     */
    result: string
    /**
     * passing year
     */
    passingYear: string
    /**
     * educational achievements
     */
    achievement: string
    /**
     * education period
     */
    duration: string
    edit?: boolean
}
