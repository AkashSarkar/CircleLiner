
export interface IInfoProfileComponentState {
  /**
   * Default birth day
   */
  defaultBirthday: any
  /**
   * Web URL
   */
  webUrl: string
  /**
   * User company name
   */
  companyName: string

  /**
   * User twitter id
   */
  twitterId: string

}
