import {Profile} from 'core/domain/users/index'
import {Education} from 'core/domain/users/education'
import {Profession} from 'core/domain/users/profession'
export interface IInfoProfileComponentProps {
    /**
     * User Id
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    userId: string
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IInfoProfileComponentProps
     */
    info?: Profile
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * User professions
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    professions?: Map<string, Profession>
    /**
     * User educations
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    educations?: Map<string, Education>
    /**
     * is current user authenticated
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    isAuthed?: boolean
    /**
     * Current locale language
     */
    currentLanguage?: string

    /**
     * Styles
     */
    classes?: any
}
