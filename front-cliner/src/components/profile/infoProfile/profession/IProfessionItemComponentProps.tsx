import {Profession} from 'core/domain/users/profession'
import {UserTie} from 'core/domain/circles/index'
import {Map} from 'immutable'
import {Education} from 'core/domain/users/education'

export interface IProfessionItemComponentProps {
    /**
     * education id
     */
    professionId: string
    /**
     * User profession post id
     */
    id?: string
    /**
     * user id
     */
    userId?: string
    /**
     * company name
     */
    companyName?: string
    /**
     * company business
     */
    companyBusiness?: string
    /**
     * work position
     */
    designation?: string
    /**
     * work department
     */
    department?: string
    /**
     * working experience
     */
    experience?: string
    /**
     * work responsibilities
     */
    responsibilities?: string
    /**
     * company location
     */
    companyLocation?: string
    /**
     * when start working at this company
     */
    workFrom?: string
    /**
     * when left company
     */
    workTo?: string
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * match
     */
    match?: any
    /**
     * checking request user id with authorize user id
     */
    isAuthed?: boolean
    /**
     * Post model
     */
    userProfession?: Map<string, any>
    /**
     * Save a post
     *
     * @memberof IProfessionItemComponentProps
     */
    professionUpdate?: (profession: Profession) => any
    /**
     * edit profession(boolean)
     */
    edit?: boolean
    /**
     * delete profession by profession id
     */
    delete?: (professionId?: string) => any
}
