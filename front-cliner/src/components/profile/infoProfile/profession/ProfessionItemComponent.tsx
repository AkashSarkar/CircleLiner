// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

// - Import API

// - Import actions
import * as professionActions from 'store/actions/userActions/professionActions'

import {IProfessionItemComponentProps} from './IProfessionItemComponentProps'
import {IProfessionItemComponentState} from './IProfessionItemComponentState'
import {Profession} from 'core/domain/users/profession'

/**
 * Create component class
 */
export class ProfessionItemComponent extends Component<IProfessionItemComponentProps, IProfessionItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: IProfessionItemComponentProps) {
        super(props)
        const {userProfession} = props
        this.state = {
            isEdit: false,
            /**
             * checking for edit and delete show
             */
            isChecked: false,
            /**
             * education level
             */
            companyName: this.props.edit && userProfession ? userProfession.get('companyName') : '',
            /**
             * education degree name
             */
            companyBusiness: this.props.edit && userProfession ? userProfession.get('companyBusiness') : '',
            /**
             * education major subject
             */
            designation: this.props.edit && userProfession ? userProfession.get('designation') : '',
            /**
             * education major subject
             */
            companyLocation: this.props.edit && userProfession ? userProfession.get('companyLocation') : '',
            /**
             * education major subject
             */
            department: this.props.edit && userProfession ? userProfession.get('department') : '',
            /**
             * education major subject
             */
            experience: this.props.edit && userProfession ? userProfession.get('experience') : '',
            /**
             * education major subject
             */
            responsibilities: this.props.edit && userProfession ? userProfession.get('responsibilities') : '',
            /**
             * education major subject
             */
            workFrom: this.props.edit && userProfession ? userProfession.get('workFrom') : '',
            /**
             * education major subject
             */
            workTo: this.props.edit && userProfession ? userProfession.get('workTo') : '',
        }
    }

    render() {
        const {translate,
            companyName,
            designation,
            department,
            workFrom,
            workTo,
        } = this.props

        return (
            <div className='row'>
                <div className='col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'>
                    <li>
                        <span className='title'>{companyName}</span>
                        <span className='text'>{designation}</span>
                        <span className='date'>{workFrom}-{workTo}</span>
                    </li>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IProfessionItemComponentProps) => {
    return {
        professionUpdate: (profession: Profession) => dispatch(professionActions.dbUpdateProfession(profession)),
        delete: (postId: string) => dispatch(professionActions.dbDeleteProfessionInfo(postId)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IProfessionItemComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((ProfessionItemComponent as any) as any)
