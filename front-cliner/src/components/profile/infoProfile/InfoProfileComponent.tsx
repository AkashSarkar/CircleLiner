// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import {Route, Switch, withRouter, Redirect, NavLink} from 'react-router-dom'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import EducationItemComponent from './education/EducationItemComponent'
import ProfessionItemComponent from './profession/ProfessionItemComponent'

// - Import API

// - Import actions

import {IInfoProfileComponentProps} from './IInfoProfileComponentProps'
import {IInfoProfileComponentState} from './IInfoProfileComponentState'
import {Profession} from 'core/domain/users/profession'
import {Education} from 'core/domain/users/education'
import {withStyles} from '@material-ui/core/styles'

const styles = (theme: any) => ({
    edit_hover_class: {
        '&:a': {
            backgroundColor: theme.palette.background.default,
        },
                    width: 100,
                    height: 100,
                    backgroundColor: 'red'
    },
})

/**
 * Create component class
 */
export class InfoProfileComponent extends Component<IInfoProfileComponentProps, IInfoProfileComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IInfoProfileComponentProps) {
        super(props)
        // Defaul state
        this.state = {
            /**
             * Default birth day
             */
            defaultBirthday: (props.info && props.info.birthday) ? moment.unix(props.info!.birthday!).toDate() : '',
            /**
             * Web URL
             */
            webUrl: (props.info && props.info.webUrl) ? props.info.webUrl : '',
            /**
             * User company name
             */
            companyName: (props.info && props.info.companyName) ? props.info.companyName : '',
            /**
             * User twitter id
             */
            twitterId: (props.info && props.info.twitterId) ? props.info.twitterId : ''

        }
        this.educationList = this.educationList.bind(this)
        this.professionList = this.professionList.bind(this)
    }

    professionList = () => {
        const item: any[] = []
        const parsedProfession: Profession[] = []
        this.props.professions!.forEach((profession: Profession) => {
            parsedProfession.push({
                ...Map(profession!).toJS()
            })
        })
        parsedProfession.map((profession: Profession) => {
            item.push(
                <ProfessionItemComponent key={profession.id!}
                                         professionId={profession.id!}
                                         companyName={profession.companyName!}
                                         companyBusiness={profession.companyBusiness!}
                                         designation={profession.designation!}
                                         department={profession.department!}
                                         experience={profession.experience!}
                                         responsibilities={profession.responsibilities!}
                                         companyLocation={profession.companyLocation!}
                                         workFrom={profession.workFrom!}
                                         workTo={profession.workTo!}
                                         isAuthed={this.props.isAuthed!}/>
            )
        })
        return item
    }

    educationList = () => {
        const item: any[] = []
        const parsedEducation: Education[] = []
        this.props.educations!.forEach((education) => {
            parsedEducation.push({
                ...Map(education!).toJS()
            })
        })
        parsedEducation.map((education: Education) => {
            item.push(
                <EducationItemComponent key={education.id!}
                                        isAuthed={this.props.isAuthed!}
                                        educationId={education.id!}
                                        educationLevel={education.educationLevel!}
                                        degreeTitle={education.degreeTitle!}
                                        group={education.group!}
                                        instituteName={education.instituteName!}
                                        result={education.result!}
                                        passingYear={education.passingYear!}
                                        achievement={education.achievement!}
                                        duration={education.duration!}
                />
            )
        })
        return item
    }

    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        const {translate, currentLanguage, classes} = this.props
        const {defaultBirthday, webUrl, twitterId, companyName} = this.state
        return (
            <>
                {/* Left Sidebar */}
                <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Profile Intro</h6>
                    </div>
                    <div className='ui-block-content'>
                        {/* W-Personal-Info */}
                        <ul className='widget w-personal-info item-block'>
                            <div className='more' style={{float: 'right'}}>
                                <NavLink to={`/${this.props.userId}/about`}>
                                    Edit
                                </NavLink>
                            </div>
                            {/*<View overlay="red-strong">*/}
                                {/*<img src="https://mdbootstrap.com/img/Photos/Horizontal/People/6-col/img%20(7).jpg" className="img-fluid" alt="" />*/}
                                {/*<Mask className="flex-center">*/}
                                    {/*<p className="white-text">Strong overlay</p>*/}
                                {/*</Mask>*/}
                            {/*</View>*/}
                            {/*<div className={classes.edit_hover_class}>*/}
                                {/*<NavLink to={`/${this.props.userId}/about`}>*/}
                                    {/*Edit*/}
                                {/*</NavLink>*/}
                                {/*<a href='#'><img src='http://placehold.it/50x50' /></a>*/}
                            {/*</div>*/}
                            <li>
                                <span className='title'>Education</span>
                            </li>
                            {this.educationList()}
                            <li>
                                <span className='title'>Profession</span>
                            </li>
                            {this.professionList()}
                            <li>
                                <span className='title'>Hobbies</span>
                                <span className='text'>{(this.props.info ? this.props.info.twitterId : '')}</span>
                            </li>
                            <hr/>
                            <div className='more' style={{float: 'right'}}>
                                <NavLink to='/settings'>
                                    <span>Edit</span>
                                </NavLink>
                                {/*<svg className='olymp-three-dots-icon'>*/}
                                {/*<use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>*/}
                                {/*</svg>*/}
                                {/*<ul className='more-dropdown'>*/}
                                {/*<NavLink to='/settings'>*/}
                                {/*<span>Edit</span>*/}
                                {/*</NavLink>*/}
                                {/*</ul>*/}
                            </div>
                            <li>
                                <span className='title'>About</span>
                                <span className='text'>{(this.props.info ? this.props.info.tagLine : '')}</span>
                            </li>
                            <li>
                                <span className='title'>Email</span>
                                <span className='text'>{(this.props.info ? this.props.info.webUrl : '')}</span>
                            </li>
                        </ul>
                        {/* .. end W-Personal-Info */}
                    </div>
                </div>
                {/* ... end Left Sidebar */}
            </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IInfoProfileComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IInfoProfileComponentProps) => {
    const userId = ownProps.userId
    const uid = state.getIn(['authorize', 'uid'])
    return {
        currentLanguage: getActiveLanguage(state.get('locale')).code,
        translate: getTranslate(state.get('locale')),
        info: state.getIn(['user', 'info', userId]),
        isAuthed: userId === uid,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(InfoProfileComponent as any) as any)