export interface IBlockPeopleItemComponentProps {

    /**
     * User full name
     *
     * @type {string}
     * @memberof INotifyItemComponentProps
     */
    fullName: string

    /**
     * User avatar
     *
     * @type {string}
     * @memberof INotifyItemComponentProps
     */
    avatar: string

    /**
     * Notifier identifier
     *
     * @type {string}
     * @memberof INotifyItemComponentProps
     */
    userId: string

    /**
     * Delete a notification
     *
     * @memberof INotifyItemComponentProps
     */
    unblock?: (userId: string) => any

    /**
     * Material ui styles
     */
    classes?: any
  }
  