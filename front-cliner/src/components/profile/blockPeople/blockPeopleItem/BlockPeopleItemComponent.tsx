// - Import react components
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { push } from 'react-router-redux'
import { withStyles } from '@material-ui/core/styles'

// - Import app components
import UserAvatar from 'components/userAvatar/index'

// - Import API

// - Import actions
import * as notifyActions from 'store/actions/notifyActions/notifyActions'

import { IBlockPeopleItemComponentProps } from './IBlockPeopleItemComponentProps'
import { IBlockPeopleItemComponentState } from './IBlockPeopleItemComponentState'

/**
 * Create component class
 */
export class BlockPeopleItemComponent extends Component<IBlockPeopleItemComponentProps,IBlockPeopleItemComponentState> {

  static propTypes = {
        /**
         * Notification description
         */
    description: PropTypes.string,
        /**
         * Which user relates to the notification item
         */
    fullName: PropTypes.string,
        /**
         * Avatar of the user who relate to the notification item
         */
    avatar: PropTypes.string,
        /**
         * Notification identifier
         */
    id: PropTypes.string,

        /**
         * Which address notification refers
         */
    url: PropTypes.string,
        /**
         * The notifier user identifier
         */
    notifierUserId: PropTypes.string,
        /**
         * Close notification popover
         */
    closeNotify: PropTypes.func
  }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
  constructor (props: IBlockPeopleItemComponentProps) {
    super(props)

        // Defaul state
    this.state = {
    }

        // Binding functions to `this`

  }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
  render () {
    let { fullName, avatar, classes } = this.props

    return (
        <li>
            <div className='author-thumb'>
                <img src='/img/avatar15-sm.jpg' alt='author'/>
            </div>
            <div className='notification-event'>
                <a href='#' className='h6 notification-friend'>Tamara Romanoff</a>
            </div>
            <span className='notification-icon'>
              <a href='#' className='accept-request'>
                <span className='icon-add'>
                  <svg className='olymp-happy-face-icon'><use
                      xlinkHref='svg-icons/sprites/icons.svg#olymp-happy-face-icon'/></svg>
                </span>
                Unblock
              </a>
            </span>
        </li>
    )
  }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IBlockPeopleItemComponentProps) => {
  return {
    // unblock: (id: string) => dispatch(notifyActions.dbDeleteNotification(id))
  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: any, ownProps: IBlockPeopleItemComponentProps) => {
  return {

  }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((BlockPeopleItemComponent as any) as any )
