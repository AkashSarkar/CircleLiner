import {BlockUser} from 'core/domain/users/blockUser'
import {Map} from 'immutable'

export interface IBlockPeopleComponentProps {

    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * Recieve request close function
     */
    onRequestClose: () => void
    /**
     * Post write style
     */
    style?: {}

    /**
     * Save a post
     *
     * @memberof IPostWriteComponentProps
     */
    blockUser?: (block: BlockUser, callback: Function) => any

    /**
     * Update a post
     *
     * @memberof IPostWriteComponentProps
     */
    update?: (post: Map<string, any>, callback: Function) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    fullName: string

    blockedUserId: string

}
