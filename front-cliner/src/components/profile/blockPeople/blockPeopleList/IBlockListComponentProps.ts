import {Profile} from 'core/domain/users/index'
import {Notification} from 'core/domain/notifications/index'
import {Map} from 'immutable'

export interface IBlockListComponentProps {
    userId: any
    /**
     * Notifications
     */
    notifications?: Map<string, any>

    /**
     * Users' profile
     */
    info?: Map<string, Profile>

    /**
     * Close notification
     */
    onRequestClose: () => void

    /**
     * User notifications popover is opem {true} or not {false}
     */
    open: boolean
    loadNotification?: (page: any, limit: any) => any
    hasMoreNotification?: boolean

    /**
     * Keep element
     */
    anchorEl?: any

    /**
     * Material ui styles
     */
    classes?: any
}
