// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
import {IBlockListComponentProps} from './IBlockListComponentProps'
import {IBlockListComponentState} from './IBlockListComponentState'
import Setting from 'layouts/setting/index'
import {BlockPeopleItemComponent} from 'components/profile/blockPeople/blockPeopleItem/BlockPeopleItemComponent'

export class BlockListComponent extends Component<IBlockListComponentProps, IBlockListComponentState> {

    constructor(props: IBlockListComponentProps) {
        super(props)

    }

    render() {
        return (
            <Setting userId={this.props.userId}>
                <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Blocked People List</h6>
                    </div>
                    {/* Notification List Frien Requests */}
                    <ul className='notification-list friend-requests'>
                        <BlockPeopleItemComponent fullName='' avatar='' userId=''/>
                    </ul>
                    {/* ... end Notification List Frien Requests */}
                </div>
            </Setting>

        )
    }
}

const mapDispatchToProps = (dispatch: any, ownProps: IBlockListComponentProps) => {
    return {
       // loadNotification: (page: number, limit: number) => dispatch(notifyActions.dbGetPaginatedNotifications(page, limit))

    }
}

const mapStateToProps = (state: Map<string, any>, ownProps: IBlockListComponentProps) => {
    const {userId} = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
        userId: userId,
        info: state.getIn(['user', 'info'])
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((BlockListComponent as any) as any)
