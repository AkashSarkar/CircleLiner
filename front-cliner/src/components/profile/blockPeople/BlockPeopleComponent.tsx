// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
// import Datepicker from 'react-day-picker/moment'
// import moment from 'moment'

import {Card, CardActions, CardHeader, CardMedia, CardContent} from '@material-ui/core'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Paper from '@material-ui/core/Paper'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import TextField from '@material-ui/core/TextField'
import Tooltip from '@material-ui/core/Tooltip'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import SvgRemoveImage from '@material-ui/icons/RemoveCircle'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import {withStyles} from '@material-ui/core/styles'
import {Manager, Target, Popper} from 'react-popper'
import Grow from '@material-ui/core/Grow'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import classNames from 'classnames'

// - Import app components
import ImageGallery from 'components/imageGallery/index'
import Img from 'components/img/index'
import UserAvatarComponent from 'components/userAvatar/index'
import {IBlockPeopleComponentProps} from './IBlockPeopleComponentProps'
import {IBlockPeopleComponentState} from './IBlockPeopleComponentState'
import {BlockUser} from 'core/domain/users/blockUser'
// - Import API
import * as PostAPI from 'api/PostAPI'

// - Import actions
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
import * as userActions from 'store/actions/userActions/userActions'
import Grid from '@material-ui/core/Grid/Grid'

// - Import Layouts
import LModal from 'layouts/modal/index'

const styles = (theme: any) => ({
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0,
            overflowY: 'auto'
        }
    },
    backdrop: {
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: '-1',
        position: 'fixed',
        willChange: 'opacity',
        backgroundColor: 'rgba(251, 249, 249, 0.5)',
        WebkitTapHighlightColor: 'transparent'
    },
    content: {
        padding: 0,
        paddingTop: 0
    },
    dialogRoot: {
        paddingTop: 0
    },
    popperOpen: {
        zIndex: 10
    },
    popperClose: {
        pointerEvents: 'none',
        zIndex: 0
    },
    author: {
        paddingRight: 70
    }
})

// - Create PostWrite component class
export class BlockPeopleComponent extends Component<IBlockPeopleComponentProps, IBlockPeopleComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    styles = {
        avatar: {
            border: '2px solid rgb(255, 255, 255)'
        },
        paper: {
            width: '90%',
            height: '100%',
            margin: '0 auto',
            display: 'block'
        },
        title: {
            padding: '24px 24px 20px 24px',
            font: '500 20px Roboto,RobotoDraft,Helvetica,Arial,sans-serif',
            display: 'flex',
            wordWrap: 'break-word',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            flexGrow: 1
        },
        actions: {
            display: 'flex',
            justifyContent: 'flex-end',
            padding: '24px 24px 20px'
        },
        updateButton: {
            marginLeft: '10px'
        },
        dialogGallery: {
            width: '',
            maxWidth: '530px',
            borderRadius: '4px'
        },
        iconButtonSmall: {},
        iconButton: {}

    }

    constructor(props: IBlockPeopleComponentProps) {

        super(props)

        // Binding functions to `this`
        this.handleBlock = this.handleBlock.bind(this)

    }

    /**
     * Handle send status post to the server
     * @param  {event} evt passed by clicking on the post button
     */
    handleBlock = () => {
        const {
            onRequestClose,
            blockUser,
            blockedUserId,
        } = this.props
        blockUser!({
            blockedUserId: blockedUserId,
        }, onRequestClose)
    }

    componentWillReceiveProps(nextProps: IBlockPeopleComponentProps) {
        if (!nextProps.open) {

        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {classes, translate} = this.props
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose}
                    modalTitle={`Block ${this.props.fullName}`}>
                <div style={this.styles.title as any}>Sure you want to block {this.props.fullName}?</div>
                <div className={classes.bottomPaperSpace}></div>
                <Button onClick={this.props.onRequestClose}> {translate!('profile.cancelButton')} </Button>
                <Button variant='raised'
                        color='primary'
                        style={this.styles.updateButton}
                        onClick={this.handleBlock}
                >Confirm</Button>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IBlockPeopleComponentProps) => {
    return {
        blockUser: (block: BlockUser, callBack: Function) => dispatch(userActions.dbBlockPeople(block, callBack)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IBlockPeopleComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
    }
}
// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(BlockPeopleComponent as any) as any)