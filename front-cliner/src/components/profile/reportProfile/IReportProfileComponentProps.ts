import {ReportUser} from 'core/domain/users/reportUser'
import {Profile} from 'core/domain/users/index'

export interface IReportProfileComponentProps {

    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    info?: Profile

    /**
     * User full name
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    fullName?: string
    /**
     * report Model
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    reportModel?: Map<string, any>

    /**
     * Edit profile dialog is open {true} or not {false}
     *
     * @type {boolean}
     * @memberof IEditProfileComponentProps
     */
    open?: boolean

    /**
     * post report
     *
     * @memberof IEditProfileComponentProps
     */
    reportUser?: (report: ReportUser,  callback: Function) => any

    /**
     * Update user profile
     *
     * @memberof IEditProfileComponentProps
     */
    update?: (report: ReportUser) => any

    /**
     * On edit profile dialog close event
     *
     * @memberof IEditProfileComponentProps
     */
    onRequestClose: () => void

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     */
    currentLanguage?: string

    reportedUserId: string

}
