// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import EventListener, {withOptions} from 'react-event-listener'
import {Map} from 'immutable'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import AppDialogTitle from 'layouts/dialogTitle/index'
import {ReportUser} from 'core/domain/users/reportUser'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import * as globalActions from 'store/actions/globalActions'
import * as imageGalleryActions from 'store/actions/imageGalleryActions'

import {IReportProfileComponentProps} from './IReportProfileComponentProps'
import {IReportProfileComponentState} from './IReportProfileComponentState'
import {Profile} from 'core/domain/users/index'
// - Import Layouts
import LModal from 'layouts/modal/index'

const styles = (theme: any) => ({
    dialogTitle: {
        padding: 0
    },
    dialogContentRoot: {
        maxHeight: 400,
        minWidth: 330,
        [theme.breakpoints.down('xs')]: {
            maxHeight: '100%',
        }

    },
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0
        }
    },
    fixedDownStickyXS: {
        [theme.breakpoints.down('xs')]: {
            position: 'fixed',
            bottom: 0,
            right: 0,
            background: 'white',
            width: '100%'
        }
    },
    bottomPaperSpace: {
        height: 16,
        [theme.breakpoints.down('xs')]: {
            height: 90
        }
    },
    box: {
        padding: '0px 24px 0px',
        display: 'flex'

    },
    bottomTextSpace: {
        marginBottom: 15
    },
    dayPicker: {
        width: '100%',
        padding: '13px 0px 8px'
    }
})

/**
 * Create component class
 */
export class ReportProfileComponent extends Component<IReportProfileComponentProps, IReportProfileComponentState> {
    //
    // static propTypes = {
    //     fullName: PropTypes.string.isRequired
    // }

    styles = {
        avatar: {
            border: '2px solid rgb(255, 255, 255)'
        },
        paper: {
            width: '90%',
            height: '100%',
            margin: '0 auto',
            display: 'block'
        },
        title: {
            padding: '24px 24px 20px 24px',
            font: '500 20px Roboto,RobotoDraft,Helvetica,Arial,sans-serif',
            display: 'flex',
            wordWrap: 'break-word',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            flexGrow: 1
        },
        actions: {
            display: 'flex',
            justifyContent: 'flex-end',
            padding: '24px 24px 20px'
        },
        updateButton: {
            marginLeft: '10px'
        },
        dialogGallery: {
            width: '',
            maxWidth: '530px',
            borderRadius: '4px'
        },
        iconButtonSmall: {},
        iconButton: {}

    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IReportProfileComponentProps) {
        super(props)
        const {reportModel} = props
        // Defaul state
        this.state = {
            isOther: false,
            /**
             * If it's true the winow is in small size
             */
            isSmall: false,
            /**
             * User full name input value
             */
            reportText: '',
            /**
             * Confirm button disable true
             */
            disabledButton: true
        }

        // Binding functions to `this`
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleForm = this.handleForm.bind(this)

    }

    handleReportInputChange = (event: any) => {
        const data = event.target.value
        this.setState({reportText: data})
        if (data.length === 0 || data.trim() === '') {
            this.setState({
                reportText: data,
                disabledButton: true
            })
        } else {
            this.setState({
                reportText: data,
                disabledButton: false
            })
        }
    }
    /**
     * Handle data on input change
     * @param  {event} evt is an event of inputs of element on change
     */
    handleInputChange = (event: any) => {
        const value = event.target.value
        console.log(value)
        switch (value) {
            case '0':
                this.setState({
                    reportText: `Report something shared by ${this.props.fullName}`,
                    isOther: false,
                    disabledButton: false
                })
                break
            case '1':
                this.setState({
                    reportText: `Report this profile ${this.props.fullName}`,
                    isOther: false,
                    disabledButton: false,
                })
                break
            case '2':
                this.setState({
                    reportText: `I want to help ${this.props.fullName}`,
                    isOther: false,
                    disabledButton: false
                })
                break
            case '3':
                this.setState({
                    reportText: this.state.reportText,
                    isOther: true,
                    disabledButton: true
                })
                break
            default:
        }
    }

    handleForm = () => {
        console.log(this.state.reportText)
        const {
            onRequestClose,
            reportUser,
            reportedUserId,
        } = this.props

        const {
            reportText
        } = this.state

        reportUser!({
            reportedUserId: reportedUserId,
            reportText: reportText,
        }, onRequestClose)
    }
    componentWillReceiveProps(nextProps: IReportProfileComponentProps) {
        if (!nextProps.open) {

        }
    }

    /**
     * Handle resize event for window to change sidebar status
     * @param  {any} event is the event is passed by winodw resize event
     */
    handleResize = (event: any) => {

        // Set initial state
        let width = window.innerWidth

        if (width > 900) {
            this.setState({
                isSmall: false
            })

        } else {
            this.setState({
                isSmall: true
            })
        }
    }

    componentDidMount() {
        this.handleResize(null)
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {

        const {classes, translate, currentLanguage} = this.props

        return (
            <LModal open={this.props.open!} onClose={this.props.onRequestClose} modalTitle={`Report about ${this.props.fullName!}`}>
                <div>
                    <div className=''>
                        <span>
                              {/*<div className='radio'>*/}
                              {/*<label>*/}
                                {/*<input type='radio'*/}
                                       {/*value={0}*/}
                                       {/*onChange={this.handleInputChange}*/}
                                       {/*name='report'/>*/}
                                   {/*<span className='circle'/>*/}
                                  {/*<span className='check'/>*/}
                                  {/*Report something shared by {this.props.fullName!}*/}
                              {/*</label>*/}
                            {/*</div>*/}
                            <input type='radio'
                                   onChange={this.handleInputChange}
                                   value={0}
                                   name='report'/>
                            <span>Report something shared by {this.props.fullName!}</span>
                            <input type='radio'
                                   onChange={this.handleInputChange}
                                   value={1}
                                   name='report'/>
                            <span>Report this profile {this.props.fullName}</span>
                            <input type='radio'
                                   onChange={this.handleInputChange}
                                   value={2}
                                   name='report'/>
                            <span>I want to help {this.props.fullName!}</span>
                             <input type='radio'
                                    onChange={this.handleInputChange}
                                    value={3}
                                    name='report'/>
                            <span>Others</span>
                        </span>
                    </div>
                </div>
                {this.state.isOther ? <div>
                    <div className={classes.box}>
                        <TextField
                            className={classes.bottomTextSpace}
                            label='What is the report about..'
                            onChange={this.handleReportInputChange}
                            name='reportText'
                            fullWidth
                        />
                    </div>
                </div> : ''}
                <Button onClick={this.props.onRequestClose}> {translate!('profile.cancelButton')} </Button>
                <Button variant='raised' color='primary'
                        disabled={this.state.disabledButton}
                        style={this.styles.updateButton}
                        onClick={this.handleForm}> Confirm
                </Button>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IReportProfileComponentProps) => {
    return {
        reportUser: (report: ReportUser, callBack: Function) => dispatch(userActions.dbReportProfile(report, callBack)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IReportProfileComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])

    return {
        translate: getTranslate(state.get('locale'))
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(ReportProfileComponent as any) as any)
