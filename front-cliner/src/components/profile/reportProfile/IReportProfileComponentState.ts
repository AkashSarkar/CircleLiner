
export interface IReportProfileComponentState {
  /**
   * Report text input value
   *
   * @type {string}
   * @memberof IEditProfileComponentState
   */
  reportText: string

  /**
   * Edit profile page is small size {true} or big {false}
   *
   * @type {boolean}
   * @memberof IEditProfileComponentState
   */
  isSmall: boolean

  /**
   * If it's true post button will be disabled
   */
  disabledButton: boolean

  isOther: boolean
}
