import { Industry } from 'core/domain/users/industry'
import {Map} from 'immutable'

export interface IIndustryComponentProps {

    /**
     * Open check
     *
     * @memberof IIndustryComponentProps
     */
    open?: boolean

    /**
     * industry model
     */
    industry?: Map<string, any>

    /**
     * User's industry list
     *
     * @type {string}
     * @memberof IIndustryComponentProps
     */
    industries?: Map<string, Industry>

    /**
     * add
     *
     * @memberof IIndustryComponentProps
     */
    add?: (indsutry: Industry) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof IIndustryComponentProps
     */
    currentLanguage?: string

    /**
     * show industry
     *
     * @memberof IIndustryComponentProps
     */
    industryName?: string

    /**
     * show industry
     *
     * @memberof IIndustryComponentProps
     */
    concentratedIndustryName?: string

    /**
     * Router match
     *
     * @type {*}
     * @memberof IIndustryComponentProps
     */
    match: any

    /**
     * Authed user check
     *
     * @memberof IIndustryComponentProps
     */
    authed?: boolean

    /**
     * On request close function
     *
     * @memberof IIndustryComponentProps
     */
    onRequestClose: () => void

    /**
     * url user id
     */
    userId?: string
  }
