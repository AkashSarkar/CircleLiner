// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import IndustryFormComponent from './industryForm/index'
import {IIndustryComponentProps} from './IIndustryComponentProps'
import {IIndustryComponentState} from './IIndustryComponentState'

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'

// - Import layouts
import About from 'layouts/about/index'
import {Industry} from 'core/domain/users/index'

/**
 * Create component class
 */
export class IndustryComponent extends Component<IIndustryComponentProps, IIndustryComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IIndustryComponentProps) {
        super(props)
        this.state = {
            /**
             * User's industry
             */
            industryName: '',

            /**
             * User's Concentrated industry
             */
            concentratedIndustryName: '',
             }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })
         console.log(name, value)
        switch (name) {
            case 'industryName':
                this.setState({
                    industryName: value
                })
                break
            case 'concentratedIndustryName':
                this.setState({
                    concentratedIndustryName: value
                })
                break
            default:
        }
    }

    /**
     * Handle register form
     */
    handleForm = () => {
        const {
            industryName,
            concentratedIndustryName,
        } = this.state
        const {
            add,
        } = this.props
        add!({
            industryName: industryName,
            concentratedIndustryName: concentratedIndustryName,
        })
    }

    industryList = () => {
        const item: any[] = []
        const parsedIndustry: Industry[] = []
        this.props.industries!.forEach((industry) => {
            parsedIndustry.push({
                ...Map(industry!).toJS()
            })
        })
        parsedIndustry.map((industry: Industry) => {
            item.push(
                <IndustryFormComponent key={industry.id!}
                              industryId={industry.id!}
                              industryName={industry.industryName!}
                              concentratedIndustryName={industry.concentratedIndustryName!}
                />
            )
        })
        return item
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <About userId={this.props.userId}>
               <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Industry</h6>
                    </div>
                       {this.industryList()}
                   {this.props.industries ? '' : <div className='ui-block-content'>
                    <div className='row'>
                        <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Industry</label>
                                <input className='form-control'
                                       name='industryName'
                                       onChange={this.handleInputChange}
                                       type='text'
                                       defaultValue=''/>
                                <span className='material-input'/></div>
                        </div>

                        <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className='form-group label-floating'>
                                <label className='control-label'>concentrated Industry</label>
                                <input className='form-control'
                                       name='concentratedIndustryName'
                                       onChange={this.handleInputChange}
                                       type='text'
                                       defaultValue=''/>
                                <span className='material-input'/></div>
                        </div>
                        <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                            <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                               onClick={this.handleForm}>Save</a>
                        </div>
                    </div>
                </div>}
              </div>
            </About>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IIndustryComponentProps) => {
    return {
        add: (add: Industry) => dispatch(userActions.dbAddUserIndustry(add)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IIndustryComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const industries = state.getIn(['user', 'industry',uid])
    const {userId} = ownProps.match.params
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        userId: userId,
        isAuthed: userId === uid,
        industries: industries ? industries : [],
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((IndustryComponent as any) as any)
