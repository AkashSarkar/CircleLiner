import { Industry } from 'core/domain/users/industry'
import {Map} from 'immutable'

export interface IIndustryFormComponentProps {
    /**
     * Open check
     *
     * @memberof IIndustryComponentProps
     */
    open?: boolean

    /**
     * industry model
     */
    industry?: Map<string, any>

    /**
     * Update
     *
     * @memberof IIndustryComponentProps
     */
    update?: (indsutry: Industry) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof IIndustryComponentProps
     */
    currentLanguage?: string

    /**
     * show industry
     *
     * @memberof IIndustryComponentProps
     */
    industryName?: string

    /**
     * show industry
     *
     * @memberof IIndustryComponentProps
     */
    concentratedIndustryName?: string

    /**
     * Authed user check
     *
     * @memberof IIndustryComponentProps
     */
    authed?: boolean

    /**
     * url user id
     */
    userId?: string

    /**
     * url user id
     */
    industryId?: string
  }
