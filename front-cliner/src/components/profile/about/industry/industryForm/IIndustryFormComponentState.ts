
export interface IIndustryFormComponentState {

    /**
     * phone number input value
     *
     * @type {string}
     * @memberof IIndustryComponentState
     */
    industryName: string

    /**
     * Birthplace input value
     *
     * @type {string}
     * @memberof IIndustryComponentState
     */
    concentratedIndustryName: string
}
