// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import {IIndustryFormComponentProps} from './IIndustryFormComponentProps'
import {IIndustryFormComponentState} from './IIndustryFormComponentState'

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'

// - Import layouts
import About from 'layouts/about/index'
import {Industry} from 'core/domain/users/index'

/**
 * Create component class
 */
export class IndustryFormComponent extends Component<IIndustryFormComponentProps, IIndustryFormComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IIndustryFormComponentProps) {
        super(props)
        const {industry} = props
        this.state = {
            /**
             * User's industry
             */
            industryName: industry ? industry.get('industryName', '') : '',

            /**
             * User's Concentrated industry
             */
            concentratedIndustryName: industry ? industry.get('concentratedIndustryName', '') : '',
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    componentWillReceiveProps(nextProps: IIndustryFormComponentProps) {
        if (!nextProps.open) {
            const {industry} = this.props
            this.setState({
                /**
                 * User's industry
                 */
                industryName: industry ? industry.get('industryName', '') : '',

                /**
                 * User's Concentrated industry
                 */
                concentratedIndustryName: industry ? industry.get('concentratedIndustryName', '') : '',
            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'industryName':
                this.setState({
                    industryName: value
                })
                break
            case 'concentratedIndustryName':
                this.setState({
                    concentratedIndustryName: value
                })
                break
       default:
        }
    }

    /**
     * Handle register form
     */
    handleForm = (evt: any, industryId?: string) => {
        const {
            industryName,
            concentratedIndustryName,
        } = this.state
        const {
            update,
        } = this.props
        update!({
            id: industryId,
            industryName: industryName,
            concentratedIndustryName: concentratedIndustryName,
        })
    }

    render() {
      return(
        <div className='ui-block-content'>
            <div className='row'>
                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                    <div className='form-group label-floating'>
                        <label className='control-label'>Industry</label>
                        <input className='form-control'
                               name='industryName'
                               onChange={this.handleInputChange}
                               type='text'
                               defaultValue={this.props.industryName!}/>
                        <span className='material-input'/></div>
                </div>

                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                    <div className='form-group label-floating'>
                        <label className='control-label'>concentrated Industry</label>
                        <input className='form-control'
                               name='concentratedIndustryName'
                               onChange={this.handleInputChange}
                               type='text'
                               defaultValue={this.props.concentratedIndustryName!}/>
                        <span className='material-input'/></div>
                </div>
                <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                    <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                       onClick={(evt: any) => this.handleForm(evt, this.props.industryId)}>Update</a>
                </div>
            </div>
        </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IIndustryFormComponentProps) => {
    return {
        update: (industry: Industry) => dispatch(userActions.dbUpdateUserIndustry(industry)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IIndustryFormComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const industries = state.getIn(['user', 'industry',uid])
    // const {userId} = ownProps.match.params
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        // userId: userId,
        // isAuthed: userId === uid,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((IndustryFormComponent as any) as any)
