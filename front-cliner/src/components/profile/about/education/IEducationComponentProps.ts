import {Profile} from 'core/domain/users/index'
import {Education} from 'core/domain/users/education'

export interface IEducationComponentProps {
    /**
     * education model
     */
    userEducation?: Map<string, any>
    /**
     * Save a post
     *
     * @memberof IPostWriteComponentProps
     */
    education?: (education: Education) => any
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    edit?: boolean
    /**
     * Edit profile dialog is open {true} or not {false}
     *
     * @type {boolean}
     * @memberof IEditProfileComponentProps
     */
    open?: boolean
    /**
     * Update user profile
     *
     * @memberof IEditProfileComponentProps
     */
    update?: (education: Map<string, any>) => any
    /**
     * On edit profile dialog close event
     *
     * @memberof IEditProfileComponentProps
     */
    onRequestClose?: () => void
    /**
     * Styles
     */
    classes?: any
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * Current locale language
     */
    currentLanguage?: string
    /**
     * match
     */
    match?: any
    /**
     * url user id
     */
    userId?: string
    /**
     * load current user education
     */
    getEducationData?: () => any
    /**
     * is current user authenticated
     */
    isAuthed?: boolean
}
