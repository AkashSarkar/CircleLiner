// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
// - Import app components

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import {IEducationListComponentProps} from './IEducationListComponentProps'
import {IEducationListComponentState} from './IEducationListComponentState'
import {Education} from 'core/domain/users/education'
import EducationItemComponent from '../educationListItem/EducationItemComponent'
import {Profession} from 'core/domain/users/profession'
import {Comment} from 'core/domain/comments/index'
import * as PostAPI from 'api/PostAPI'

// import {Image} from 'core/domain/imageGallery'

/**
 * Create component class
 */
export class EducationListComponent extends Component<IEducationListComponentProps, IEducationListComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IEducationListComponentProps) {
        super(props)
        this.state = {}

        this.educationList = this.educationList.bind(this)
    }

    educationList = () => {
        const item: any[] = []
        let parsedEducation: Education[] = []
        // parsedEducation = this.props.educations!.toJS()
        // console.log('educ',parsedEducation)
        this.props.educations!.forEach((education) => {
            parsedEducation.push({
                ...Map(education!).toJS()
            })
        })
        parsedEducation.map((education: Education) => {
            item.push(
                <EducationItemComponent key={education.id!}
                                        isAuthed={this.props.isAuthed!}
                                        educationId={education.id!}
                                        educationLevel={education.educationLevel!}
                                        degreeTitle={education.degreeTitle!}
                                        group={education.group!}
                                        instituteName={education.instituteName!}
                                        result={education.result!}
                                        passingYear={education.passingYear!}
                                        achievement={education.achievement!}
                                        duration={education.duration!}
                />
            )
        })
        return item
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <div className='ui-block'>
                <div className='ui-block-title'>
                    <h6 className='title'>Educational Acknowledgment</h6>
                    <a href='#' className='more'>
                        <svg className='olymp-three-dots-icon'>
                            <use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                        </svg>
                    </a>
                </div>
                <div className='ui-block-content'>
                    <div className='row'>
                        <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                            <ul className='widget w-personal-info item-block'>
                                {this.educationList()}
                                <hr/>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IEducationListComponentProps) => {
    return {
        loadPeople: (page: number, limit: number) => dispatch(userActions.dbGetPeopleInfo(page, limit))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IEducationListComponentProps) => {
    const education = state.getIn(['user', 'education', ownProps.userId])
    console.log(education)
    return {
        translate: getTranslate(state.get('locale')),
        educations: education ? education : []
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((EducationListComponent as any) as any)
