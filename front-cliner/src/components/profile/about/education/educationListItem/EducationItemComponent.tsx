// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import ImgCover from 'components/profile/imgCover/index'
import UserAvatar from 'components/userAvatar/index'
import ImageGallery from 'components/imageGallery/index'
import AppDialogTitle from 'layouts/dialogTitle/index'
import AppInput from 'layouts/appInput/index'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as educationActions from 'store/actions/userActions/educationActions'
import {IEducationItemComponentProps} from './IEducationItemComponentProps'
import {IEducationItemComponentState} from './IEducationItemComponentState'
import {push} from 'react-router-redux'
import {Education} from 'core/domain/users/education'

/**
 * Create component class
 */
export class EducationItemComponent extends Component<IEducationItemComponentProps, IEducationItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: IEducationItemComponentProps) {
        super(props)
        const {userEducation} = props
        this.state = {
            isEdit: false,
            /**
             * education level
             */
            educationLevel: this.props.edit && userEducation ? userEducation.get('educationLevel') : '',
            /**
             * education degree name
             */
            degreeTitle: this.props.edit && userEducation ? userEducation.get('degreeTitle') : '',
            /**
             * education major subject
             */
            group: this.props.edit && userEducation ? userEducation.get('group') : '',
            /**
             * education major subject
             */
            instituteName: this.props.edit && userEducation ? userEducation.get('instituteName') : '',
            /**
             * education major subject
             */
            result: this.props.edit && userEducation ? userEducation.get('result') : '',
            /**
             * education major subject
             */
            passingYear: this.props.edit && userEducation ? userEducation.get('passingYear') : '',
            /**
             * education major subject
             */
            achievement: this.props.edit && userEducation ? userEducation.get('achievement') : '',
            /**
             * education major subject
             */
            duration: this.props.edit && userEducation ? userEducation.get('duration') : '',
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleForm = this.handleForm.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'educationLevel':
                this.setState({
                    educationLevel: value,
                })
                break
            case 'degreeTitle':
                this.setState({
                    degreeTitle: value,
                })
                break
            case 'group':
                this.setState({
                    group: value
                })
                break
            case 'instituteName':
                this.setState({
                    instituteName: value
                })
                break
            case 'result':
                this.setState({
                    result: value
                })
                break
            case 'passingYear':
                this.setState({
                    passingYear: value
                })
                break
            case 'achievement':
                this.setState({
                    achievement: value
                })
                break
            case 'duration':
                this.setState({
                    duration: value
                })
                break
            default:

        }
    }

    handleEdit = () => {
        this.setState({
            isEdit: true
        })
    }
    handleEditCancel = () => {
        this.setState({
            isEdit: false
        })
    }
    /**
     * Handle register form
     */
    handleForm = (evt: any, educationId?: string) => {
        const {
            educationLevel,
            degreeTitle,
            group,
            instituteName,
            result,
            passingYear,
            achievement,
            duration,
        } = this.state

        const {
            education,
        } = this.props
        education!({
            id: educationId,
            educationLevel: educationLevel,
            degreeTitle: degreeTitle,
            group: group,
            instituteName: instituteName,
            result: result,
            passingYear: passingYear,
            achievement: achievement,
            duration: duration,
        })
        this.setState({
            isEdit: false
        })
    }
    handleDelete = (evt: any, educationId?: string) => {
        this.props.delete!(educationId)
    }

    render() {
        const {translate, educationLevel, degreeTitle, group, instituteName, result, passingYear, achievement, duration} = this.props
        return (
            <>{this.state.isEdit ?
                <div className='ui-block-content'>
                    <form>
                        <div className='row'>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Level of Education *</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleInputChange}
                                           name='educationLevel'
                                           defaultValue={educationLevel}/>
                                    <span className='material-input'/>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Exam/Degree Title *</label>
                                    <input className='form-control'
                                           name={'degreeTitle'}
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={degreeTitle}/>
                                    <span className='material-input'/>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Concentration/Major/Group *</label>
                                    <input className='control-label'
                                           name={'group'}
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={group}/>
                                    <span className='material-input'/>
                                </div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Institute Name *</label>
                                    <input className='form-control'
                                           name={'instituteName'}
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={instituteName}/>
                                    <span className='material-input'/>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Result</label>
                                    <input className='form-control'
                                           name={'result'}
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={result}/>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Year of Passing *</label>
                                    <input className='form-control'
                                           name={'passingYear'}
                                           onChange={this.handleInputChange}
                                           defaultValue={passingYear}
                                           type='text'/>
                                    <span className='material-input'/>
                                </div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Achievement</label>
                                    <input className='form-control'
                                           name={'achievement'}
                                           onChange={this.handleInputChange}
                                           defaultValue={achievement}
                                           type='text'/>
                                </div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Duration</label>
                                    <input className='form-control'
                                           name={'duration'}
                                           onChange={this.handleInputChange}
                                           defaultValue={duration}
                                           type='text'/>
                                </div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-6 col-6'>
                                <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                   onClick={this.handleEditCancel}
                                >Cancel</a>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-6 col-6'>
                                <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                   onClick={(evt: any) => this.handleForm(evt, this.props.educationId)}
                                >Update</a>
                            </div>
                        </div>
                    </form>
                </div> :
                <div className='row'>
                    <div className='col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'>
                        {this.props.isAuthed ? <li onDoubleClick={this.handleEdit}>
                            <span className='text'>{instituteName}</span>
                            <span className='title'>{educationLevel}</span>
                            <span className='date'>{degreeTitle}</span>
                        </li> : <li>
                            <span className='text'>{instituteName}</span>
                            <span className='title'>{educationLevel}</span>
                            <span className='date'>{degreeTitle}</span>
                        </li>}
                    </div>
                    {this.props.isAuthed ? <div className='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12'>
                        <div className='more'>
                            <svg className='olymp-three-dots-icon'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                            </svg>
                            <ul className='more-dropdown'>
                                <li><a href={'javascript:(0)'} onClick={this.handleEdit}>Edit</a></li>
                                <li><a href={'javascript:(0)'}
                                       onClick={(evt: any) => this.handleDelete(evt, this.props.educationId)}>Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div> : ''}
                </div>
            }
            </>

        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IEducationItemComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url)),
        education: (education: Education) => dispatch(educationActions.dbUpdateEducation(education)),
        delete: (postId: string) => dispatch(educationActions.dbDeleteEducationInfo(postId)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IEducationItemComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((EducationItemComponent as any) as any)
