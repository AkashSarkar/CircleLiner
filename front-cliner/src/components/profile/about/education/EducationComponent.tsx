// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import {Education} from 'core/domain/users/education'
import EducationListComponent from './educationList/EducationListComponent'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as educationActions from 'store/actions/userActions/educationActions'
import {IEducationComponentProps} from './IEducationComponentProps'
import {IEducationComponentState} from './IEducationComponentState'

// - Import layouts
import About from 'layouts/about/index'

/**
 * Create component class
 */
export class SettingsProfessionComponent extends Component<IEducationComponentProps, IEducationComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IEducationComponentProps) {
        super(props)
        const {userEducation} = props
        this.state = {
            /**
             * education level
             */
            educationLevel: this.props.edit && userEducation ? userEducation.get('educationLevel') : '',
            /**
             * education degree name
             */
            degreeTitle: this.props.edit && userEducation ? userEducation.get('degreeTitle') : '',
            /**
             * education major subject
             */
            group: this.props.edit && userEducation ? userEducation.get('group') : '',
            /**
             * education major subject
             */
            instituteName: this.props.edit && userEducation ? userEducation.get('instituteName') : '',
            /**
             * education major subject
             */
            result: this.props.edit && userEducation ? userEducation.get('result') : '',
            /**
             * education major subject
             */
            passingYear: this.props.edit && userEducation ? userEducation.get('passingYear') : '',
            /**
             * education major subject
             */
            achievement: this.props.edit && userEducation ? userEducation.get('achievement') : '',
            /**
             * education major subject
             */
            duration: this.props.edit && userEducation ? userEducation.get('duration') : '',
            /**
             * education title input error
             */
            educationLevelInputError: '',
            /**
             *  education time input error
             */
            degreeTitleInputError: '',
            /**
             * education major subject error
             */
            groupInputError: '',
            /**
             * education instituteName error
             */
            instituteNameInputError: '',
            /**
             * education passing year error
             */
            passingYearInputError: '',
        }
        // Binding function to `this`
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleForm = this.handleForm.bind(this)
    }

    componentWillReceiveProps(nextProps: IEducationComponentProps) {
        if (!nextProps.open) {
            const {userEducation} = this.props
            this.setState({
                /**
                 * education level
                 */
                educationLevel: this.props.edit && userEducation ? userEducation.get('educationLevel') : '',
                /**
                 * education degree name
                 */
                degreeTitle: this.props.edit && userEducation ? userEducation.get('degreeTitle') : '',
                /**
                 * education major subject
                 */
                group: this.props.edit && userEducation ? userEducation.get('group') : '',
                /**
                 * education major subject
                 */
                instituteName: this.props.edit && userEducation ? userEducation.get('instituteName') : '',
                /**
                 * education major subject
                 */
                result: this.props.edit && userEducation ? userEducation.get('result') : '',
                /**
                 * education major subject
                 */
                passingYear: this.props.edit && userEducation ? userEducation.get('passingYear') : '',
                /**
                 * education major subject
                 */
                achievement: this.props.edit && userEducation ? userEducation.get('achievement') : '',
                /**
                 * education major subject
                 */
                duration: this.props.edit && userEducation ? userEducation.get('duration') : '',
                /**
                 * education title input error
                 */
                educationLevelInputError: '',
                /**
                 *  education time input error
                 */
                degreeTitleInputError: '',
                /**
                 * education major subject error
                 */
                groupInputError: '',
                /**
                 * education instituteName error
                 */
                instituteNameInputError: '',
                /**
                 * education passing year error
                 */
                passingYearInputError: '',
            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'educationLevel':
                this.setState({
                    educationLevel: value,
                    educationLevelInputError: '',
                })
                break
            case 'degreeTitle':
                this.setState({
                    degreeTitle: value,
                    degreeTitleInputError: '',
                })
                break
            case 'group':
                this.setState({
                    group: value,
                    groupInputError: '',
                })
                break
            case 'instituteName':
                this.setState({
                    instituteName: value,
                    instituteNameInputError: '',
                })
                break
            case 'result':
                this.setState({
                    result: value
                })
                break
            case 'passingYear':
                this.setState({
                    passingYear: value,
                    passingYearInputError: '',
                })
                break
            case 'achievement':
                this.setState({
                    achievement: value
                })
                break
            case 'duration':
                this.setState({
                    duration: value
                })
                break
            default:

        }
    }

    /**
     * Handle register form
     */
    handleForm = () => {
        const {translate} = this.props
        let error = false
        if (this.state.educationLevel === '') {
            this.setState({
                educationLevelInputError: 'This field is required'
            })
            error = true

        } else if (this.state.degreeTitle === '') {
            this.setState({
                degreeTitleInputError: 'This field is required'
            })
            error = true

        }  else if (this.state.group === '') {
            this.setState({
                groupInputError: 'This field is required'
            })
            error = true

        }  else if (this.state.instituteName === '') {
            this.setState({
                instituteNameInputError: 'This field is required'
            })
            error = true
        } else if (this.state.passingYear === '') {
            this.setState({
                passingYearInputError: 'This field is required'
            })
            error = true
        }

        const {
            educationLevel,
            degreeTitle,
            group,
            instituteName,
            result,
            passingYear,
            achievement,
            duration,
        } = this.state

        const {
            education,
        } = this.props
        if (!error) {
            education!({
                educationLevel: educationLevel,
                degreeTitle: degreeTitle,
                group: group,
                instituteName: instituteName,
                result: result,
                passingYear: passingYear,
                achievement: achievement,
                duration: duration,
            })
        }
    }

    componentWillMount() {
        const {getEducationData} = this.props
        getEducationData!()
    }
    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <About userId={this.props.userId!}>
                <EducationListComponent userId={this.props.userId!} isAuthed={this.props.isAuthed!} />
                {this.props.isAuthed ?
                            <div className='ui-block'>
                                <div className='ui-block-title'>
                                    <h6 className='title'>Your Education</h6>
                                </div>
                                <div className='ui-block-content'>
                                    {/* Educational Information Form  */}
                                    <form>
                                        <div className='row'>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Level of Education *</label>
                                                    <input className='form-control'
                                                           type='text'
                                                           onChange={this.handleInputChange}
                                                           name='educationLevel'
                                                           defaultValue=''/>
                                                    <span className='material-input'/>
                                                    <span style={{ color: 'red' }}>
                                                      {this.state.educationLevelInputError.trim() ? this.state.educationLevelInputError : ''}
                                                  </span>
                                                </div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Exam/Degree Title *</label>
                                                    <input className='form-control'
                                                           name={'degreeTitle'}
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue={''} />
                                                    <span className='material-input'/>
                                                    <span style={{ color: 'red' }}>
                                                    {this.state.degreeTitleInputError.trim() ? this.state.degreeTitleInputError : ''}
                                                    </span>
                                                </div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Concentration/Major/Group *</label>
                                                    <input className='control-label'
                                                           name={'group'}
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue={''} />
                                                    <span className='material-input'/>
                                                    <span style={{ color: 'red' }}>
                                                    {this.state.groupInputError.trim() ? this.state.groupInputError : ''}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Institute Name *</label>
                                                    <input className='form-control'
                                                           name={'instituteName'}
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue={''} />
                                                    <span className='material-input'/>
                                                    <span style={{ color: 'red' }}>
                                                    {this.state.instituteNameInputError.trim() ? this.state.instituteNameInputError : ''}
                                                    </span>
                                                </div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Result</label>
                                                    <input className='form-control'
                                                           name={'result'}
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue={''} />
                                                </div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Year of Passing *</label>
                                                    <input className='form-control'
                                                           name={'passingYear'}
                                                           onChange={this.handleInputChange}
                                                           defaultValue={''}
                                                           type='text' />
                                                    <span className='material-input'/>
                                                    <span style={{ color: 'red' }}>
                                                    {this.state.passingYearInputError.trim() ? this.state.passingYearInputError : ''}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Achievement</label>
                                                    <input className='form-control'
                                                           name={'achievement'}
                                                           onChange={this.handleInputChange}
                                                           defaultValue={''}
                                                           type='text'/>
                                                </div>
                                            </div>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Duration</label>
                                                    <input className='form-control'
                                                           name={'duration'}
                                                           onChange={this.handleInputChange}
                                                           defaultValue={''}
                                                           type='text' />
                                                </div>
                                            </div>
                                            <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                                <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                                   onClick={this.handleForm}>Save</a>
                                            </div>
                                        </div>
                                    </form>
                                    {/* ... end Educational Information Form  */}
                                </div>
                            </div>
                    : ''}
            </About>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IEducationComponentProps) => {
    const {userId} = ownProps.match.params
    return {
        education: (education: Education, callBack: Function) => dispatch(educationActions.dbAddEducationalInfo(education, callBack)),
        getEducationData: () =>  dispatch(educationActions.dbGetUsersEducationInfoById(userId))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IEducationComponentProps) => {
    const {userId} = ownProps.match.params
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
        userId: userId,
        isAuthed: userId === uid,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((SettingsProfessionComponent as any) as any)
