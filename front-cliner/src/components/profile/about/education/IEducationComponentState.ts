export interface IEducationComponentState {
    /**
     * education level
     */
    educationLevel: string
    /**
     * education degree
     */
    degreeTitle: string
    /**
     * education major subject
     */
    group: string
    /**
     * institute name
     */
    instituteName: string
    /**
     * education result
     */
    result: string
    /**
     * passing year
     */
    passingYear: string
    /**
     * educational achievements
     */
    achievement: string
    /**
     * education period
     */
    duration: string
    /**
     * education edit
     */
    edit?: boolean
    /**
     * education title input error
     */
    educationLevelInputError: string
    /**
     * education time input error
     */
    degreeTitleInputError: string
    /**
     * education major subject error
     */
    groupInputError: string
    /**
     * education instituteName error
     */
    instituteNameInputError: string
    /**
     * education passing year error
     */
    passingYearInputError: string
}
