import { Profile } from 'core/domain/users/index'

export interface IAboutSidebarComponentProps {
  /**
   * Styles
   */
  classes?: any
  /**
   * Translate to locale string
   */
  translate?: (state: any) => any
  /**
   * Current locale language
   */
  currentLanguage?: string
  /**
   * user id
   */
   userId?: string
  /**
   * match
   */
   match?: any
}
