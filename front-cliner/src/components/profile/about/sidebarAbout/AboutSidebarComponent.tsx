// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import {NavLink} from 'react-router-dom'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Paper from '@material-ui/core/Paper'
import { withStyles } from '@material-ui/core/styles'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import DraftsIcon from '@material-ui/icons/Drafts'
import SendIcon from '@material-ui/icons/Send'

// - Import app components

// - Import API

// - Import actions

import {IAboutSidebarComponentProps} from './IAboutSidebarComponentProps'
import {IAboutSidebarComponentState} from './IAboutSidebarComponentState'

const styles = (theme: any) => ({
    menuItem: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
    },
    primary: {},
    icon: {},
})
/**
 * Create component class
 */
export class AboutSidebarComponent extends Component<IAboutSidebarComponentProps, IAboutSidebarComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IAboutSidebarComponentProps) {
        super(props)
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <div className='ui-block'>
                <div className='your-profile'>
                    <div aria-multiselectable='true'>
                        <div className='card'>
                            <div className='collapse show' aria-labelledby='headingOne'>
                                <ul className='your-profile-menu'>
                                    <li>
                                        <NavLink to={`/${this.props.userId}/about`}>
                                            <span>Profession</span>
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to={`/${this.props.userId}/about/interests`}>
                                        <span>Hobbies and
                                        Interests</span>
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to={`/${this.props.userId}/about/education`}>
                                            <span>Education</span>
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to={`/${this.props.userId}/about/industry`}>
                                            <span>Industry</span>
                                        </NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IAboutSidebarComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IAboutSidebarComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(AboutSidebarComponent as any) as any)
