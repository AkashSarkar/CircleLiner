// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getTranslate, getActiveLanguage } from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import { grey } from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, { withOptions } from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import  FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import { withStyles } from '@material-ui/core/styles'

// - Import app components

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import * as globalActions from 'store/actions/globalActions'
import * as imageGalleryActions from 'store/actions/imageGalleryActions'
// - Import props and state
import { IInerestComponentProps } from './IInerestComponentProps'
import { IInterestComponentState } from './IInterestComponentState'
// - Import layouts
import About from 'layouts/about/index'
/**
 * Create component class
 */
export class InterestComponent extends Component<IInerestComponentProps, IInterestComponentState> {

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props: IInerestComponentProps) {
    super(props)
  }

  render() {
    const { classes, translate, currentLanguage } = this.props
    return (
        <About userId={this.props.userId}>
          <div className='ui-block'>
              <div className='ui-block-title'>
                  <h6 className='title'>Hobbies and Interests</h6>
              </div>
              <div className='ui-block-content'>
                  {/* Form Hobbies and Interests */}
                  <form>
                      <div className='row'>
                          <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Hobbies</label>
                                  <textarea className='form-control' defaultValue={'I like to ride the bike to work, swimming, and working out. I also like reading design magazines, go to museums, and binge watching a good tv show while it’s raining outside.\n\t\t\t\t\t\t\t\t\t\t\t\t\t'} />
                                  <span className='material-input' /></div>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Favourite TV Shows</label>
                                  <textarea className='form-control' defaultValue={'Breaking Good, RedDevil, People of Interest, The Running Dead, Found,  American Guy.'} />
                                  <span className='material-input' /></div>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Favourite Movies</label>
                                  <textarea className='form-control' defaultValue={'Idiocratic, The Scarred Wizard and the Fire Crown,  Crime Squad, Ferrum Man. '} />
                                  <span className='material-input' /></div>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Favourite Games</label>
                                  <textarea className='form-control' defaultValue={'The First of Us, Assassin’s Squad, Dark Assylum, NMAK16, Last Cause 4, Grand Snatch Auto. '} />
                                  <span className='material-input' /></div>
                              <button className='btn btn-secondary btn-lg full-width'>Cancel</button>
                          </div>
                          <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Favourite Music Bands / Artists</label>
                                  <textarea className='form-control' defaultValue={'Iron Maid, DC/AC, Megablow, The Ill, Kung Fighters, System of a Revenge.'} />
                                  <span className='material-input' /></div>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Favourite Books</label>
                                  <textarea className='form-control' defaultValue={'The Crime of the Century, Egiptian Mythology 101, The Scarred Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air and Water.'} />
                                  <span className='material-input' /></div>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Favourite Writers</label>
                                  <textarea className='form-control' defaultValue={'Martin T. Georgeston, Jhonathan R. Token, Ivana Rowle, Alexandria Platt, Marcus Roth. '} />
                                  <span className='material-input' /></div>
                              <div className='form-group label-floating'>
                                  <label className='control-label'>Other Interests</label>
                                  <textarea className='form-control' defaultValue={'Swimming, Surfing, Scuba Diving, Anime, Photography, Tattoos, Street Art.'} />
                                  <span className='material-input' /></div>
                              <button className='btn btn-primary btn-lg full-width'>Save all Changes</button>
                          </div>
                      </div>
                  </form>
                  {/* ... end Form Hobbies and Interests */}
              </div>
          </div>

        </About>
    )
  }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IInerestComponentProps) => {
  return {
  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IInerestComponentProps) => {
    const {userId} = ownProps.match.params
  return {
      translate: getTranslate(state.get('locale')),
      userId: userId
  }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((InterestComponent as any) as any)
