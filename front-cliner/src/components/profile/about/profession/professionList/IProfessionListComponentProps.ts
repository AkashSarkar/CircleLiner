import { Profile } from 'core/domain/users/index'
import {Map} from 'immutable'
import {Profession} from 'core/domain/users/profession'
export interface IProfessionListComponentProps {
    /**
     * checking request user id with authorize user id
     */
     isAuthed?: boolean
    /**
     * company name
     */
    companyName?: string
    /**
     * company business
     */
    companyBusiness?: string
    /**
     * work position
     */
    designation?: string
    /**
     * work department
     */
    department?: string
    /**
     * working experience
     */
    experience?: string
    /**
     * work responsibilities
     */
    responsibilities?: string
    /**
     * company location
     */
    companyLocation?: string
    /**
     * when start working at this company
     */
    workFrom?: string
    /**
     * when left company
     */
    workTo?: string
    /**
     * uid
     *
     * @type {Profile}
     * @memberof IProfessionListComponentProps
     */
     userId: string

    /**
     * profession id
     *
     * @type {Profile}
     * @memberof IProfessionListComponentProps
     */
     professionId?: string
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IProfessionListComponentProps
     */
     professions?: Map<string, Profession>
    /**
     * style
     */
     classes?: any

    /**
     * Translate to locale string
     */
     translate?: (state: any) => any

    /**
     * Current locale language
     */
     currentLanguage?: string
}
