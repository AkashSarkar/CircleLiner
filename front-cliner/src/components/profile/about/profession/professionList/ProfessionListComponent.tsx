// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
// - Import app components

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import * as professionActions from 'store/actions/userActions/professionActions'
import {IProfessionListComponentProps} from './IProfessionListComponentProps'
import {IProfessionListComponentState} from './IProfessionListComponentState'
import ProfessionItemComponent from '../professionListItem/ProfessionItemComponent'
import {Profession} from 'core/domain/users/profession'
import {Image} from 'core/domain/imageGallery/index'
import {UserTie} from 'core/domain/circles/userTie'

/**
 * Create component class
 */
export class PeopleSugessionComponent extends Component<IProfessionListComponentProps, IProfessionListComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IProfessionListComponentProps) {
        super(props)
        this.state = {}

        this.professionList = this.professionList.bind(this)
    }

    professionList = () => {
        const item: any[] = []
        const parsedProfession: Profession[] = []
        this.props.professions!.forEach((profession) => {
            parsedProfession.push({
                ...Map(profession!).toJS()
            })
        })
        parsedProfession.map((profession: Profession) => {
            item.push(
                            <ProfessionItemComponent key={profession.id!}
                                                     professionId={profession.id!}
                                                     companyName={profession.companyName!}
                                                     companyBusiness={profession.companyBusiness!}
                                                     designation={profession.designation!}
                                                     department={profession.department!}
                                                     experience={profession.experience!}
                                                     responsibilities={profession.responsibilities!}
                                                     companyLocation={profession.companyLocation!}
                                                     workFrom={profession.workFrom!}
                                                     workTo={profession.workTo!}
                                                     isAuthed={this.props.isAuthed!}/>
            )
        })
        return item
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <div className='ui-block'>
                <div className='ui-block-title'>
                    <h6 className='title'>Professional Acknowledgment</h6>
                    <a href='#' className='more'>
                        <svg className='olymp-three-dots-icon'>
                            <use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                        </svg>
                    </a>
                </div>
                <div className='ui-block-content'>
                    <div className='row'>
                        <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                            <ul className='widget w-personal-info item-block'>
                                {this.professionList()}
                                <hr/>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IProfessionListComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IProfessionListComponentProps) => {
    const profession = state.getIn(['user', 'profession', ownProps.userId])
    const uid = state.getIn(['authorize', 'uid'], 0)
    return {
        translate: getTranslate(state.get('locale')),
        professions: profession ? profession : [],
        isAuthed: ownProps.userId === uid
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PeopleSugessionComponent as any) as any)
