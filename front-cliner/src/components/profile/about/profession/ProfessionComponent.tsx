// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import ProfessionList from './professionList/ProfessionListComponent'

// - Import API

// - Import actions
import * as professionActions from 'store/actions/userActions/professionActions'

import {IProfessionComponentProps} from './IProfessionComponentProps'
import {IProfessionComponentState} from './IProfessionComponentState'
import {Profession} from 'core/domain/users/profession'

// - Import layouts
import About from 'layouts/about/index'
import {IEducationComponentProps} from 'components/profile/about/education/IEducationComponentProps'

/**
 * Create component class
 */
export class ProfessionComponent extends Component<IProfessionComponentProps, IProfessionComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IProfessionComponentProps) {
        super(props)
        const {professionModel} = props

        this.state = {
            /**
             * checking for edit and delete show
             */
            isChecked: false,
            /**
             * company name
             */
            companyName: this.props.edit && professionModel ? professionModel.get('companyName', '') : '',
            /**
             * company business
             */
            companyBusiness: this.props.edit && professionModel ? professionModel.get('companyBusiness', '') : '',
            /**
             * work position
             */
            designation: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
            /**
             * work department
             */
            department: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
            /**
             * working experience
             */
            experience: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
            /**
             * work responsibilities
             */
            responsibilities: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
            /**
             * company location
             */
            companyLocation: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
            /**
             * when start working at this company
             */
            workFrom: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
            /**
             * work description
             */
            workTo: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
            /**
             * company name error
             */
            companyNameInputError: '',
            /**
             * designation error
             */
            designationInputError: '',
            /**
             * experience error
             */
            experienceInputError: '',
            /**
             * workFrom error
             */
            workFromInputError: '',
            /**
             * workTo error
             */
            workToInputError: '',
        }

        // Binding functions to `this`
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleProfessionForm = this.handleProfessionForm.bind(this)
        // this.toggleForm = this.toggleForm.bind(this)
    }
    componentWillReceiveProps(nextProps: IProfessionComponentProps) {
        if (!nextProps.open) {
            const {professionModel} = this.props
            this.setState({
                /**
                 * checking for edit and delete show
                 */
                isChecked: false,
                /**
                 * company name
                 */
                companyName: this.props.edit && professionModel ? professionModel.get('companyName', '') : '',
                /**
                 * company business
                 */
                companyBusiness: this.props.edit && professionModel ? professionModel.get('companyBusiness', '') : '',
                /**
                 * work position
                 */
                designation: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
                /**
                 * work department
                 */
                department: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
                /**
                 * working experience
                 */
                experience: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
                /**
                 * work responsibilities
                 */
                responsibilities: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
                /**
                 * company location
                 */
                companyLocation: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
                /**
                 * when start working at this company
                 */
                workFrom: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
                /**
                 * work description
                 */
                workTo: this.props.edit && professionModel ? professionModel.get('designation', '') : '',
                /**
                 * company name error
                 */
                companyNameInputError: '',
                /**
                 * designation error
                 */
                designationInputError: '',
                /**
                 * experience error
                 */
                experienceInputError: '',
                /**
                 * workFrom error
                 */
                workFromInputError: '',
                /**
                 * workTo error
                 */
                workToInputError: '',
            })
        }
        // Binding functions to `this`
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleProfessionForm = this.handleProfessionForm.bind(this)
        // this.toggleForm = this.toggleForm.bind(this)
    }

    // toggleForm = () => {
    //     this.setState({isChecked: !this.state.isChecked})
    // }

    handleOnChange(event: any) {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name
        this.setState({
            [name]: value
        })
        switch (name) {
            case 'companyName':
                this.setState({
                    companyName: value,
                    companyNameInputError: ''
                })
                break
            case 'companyBusiness':
                this.setState({
                    companyBusiness: value,
                })
                break
            case 'designation':
                this.setState({
                    designation: value,
                    designationInputError: '',
                })
                break
            case 'department':
                this.setState({
                    department: value
                })
                break
            case 'experience':
                this.setState({
                    experience: value,
                    experienceInputError: '',
                })
                break
            case 'responsibilities':
                this.setState({
                    responsibilities: value
                })
                break
            case 'companyLocation':
                this.setState({
                    companyLocation: value
                })
                break
            case 'workFrom':
                this.setState({
                    workFrom: value,
                    workFromInputError: '',
                })
                break
            case 'workTo':
                this.setState({
                    workTo: value,
                    workToInputError: '',
                })
                break
            case 'currentlyWorking':
                if (value) {
                    this.setState({
                        isChecked: true,
                        workTo: 'continuing',
                        workToInputError: '',
                    })
                } else {
                    this.setState({
                        isChecked: false,
                        workTo: '',
                        workToInputError: '',
                    })
                }
                break
            default:
        }
    }

    handleProfessionForm(event: any) {
        let error = false
        if (this.state.companyName.trim() === '') {
            this.setState({
                companyNameInputError: 'This field is required.',
            })
            error = true

        } else if (this.state.designation.trim() === '') {
            this.setState({
                designationInputError: 'This field is required.',
            })
            error = true

        } else if (this.state.experience.trim() === '') {
            this.setState({
                experienceInputError: 'This field is required.',
            })
            error = true

        } else if (this.state.workFrom.trim() === '') {
            this.setState({
                workFromInputError: 'This field is required.',
            })
            error = true

        } else if (this.state.workTo.trim() === '') {
            this.setState({
                workToInputError: 'This field is required.',
            })
            error = true

        }
        const {
            companyName,
            companyBusiness,
            designation,
            department,
            experience,
            responsibilities,
            companyLocation,
            workFrom,
            workTo,
        } = this.state

        const {
            id,
            edit,
            postProfession,
            updateProfession,
            professionModel
        } = this.props
        console.log(error)
        if (!error) {
            postProfession!({
                companyName: companyName,
                companyBusiness: companyBusiness,
                designation: designation,
                department: department,
                experience: experience,
                responsibilities: responsibilities,
                companyLocation: companyLocation,
                workFrom: workFrom,
                workTo: workTo,
            })
        }
    }
    componentWillMount() {
        const {getProfessionData} = this.props
        getProfessionData!()
    }
    render() {
        const {classes, translate} = this.props
        return (
            <About userId={this.props.userId!}>
                <ProfessionList
                    userId={this.props.userId!}
                    isAuthed={this.props.isAuthed!}
                /> {this.props.isAuthed ? <div className='ui-block'>
                <div className='ui-block-title'>
                    <h6 className='title'>Your Profession</h6>
                </div>
                <div className='ui-block-content'>
                    <form>
                        <div className='row'>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Company Name *</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='companyName'
                                           defaultValue=''/>
                                    <span className='material-input'/>
                                    <span style={{ color: 'red' }}>
                                            {this.state.companyNameInputError.trim() ? this.state.companyNameInputError : ''}
                                     </span>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Company Business</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='companyBusiness'
                                           defaultValue=''/>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Designation *</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='designation'
                                           defaultValue=''/>
                                    <span className='material-input'/>
                                    <span style={{ color: 'red' }}>
                                            {this.state.designationInputError.trim() ? this.state.designationInputError : ''}
                                     </span>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Department</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='department'
                                           defaultValue=''/>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Area of Expertise *</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='experience'
                                           defaultValue=''/>
                                    <span className='material-input'/>
                                    <span style={{ color: 'red' }}>
                                            {this.state.experienceInputError.trim() ? this.state.experienceInputError : ''}
                                     </span>
                                </div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Responsibilities</label>
                                    <textarea className='form-control'
                                              onChange={this.handleOnChange}
                                              name='responsibilities'
                                              defaultValue=''>

                                        </textarea>
                                </div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Company Location</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='companyLocation'
                                           defaultValue=''/>
                                </div>
                                <label className='control-label'>Employment Period *</label>
                                <div className='row'>
                                    <div className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <label className='control-label'>From</label>
                                        <input className='form-control'
                                               type='text'
                                               onChange={this.handleOnChange}
                                               name='workFrom'
                                               defaultValue=''/>
                                        <span className='material-input'/>
                                        <span style={{ color: 'red' }}>
                                            {this.state.workFromInputError.trim() ? this.state.workFromInputError : ''}
                                             </span>
                                    </div>

                                    {this.state.isChecked ? <div className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <div>Continuing</div>
                                        <span className='material-input'/>
                                        <span style={{ color: 'red' }}>
                                            {this.state.workToInputError.trim() ? this.state.workToInputError : ''}
                                            </span>
                                    </div> : <div className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <label className='control-label'>To</label>
                                        <input className='form-control'
                                               type='text'
                                               onChange={this.handleOnChange}
                                               name='workTo'
                                               defaultValue=''/>
                                        <span className='material-input'/>
                                        <span style={{ color: 'red' }}>
                                            {this.state.workToInputError.trim() ? this.state.workToInputError : ''}
                                            </span>
                                    </div>}
                                </div>
                                <div className='checkbox'>
                                    <label>
                                        <input
                                            onChange={this.handleOnChange}
                                            name='currentlyWorking'
                                            type='checkbox'/>
                                        <span className='checkbox-material'>
                                            <span className='check'/></span>
                                        I currently work here.
                                    </label>
                                </div>
                            </div>

                            <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                   onClick={this.handleProfessionForm}>Save</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div> : ''}

            </About>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IProfessionComponentProps) => {
    const {userId} = ownProps.match.params
    return {
        postProfession: (postProfession: Profession) => dispatch(professionActions.dbAddProfession(postProfession)),
        getProfessionData: () => dispatch(professionActions.dbGetUserProfessionByUserId(userId))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IProfessionComponentProps) => {
    const {userId} = ownProps.match.params
    const uid = state.getIn(['authorize', 'uid'])
    return {
        userId: userId,
        translate: getTranslate(state.get('locale')),
        isAuthed: userId === uid
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((ProfessionComponent as any) as any)
