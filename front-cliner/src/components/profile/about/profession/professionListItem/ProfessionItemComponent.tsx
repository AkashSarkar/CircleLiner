// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import ImgCover from 'components/profile/imgCover/index'
import UserAvatar from 'components/userAvatar/index'
import ImageGallery from 'components/imageGallery/index'
import AppDialogTitle from 'layouts/dialogTitle/index'
import AppInput from 'layouts/appInput/index'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as professionActions from 'store/actions/userActions/professionActions'

import {IProfessionItemComponentProps} from './IProfessionItemComponentProps'
import {IProfessionItemComponentState} from './IProfessionItemComponentState'
import {Profession} from 'core/domain/users/profession'
import {push} from 'react-router-redux'
import * as educationActions from 'store/actions/userActions/educationActions'
import {Education} from 'core/domain/users/education'

/**
 * Create component class
 */
export class ProfessionItemComponent extends Component<IProfessionItemComponentProps, IProfessionItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: IProfessionItemComponentProps) {
        super(props)
        const {userProfession} = props
        this.state = {
            isEdit: false,
            /**
             * checking for edit and delete show
             */
            isChecked: false,
            /**
             * education level
             */
            companyName: this.props.edit && userProfession ? userProfession.get('companyName') : '',
            /**
             * education degree name
             */
            companyBusiness: this.props.edit && userProfession ? userProfession.get('companyBusiness') : '',
            /**
             * education major subject
             */
            designation: this.props.edit && userProfession ? userProfession.get('designation') : '',
            /**
             * education major subject
             */
            companyLocation: this.props.edit && userProfession ? userProfession.get('companyLocation') : '',
            /**
             * education major subject
             */
            department: this.props.edit && userProfession ? userProfession.get('department') : '',
            /**
             * education major subject
             */
            experience: this.props.edit && userProfession ? userProfession.get('experience') : '',
            /**
             * education major subject
             */
            responsibilities: this.props.edit && userProfession ? userProfession.get('responsibilities') : '',
            /**
             * education major subject
             */
            workFrom: this.props.edit && userProfession ? userProfession.get('workFrom') : '',
            /**
             * education major subject
             */
            workTo: this.props.edit && userProfession ? userProfession.get('workTo') : '',
        }
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleForm = this.handleForm.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
    }

    handleOnChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'companyName':
                this.setState({
                    companyName: value,
                })
                break
            case 'companyBusiness':
                this.setState({
                    companyBusiness: value,
                })
                break
            case 'designation':
                this.setState({
                    designation: value
                })
                break
            case 'companyLocation':
                this.setState({
                    companyLocation: value
                })
                break
            case 'department':
                this.setState({
                    department: value
                })
                break
            case 'experience':
                this.setState({
                    experience: value
                })
                break
            case 'responsibilities':
                this.setState({
                    responsibilities: value
                })
                break
            case 'workFrom':
                this.setState({
                    workFrom: value
                })
                break
            case 'workTo':
                this.setState({
                    workTo: value
                })
                break
            case 'currentlyWorking':
                if (value) {
                    this.setState({
                        isChecked: true,
                        workTo: 'continuing',
                    })
                } else {
                    this.setState({
                        isChecked: false,
                        workTo: '',
                    })
                }
                break
            default:

        }
    }

    handleEdit = () => {
        this.setState({
            isEdit: true
        })
    }
    handleEditCancel = () => {
        this.setState({
            isEdit: false
        })
    }
    /**
     * Handle register form
     */
    handleForm = (evt: any, professionId?: string) => {
        const {
            companyName,
            companyBusiness,
            designation,
            companyLocation,
            department,
            experience,
            responsibilities,
            workFrom,
            workTo,
        } = this.state

        const {
            professionUpdate,
        } = this.props
        // if (title.trim() === '' && period.trim() === '' && description.trim() === '') {
        //     return
        // }
        // const updatedPost = userEducation!.set('id', this.props.educationPostId)
        //     .set('title', title)
        //     .set('period', period)
        //     .set('description', description)
        professionUpdate!({
            id: professionId,
            companyName: companyName,
            companyBusiness: companyBusiness,
            designation: designation,
            companyLocation: companyLocation,
            department: department,
            experience: experience,
            responsibilities: responsibilities,
            workFrom: workFrom,
            workTo: workTo,
        })
        this.setState({
            isEdit: false
        })
    }

    handleDelete = (evt: any, professionId?: string) => {
        this.props.delete!(professionId)
    }

    render() {
        const {translate,
            companyName,
            companyBusiness,
            designation,
            isAuthed,
            companyLocation,
            department,
            experience,
            responsibilities,
            workFrom,
            workTo,
        } = this.props

        return (
            <>{this.state.isEdit ?
            <div className='ui-block-content'>
                {/* Personal Information Form  */}
                <form>
                    <div className='row'>
                        <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Company Name *</label>
                                <input className='form-control'
                                       type='text'
                                       onChange={this.handleOnChange}
                                       name='companyName'
                                       defaultValue={companyName}/>
                                <span className='material-input'/>
                                {/*<span style={{ color: 'red' }}>*/}
                                            {/*{this.state.companyNameInputError.trim() ? this.state.companyNameInputError : ''}*/}
                                     {/*</span>*/}
                            </div>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Company Business</label>
                                <input className='form-control'
                                       type='text'
                                       onChange={this.handleOnChange}
                                       name='companyBusiness'
                                       defaultValue={companyBusiness}/>
                            </div>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Designation *</label>
                                <input className='form-control'
                                       type='text'
                                       onChange={this.handleOnChange}
                                       name='designation'
                                       defaultValue={designation}/>
                                <span className='material-input'/>
                                {/*<span style={{ color: 'red' }}>*/}
                                            {/*{this.state.designationInputError.trim() ? this.state.designationInputError : ''}*/}
                                     {/*</span>*/}
                            </div>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Department</label>
                                <input className='form-control'
                                       type='text'
                                       onChange={this.handleOnChange}
                                       name='department'
                                       defaultValue={department}/>
                            </div>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Area of Expertise *</label>
                                <input className='form-control'
                                       type='text'
                                       onChange={this.handleOnChange}
                                       name='experience'
                                       defaultValue={experience}/>
                                <span className='material-input'/>
                                {/*<span style={{ color: 'red' }}>*/}
                                            {/*{this.state.experienceInputError.trim() ? this.state.experienceInputError : ''}*/}
                                     {/*</span>*/}
                            </div>
                        </div>
                        <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Responsibilities</label>
                                <textarea className='form-control'
                                          onChange={this.handleOnChange}
                                          name='responsibilities'
                                          defaultValue={responsibilities}>
                                </textarea>
                            </div>
                            <div className='form-group label-floating'>
                                <label className='control-label'>Company Location</label>
                                <input className='form-control'
                                       type='text'
                                       onChange={this.handleOnChange}
                                       name='companyLocation'
                                       defaultValue={companyLocation}/>
                            </div>
                            <label className='control-label'>Employment Period *</label>
                            <div className='row'>
                                <div className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <label className='control-label'>From</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='workFrom'
                                           defaultValue={workFrom}/>
                                    <span className='material-input'/>
                                    {/*<span style={{ color: 'red' }}>*/}
                                            {/*{this.state.workFromInputError.trim() ? this.state.workFromInputError : ''}*/}
                                             {/*</span>*/}
                                </div>

                                {this.state.isChecked ? <div className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div>Continuing</div>
                                    <span className='material-input'/>
                                    {/*<span style={{ color: 'red' }}>*/}
                                            {/*{this.state.workToInputError.trim() ? this.state.workToInputError : ''}*/}
                                            {/*</span>*/}
                                </div> : <div className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <label className='control-label'>To</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleOnChange}
                                           name='workTo'
                                           defaultValue={workTo}/>
                                    <span className='material-input'/>
                                    {/*<span style={{ color: 'red' }}>*/}
                                            {/*{this.state.workToInputError.trim() ? this.state.workToInputError : ''}*/}
                                            {/*</span>*/}
                                </div>}
                            </div>
                            <div className='checkbox'>
                                <label>
                                    <input
                                        onChange={this.handleOnChange}
                                        name='currentlyWorking'
                                        type='checkbox'/>
                                    <span className='checkbox-material'>
                                            <span className='check'/></span>
                                    I currently work here.
                                </label>
                            </div>
                        </div>
                        <div className='col col-lg-6 col-md-6 col-sm-6 col-6'>
                            <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                               onClick={this.handleEditCancel}
                            >Cancel</a>
                        </div>
                        <div className='col col-lg-6 col-md-6 col-sm-6 col-6'>
                            <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                               onClick={(evt: any) => this.handleForm(evt, this.props.professionId)}
                            >Update</a>
                        </div>
                    </div>
                </form>
                {/* ... end Personal Information Form  */}
            </div> :
            <div className='row'>
                <div className='col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'>
                    <li onDoubleClick={this.handleEdit}>
                        <span className='title'>{companyName}</span>
                        <span className='date'>{department}</span>
                        <span className='text'>{designation}</span>
                        <span className='text'>{workFrom}-{workTo}</span>
                        <p className='text'>{responsibilities}</p>
                    </li>
                </div>
                {isAuthed ? <div className='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12'>
                    <div className='more'>
                        <svg className='olymp-three-dots-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                        </svg>
                        <ul className='more-dropdown'>
                            <li><a href={'javascript:(0)'} onClick={this.handleEdit}>Edit</a></li>
                            <li><a href={'javascript:(0)'}
                                   onClick={(evt: any) => this.handleDelete(evt, this.props.professionId)}>Delete</a>
                            </li>
                        </ul>
                    </div>
                </div> : ''}
            </div>
            }
            </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IProfessionItemComponentProps) => {
    return {
        professionUpdate: (profession: Profession) => dispatch(professionActions.dbUpdateProfession(profession)),
        delete: (postId: string) => dispatch(professionActions.dbDeleteProfessionInfo(postId)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IProfessionItemComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((ProfessionItemComponent as any) as any)
