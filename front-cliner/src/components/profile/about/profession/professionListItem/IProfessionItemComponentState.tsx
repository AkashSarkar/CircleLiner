
export interface IProfessionItemComponentState {
    isEdit: boolean
    /**
     * checking for edit and delete show
     */
    isChecked: boolean
    /**
     * company name
     */
    companyName: string
    /**
     * Company business
     */
    companyBusiness: string
    /**
     * Professional designation
     */
    designation: string
    /**
     * Office location
     */
    companyLocation: string
    /**
     * Profession department
     */
    department: string
    /**
     * passing year
     */
    experience: string
    /**
     * educational achievements
     */
    responsibilities: string
    /**
     * education period
     */
    workFrom: string
    /**
     * education period
     */
    workTo: string
    /**
     * education edit
     */
    edit?: boolean
}
