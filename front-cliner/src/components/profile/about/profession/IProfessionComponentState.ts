
export interface IProfessionComponentState {
    /**
     * checking for edit and delete show
     */
     isChecked: boolean
    /**
     * company name
     */
    companyName: string
    /**
     * company business
     */
    companyBusiness: string
    /**
     * work position
     */
    designation: string
    /**
     * work department
     */
    department: string
    /**
     * working experience
     */
    experience: string
    /**
     * work responsibilities
     */
    responsibilities: string
    /**
     * company location
     */
    companyLocation: string
    /**
     * when start working at this company
     */
    workFrom: string
    /**
     * when left company
     */
    workTo: string
    /**
     * company name error
     */
    companyNameInputError: string
    /**
     * designation error
     */
    designationInputError: string
    /**
     * experience error
     */
    experienceInputError: string
    /**
     * workFrom error
     */
    workFromInputError: string
    /**
     * workTo error
     */
    workToInputError: string

}
