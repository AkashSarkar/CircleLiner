import { Profession } from 'core/domain/users/profession'
import {Map} from 'immutable'

export interface IProfessionComponentProps {
    /**
     * checking request user id with authorize user id
     */
     isAuthed?: boolean
    /**
     * user id
     */
     userId?: string
    /**
     * profession post id
     */
     id?: string
    /**
     * If it's true, post will be in edit view
     */
     edit?: boolean
    /**
     * profession model
     */
     professionModel?: Map<string, any>

    /**
     * Save a profession
     *
     * @memberof IProfessionComponentProps
     */
     postProfession?: (postProfession: Profession) => any

    /**
     * Update a profession
     *
     * @memberof IProfessionComponentProps
     */
     updateProfession?: (postProfession: Map<string, any>) => any

    /**
     * Styles
     */
     classes?: any

    /**
     * Translate to locale string
     */
     translate?: (state: any) => any
    /**
     * match
     */
     match?: any

    /**
     * load current user profession
     */
    getProfessionData?: () => any
    /**
     * Edit profile dialog is open {true} or not {false}
     *
     * @type {boolean}
     * @memberof IEditProfileComponentProps
     */
    open?: boolean
}
