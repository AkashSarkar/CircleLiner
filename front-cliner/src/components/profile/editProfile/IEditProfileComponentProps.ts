import {Profile} from 'core/domain/users/index'

export interface IEditProfileComponentProps {

    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    info?: Profile

    /**
     * User profile banner addresss
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    banner: string

    /**
     * User avatar address
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    avatar: string

    /**
     * User full name
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    fullName: string

    /**
     * User first name
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    firstName: string

    /**
     * User first name
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    lastName: string

    /**
     * User industry
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    industry: string

    /**
     * User tagline
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    tagLine: string

    /**
     * Edit profile dialog is open {true} or not {false}
     *
     * @type {boolean}
     * @memberof IEditProfileComponentProps
     */
    open?: boolean

    /**
     * User concentrated industry
     *
     * @type {string}
     * @memberof IEditProfileComponentProps
     */
    concentratedIndustry?: any

    /**
     * Update user profile
     *
     * @memberof IEditProfileComponentProps
     */
    update?: (profile: Profile) => void

    /**
     * On edit profile dialog close event
     *
     * @memberof IEditProfileComponentProps
     */
    onRequestClose?: () => void

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     */
    currentLanguage?: string
}
