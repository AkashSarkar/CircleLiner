// - Import react components
import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import InfiniteScroll from 'react-infinite-scroller'
import {Map, List as ImuList} from 'immutable'

// - Material UI
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import LoadMoreProgressComponent from 'layouts/loadMoreProgress'
import JobComponent from 'src/components/job/jobComponent'

// - Import API
import * as PostAPI from 'api/PostAPI'

// - Import domain
import {Post} from 'core/domain/posts'
import {Job} from 'core/domain/jobs/job'

// - Import interfaces
import {IJobSeekerStreamComponentProps} from './IJobSeekerStreamComponentProps'
import {IJobSeekerStreamComponentState} from './IJobSeekerStreamComponentState'

const styles = (theme: any) => ({})

export class JobSeekerStreamComponent extends Component<IJobSeekerStreamComponentProps, IJobSeekerStreamComponentState> {
    styles = {}

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobSeekerStreamComponentProps) {
        super(props)
        const {} = props
        this.state = {}
        // Binding functions to `this`
        this.postLoad = this.postLoad.bind(this)
    }

    /**
     * Scroll loader
     */
    scrollLoad = (page: number) => {
        const {loadStream} = this.props
        loadStream!(page, 10)
    }
    /**
     * Create a list of posts
     * @return {DOM} posts
     */
    postLoad = () => {

        let {match} = this.props
        console.log(this.props.jobs)
        let posts: Map<string, Map<string, any>> = this.props.jobs
        console.log('jobs=============================================')
        console.log(posts)
        let {tag} = match.params
        if (posts === undefined || !(posts.keySeq().count() > 0)) {

            return (

                <h1>
                    'Nothing has been shared.'
                </h1>

            )
        } else {
            let postBack = {divided: false, oddPostList: [], evenPostList: []}
            let parsedPosts: ImuList<any> = ImuList()
            posts.forEach((post: Map<string, any>) => {
                if (tag) {
                    let regex = new RegExp('#' + tag, 'g')
                    let postMatch = String(post.get('body', '')).match(regex)
                    if (postMatch !== null) {
                        parsedPosts = parsedPosts.push(post)
                    }
                } else {
                    parsedPosts = parsedPosts.push(post)
                }
            })
            const sortedPosts = PostAPI.sortImuObjectsDate(parsedPosts)
            if (sortedPosts.count() > 6) {
                postBack.divided = true

            } else {
                postBack.divided = false
            }
            let index = 0
            sortedPosts.forEach((post) => {

                let newPost: any = (
                    <JobComponent key={`${post!.get('id')}-stream-div-post`} job={post! as any}/>
                )

                if ((index % 2) === 1 && postBack.divided) {
                    postBack.oddPostList.push(newPost as never)
                } else {
                    postBack.evenPostList.push(newPost as never)
                }
                ++index
            })
            return postBack
        }
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {hasMorePosts, translate} = this.props
        const postList = this.postLoad() as { evenPostList: Post[], oddPostList: Post[], divided: boolean } | any
        return (
            <InfiniteScroll
                pageStart={0}
                loadMore={this.scrollLoad}
                hasMore={hasMorePosts}
                useWindow={true}
                loader={<LoadMoreProgressComponent key='stream-load-more-progress'/>}
            >
                <div id='newsfeed-items-grid'>
                    {postList.evenPostList}
                    {postList.divided
                        ? postList.oddPostList
                        : ''}
                </div>
            </InfiniteScroll>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobSeekerStreamComponentProps) => {
    return {}
}
/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobSeekerStreamComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),
    }
}
// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(JobSeekerStreamComponent as any) as any) as typeof JobSeekerStreamComponent