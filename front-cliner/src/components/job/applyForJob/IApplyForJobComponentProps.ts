import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'
import {Map} from 'immutable'

export interface IApplyForJobComponentProps {

    open: boolean
    /**
     * Recieve request close function
     */
    onRequestClose: () => void
    /**
     * url user id
     */
    userId?: string
    /**
     * checking request user id with authorize user id
     */
    isAuthed?: boolean
    /**
     * Personal information model
     */
    JobAsCompany?: Map<string, any>

    /**
     * Save a post
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    jobPostAsCompany?: (jobPost: JobAsCompany) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    currentLanguage?: string

    /**
     * load current user education
     */
    getJobpostAsCompanyData?: (userId: string) => any

    /**
     * match
     */
    match?: any
}
