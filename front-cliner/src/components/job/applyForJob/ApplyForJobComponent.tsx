// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import JobComponent from 'src/components/job/jobComponent'

// - Import API

// - Import actions
import * as jobActions from 'store/actions/jobActions'

// - Import layouts
import LModal from 'layouts/modal/index'

// - Import Interfaces
import {IApplyForJobComponentProps} from './IApplyForJobComponentProps'
import {IApplyForJobComponentState} from './IApplyForJobComponentState'

/**
 * Create component class
 */
export class ApplyForJobComponent extends Component<IApplyForJobComponentProps, IApplyForJobComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IApplyForJobComponentProps) {
        super(props)
        this.state = {

            companyName: '',

            industry: '',

            altCompanyName: '',

            bunsinessDescription: '',

            companyAddress: '',

            phone: '',

            email: '',

            website: '',

            jobTitle: '',

            skill: '',

            jobType: '',

            eduRequirement: '',

            levelOfSkill: '',

            salary: '',

            noOfVacancy: '',

            responsibilities: '',
        }
        // Binding function to `this`
    }

    componentWillReceiveProps(nextProps: IApplyForJobComponentProps) {

        if (!nextProps.open) {
            const {JobAsCompany} = this.props
            this.setState({
                /**
                 * User's firstname
                 */
                companyName: JobAsCompany ? JobAsCompany.get('companyName', '') : '',

                /**
                 * User's fullname
                 */
                industry: JobAsCompany ? JobAsCompany.get('industry', '') : '',

                /**
                 * User's fullname
                 */
                altCompanyName: JobAsCompany ? JobAsCompany.get('altCompanyName', '') : '',

                /**
                 * User's email
                 */
                bunsinessDescription: JobAsCompany ? JobAsCompany.get('bunsinessDescription', '') : '',

                /**
                 * User's date of birth
                 */
                companyAddress: JobAsCompany ? JobAsCompany.get('companyAddress', '') : '',

                /**
                 * User's website
                 */
                phone: JobAsCompany ? JobAsCompany.get('phone', '') : '',

                /**
                 * User's phone
                 */
                email: JobAsCompany ? JobAsCompany.get('email', '') : '',

                /**
                 * User's birth place
                 */
                website: JobAsCompany ? JobAsCompany.get('website', '') : '',

                /**
                 * User's gender
                 */
                jobTitle: JobAsCompany ? JobAsCompany.get('jobTitle', '') : '',

                /**
                 * User's marital status
                 */
                skill: JobAsCompany ? JobAsCompany.get('skill', '') : '',

                /**
                 * User's date of birth
                 */
                jobType: JobAsCompany ? JobAsCompany.get('jobType', '') : '',

                /**
                 * User's website
                 */
                eduRequirement: JobAsCompany ? JobAsCompany.get('eduRequirement', '') : '',

                /**
                 * User's phone
                 */
                levelOfSkill: JobAsCompany ? JobAsCompany.get('levelOfSkill', '') : '',

                /**
                 * User's birth place
                 */
                salary: JobAsCompany ? JobAsCompany.get('salary', '') : '',

                /**
                 * User's gender
                 */
                noOfVacancy: JobAsCompany ? JobAsCompany.get('noOfVacancy', '') : '',

                /**
                 * User's marital status
                 */
                responsibilities: JobAsCompany ? JobAsCompany.get('responsibilities', '') : '',
            })
        }
        // Binding function to `this`
    }
    // componentWillMount() {
    //     const {getJobpostAsCompanyData, userId} = this.props
    //     getJobpostAsCompanyData!(userId!)
    // }

    componentDidMount() {
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        const {
            companyName,
            industry,
            altCompanyName,
            bunsinessDescription,
            companyAddress,
            phone,
            email,
            website,
            jobTitle,
            skill,
            jobType,
            eduRequirement,
            levelOfSkill,
            salary,
            noOfVacancy,
            responsibilities,
        } = this.state
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose} modalTitle={'Submit a proposal'}>
                <div className='container'>
                    <div className='ui-block'>
                        <div className='ui-block-content'>
                            <div className='edit-my-poll-content'>
                                {/*<JobComponent key={''} job={'' as any}/>*/}
                                <form className='resume-form'>
                                    <h4>Additional Details</h4>
                                    <div className='form-group label-floating is-empty'>
                                        <label className='control-label'>Your Cover Letter</label>
                                        <textarea className='form-control' defaultValue={''}/>
                                        <span className='material-input'/>
                                    </div>
                                    <div className='form-group label-floating is-empty'>
                                        <p className='dotted'
                                           style={{border: '1px dotted', padding: 20, textAlign: 'center'}}> Drag or
                                            Upload your CV here.</p>
                                        <span className='material-input'/>
                                    </div>
                                    <p style={{textAlign: 'center'}}>You may drag or upload your resume from here. The
                                        file must be in .pdf format and less than 25mb</p>
                                    <div className='align-center'>
                                        <a href='#' className='btn btn-primary btn-lg m-3'>Submit Application</a>
                                        <a href='#' onClick={this.props.onRequestClose} className='btn btn-secondary btn-lg m-3'>Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IApplyForJobComponentProps) => {
    return {
        // jobPostAsCompany: (jobPost: JobAsCompany) => dispatch(jobActions.dbAddCompanyJobpost(jobPost)),
        // getJobpostAsCompanyData: (userId: string) => dispatch(jobActions.dbGetJobpostAsCompanyById(userId))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IApplyForJobComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)

    return {
        translate: getTranslate(state.get('locale')),
        userId: uid
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((ApplyForJobComponent as any) as any)
