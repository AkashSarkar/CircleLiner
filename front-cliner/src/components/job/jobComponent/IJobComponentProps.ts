import {Map} from 'immutable'

export interface IJobComponentProps {
    /**
     * Job object
     */
    job: Map<string, any>
    /**
     * Current user is the owner of the post {true} or not {false}
     *
     * @type {boolean}
     * @memberof IPostComponentProps
     */
    isPostOwner?: boolean
    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}