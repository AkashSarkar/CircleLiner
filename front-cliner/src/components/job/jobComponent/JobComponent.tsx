// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import {push} from 'react-router-redux'
import PropTypes from 'prop-types'
import moment from 'moment/moment'
import Linkify from 'react-linkify'
import copy from 'copy-to-clipboard'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
// - Material UI
import {Manager, Target, Popper} from 'react-popper'
import {withStyles} from '@material-ui/core/styles'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import LinearProgress from '@material-ui/core/LinearProgress'
import Badge from '@material-ui/core/Badge'
import IconButton from '@material-ui/core/IconButton'
import MailIcon from '@material-ui/icons/Mail'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

// - Import app components
// - Import actions
import {IJobComponentProps} from './IJobComponentProps'
import {IJobComponentState} from './IJobComponentState'

const styles = (theme: any) => ({
    margin: {
        margin: theme.spacing.unit * 2,
    },
    padding: {
        padding: `0 ${theme.spacing.unit * 2}px`,
    },
})

export class JobComponent extends Component<IJobComponentProps, IJobComponentState> {
    styles = {}

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobComponentProps) {
        super(props)
        const {job} = props
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        // Define variables
        const {job, isPostOwner, translate, classes} = this.props
        const {postMenuAnchorEl, isPostMenuOpen} = this.state
        const rightIconMenu = (
            <div className='more'>
                {/*<svg className='olymp-three-dots-icon' onClick={this.openPostMenu.bind(this)}*/}
                {/*aria-owns={isPostMenuOpen! ? 'post-menu' : ''}>*/}
                {/*<use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>*/}
                {/*</svg>*/}
                {/*<ClickAwayListener onClickAway={this.closePostMenu}>*/}
                {/*<ul role='menu' className='more-dropdown'>*/}
                {/*<li onClick={this.handleOpenStatusPostWrite}> {translate!('post.edit')} </li>*/}
                {/*<li onClick={this.handleDelete}> {translate!('post.delete')} </li>*/}
                {/*<li*/}
                {/*onClick={() => this.props.toggleDisableComments!(!job.get('disableComments'))}>*/}
                {/*{job.get('disableComments') ? translate!('post.enableComments') : translate!('post.disableComments')}*/}
                {/*</li>*/}
                {/*<li*/}
                {/*onClick={() => this.props.toggleSharingComments!(!job.get('disableSharing'))}>*/}
                {/*{job.get('disableSharing') ? translate!('post.enableSharing') : translate!('post.disableSharing')}*/}
                {/*</li>*/}
                {/*</ul>*/}
                {/*</ClickAwayListener>*/}
            </div>
        )
        const {
            ownerUserId,
            ownerDisplayName,
            creationDate,
            image,
            postData,
            description,
            id,
            disableComments,
            commentCounter,
            disableSharing,
        } = job.toJS()
        return (
            <div className='ui-block'>
                <article className='hentry post'>
                    <div className='post__author author vcard inline-items'>
                        <NavLink to='/job/:jobId' style={{float: 'right'}} >See details</NavLink>
                        <div className='author-date'>
                            <NavLink to={`/job/${id}`}
                                     className='h5 post__author-name fn'>{'Job Title'}</NavLink>
                            {creationDate ? <div className='post__date'>
                                <time className='published'
                                      dateTime='2017-03-24T18:18'>{moment.unix(creationDate!).fromNow() + ' | ' + 'job-type' + ' | ' + 'Company/Non-company'}</time>
                            </div> : <LinearProgress color='primary'/>}
                        </div>
                        {isPostOwner ? rightIconMenu : ''}
                        {/* Single Post */}
                        <article >
                            <p>Labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud tation ullamco laboris
                                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                sunt in culpa qui officia unt mollit anim id est laborum aperiam, eaque ipsa quae ab illo
                                inventore veritatis  quasi architecto.
                            </p>
                            <div>
                                <a href='#' className='badge badge-secondary p-2 m-1'>Laravel</a>
                                <a href='#' className='badge badge-secondary p-2 m-1'>Lumen</a>
                                <a href='#' className='badge badge-secondary p-2 m-1'>Word Document</a>
                                <a href='#' className='badge badge-secondary p-2 m-1'>Communication</a>
                            </div>
                            <div className='m-2'>
                                <span className='h6'>
                                    Company name
                                </span>
                                <span> | </span>
                                <span className='h6'>
                                    Loaction
                                </span>
                            </div>
                        </article>
                    {/* ... end Single Post */}
                    </div>
                </article>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobComponentProps) => {
    const {job} = ownProps
    return {}
}
/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),
    }
}
// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(JobComponent as any) as any)