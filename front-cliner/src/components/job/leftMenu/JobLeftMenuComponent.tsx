import React, {Component} from 'react'
import {IJobLeftMenuComponentProps} from './IJobLeftMenuComponentProps'
import {IJobLeftMenuComponentState} from './IJobLeftMenuComponentState'
import {withStyles} from '@material-ui/core/styles'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {getActiveLanguage, getTranslate} from 'react-localize-redux'
import {Map} from 'immutable'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import WorkIcon from '@material-ui/icons/Work'
import {NavLink} from 'react-router-dom'

const styles = (theme: any) => ({
    menuItem: {
        backgroundColor: '#F9F9F9',
    },
    primary: {},
    icon: {},
    navLink: {
        '&:focus': {
            backgroundColor: '#F9F9F9',
            '& $primary, & $icon': {
                color: '#37A000',
            },
        },
        backgroundColor: '#F9F9F9',
        color: '#1a2501',
        padding: '9px 20px',
        ':active': {
            borderLeftWidth: '0 !important'
        }
    }
})

export class JobLeftMenuComponent extends Component<IJobLeftMenuComponentProps, IJobLeftMenuComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobLeftMenuComponentProps) {
        super(props)

        // Default state
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate, classes} = this.props
        return (
            <div className='ui-block'>
                <MenuList>
                    <MenuItem>
                        <NavLink to={`/find-job`}>
                            My Jobs
                        </NavLink>
                    </MenuItem>
                    <MenuItem>
                        <NavLink to={`/find-job/recommended`}>
                            Recommended
                        </NavLink>
                    </MenuItem>
                    <MenuItem>
                        <NavLink to={`/find-job/approaches`}>
                            Approaches
                        </NavLink>
                    </MenuItem>
                    <MenuItem>
                        <NavLink to={`/find-job/saved-jobs`}>
                            Saved Jobs
                        </NavLink>
                    </MenuItem>
                    <MenuItem>
                        <NavLink to={`/find-job/posted-jobs`}>
                            Posted Jobs
                        </NavLink>
                    </MenuItem>
                    <MenuItem>
                        <NavLink to={`/find-job/searched-jobs`}>
                            Search History
                        </NavLink>
                    </MenuItem>
                </MenuList>
            </div>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IJobLeftMenuComponentProps) => {
    return {}
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobLeftMenuComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid])
    return {
        userId: uid,
        fullName: user.fullName ? user.fullName : '',
        translate: getTranslate(state.get('locale')),
        currentLanguage: getActiveLanguage(state.get('locale')).code,
    }
}
// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(JobLeftMenuComponent as any) as any)