export interface IJobLeftMenuComponentProps {

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Styles
     */
    classes?: any

}