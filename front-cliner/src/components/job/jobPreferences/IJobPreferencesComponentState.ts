export interface IJobPreferencesComponentState {
    jobPreference: string
    wantToWork: string
    location: string
    role: string
    industry: string
    preferToWork: string
    remoteWork: string
    workFrom: string
    workTo: string
    jobType?: any

}
