import {JobPreferences} from 'core/domain/jobs/jobPreferences'
import {Map} from 'immutable'

export interface IJobPreferencesComponentProps {
    jobPreferences?: (jobPreferences: JobPreferences) => any

    userId: string

    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    currentLanguage?: string

}
