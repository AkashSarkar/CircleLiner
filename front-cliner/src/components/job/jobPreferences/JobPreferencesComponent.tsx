// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import {IJobPreferencesComponentProps} from './IJobPreferencesComponentProps'
import {IJobPreferencesComponentState} from './IJobPreferencesComponentState'

// - Import layouts
import Setting from 'layouts/setting'
import {JobPreferences} from 'core/domain/jobs/jobPreferences'
import * as jobActions from 'store/actions/jobActions'

/**
 * Create component class
 */
export class JobPreferencesComponent extends Component<IJobPreferencesComponentProps, IJobPreferencesComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobPreferencesComponentProps) {
        super(props)
        this.state = {
            jobPreference: '',
            wantToWork: '',
            location: '',
            role: '',
            industry: '',
            preferToWork: '',
            remoteWork: '',
            workFrom: '',
            workTo: '',
            jobType: '',
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })
        switch (name) {
            case 'job-preference':
                this.setState({
                    jobPreference: value
                })
                break
            case 'wantToWork':
                this.setState({
                    wantToWork: value
                })
                break
            case 'location':
                this.setState({
                    location: value
                })
                break
            case 'role':
                this.setState({
                    role: value
                })
                break
            case 'industry':
                this.setState({
                    industry: value
                })
                break
            case 'preferToWork':
                this.setState({
                    preferToWork: value
                })
                break
            case 'remoteWork':
                this.setState({
                    remoteWork: value
                })
                break
            case 'workFrom':
                this.setState({
                    workFrom: value
                })
                break
            case 'workTo':
                this.setState({
                    workTo: value
                })
                break
            case 'fullTime':
                let jobtype = [...this.state.jobType || [], ...[value]
                ]
                this.setState({
                    jobType: jobtype
                })
                break
            case 'contract':
                this.setState({
                    jobType: value
                })
                break
            case 'internship':
                this.setState({
                    jobType: value
                })
                break
            default:
        }
    }

    handleForm = () => {
        const {
            jobPreference,
            wantToWork,
            location,
            role,
            industry,
            preferToWork,
            remoteWork,
            workFrom,
            workTo,
            jobType,
        } = this.state
        const {
            jobPreferences,
        } = this.props

        jobPreferences!({
            jobPreferance: jobPreference,
            wantToWork: wantToWork,
            location: location,
            role: role,
            industry: industry,
            preferToWork: preferToWork,
            remoteWork: remoteWork,
            workFrom: workFrom,
            workTo: workTo,
            jobType: jobType
        })

    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <Setting>
                <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Job Search Status</h6>
                    </div>
                    <div className='ui-block-content'>
                        <div className='radio'>
                            <label>
                                <input type='radio'
                                       value='Actively looking for job now.'
                                       name='job-preference'
                                       onChange={this.handleInputChange}
                                />
                                <span className='circle'/><
                                span className='check'/>
                                Actively looking for job now.
                            </label>
                        </div>
                        <div className='radio'>
                            <label>
                                <input type='radio'
                                       value='Open, but not actively looking.'
                                       name='job-preference'
                                       onChange={this.handleInputChange}
                                />
                                <span className='circle'/><
                                span className='check'/>
                                Open, but not actively looking.
                            </label>
                        </div>
                        <div className='radio'>
                            <label>
                                <input type='radio'
                                       value='Not interested in jobs.'
                                       name='job-preference'
                                       onChange={this.handleInputChange}
                                />
                                <span className='circle'/> Not interested in jobs.
                                <span className='check'/>

                            </label>
                        </div>
                    </div>

                    <div className='ui-block-title'>
                        <h6 className='title'>Match Preferences</h6>
                    </div>
                    <div className='ui-block-content'>
                        <form>
                            <div className='row'>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Topics you want to work with</label>
                                        <textarea className='form-control'
                                                  name='wantToWork'
                                                  defaultValue=''
                                                  onChange={this.handleInputChange}
                                        >
                                        </textarea>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Location</label>
                                        <input className='form-control'
                                               type='text'
                                               name='location'
                                               onChange={this.handleInputChange}
                                               defaultValue=''/>
                                        <span className='material-input'/>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Role</label>
                                        <input className='form-control'
                                               type='text'
                                               name='role'
                                               onChange={this.handleInputChange}
                                               defaultValue=''/>
                                    </div>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Industry</label>
                                        <input className='form-control'
                                               type='text'
                                               name='industry'
                                               onChange={this.handleInputChange}
                                               defaultValue=''/>
                                        <span className='material-input'/>
                                    </div>
                                </div>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Topics you prefer not to work with</label>
                                        <textarea className='form-control'
                                                  name='preferToWork'
                                                  onChange={this.handleInputChange}
                                                  defaultValue=''>
                                        </textarea>
                                    </div>
                                    <div className='checkbox'>
                                        <label>
                                            <input
                                                name='remoteWork'
                                                onChange={this.handleInputChange}
                                                type='checkbox'/>
                                            <span className='checkbox-material'>
                                                     <span className='check'/></span>
                                            Willing to work remotely
                                        </label>
                                    </div>
                                    <br/>
                                    <br/>
                                    <label className='control-label'>Experience Level</label>
                                    <div className='row'>
                                        <div
                                            className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                            <label className='control-label'>Minimum</label>
                                            <input className='form-control'
                                                   type='text'
                                                   name='workFrom'
                                                   onChange={this.handleInputChange}
                                                   defaultValue=''/>
                                            <span className='material-input'/>
                                        </div>
                                        <div
                                            className='form-group label-floating col col-lg-6 col-md-6 col-sm-12 col-12'>
                                            <label className='control-label'>Maximum</label>
                                            <input className='form-control'
                                                   type='text'
                                                   name='workTo'
                                                   onChange={this.handleInputChange}
                                                   defaultValue=''/>
                                            <span className='material-input'/>
                                        </div>
                                    </div>
                                </div>
                                <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                    <label className='control-label'>Job Type</label>
                                    <div className='form-group label-floating'>
                                        <div className='checkbox inline-block pr-3'>
                                            <label>
                                                <input
                                                    name='fullTime'
                                                    onChange={this.handleInputChange}
                                                    value='fullTime'
                                                    type='checkbox'/>
                                                <span className='checkbox-material'>
                                                     <span className='check'/></span>
                                                Full-time
                                            </label>
                                        </div>
                                        <div className='checkbox inline-block pr-3'>
                                            <label>
                                                <input
                                                    name='fullTime'
                                                    onChange={this.handleInputChange}
                                                    value='contract'
                                                    type='checkbox'/>
                                                <span className='checkbox-material'>
                                                     <span className='check'/></span>
                                                Contract
                                            </label>
                                        </div>
                                        <div className='checkbox inline-block pr-3'>
                                            <label>
                                                <input
                                                    name='fullTime'
                                                    onChange={this.handleInputChange}
                                                    value='internship'
                                                    type='checkbox'/>
                                                <span className='checkbox-material'>
                                                     <span className='check'/></span>
                                                Internship
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div className='ui-block-title'>
                        <h6 className='title'>Saved Resume</h6>
                    </div>
                    <div className='ui-block-content align-center'>
                        <p className='h6'>Upload your resume</p>
                        <div className='file-upload'>
                            <label htmlFor='upload' className='file-upload__label'>Upload Button</label>
                            <input id='upload' className='file-upload__input' type='file' name='file-upload'/>
                        </div>

                        <span>Allows *.pdf, *.doc, *.docx, *.odt and *.txt – up to 2MB</span>
                    </div>

                    <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                        <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                           onClick={this.handleForm}

                        >save</a>
                    </div>
                </div>
            </Setting>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobPreferencesComponentProps) => {
    return {
        jobPreferences: (jobPreferences: JobPreferences) => dispatch(jobActions.dbAddJobPreferences(jobPreferences))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobPreferencesComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        fullName: user.fullName,
        userId: uid
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((JobPreferencesComponent as any) as any)
