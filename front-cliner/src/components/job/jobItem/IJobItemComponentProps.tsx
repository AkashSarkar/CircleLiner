import {Profession} from 'core/domain/users/profession'
import {UserTie} from 'core/domain/circles/index'
import {Map} from 'immutable'
import {Profile} from 'core/domain/users'
import {Education} from 'core/domain/users/education'
import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'

export interface IJobItemComponentProps {
    /**
     * User education post id
     */
    id?: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    companyName: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    industry: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    altCompanyName: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    bunsinessDescription: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    companyAddress: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    phone: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    email: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    website: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobTitle: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    skill: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobType: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    eduRequirement: string

    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    levelOfSkill: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    salary: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    noOfVacancy: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    responsibilities: string
    /**
     * education id
     */
    jobAsCompanyId: string
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * Job as company model
     */
    jobAsCompany?: Map<string, any>
    /**
     * Save a post
     *
     * @memberof IPostWriteComponentProps
     */
    jobAsCompanyService?: (jobAsCompanyService: JobAsCompany, postId: string) => any
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    edit?: boolean
    /**
     * User's banner URL
     *
     * @type {string}
     * @memberof IEditProfileComponentState
     */
    open?: boolean

    /**
     * Styles
     */
    classes?: any
    /**
     * Current locale language
     */
    currentLanguage?: string
    /**
     * match
     */
    match?: any
    /**
     * user id
     */
    userId?: string
    /**
     * delete education by education id
     */
    delete?: (jobAsCompanyId?: string) => any
    /**
     * user authentication
     */
    isAuthed: boolean
}
