// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
import {push} from 'react-router-redux'
import {NavLink} from 'react-router-dom'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import JobSugestion from 'src/components/sugestionBox/jobs'
import ApplyForJob from 'src/components/job/applyForJob'

// - Import Layouts
import HomeView from 'layouts/views/homeView'

// - Import actions
import * as jobActions from 'store/actions/jobActions'

// - Import interfaces
import {IJobItemComponentProps} from './IJobItemComponentProps'
import {IJobItemComponentState} from './IJobItemComponentState'

// - import domains
import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'

/**
 * Create component class
 */
export class JobItemComponent extends Component<IJobItemComponentProps, IJobItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: IJobItemComponentProps) {
        super(props)
        const {jobAsCompany} = props
        this.state = {
            isEdit: false,
            /**
             * education level
             */
            companyName: this.props.edit && jobAsCompany ? jobAsCompany.get('companyName') : '',
            /**
             * education degree name
             */
            industry: this.props.edit && jobAsCompany ? jobAsCompany.get('industry') : '',
            /**
             * education major subject
             */
            altCompanyName: this.props.edit && jobAsCompany ? jobAsCompany.get('altCompanyName') : '',
            /**
             * education major subject
             */
            bunsinessDescription: this.props.edit && jobAsCompany ? jobAsCompany.get('bunsinessDescription') : '',
            /**
             * education major subject
             */
            companyAddress: this.props.edit && jobAsCompany ? jobAsCompany.get('companyAddress') : '',
            /**
             * education major subject
             */
            phone: this.props.edit && jobAsCompany ? jobAsCompany.get('phone') : '',
            /**
             * education major subject
             */
            email: this.props.edit && jobAsCompany ? jobAsCompany.get('email') : '',
            /**
             * education major subject
             */
            website: this.props.edit && jobAsCompany ? jobAsCompany.get('website') : '',
            /**
             * education major subject
             */
            jobTitle: this.props.edit && jobAsCompany ? jobAsCompany.get('jobTitle') : '',
            /**
             * education major subject
             */
            skill: this.props.edit && jobAsCompany ? jobAsCompany.get('skill') : '',
            /**
             * education major subject
             */
            jobType: this.props.edit && jobAsCompany ? jobAsCompany.get('jobType') : '',
            /**
             * education major subject
             */
            eduRequirement: this.props.edit && jobAsCompany ? jobAsCompany.get('eduRequirement') : '',
            /**
             * education major subject
             */
            levelOfSkill: this.props.edit && jobAsCompany ? jobAsCompany.get('levelOfSkill') : '',
            /**
             * education major subject
             */
            salary: this.props.edit && jobAsCompany ? jobAsCompany.get('salary') : '',
            /**
             * education major subject
             */
            noOfVacancy: this.props.edit && jobAsCompany ? jobAsCompany.get('noOfVacancy') : '',
            /**
             * education major subject
             */
            responsibilities: this.props.edit && jobAsCompany ? jobAsCompany.get('responsibilities') : '',
            /**
             * Job apply modal open
             */
            openProposalModal: false
        }
        this.handleEdit = this.handleEdit.bind(this)
        this.handleEditCancel = this.handleEditCancel.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
        this.handleOpenProposalSubmit = this.handleOpenProposalSubmit.bind(this)
        this.handleCloseProposalSubmit = this.handleCloseProposalSubmit.bind(this)
    }

    handleEdit = () => {
        this.setState({
            isEdit: true
        })
    }
    handleEditCancel = () => {
        this.setState({
            isEdit: false
        })
    }

    handleDelete = (evt: any, jobAsCompanyId?: string) => {
        this.props.delete!(jobAsCompanyId)
    }

    /**
     * Open award write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenProposalSubmit = () => {
        this.setState({
            openProposalModal: true
        })
    }
    // Close handlers
    /**
     * Close post write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseProposalSubmit = () => {
        this.setState({
            openProposalModal: false
        })
    }

    render() {
        const {
            translate,
            companyName,
            industry,
            altCompanyName,
            bunsinessDescription,
            companyAddress,
            phone,
            email,
            website,
            jobTitle,
            skill,
            jobType,
            eduRequirement,
            levelOfSkill,
            salary,
            noOfVacancy,
            responsibilities,
        } = this.props
        return (
            <HomeView>
            <div className='container'>
                <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Job Details</h6>
                    </div>
                    <div className='ui-block-content'>
                        <div className='row'>
                            <div className='col col-lg-8 col-md-8 col-sm-12 col-12'>
                                <div className='table-careers'>
                                        <div className='title-block align-center'>
                                            <h5 className='logo-title'>Job Title</h5>
                                            <div className=''>Industry</div>
                                            <p className=''>Job-type</p>
                                        </div>
                                    <br/>
                                    <h6>No. of vacancy: <span>5</span></h6>
                                    <h6>Educational Requirements</h6>
                                    <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et lorem.</p>

                                    <h6>Skills</h6>
                                    <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et lorem.</p>
                                    <h6>Level of Skill: <span>Expert</span></h6>

                                    <h6>Responsibilities</h6>
                                    <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et lorem.</p>

                                    <h6>Benifits</h6>
                                    <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et lorem.</p>
                                </div>
                              <hr/>
                                <ul className='table-careers'>
                                    <li>
                                        <span className='position bold'>6 people applied for this job</span>
                                    </li>
                                </ul>
                                <ul className='widget w-featured-topics table-careers'>
                                    <h5>Available jobs of this company:</h5>
                                    <li>
                                        <div className='content'>
                                            <h5>Job Title</h5>
                                            <time className='entry-date updated' dateTime='2017-06-24T18:18'>Duration for applicaton</time>
                                            <div>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</div>
                                            <a href='#' style={{float: 'right'}} className='forums'>See details</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='content'>
                                            <h5>Job Title</h5>
                                            <time className='entry-date updated' dateTime='2017-06-24T18:18'>Duration for applicaton</time>
                                            <div>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</div>
                                            <a href='#' style={{float: 'right'}} className='forums'>See details</a>
                                        </div>
                                    </li>
                                </ul>
                              <div>
                              </div>
                                <div>

                                </div>
                            </div>
                            <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                <div className='row'>
                                    <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                        <ul>
                                            <li>
                                                <a  onClick={this.handleOpenProposalSubmit} className='btn btn-green btn-md-2' style={{width: 200}}>
                                                    Submit a proposal
                                                </a>
                                            </li>
                                            <ApplyForJob open={this.state.openProposalModal}
                                                         onRequestClose={this.handleCloseProposalSubmit}/>
                                            <li>
                                                <NavLink to='#' className='btn btn-outline-secondary btn-md-2' data-toggle='modal' data-target='#faqs-popup' style={{color: 'black', width: 200}}>
                                                    Save Job
                                                </NavLink>
                                            </li>
                                         </ul>
                                    </div>
                                    <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                        <div className='ui-block'>
                                            <div className='ui-block-title'>
                                                <h6 className='title'>Company Details</h6>
                                            </div>
                                            <div className='ui-block-content'>
                                                {/* Company Details */}
                                                <div className='widget w-about'>
                                                    <a href='#' className='logo'>
                                                        <div className='img-wrap'>
                                                            <img src='/img/logo-colored.png' alt='Olympus' />
                                                        </div>
                                                        <div className='title-block'>
                                                            <h5 className='logo-title'>Company Name</h5>
                                                            <p className=''>Address</p>
                                                            <div className='sub-title'>Phone</div>
                                                        </div>
                                                    </a>
                                                    <h6>Desciption</h6>
                                                    <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et lorem.</p>
                                                    <ul className='socials'>
                                                        <li>
                                                            <h6>Website</h6>
                                                            <a href='#'>
                                                                <svg className='svg-inline--fa fa-facebook-square fa-w-14' aria-hidden='true' data-prefix='fab' data-icon='facebook-square' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512' data-fa-i2svg><path fill='currentColor' d='M448 80v352c0 26.5-21.5 48-48 48h-85.3V302.8h60.6l8.7-67.6h-69.3V192c0-19.6 5.4-32.9 33.5-32.9H384V98.7c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9H184v67.6h60.9V480H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48z' /></svg>{/* <i class='fab fa-facebook-square' aria-hidden='true'></i> */}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                {/* ... end Company Details */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                        <JobSugestion userId={this.props.userId!} isOpen={true}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*<div className='row'>*/}
                    {/*<div className='col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'>*/}
                        {/*<li onDoubleClick={this.handleEdit}>*/}
                            {/*{this.props.companyName ? <span className='text h5'>{companyName}</span> :*/}
                                {/*<span className='text h5'>{altCompanyName}</span>}*/}
                            {/*<span className='text'>{industry}</span>*/}
                            {/*<span className='date'>{bunsinessDescription}</span>*/}
                            {/*<span className='text'>{companyAddress}</span>*/}
                            {/*<span className='title'>{phone}</span>*/}
                            {/*<span className='date'>{email}</span>*/}
                            {/*<span className='text'>{website}</span>*/}
                            {/*<span className='date'>{jobTitle}</span>*/}
                            {/*<span className='text'>{skill}</span>*/}
                            {/*<span className='title'>{jobType}</span>*/}
                            {/*<span className='date'>{eduRequirement}</span>*/}
                            {/*<span className='date'>{levelOfSkill}</span>*/}
                            {/*<span className='text'>{salary}</span>*/}
                            {/*<span className='title'>{noOfVacancy}</span>*/}
                            {/*<span className='date'>{responsibilities}</span>*/}
                        {/*</li>*/}
                    {/*</div>*/}
                    {/*<div className='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12'>*/}
                        {/*<div className='more'>*/}
                            {/*<svg className='olymp-three-dots-icon'>*/}
                                {/*<use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>*/}
                            {/*</svg>*/}
                            {/*<ul className='more-dropdown'>*/}
                                {/*<li><a href={'javascript:(0)'} onClick={this.handleEdit}>Edit</a></li>*/}
                                {/*<li><a href={'javascript:(0)'}*/}
                                       {/*onClick={(evt: any) => this.handleDelete(evt, this.props.jobAsCompanyId)}>Delete</a>*/}
                                {/*</li>*/}
                            {/*</ul>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
            </div>
            </HomeView>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobItemComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url)),
        jobAsCompanyService: (jobAsCompanyService: JobAsCompany, postId: string) => dispatch(jobActions.dbUpdateJobpostAsCompany(jobAsCompanyService, postId)),
        delete: (jobPostId: string) => dispatch(jobActions.dbDeleteJobpostAsCompany(jobPostId)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobItemComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((JobItemComponent as any) as any)
