import { JobAsNonCompany } from 'core/domain/jobs/jobAsNonCompany'
import {Map} from 'immutable'

export interface IJobAsNonCompanyComponentProps {

     open?: boolean
    /**
     * url user id
     */
    userId?: string

    /**
     * checking request user id with authorize user id
     */
     isAuthed?: boolean

    /**
     * Personal information model
     */
    JobAsNonCompany?: Map<string, any>

    /**
     * Save a post
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    jobPostAsNonCompany?: (jobPost: JobAsNonCompany) => any

    /**
     * Styles
     */
     classes?: any

    /**
     * Translate to locale string
     */
     translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
     currentLanguage?: string

    /**
     * load current user education
     */
     getJobpostAsNonCompanyData?: (userId: string) => any

    /**
     * match
     */
     match?: any
  }
