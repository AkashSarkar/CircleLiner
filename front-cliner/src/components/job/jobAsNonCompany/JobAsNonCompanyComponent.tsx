// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
import JobAsNonCompanyListComponent from './jobAsNonCompanyList/JobAsNonCompanyListComponent'

// - Import app components
import {IJobAsNonCompanyComponentProps} from './IJobAsNonCompanyComponentProps'
import {IJobAsNonCompanyComponentState} from './IJobAsNonCompanyComponentState'

// - Import API

// - Import actions
import * as jobActions from 'store/actions/jobActions'

// - Import layouts
import HomeView from 'layouts/views/homeView'
import {JobAsNonCompany} from 'core/domain/jobs/jobAsNonCompany'

/**
 * Create component class
 */
export class JobAsNonCompanyComponent extends Component<IJobAsNonCompanyComponentProps, IJobAsNonCompanyComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobAsNonCompanyComponentProps) {
        super(props)
        const {JobAsNonCompany} = props
        this.state = {
            /**
             * Employer's name
             */
            name: JobAsNonCompany ? JobAsNonCompany.get('name', '') : '',

            /**
             * Employer's phone
             */
            phone: JobAsNonCompany ? JobAsNonCompany.get('phone', '') : '',

            /**
             * Employer's designation
             */
            designation: JobAsNonCompany ? JobAsNonCompany.get('designation', '') : '',

            /**
             * Employer's email
             */
            email: JobAsNonCompany ? JobAsNonCompany.get('email', '') : '',

            /**
             * Job title
             */
            jobTitle: JobAsNonCompany ? JobAsNonCompany.get('jobTitle', '') : '',

            /**
             * Job Location
             */
            location: JobAsNonCompany ? JobAsNonCompany.get('location', '') : '',

            /**
             * Skill
             */
            skill: JobAsNonCompany ? JobAsNonCompany.get('skill', '') : '',

            /**
             * Job type
             */
            jobType: JobAsNonCompany ? JobAsNonCompany.get('jobType', '') : '',

            /**
             * Educational requirement
             */
            eduRequirement: JobAsNonCompany ? JobAsNonCompany.get('eduRequirement', '') : '',

            /**
             * Industry
             */
            industry: JobAsNonCompany ? JobAsNonCompany.get('industry', '') : '',

            /**
             * Level of skill
             */
            levelOfSkill: JobAsNonCompany ? JobAsNonCompany.get('levelOfSkill', '') : '',

            /**
             * salary
             */
            salary: JobAsNonCompany ? JobAsNonCompany.get('salary', '') : '',

            /**
             * Nummber of vacancy
             */
            noOfVacancy: JobAsNonCompany ? JobAsNonCompany.get('noOfVacancy', '') : '',

            /**
             * job responsibilities
             */
            responsibilities: JobAsNonCompany ? JobAsNonCompany.get('responsibilities', '') : '',
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    componentWillReceiveProps(nextProps: IJobAsNonCompanyComponentProps) {
        if (!nextProps.open) {
            const {JobAsNonCompany} = this.props
            this.setState({
                /**
                 * Employer's name
                 */
                name: JobAsNonCompany ? JobAsNonCompany.get('name', '') : '',

                /**
                 * Employer's phone
                 */
                phone: JobAsNonCompany ? JobAsNonCompany.get('phone', '') : '',

                /**
                 * Employer's designation
                 */
                designation: JobAsNonCompany ? JobAsNonCompany.get('designation', '') : '',

                /**
                 * Employer's email
                 */
                email: JobAsNonCompany ? JobAsNonCompany.get('email', '') : '',

                /**
                 * Job title
                 */
                jobTitle: JobAsNonCompany ? JobAsNonCompany.get('jobTitle', '') : '',

                /**
                 * Job Location
                 */
                location: JobAsNonCompany ? JobAsNonCompany.get('location', '') : '',

                /**
                 * Skill
                 */
                skill: JobAsNonCompany ? JobAsNonCompany.get('skill', '') : '',

                /**
                 * Job type
                 */
                jobType: JobAsNonCompany ? JobAsNonCompany.get('jobType', '') : '',

                /**
                 * Educational requirement
                 */
                eduRequirement: JobAsNonCompany ? JobAsNonCompany.get('eduRequirement', '') : '',

                /**
                 * Industry
                 */
                industry: JobAsNonCompany ? JobAsNonCompany.get('industry', '') : '',

                /**
                 * Level of skill
                 */
                levelOfSkill: JobAsNonCompany ? JobAsNonCompany.get('levelOfSkill', '') : '',

                /**
                 * salary
                 */
                salary: JobAsNonCompany ? JobAsNonCompany.get('salary', '') : '',

                /**
                 * Nummber of vacancy
                 */
                noOfVacancy: JobAsNonCompany ? JobAsNonCompany.get('noOfVacancy', '') : '',

                /**
                 * job responsibilities
                 */
                responsibilities: JobAsNonCompany ? JobAsNonCompany.get('responsibilities', '') : '',
            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'name':
                this.setState({
                    name: value
                })
                break
            case 'phone':
                this.setState({
                    phone: value
                })
                break
            case 'designation':
                this.setState({
                    designation: value
                })
                break
            case 'email':
                this.setState({
                    email: value
                })
                break
            case 'jobTitle':
                this.setState({
                    jobTitle: value
                })
                break
            case 'location':
                this.setState({
                    location: value
                })
                break
            case 'skill':
                this.setState({
                    skill: value
                })
                break
            case 'jobType':
                this.setState({
                    jobType: value
                })
                break
            case 'eduRequirement':
                this.setState({
                    eduRequirement: value
                })
                break
            case 'industry':
                this.setState({
                    industry: value
                })
                break
            case 'levelOfSkill':
                this.setState({
                    levelOfSkill: value
                })
                break
            case 'salary':
                this.setState({
                    salary: value
                })
                break
            case 'noOfVacancy':
                this.setState({
                    noOfVacancy: value
                })
                break
            case 'responsibilities':
                this.setState({
                    responsibilities: value
                })
                break
            default:
        }
    }

    /**
     * Handle register form
     */
    handleForm = () => {
        const {
            name,
            phone,
            designation,
            email,
            jobTitle,
            location,
            skill,
            jobType,
            eduRequirement,
            industry,
            levelOfSkill,
            salary,
            noOfVacancy,
            responsibilities,
        } = this.state
        const {
            jobPostAsNonCompany,
        } = this.props
        jobPostAsNonCompany!({
            name: name,
            phone: phone,
            designation: designation,
            email: email,
            jobTitle: jobTitle,
            location: location,
            skill: skill,
            jobType: jobType,
            eduRequirement: eduRequirement,
            industry: industry,
            levelOfSkill: levelOfSkill,
            salary: salary,
            noOfVacancy: noOfVacancy,
            responsibilities: responsibilities,
        })
    }

    componentWillMount() {
        const {getJobpostAsNonCompanyData, userId} = this.props
        getJobpostAsNonCompanyData!(userId!)
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <HomeView>
                <div className='container'>
                    <JobAsNonCompanyListComponent userId={this.props.userId!}/>
                    <div className='ui-block'>

                        <div className='ui-block-content'>
                            {/* Form Favorite Page Information */}
                            <form>
                                <div className='row'>
                                    <div className='ui-block-title h5'>
                                        Employer Details
                                    </div>
                                    <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Contact Person Name</label>
                                            <input className='form-control'
                                                   name='name'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Mobile</label>
                                            <input className='form-control'
                                                   name='phone'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Contact Person's Designation</label>
                                            <input className='form-control'
                                                   name='designation'
                                                   type='text'
                                                   onChange={this.handleInputChange}
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Email</label>
                                            <input className='form-control'
                                                   name='email'
                                                   onChange={this.handleInputChange}
                                                   type='email'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>

                                    <div className='ui-block-title h5'>
                                        Job Details
                                    </div>
                                    <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Job Title</label>
                                            <input className='form-control'
                                                   name='jobTitle'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Job-location</label>
                                            <input className='form-control'
                                                   name='location'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Skill</label>
                                            <input className='form-control'
                                                   name='skill'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Job Type</label>
                                            <input className='form-control'
                                                   name='jobType'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>

                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Educational Requirements</label>
                                            <input className='form-control'
                                                   name='eduRequirement'
                                                   onChange={this.handleInputChange}
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Industry</label>
                                            <input className='form-control'
                                                   name='industry'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Level of skill</label>
                                            <input className='form-control'
                                                   name='levelOfSkill'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Salary</label>
                                            <input className='form-control'
                                                   name='salary'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>No. of vacancy</label>
                                            <input className='form-control'
                                                   name='noOfVacancy'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Responsibilities</label>
                                            <textarea className='form-control'
                                                      onChange={this.handleInputChange}
                                                      name='responsibilities'
                                                      defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>

                                    <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                        <a href={'javascript:(0)'}
                                           onClick={this.handleForm}
                                           className='btn btn-primary btn-lg full-width'>Save</a>
                                    </div>
                                </div>
                            </form>
                            {/* ... end Form Favorite Page Information */}
                        </div>
                    </div>
                </div>
            </HomeView>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobAsNonCompanyComponentProps) => {
    return {
        jobPostAsNonCompany: (jobPost: JobAsNonCompany) => dispatch(jobActions.dbAddNonCompanyJobpost(jobPost)),
        getJobpostAsNonCompanyData: (userId: string) => dispatch(jobActions.dbGetJobpostAsNonCompanyById(userId))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobAsNonCompanyComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid], {})

    return {
        translate: getTranslate(state.get('locale')),
        fullName: user.fullName,
        userId: uid
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((JobAsNonCompanyComponent as any) as any)
