export interface IJobAsNonCompanyComponentState {

    /**
     * employer name
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    name: string

    /**
     * employer phone
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    phone: string

    /**
     * employer designation
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    designation: string

    /**
     * employer email
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    email: string

    /**
     * Avatar address of post owner
     *
     * @type {string}
     * @memberof JobAsCompany
     */
    jobTitle: string

    /**
     * Post view count
     *
     * @type {number}
     * @memberof JobAsCompany
     */
     location: string

    /**
     * Last post edit date
     *
     * @type {number}
     * @memberof JobAsCompany
     */
     skill: string

    /**
     * Post tags
     *
     * @type {string[]}
     * @memberof JobAsCompany
     */
      jobType: string

    /**
     * Numeber of comment on the post
     *
     * @type {number}
     * @memberof JobAsCompany
     */
      eduRequirement: string

    /**
     * The post creation date
     *
     * @type {number}
     * @memberof JobAsCompany
     */
      industry: string

    /**
     * The address of image on the post
     *
     * @type {string}
     * @memberof JobAsCompany
     */
      levelOfSkill?: string

    /**
     * Post image full path
     *
     * @type {string}
     * @memberof JobAsCompany
     */
      salary?: string

    /**
     * The adress of video on the post
     *
     * @type {string}
     * @memberof JobAsCompany
     */
      noOfVacancy?: string

    /**
     * If writing comment is disabled {true} or not {false}
     *
     * @type {Boolean}
     * @memberof JobAsCompany
     */
      responsibilities: string
}
