// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components

// - Import API

// - Import actions
import * as jobActions from 'store/actions/jobActions'

// - Import actions
import {IJobAsNonCompanyComponentProps} from './IJobAsNonCompanyComponentProps'
import {IJobAsNonCompanyComponentState} from './IJobAsNonCompanyComponentState'
import {push} from 'react-router-redux'
import {JobAsNonCompany} from 'core/domain/jobs/jobAsNonCompany'

/**
 * Create component class
 */
export class JobAsNonCompanyItemComponent extends Component<IJobAsNonCompanyComponentProps, IJobAsNonCompanyComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: IJobAsNonCompanyComponentProps) {
        super(props)
        const {JobAsNonCompany} = props
        this.state = {
            isEdit: false,
            /**
             * education level
             */
            name: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('name') : '',
            /**
             * education degree name
             */
            industry: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('industry') : '',
            /**
             * education major subject
             */
            designation: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('designation') : '',
            /**
             * education major subject
             */
            phone: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('phone') : '',
            /**
             * education major subject
             */
            email: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('email') : '',
            /**
             * education major subject
             */
            location: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('location') : '',
            /**
             * education major subject
             */
            jobTitle: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('jobTitle') : '',
            /**
             * education major subject
             */
            skill: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('skill') : '',
            /**
             * education major subject
             */
            jobType: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('jobType') : '',
            /**
             * education major subject
             */
            eduRequirement: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('eduRequirement') : '',
            /**
             * education major subject
             */
            levelOfSkill: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('levelOfSkill') : '',
            /**
             * education major subject
             */
            salary: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('salary') : '',
            /**
             * education major subject
             */
            noOfVacancy: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('noOfVacancy') : '',
            /**
             * education major subject
             */
            responsibilities: this.props.edit && JobAsNonCompany ? JobAsNonCompany.get('responsibilities') : '',
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleForm = this.handleForm.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'name':
                this.setState({
                    name: value
                })
                break
            case 'phone':
                this.setState({
                    phone: value
                })
                break
            case 'designation':
                this.setState({
                    designation: value
                })
                break
            case 'email':
                this.setState({
                    email: value
                })
                break
            case 'jobTitle':
                this.setState({
                    jobTitle: value
                })
                break
            case 'location':
                this.setState({
                    location: value
                })
                break
            case 'skill':
                this.setState({
                    skill: value
                })
                break
            case 'jobType':
                this.setState({
                    jobType: value
                })
                break
            case 'eduRequirement':
                this.setState({
                    eduRequirement: value
                })
                break
            case 'industry':
                this.setState({
                    industry: value
                })
                break
            case 'levelOfSkill':
                this.setState({
                    levelOfSkill: value
                })
                break
            case 'salary':
                this.setState({
                    salary: value
                })
                break
            case 'noOfVacancy':
                this.setState({
                    noOfVacancy: value
                })
                break
            case 'responsibilities':
                this.setState({
                    responsibilities: value
                })
                break
            default:
        }
    }
    /**
     * Handle register form
     */
    handleForm = (evt: any) => {
        const {
            name,
            industry,
            designation,
            phone,
            email,
            location,
            jobTitle,
            skill,
            jobType,
            eduRequirement,
            levelOfSkill,
            salary,
            noOfVacancy,
            responsibilities,
        } = this.state
        const {
            jobAsNonCompanyService,
        } = this.props
        const jobpostAsNonCompany = {
            name: name,
            industry: industry,
            designation: designation,
            phone: phone,
            email: email,
            location: location,
            jobTitle: jobTitle,
            skill: skill,
            jobType: jobType,
            eduRequirement: eduRequirement,
            levelOfSkill: levelOfSkill,
            salary: salary,
            noOfVacancy: noOfVacancy,
            responsibilities: responsibilities,
        }
        jobAsNonCompanyService!(jobpostAsNonCompany, this.props.jobAsNonCompanyId)
        this.setState({
            isEdit: false
        })
    }

    handleEdit = () => {
        this.setState({
            isEdit: true
        })
    }
    handleEditCancel = () => {
        this.setState({
            isEdit: false
        })
    }

    handleDelete = (evt: any, jobAsCompanyId?: string) => {
        this.props.delete!(jobAsCompanyId)
    }

    render() {
        const {
            translate,
            name,
            phone,
            designation,
            email,
            jobTitle,
            location,
            skill,
            jobType,
            eduRequirement,
            industry,
            levelOfSkill,
            salary,
            noOfVacancy,
            responsibilities,
        } = this.props
        return (
            <>{this.state.isEdit ?
                <div className='ui-block-content'>
                    <form>
                        <div className='row'>
                            <div className='ui-block-title h5'>
                                Employer Details
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Contact Person Name</label>
                                    <input className='form-control'
                                           name='name'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={name ? name : ''} />
                                    <span className='material-input' /></div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Mobile</label>
                                    <input className='form-control'
                                           name='phone'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={phone ? phone : ''} />
                                    <span className='material-input' /></div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Contact Person's Designation</label>
                                    <input className='form-control'
                                           name='designation'
                                           type='text'
                                           onChange={this.handleInputChange}
                                           defaultValue={designation ? designation : ''} />
                                    <span className='material-input' /></div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Email</label>
                                    <input className='form-control'
                                           name='email'
                                           onChange={this.handleInputChange}
                                           type='email'
                                           defaultValue={email ? email : ''} />
                                    <span className='material-input' /></div>
                            </div>

                            <div className='ui-block-title h5'>
                                Job Details
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Job Title</label>
                                    <input className='form-control'
                                           name='jobTitle'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={jobTitle ? jobTitle : ''} />
                                    <span className='material-input' /></div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Job-location</label>
                                    <input className='form-control'
                                           name='location'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={location ? location : ''}  />
                                    <span className='material-input' /></div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Skill</label>
                                    <input className='form-control'
                                           name='skill'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={skill ? skill : ''}  />
                                    <span className='material-input' /></div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Job Type</label>
                                    <input className='form-control'
                                           name='jobType'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={jobType ? jobType : ''}  />
                                    <span className='material-input' /></div>

                                <div className='form-group label-floating'>
                                    <label className='control-label'>Educational Requirements</label>
                                    <input className='form-control'
                                           name='eduRequirement'
                                           onChange={this.handleInputChange}
                                           defaultValue={eduRequirement ? eduRequirement : ''}  />
                                    <span className='material-input' /></div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Industry</label>
                                    <input className='form-control'
                                           name='industry'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={industry ? industry : ''}  />
                                    <span className='material-input' /></div>
                            </div>
                            <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Level of skill</label>
                                    <input className='form-control'
                                           name='levelOfSkill'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={levelOfSkill ? levelOfSkill : ''}  />
                                    <span className='material-input' /></div>
                            </div>
                            <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Salary</label>
                                    <input className='form-control'
                                           name='salary'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={salary ? salary : ''}  />
                                    <span className='material-input' /></div>
                            </div>
                            <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>No. of vacancy</label>
                                    <input className='form-control'
                                           name='noOfVacancy'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue={noOfVacancy ? noOfVacancy : ''} />
                                    <span className='material-input' /></div>
                            </div>
                            <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Responsibilities</label>
                                    <textarea className='form-control'
                                              onChange={this.handleInputChange}
                                              name='responsibilities'
                                              defaultValue={responsibilities ? responsibilities : ''}  />
                                    <span className='material-input' /></div>
                            </div>

                            <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                <a href={'javascript:(0)'}
                                   onClick={this.handleForm}
                                   className='btn btn-primary btn-lg full-width'>Save</a>
                            </div>
                        </div>
                    </form>
                </div> :
                <div className='row'>
                    <div className='col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'>
                        <li onDoubleClick={this.handleEdit}>
                            <span className='text'>{name}</span>
                            <span className='text'>{designation}</span>
                            <span className='text'>{location}</span>
                            <span className='text'>{industry}</span>
                            <span className='title'>{phone}</span>
                            <span className='date'>{email}</span>
                            <span className='date'>{jobTitle}</span>
                            <span className='text'>{skill}</span>
                            <span className='title'>{jobType}</span>
                            <span className='date'>{eduRequirement}</span>
                            <span className='date'>{levelOfSkill}</span>
                            <span className='text'>{salary}</span>
                            <span className='title'>{noOfVacancy}</span>
                            <span className='date'>{responsibilities}</span>
                        </li>
                    </div>
                    <div className='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12'>
                        <div className='more'>
                            <svg className='olymp-three-dots-icon'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                            </svg>
                            <ul className='more-dropdown'>
                                <li><a href={'javascript:(0)'} onClick={this.handleEdit}>Edit</a></li>
                                <li><a href={'javascript:(0)'}
                                       onClick={(evt: any) => this.handleDelete(evt, this.props.jobAsNonCompanyId)}>Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            }
            </>

        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobAsNonCompanyComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url)),
        jobAsNonCompanyService: (jobAsNonCompanyService: JobAsNonCompany, postId: string) => dispatch(jobActions.dbUpdateJobpostAsNonCompany(jobAsNonCompanyService, postId)),
        delete: (jobPostId: string) => dispatch(jobActions.dbDeleteJobpostAsNonCompany(jobPostId)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobAsNonCompanyComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((JobAsNonCompanyItemComponent as any) as any)
