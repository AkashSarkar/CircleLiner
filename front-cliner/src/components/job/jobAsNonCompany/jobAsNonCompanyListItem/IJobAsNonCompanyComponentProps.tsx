
import {Map} from 'immutable'
import {Profile} from 'core/domain/users'
import {JobAsNonCompany} from 'core/domain/jobs/jobAsNonCompany'

export interface IJobAsNonCompanyComponentProps {
    /**
     * User education post id
     */
    id?: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    name: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    industry: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    designation: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    phone: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    email: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    location: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobTitle: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    skill: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobType: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    eduRequirement: string

    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    levelOfSkill: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    salary: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    noOfVacancy: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    responsibilities: string
    /**
     * education id
     */
    jobAsNonCompanyId: string
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
    /**
     * Job as company model
     */
    JobAsNonCompany?: Map<string, any>
    /**
     * Save a post
     *
     * @memberof IPostWriteComponentProps
     */
    jobAsNonCompanyService?: (jobAsNonCompanyService: JobAsNonCompany, postId: string) => any
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IEditProfileComponentProps
     */
    edit?: boolean
    /**
     * User's banner URL
     *
     * @type {string}
     * @memberof IEditProfileComponentState
     */
    open?: boolean

    /**
     * Styles
     */
    classes?: any
    /**
     * Current locale language
     */
    currentLanguage?: string
    /**
     * match
     */
    match?: any
    /**
     * user id
     */
    userId?: string
    /**
     * delete education by education id
     */
    delete?: (educationId?: string) => any
    /**
     * user authentication
     */
    isAuthed: boolean
}
