
export interface IJobAsNonCompanyComponentState {

    isEdit: boolean
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    name: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    industry: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    designation: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    phone: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    email: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    location: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobTitle: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    skill: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobType: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    eduRequirement: string

    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    levelOfSkill: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    salary: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    noOfVacancy: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    responsibilities: string

    edit?: boolean
}
