// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import JobAsNonCompanyItemComponent from '../jobAsNonCompanyListItem/JobAsNonCompanyItemComponent'

// - Import Domains
import {JobAsNonCompany} from 'core/domain/jobs/jobAsNonCompany'

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import {IJobAsNonCompanyListComponentProps} from './IJobAsNonCompanyListComponentProps'
import {IJobAsNonCompanyListComponentState} from './IJobAsNonCompanyListComponentState'

/**
 * Create component class
 */
export class JobAsNonCompanyListComponent extends Component<IJobAsNonCompanyListComponentProps, IJobAsNonCompanyListComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobAsNonCompanyListComponentProps) {
        super(props)
        this.state = {}

        this.jobAsNonCompanyList = this.jobAsNonCompanyList.bind(this)
    }

    jobAsNonCompanyList = () => {
        const item: any[] = []
        const parsedJobAsNonCompany: JobAsNonCompany[] = []
        console.log( this.props.jobAsNonCompany!)
        this.props.jobAsNonCompany!.forEach((jobAsNonCompany) => {
            parsedJobAsNonCompany.push({
                ...Map(jobAsNonCompany!).toJS()
            })
        })
        parsedJobAsNonCompany.map((jobAsNonCompany: JobAsNonCompany) => {
            item.push(
            <JobAsNonCompanyItemComponent key={jobAsNonCompany.id!}
                                       isAuthed={this.props.isAuthed!}
                                       jobAsNonCompanyId={jobAsNonCompany.id!}
                                       name={jobAsNonCompany.name}
                                       industry={jobAsNonCompany.industry!}
                                       designation={jobAsNonCompany.designation}
                                       phone={jobAsNonCompany.phone!}
                                       email={jobAsNonCompany.email!}
                                       location={jobAsNonCompany.location}
                                       jobTitle={jobAsNonCompany.jobTitle!}
                                       skill={jobAsNonCompany.skill!}
                                       jobType={jobAsNonCompany.jobType!}
                                       eduRequirement={jobAsNonCompany.eduRequirement!}
                                       levelOfSkill={jobAsNonCompany.levelOfSkill!}
                                       salary={jobAsNonCompany.salary!}
                                       noOfVacancy={jobAsNonCompany.noOfVacancy!}
                                       responsibilities={jobAsNonCompany.responsibilities!}
            />
        )
        })
        return item
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <div className='ui-block'>
                <div className='ui-block-title'>
                    <h6 className='title'>Non-Company Jobposts</h6>
                </div>
                <div className='ui-block-content'>
                    <div className='row'>
                        <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                            <ul className='widget w-personal-info item-block'>
                                {this.jobAsNonCompanyList()}
                                <hr/>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobAsNonCompanyListComponentProps) => {
    return {
        loadPeople: (page: number, limit: number) => dispatch(userActions.dbGetPeopleInfo(page, limit))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobAsNonCompanyListComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const jobAsNonCompany = state.getIn(['jobpost', 'jobpostAsNonCompany', uid])
    console.log('sadasd')
    console.log(jobAsNonCompany)
    return {
        translate: getTranslate(state.get('locale')),
        jobAsNonCompany: jobAsNonCompany ? jobAsNonCompany : [],

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((JobAsNonCompanyListComponent as any) as any)
