import {Map} from 'immutable'

export interface IEmployerPreferencesComponentState {
    /**
     * Writing post box is open {true} or not false
     *
     * @type {boolean}
     * @memberof ISettingsSidebarComponentState
     */
    openJobModal: boolean
    employerPreference: string
    wantForWork: string
    location: string
    role: string
    industry: string
    preferToWork: string
    remoteWork: string
    workFrom: string
    workTo: string
    jobType?: any
}
