import {PersonalInfo} from 'core/domain/users/personalInfo'
import {Map} from 'immutable'
import {EmployerPreferences} from 'core/domain/jobs/employerPreferences'

export interface IEmployerPreferencesComponentProps {

    open?: boolean

    employerPreferences?: (employerPreferences: EmployerPreferences) => any

    userId: string

    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    currentLanguage?: string

}
