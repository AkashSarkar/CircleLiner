import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'
import {Map} from 'immutable'
import {Profile} from 'core/domain/users'

export interface IJobAsCompanyComponentProps {

    open?: boolean
    /**
     * url user id
     */
    userId?: string
    /**
     * checking request user id with authorize user id
     */
    isAuthed?: boolean
    /**
     * Personal information model
     */
    JobAsCompany?: Map<string, any>

    /**
     * Save a post
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    jobPostAsCompany?: (jobPost: JobAsCompany) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    currentLanguage?: string

    /**
     * load current user education
     */
    getJobpostAsCompanyData?: (userId: string) => any

    /**
     * match
     */
    match?: any

    /**
     * Rediret to another route
     *
     * @memberof IPeopleComponentProps
     */
    goTo?: (url: string) => any

    /**
     * Set title of top bar
     *
     * @memberof IPeopleComponentProps
     */
    setHeaderTitle?: (title: string) => any

    user: Profile
}
