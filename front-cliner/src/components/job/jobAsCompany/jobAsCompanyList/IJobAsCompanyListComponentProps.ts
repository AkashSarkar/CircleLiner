import { Profile } from 'core/domain/users/index'
import {Map} from 'immutable'
import {Education} from 'core/domain/users/education'
export interface IJobAsCompanyListComponentProps {
    /**
     * education level
     */
    educationLevel?: string
    /**
     * education degree
     */
    degreeTitle?: string
    /**
     * education major subject
     */
    group?: string
    /**
     * institute name
     */
    instituteName?: string
    /**
     * education result
     */
    result?: string
    /**
     * passing year
     */
    passingYear?: string
    /**
     * educational achievements
     */
    achievement?: string
    /**
     * education period
     */
    duration?: string
    /**
     * User educations
     *
     * @type {Profile}
     * @memberof IProfessionListComponentProps
     */
    jobAsCompany?: Map<string, Education>
    /**
     * class
     *
     * @type {Profile}
     * @memberof IProfessionListComponentProps
     */
     classes?: any
    /**
     * Translate to locale string
     */
     translate?: (state: any) => any
    /**
     * Current locale language
     */
     currentLanguage?: string
    /**
     * authenticate
     */
     isAuthed?: boolean
    /**
     * userId
     */
    userId?: string
    /**
     * match
     */
    match?: any
}
