// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
// - Import app components
import JobAsCompanyItemComponent from '../jobAsCompanyListItem/JobAsCompanyItemComponent'

// - Import Domains
import {Education} from 'core/domain/users/education'

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import {IJobAsCompanyListComponentProps} from './IJobAsCompanyListComponentProps'
import {IJobAsCompanyListComponentState} from './IJobAsCompanyListComponentState'
import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'

/**
 * Create component class
 */
export class JobAsCompanyListComponent extends Component<IJobAsCompanyListComponentProps, IJobAsCompanyListComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobAsCompanyListComponentProps) {
        super(props)
        this.state = {}

        this.jobAsCompanyList = this.jobAsCompanyList.bind(this)
    }

    jobAsCompanyList = () => {
        const item: any[] = []
        const parsedJobAsCompany: JobAsCompany[] = []
        this.props.jobAsCompany!.forEach((jobAsCompany) => {
            parsedJobAsCompany.push({
                ...Map(jobAsCompany!).toJS()
            })
        })
        parsedJobAsCompany.map((jobAsCompany: JobAsCompany) => {
            item.push(
            <JobAsCompanyItemComponent key={jobAsCompany.id!}
                                       isAuthed={this.props.isAuthed!}
                                       jobAsCompanyId={jobAsCompany.id!}
                                       companyName={jobAsCompany.companyName!}
                                       industry={jobAsCompany.industry!}
                                       altCompanyName={jobAsCompany.altCompanyName!}
                                       bunsinessDescription={jobAsCompany.bunsinessDescription!}
                                       companyAddress={jobAsCompany.companyAddress!}
                                       phone={jobAsCompany.phone!}
                                       email={jobAsCompany.email!}
                                       website={jobAsCompany.website!}

                                       jobTitle={jobAsCompany.jobTitle!}
                                       skill={jobAsCompany.skill!}
                                       jobType={jobAsCompany.jobType!}
                                       eduRequirement={jobAsCompany.eduRequirement!}
                                       levelOfSkill={jobAsCompany.levelOfSkill!}
                                       salary={jobAsCompany.salary!}
                                       noOfVacancy={jobAsCompany.noOfVacancy!}
                                       responsibilities={jobAsCompany.responsibilities!}
            />
        )
        })
        return item
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <div className='ui-block'>
                <div className='ui-block-title'>
                    <h6 className='title'>Jobposts</h6>
                </div>
                <div className='ui-block-content'>
                    <div className='row'>
                        <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                            <ul className='widget w-personal-info item-block'>
                                {this.jobAsCompanyList()}
                                <hr/>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobAsCompanyListComponentProps) => {
    return {
        loadPeople: (page: number, limit: number) => dispatch(userActions.dbGetPeopleInfo(page, limit))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobAsCompanyListComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const jobAsCompany = state.getIn(['jobpost', 'jobpostAsCompany', uid])
    return {
        translate: getTranslate(state.get('locale')),
        jobAsCompany: jobAsCompany ? jobAsCompany : [],

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((JobAsCompanyListComponent as any) as any)
