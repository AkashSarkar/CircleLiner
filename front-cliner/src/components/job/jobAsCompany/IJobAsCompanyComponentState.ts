export interface IJobAsCompanyComponentState {
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    companyName: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    industry: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    altCompanyName: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    bunsinessDescription: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    companyAddress: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    phone: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    email: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    website: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobTitle: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    skill: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    jobType: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    eduRequirement: string

    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    levelOfSkill: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    salary: string
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    noOfVacancy: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof IJobAsCompanyComponentState
     */
    responsibilities: string

    tabIndex: number
    postJobAs: string
    individualName: string
    individualEmail: string
    individualPhone: string
    designation: string
}
