// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import JobAsCompanyListComponent from './jobAsCompanyList/JobAsCompanyListComponent'

import {IJobAsCompanyComponentProps} from './IJobAsCompanyComponentProps'
import {IJobAsCompanyComponentState} from './IJobAsCompanyComponentState'

// - Import API

// - Import actions
import * as jobActions from 'store/actions/jobActions'

// - Import layouts
import HomeView from 'layouts/views/homeView'
import {JobAsCompany} from 'core/domain/jobs/jobAsCompany'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import {Profile} from 'core/domain/users'

const TabContainer = (props: any) => {
    return (
        <Typography component='div' style={{padding: 8 * 3}}>
            {props.children}
        </Typography>
    )
}

/**
 * Create component class
 */
export class JobAsCompanyComponent extends Component<IJobAsCompanyComponentProps, IJobAsCompanyComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IJobAsCompanyComponentProps) {
        super(props)
        const {user} = props
        this.state = {
            individualName: user ? user.fullName : '',
            individualEmail: user.email ? user.email : '',
            individualPhone: '',
            designation: '',
            companyName: '',

            industry: '',

            altCompanyName: '',

            bunsinessDescription: '',

            companyAddress: '',

            phone: '',

            email: '',

            website: '',

            jobTitle: '',

            skill: '',

            jobType: '',

            eduRequirement: '',

            levelOfSkill: '',

            salary: '',

            noOfVacancy: '',

            responsibilities: '',

            tabIndex: 0,
            postJobAs: 'Company'
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    componentWillReceiveProps(nextProps: IJobAsCompanyComponentProps) {

        if (!nextProps.open) {
            const {JobAsCompany} = this.props
            this.setState({
                /**
                 * User's firstname
                 */
                companyName: JobAsCompany ? JobAsCompany.get('companyName', '') : '',

                /**
                 * User's fullname
                 */
                industry: JobAsCompany ? JobAsCompany.get('industry', '') : '',

                /**
                 * User's fullname
                 */
                altCompanyName: JobAsCompany ? JobAsCompany.get('altCompanyName', '') : '',

                /**
                 * User's email
                 */
                bunsinessDescription: JobAsCompany ? JobAsCompany.get('bunsinessDescription', '') : '',

                /**
                 * User's date of birth
                 */
                companyAddress: JobAsCompany ? JobAsCompany.get('companyAddress', '') : '',

                /**
                 * User's website
                 */
                phone: JobAsCompany ? JobAsCompany.get('phone', '') : '',

                /**
                 * User's phone
                 */
                email: JobAsCompany ? JobAsCompany.get('email', '') : '',

                /**
                 * User's birth place
                 */
                website: JobAsCompany ? JobAsCompany.get('website', '') : '',

                /**
                 * User's gender
                 */
                jobTitle: JobAsCompany ? JobAsCompany.get('jobTitle', '') : '',

                /**
                 * User's marital status
                 */
                skill: JobAsCompany ? JobAsCompany.get('skill', '') : '',

                /**
                 * User's date of birth
                 */
                jobType: JobAsCompany ? JobAsCompany.get('jobType', '') : '',

                /**
                 * User's website
                 */
                eduRequirement: JobAsCompany ? JobAsCompany.get('eduRequirement', '') : '',

                /**
                 * User's phone
                 */
                levelOfSkill: JobAsCompany ? JobAsCompany.get('levelOfSkill', '') : '',

                /**
                 * User's birth place
                 */
                salary: JobAsCompany ? JobAsCompany.get('salary', '') : '',

                /**
                 * User's gender
                 */
                noOfVacancy: JobAsCompany ? JobAsCompany.get('noOfVacancy', '') : '',

                /**
                 * User's marital status
                 */
                responsibilities: JobAsCompany ? JobAsCompany.get('responsibilities', '') : '',
            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'individualName':
                this.setState({
                    individualName: value
                })
                break
            case 'individualEmail':
                this.setState({
                    individualEmail: value
                })
                break
            case 'individualPhone':
                this.setState({
                    individualPhone: value
                })
                break
            case 'designation':
                this.setState({
                    designation: value
                })
                break
            case 'companyName':
                this.setState({
                    companyName: value
                })
                break
            case 'industry':
                this.setState({
                    industry: value
                })
                break
            case 'altCompanyName':
                this.setState({
                    altCompanyName: value
                })
                break
            case 'bunsinessDescription':
                this.setState({
                    bunsinessDescription: value
                })
                break
            case 'companyAddress':
                this.setState({
                    companyAddress: value
                })
                break
            case 'phone':
                this.setState({
                    phone: value
                })
                break
            case 'email':
                this.setState({
                    email: value
                })
                break
            case 'website':
                this.setState({
                    website: value
                })
                break
            case 'jobTitle':
                this.setState({
                    jobTitle: value
                })
                break
            case 'skill':
                this.setState({
                    skill: value
                })
                break
            case 'jobType':
                this.setState({
                    jobType: value
                })
                break
            case 'eduRequirement':
                this.setState({
                    eduRequirement: value
                })
                break
            case 'levelOfSkill':
                this.setState({
                    levelOfSkill: value
                })
                break
            case 'salary':
                this.setState({
                    salary: value
                })
                break
            case 'noOfVacancy':
                this.setState({
                    noOfVacancy: value
                })
                break
            case 'responsibilities':
                this.setState({
                    responsibilities: value
                })
                break
            default:
        }
    }

    /**
     * Handle register form
     */
    handleForm = () => {
        const {
            individualName,
            designation,
            individualEmail,
            individualPhone,
            companyName,
            industry,
            altCompanyName,
            bunsinessDescription,
            companyAddress,
            phone,
            email,
            website,
            jobTitle,
            skill,
            jobType,
            eduRequirement,
            levelOfSkill,
            salary,
            noOfVacancy,
            responsibilities,
            postJobAs
        } = this.state
        const {
            jobPostAsCompany,
        } = this.props
        let company = {
            companyName: companyName,
            industry: industry,
            altCompanyName: altCompanyName,
            bunsinessDescription: bunsinessDescription,
            companyAddress: companyAddress,
            phone: phone,
            email: email,
            website: website,
            jobTitle: jobTitle,
            skill: skill,
            jobType: jobType,
            eduRequirement: eduRequirement,
            levelOfSkill: levelOfSkill,
            salary: salary,
            noOfVacancy: noOfVacancy,
            responsibilities: responsibilities,
            postJobAs: postJobAs
        }
        let individual = {
            individualName: individualName,
            individualPhone: individualPhone,
            individualEmail: individualEmail,
            designation: designation,
            jobTitle: jobTitle,
            skill: skill,
            jobType: jobType,
            eduRequirement: eduRequirement,
            levelOfSkill: levelOfSkill,
            salary: salary,
            noOfVacancy: noOfVacancy,
            responsibilities: responsibilities,
            postJobAs: postJobAs
        }
        {
            this.state.tabIndex === 0 ? jobPostAsCompany!(company) : jobPostAsCompany!(individual)
        }
    }

    handleChangeTab = (event: any, value: any) => {
        this.setState({tabIndex: value})
        switch (value) {
            case 0:
                this.setState({
                    postJobAs: 'Company'
                })
                break
            case 1:
                this.setState({
                    postJobAs: 'Individual'
                })
                break
            default:
                break

        }
    }

    componentWillMount() {
        const {getJobpostAsCompanyData, userId} = this.props
        getJobpostAsCompanyData!(userId!)
    }

    componentDidMount() {

    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        const {
            individualName,
            individualEmail,
            individualPhone,
            designation,
            companyName,
            industry,
            altCompanyName,
            bunsinessDescription,
            companyAddress,
            phone,
            email,
            website,
            jobTitle,
            skill,
            jobType,
            eduRequirement,
            levelOfSkill,
            salary,
            noOfVacancy,
            responsibilities,
            tabIndex
        } = this.state
        return (
            <HomeView>
                <div className='container'>
                    {/*<JobAsCompanyListComponent userId={this.props.userId!}/>*/}
                    <div className='ui-block'>

                        <div className='ui-block-content'>
                            {/* Form Favorite Page Information */}
                            <form>
                                <div className='row'>

                                    <div className='ui-block-title h5'>
                                        Job Details
                                    </div>
                                    <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Job Title</label>
                                            <input className='form-control'
                                                   name='jobTitle'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Skill</label>
                                            <textarea className='form-control'
                                                      name='skill'
                                                      onChange={this.handleInputChange}
                                                      defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Job Type</label>
                                            <input className='form-control'
                                                   name='jobType'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>

                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Educational Requirements</label>
                                            <textarea className='form-control'
                                                      name='eduRequirement'
                                                      onChange={this.handleInputChange}
                                                      defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Level of skill</label>
                                            <input className='form-control'
                                                   name='levelOfSkill'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Salary</label>
                                            <input className='form-control'
                                                   name='salary'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>No. of vacancy</label>
                                            <input className='form-control'
                                                   name='noOfVacancy'
                                                   onChange={this.handleInputChange}
                                                   type='text'
                                                   defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>
                                    <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                        <div className='form-group label-floating'>
                                            <label className='control-label'>Responsibilities</label>
                                            <textarea className='form-control'
                                                      name='responsibilities'
                                                      onChange={this.handleInputChange}
                                                      defaultValue=''/>
                                            <span className='material-input'/></div>
                                    </div>

                                    <div className='ui-block-title h5'>
                                        <AppBar position='static' color='default'>
                                            <Tabs
                                                onChange={this.handleChangeTab}
                                                value={tabIndex} centered
                                                textColor='primary'
                                            >
                                                <Tab label={'As Company'}/>
                                                <Tab label={'As Individual'}/>
                                            </Tabs>
                                        </AppBar>
                                        {tabIndex === 0 && <TabContainer>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Company Name</label>
                                                    <input className='form-control'
                                                           name='companyName'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue=''/>
                                                    <span className='material-input'/></div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Industry</label>
                                                    <input className='form-control'
                                                           name='industry'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue=''/>
                                                    <span className='material-input'/></div>
                                            </div>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Alternative Company Name</label>
                                                    <input className='form-control'
                                                           name='altCompanyName'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue=''/>
                                                    <span className='material-input'/></div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Business Description</label>
                                                    <input className='form-control'
                                                           name='bunsinessDescription'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue=''/>
                                                    <span className='material-input'/></div>
                                            </div>
                                            <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Company Address</label>
                                                    <textarea className='form-control'
                                                              name='companyAddress'
                                                              onChange={this.handleInputChange}
                                                              defaultValue=''/>
                                                    <span className='material-input'/></div>
                                            </div>
                                            <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Phone</label>
                                                    <input className='form-control'
                                                           name='phone'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue=''/>
                                                    <span className='material-input'/></div>
                                            </div>
                                            <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Email</label>
                                                    <input className='form-control'
                                                           name='email'
                                                           onChange={this.handleInputChange}
                                                           type='email'
                                                           defaultValue=''/>
                                                    <span className='material-input'/></div>
                                            </div>
                                            <div className='col col-lg-4 col-md-4 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Website</label>
                                                    <input className='form-control'
                                                           name='website'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue=''/>
                                                    <span className='material-input'/></div>
                                            </div>
                                        </TabContainer>}
                                        {tabIndex === 1 && <TabContainer>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Contact Person Name</label>
                                                    <input className='form-control'
                                                           name='individualName'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue={individualName}/>
                                                    <span className='material-input'/></div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Mobile</label>
                                                    <input className='form-control'
                                                           name='individualPhone'
                                                           onChange={this.handleInputChange}
                                                           type='text'
                                                           defaultValue={individualPhone}/>
                                                    <span className='material-input'/></div>
                                            </div>
                                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Contact Person's
                                                        Designation</label>
                                                    <input className='form-control'
                                                           name='designation'
                                                           type='text'
                                                           onChange={this.handleInputChange}
                                                           defaultValue={designation}/>
                                                    <span className='material-input'/></div>
                                                <div className='form-group label-floating'>
                                                    <label className='control-label'>Email</label>
                                                    <input className='form-control'
                                                           name='individualEmail'
                                                           onChange={this.handleInputChange}
                                                           type='email'
                                                           defaultValue={individualEmail}/>
                                                    <span className='material-input'/></div>
                                            </div>
                                        </TabContainer>}
                                    </div>

                                    <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                        <a href={'javascript:(0)'}
                                           onClick={this.handleForm}
                                           className='btn btn-primary btn-lg full-width'>Save</a>
                                    </div>
                                </div>
                            </form>
                            {/* ... end Form Favorite Page Information */}
                        </div>
                    </div>
                </div>
            </HomeView>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IJobAsCompanyComponentProps) => {
    return {
        jobPostAsCompany: (jobPost: JobAsCompany) => dispatch(jobActions.dbAddCompanyJobpost(jobPost)),
        getJobpostAsCompanyData: (userId: string) => dispatch(jobActions.dbGetJobpostAsCompanyById(userId))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IJobAsCompanyComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid], {}) as Profile
    return {
        translate: getTranslate(state.get('locale')),
        userId: uid,
        user: user
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((JobAsCompanyComponent as any) as any)
