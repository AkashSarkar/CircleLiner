import {Map} from 'immutable'
import {Job} from 'core/domain/jobs/job'

export interface IJobStremComponentProps {
    /**
     * Load the data for stream
     */
    loadStream?: (page: number, limit: number) => any

    /**
     * If there is more post {true} or not {false}
     */
    hasMorePosts?: boolean
    /**
     * Posts for stream
     *
     * @type {{[jobId: string]: Job}}
     * @memberof IJobStreamComponentProps
     */
    jobs:  Map<string, Map<string, any>>
    /**
     * Router match property
     */
    match?: any
    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}