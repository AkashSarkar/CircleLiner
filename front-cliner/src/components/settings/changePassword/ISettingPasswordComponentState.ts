
export interface ISettingPasswordComponentState {
    /**
     * Old Password input value
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    oldPasswordInput: string

    /**
     * Old Password input error text
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    oldPasswordInputError: string
    /**
     * Password input value
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    passwordInput: string

    /**
     * Password input error text
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    passwordInputError: string

    /**
     * Confirm input value
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    confirmInput: string

    /**
     * Confirm input error
     *
     * @type {string}
     * @memberof ISettingComponentState
     */
    confirmInputError: string
}
