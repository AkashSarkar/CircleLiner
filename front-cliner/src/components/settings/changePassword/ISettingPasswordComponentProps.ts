export interface ISettingPasswordComponentProps {
    /**
     * Login user
     *
     * @memberof ISettingComponentProps
     */
    updatePassword?: (email: string, oldPassword: string, newPassword: string) => any

    /**
     * Redirect to home page
     *
     * @memberof ISettingComponentProps
     */
    homePage?: () => void

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any, param?: {}) => any
}
