// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {push} from 'react-router-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import ImgCover from 'components/profile/imgCover/index'
import UserAvatarComponent from 'components/userAvatar/index'
import ImageGallery from 'components/imageGallery/index'
import AppDialogTitle from 'layouts/dialogTitle/index'
import AppInput from 'layouts/appInput/index'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as authorizeActions from 'src/store/actions/authorizeActions'

import {ISettingPasswordComponentProps} from './ISettingPasswordComponentProps'
import {ISettingPasswordComponentState} from './ISettingPasswordComponentState'
import { NavLink } from 'react-router-dom'

// - Import layouts
import Setting from 'layouts/setting/index'

const styles = (theme: any) => ({
    dialogTitle: {
        padding: 0
    },
    dialogContentRoot: {
        maxHeight: 400,
        minWidth: 330,
        [theme.breakpoints.down('xs')]: {
            maxHeight: '100%',
        }

    },
    fullPageXs: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '100%',
            margin: 0
        }
    },
    fixedDownStickyXS: {
        [theme.breakpoints.down('xs')]: {
            position: 'fixed',
            bottom: 0,
            right: 0,
            background: 'white',
            width: '100%'
        }
    },
    bottomPaperSpace: {
        height: 16,
        [theme.breakpoints.down('xs')]: {
            height: 90
        }
    },
    box: {
        padding: '0px 24px 0px',
        display: 'flex'

    },
    bottomTextSpace: {
        marginBottom: 15
    },
    dayPicker: {
        width: '100%',
        padding: '13px 0px 8px'
    }
})

/**
 * Create component class
 */
export class SettingPasswordComponent extends Component<ISettingPasswordComponentProps, ISettingPasswordComponentState> {

    styles = {
        avatar: {
            border: '2px solid rgb(255, 255, 255)'
        },
        paper: {
            width: '90%',
            height: '100%',
            margin: '0 auto',
            display: 'block'
        },
        title: {
            padding: '24px 24px 20px 24px',
            font: '500 20px Roboto,RobotoDraft,Helvetica,Arial,sans-serif',
            display: 'flex',
            wordWrap: 'break-word',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            flexGrow: 1
        },
        actions: {
            display: 'flex',
            justifyContent: 'flex-end',
            padding: '24px 24px 20px'
        },
        updateButton: {
            marginLeft: '10px'
        },
        dialogGallery: {
            width: '',
            maxWidth: '530px',
            borderRadius: '4px'
        },
        iconButtonSmall: {},
        iconButton: {}

    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISettingPasswordComponentProps) {
        super(props)
        this.state = {
            oldPasswordInput: '',
            oldPasswordInputError: '',
            passwordInput: '',
            passwordInputError: '',
            confirmInput: '',
            confirmInputError: ''
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
    }

    /**
     * Handle data on input change
     * @param  {event} evt is an event of inputs of element on change
     */
    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'oldPasswordInput':
                this.setState({
                    oldPasswordInputError: ''
                })
                break
            case 'passwordInput':
                this.setState({
                    passwordInputError: ''
                })
                break
            case 'confirmInput':
                this.setState({
                    confirmInputError: '',
                    passwordInputError: ''
                })

                break
            default:

        }
    }

    /**
     * Handle register form
     */
    handleForm = () => {
        const {translate} = this.props
        let error = false
        if (this.state.oldPasswordInput === '') {
            console.log('old password')
            this.setState({
                oldPasswordInputError: translate!('changePassword.oldPasswordRequiredError')
            })
            error = true

        } else if (this.state.passwordInput === '') {
            this.setState({
                passwordInputError: translate!('changePassword.newPasswordRequiredError')
            })
            error = true

        } else if (this.state.confirmInput === '') {
            this.setState({
                confirmInputError: translate!('changePassword.confirmPasswordRequiredError')
            })
            error = true

        } else if (this.state.confirmInput !== this.state.passwordInput) {
            this.setState({
                confirmInputError: translate!('changePassword.confirmPasswordEqualNewPasswordError')
            })
            error = true

        }

        if (!error) {
            this.props.updatePassword!(
                this.state.oldPasswordInput,
                this.state.passwordInput,
                this.state.confirmInput
            )
        }

    }

    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {classes, translate} = this.props
        return (
            <Setting>
                <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Change Password</h6>
                    </div>
                    <div className='ui-block-content'>
                        {/* Change Password Form */}
                        <form>
                            <div className='row'>
                                <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                    <div className='form-group label-floating is-empty'>
                                        <label className='control-label'>{translate!('changePassword.oldPasswordLabel')}</label>
                                        <input className='form-control'
                                               onChange={this.handleInputChange}
                                               name='oldPasswordInput'
                                               type='password'/>
                                        <span className='material-input'/>
                                        <span style={{ color: 'red' }}>
                                            {this.state.oldPasswordInputError.trim() ? this.state.oldPasswordInputError : ''}
                                        </span>
                                    </div>
                                </div>
                                <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                    <div className='form-group label-floating  is-empty'>
                                        <label
                                            className='control-label'>{translate!('changePassword.newPasswordLabel')}</label>
                                        <input className='form-control'
                                               onChange={this.handleInputChange}
                                               name='passwordInput'
                                               type='password'/>
                                        <span style={{ color: 'red' }}>
                                            {this.state.passwordInputError.trim() ? this.state.passwordInputError : ''}
                                        </span>
                                    </div>
                                </div>
                                <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                    <div className='form-group label-floating is-empty'>
                                        <label
                                            className='control-label'>{translate!('changePassword.confirmPasswordLabel')}</label>
                                        <input className='form-control'
                                               onChange={this.handleInputChange}
                                               name='confirmInput'
                                               type='password'/>
                                        <span className='material-input'/>
                                        <span style={{ color: 'red' }}>
                                            {this.state.confirmInputError.trim() ? this.state.confirmInputError : ''}
                                        </span>
                                    </div>
                                </div>
                                <div className='remember col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                    <NavLink to='/resetPassword'
                                             className={'forgot'}>Forgot my Password</NavLink>
                                </div>
                                <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                    <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                       onClick={this.handleForm}>{translate!('changePassword.changePasswordButton')}</a>
                                </div>
                            </div>
                        </form>
                        {/* ... end Change Password Form */}
                    </div>
                </div>
            </Setting>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISettingPasswordComponentProps) => {
    return {
        updatePassword: (oldPassword: string, newPassword: string) => {
            dispatch(authorizeActions.dbUpdatePassword(oldPassword, newPassword))
        },
        homePage: () => {
            dispatch(push('/'))
        }

    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ISettingPasswordComponentProps) => {
    return {
        translate: getTranslate(state.get('locale'))
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(SettingPasswordComponent as any) as any)
