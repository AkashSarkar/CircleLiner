// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'

import {ISettingsAccountComponentProps} from './ISettingsAccountComponentProps'
import {ISettingsAccountComponentState} from './ISettingsAccountComponentState'
import {AccountSetting} from 'core/domain/users/accountSettings'

// - Import layouts
import Setting from 'layouts/setting'

/**
 * Create component class
 */
export class SettingsAccountComponent extends Component<ISettingsAccountComponentProps, ISettingsAccountComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISettingsAccountComponentProps) {
        super(props)
        const {accountSetting} = props
        this.state = {

            /**
             * Follower privacy
             */
            followPrivacy: accountSetting ? accountSetting.get('followPrivacy', '') : '',

            /**
             * Post privacy
             */
            postPrivacy: accountSetting ? accountSetting.get('postPrivacy', '') : '',

            /**
             * notification sound
             */
            notificationSound: false,

            /**
             * email notification sound
             */
            emailNotificationSound: false,

            /**
             * follower birthday notification sound
             */
            followerBirthdayNotificationSound: false,

            /**
             * chat sound
             */
            chatSound: false,
            accountDeactivate: false
        }

        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    componentWillReceiveProps(nextProps: ISettingsAccountComponentProps) {
        if (!nextProps.open) {
            const {accountSetting} = this.props
            this.setState({
                /**
                 * Follower privacy
                 */
                followPrivacy: accountSetting ? accountSetting.get('followPrivacy', '') : '',

                /**
                 * Post privacy
                 */
                postPrivacy: accountSetting ? accountSetting.get('postPrivacy', '') : '',

                /**
                 * notification sound
                 */
                notificationSound: false,

                /**
                 * email notification sound
                 */
                emailNotificationSound: false,

                /**
                 * follower birthday notification sound
                 */
                followerBirthdayNotificationSound: false,

                /**
                 * chat sound
                 */
                chatSound: false,
                /**
                 * Deactivate account
                 */
                accountDeactivate: false,

            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    /**
     * Handle input change in settings account form
     */
    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name
        this.setState({
            [name]: value
        })
        console.log(name)
        switch (name) {
            case 'followPrivacy':
                this.setState({
                    followPrivacy: value
                })
                break
            case 'postPrivacy':
                this.setState({
                    postPrivacy: value
                })
                break
            case 'optionsNotificationSoundboxes':
                this.setState({
                    notificationSound: !this.state.notificationSound
                })
                break
            case 'optionsEmailNotificationboxes':
                this.setState({
                    emailNotificationSound: !this.state.emailNotificationSound
                })
                break
            case 'optionsFollowerBirthdayboxes':
                this.setState({
                    followerBirthdayNotificationSound: !this.state.followerBirthdayNotificationSound
                })
                break
            case 'optionsChatboxes':
                this.setState({
                    chatSound: !this.state.chatSound
                })
                break
            case 'optionsAccountDeactivateboxes':
                this.setState({
                    accountDeactivate: !this.state.accountDeactivate
                })
                break
            default:
        }
    }

    /**
     * Handle submit of settings account form
     */
    handleForm = () => {
        const {
            followPrivacy,
            postPrivacy,
            notificationSound,
            emailNotificationSound,
            followerBirthdayNotificationSound,
            chatSound,
            accountDeactivate
        } = this.state

        const {
            post,
        } = this.props

        post!({
            followPrivacy: followPrivacy,
            postPrivacy: postPrivacy,
            notificationSound: notificationSound,
            emailNotificationSound: emailNotificationSound,
            followerBirthdayNotificationSound: followerBirthdayNotificationSound,
            chatSound: chatSound,
            accountDeactivate: accountDeactivate
        })
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <Setting>
                <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Account Settings</h6>
                    </div>
                    <div className='ui-block-content'>
                        {/* Personal Account Settings Form */}
                        <form>
                            <div className='row'>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>First Name</label>
                                        <input className='form-control'
                                               name='followPrivacy'
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue='public'/>
                                        <span className='material-input'/></div>
                                </div>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Last Name</label>
                                        <input className='form-control'
                                               name='followPrivacy'
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue='public'/>
                                        <span className='material-input'/></div>
                                </div>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Display Name</label>
                                        <input className='form-control'
                                               name='followPrivacy'
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue='public'/>
                                        <span className='material-input'/></div>
                                </div>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Who Can Follow You?</label>
                                        <input className='form-control'
                                               name='followPrivacy'
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue='public'/>
                                        <span className='material-input'/></div>
                                </div>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <div className='form-group label-floating'>
                                        <label className='control-label'>Who Can View Your Posts</label>
                                        <input className='form-control'
                                               name='postPrivacy'
                                               onChange={this.handleInputChange}
                                               type='text'
                                               defaultValue='public'/>
                                        <span className='material-input'/></div>
                                </div>
                                <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                    <div className='description-toggle'>
                                        <div className='description-toggle-content'>
                                            <div className='h6'>Notifications Sound</div>
                                            <p>A sound will be played each time you receive a new activity
                                                notification</p>
                                        </div>
                                        <div className='togglebutton'>
                                            <label>
                                                <input name='optionsNotificationSoundboxes'
                                                       type='checkbox'
                                                       onChange={this.handleInputChange}
                                                       defaultChecked/><span className='toggle'/>
                                            </label>
                                        </div>
                                    </div>
                                    <div className='description-toggle'>
                                        <div className='description-toggle-content'>
                                            <div className='h6'>Notifications Email</div>
                                            <p>We’ll send you an email to your account each time you receive a new
                                                activity notification</p>
                                        </div>
                                        <div className='togglebutton'>
                                            <label>
                                                <input name='optionsEmailNotificationboxes'
                                                       onChange={this.handleInputChange}
                                                       type='checkbox'
                                                       defaultChecked/>
                                                <span className='toggle'/>
                                            </label>
                                        </div>
                                    </div>
                                    <div className='description-toggle'>
                                        <div className='description-toggle-content'>
                                            <div className='h6'>Friend’s Birthdays</div>
                                            <p>Choose wheather or not receive notifications about your friend’s
                                                birthdays on your newsfeed</p>
                                        </div>
                                        <div className='togglebutton'>
                                            <label>
                                                <input name='optionsFollowerBirthdayboxes'
                                                       onChange={this.handleInputChange}
                                                       type='checkbox'
                                                       defaultChecked/>
                                                <span className='toggle'/>
                                            </label>
                                        </div>
                                    </div>
                                    <div className='description-toggle'>
                                        <div className='description-toggle-content'>
                                            <div className='h6'>Chat Message Sound</div>
                                            <p>A sound will be played each time you receive a new message on an inactive
                                                chat window</p>
                                        </div>
                                        <div className='togglebutton'>
                                            <label>
                                                <input name='optionsChatboxes'
                                                       onChange={this.handleInputChange}
                                                       type='checkbox'
                                                       defaultChecked/>
                                                <span className='toggle'/>
                                            </label>
                                        </div>
                                    </div>
                                    <div className='description-toggle'>
                                        <div className='description-toggle-content'>
                                            <div className='h6'>Deactivate Your Account</div>
                                        </div>
                                        <div className='togglebutton'>
                                            <label>
                                                <input name='optionsAccountDeactivateboxes'
                                                       onChange={this.handleInputChange}
                                                       type='checkbox'
                                                       defaultChecked/>
                                                <span className='toggle'/>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <button className='btn btn-secondary btn-lg full-width'>Restore all Attributes
                                    </button>
                                </div>
                                <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                    <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                       onClick={this.handleForm}>Save all
                                        Changes</a>
                                </div>
                            </div>
                        </form>
                        {/* ... end Personal Account Settings Form  */}

                    </div>
                </div>
            </Setting>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISettingsAccountComponentProps) => {
    return {
        post: (post: AccountSetting, callBack: Function) => dispatch(userActions.dbAddAccountSettings(post, callBack)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ISettingsAccountComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale'))
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((SettingsAccountComponent as any) as any)
