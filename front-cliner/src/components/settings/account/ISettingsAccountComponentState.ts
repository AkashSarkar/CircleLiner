export interface ISettingsAccountComponentState {

    /**
     * Set privacy for follow
     */
    followPrivacy: string

    /**
     * Set privacy for post
     */
    postPrivacy: string

    /**
     * Set notification sound true/false
     */
    notificationSound: boolean

    /**
     * Set email notification sound true/false
     */
    emailNotificationSound: boolean

    /**
     * Set follower birthday botification bound true/false
     */
    followerBirthdayNotificationSound: boolean

    /**
     * Set chat sound true/false
     */
    chatSound: boolean
    accountDeactivate: boolean
}
