import {AccountSetting} from 'core/domain/users/accountSettings'
import {Map} from 'immutable'

export interface ISettingsAccountComponentProps {

    /**
     * Open
     *
     * @memberof ISettingsAccountComponentProps
     */
    open?: boolean

    /**
     * Post model
     */
    accountSetting?: Map<string, any>

    /**
     * Save a post
     *
     * @memberof ISettingsAccountComponentProps
     */
    post?: (post: AccountSetting) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     */
    currentLanguage?: string
}
