// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getTranslate, getActiveLanguage } from 'react-localize-redux'
import { NavLink } from 'react-router-dom'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import { Map } from 'immutable'

// - Import app components

// - Import API

// - Import actions

import { ISettingsSidebarComponentProps } from './ISettingsSidebarComponentProps'
import { ISettingsSidebarComponentState } from './ISettingsSidebarComponentState'
/**
 * Create component class
 */
export class SettingsProfessionComponent extends Component<ISettingsSidebarComponentProps, ISettingsSidebarComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISettingsSidebarComponentProps) {
        super(props)
        // Defaul state
        this.state = {
        }
    }

    render() {
        const { classes, translate, currentLanguage, notifyCount } = this.props
        return (
            <>
            <div className='ui-block'>
                {/* Your Profile  */}
                <div className='your-profile'>
                    <div className='ui-block-title ui-block-title-small'>
                        <h6 className='title'>Settings</h6>
                    </div>
                    <div id='accordion' role='tablist' aria-multiselectable='true'>
                        <div className='card'>
                            <div className='card-header' role='tab' id='headingOne'>
                                <h6 className='mb-0'>
                                    <a data-toggle='collapse' data-parent='#accordion' href='#collapseOne'
                                       aria-expanded='true' aria-controls='collapseOne'>
                                        Profile Settings
                                        <svg className='olymp-dropdown-arrow-icon'>
                                            <use
                                                xlinkHref='/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon'/>
                                        </svg>
                                    </a>
                                </h6>
                            </div>
                            <div id='collapseOne' className='collapse show' role='tabpanel'
                                 aria-labelledby='headingOne'>
                                <ul className='your-profile-menu'>
                                    <li>
                                        <NavLink to='/settings'>
                                            <span>Personal
                                        Information</span>
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to='/settings/account'>
                                            <span>Account Settings</span>
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to='/settings/changepassword'>
                                            <span>Change Password</span>
                                        </NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='ui-block-title'>
                        <NavLink to='/notification'>
                            <span className='h6 title'>Notification</span>
                            <span className='items-round-little bg-primary'>{notifyCount}</span>
                        </NavLink>
                    </div>
                    <div className='ui-block-title'>
                        <NavLink to='/email-notification'>
                            <span className='h6 title'>Email Notification</span>
                            <span className='items-round-little bg-primary'>{notifyCount}</span>
                        </NavLink>
                    </div>
                    <div className='ui-block-title'>
                        <NavLink to='/web-notification'>
                            <span className='h6 title'>Web Notification</span>
                            <span className='items-round-little bg-primary'>{notifyCount}</span>
                        </NavLink>
                    </div>
                    <div className='ui-block-title'>
                        <NavLink to='/mobile-notification'>
                            <span className='h6 title'>Mobile Notification</span>
                            <span className='items-round-little bg-primary'>{notifyCount}</span>
                        </NavLink>
                    </div>
                    <div className='ui-block-title'>
                        <NavLink to='/block-list'>
                            <span className='h6 title'>Blocked People</span>
                        </NavLink>
                    </div>
                </div>
                {/* ... end Your Profile  */}
            </div>
                <div className='ui-block'>
                    <div id='job' role='tablist' aria-multiselectable='true'>
                        <div className='card'>
                            <div className='card-header' role='tab' id='headingOne'>
                                <h6 className='mb-0'>
                                    <a data-toggle='collapse' data-parent='#job' href='#collapseJob'
                                       aria-expanded='true' aria-controls='collapseJob'>
                                        Job Settings
                                        <svg className='olymp-dropdown-arrow-icon'>
                                            <use
                                                xlinkHref='/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon'/>
                                        </svg>
                                    </a>
                                </h6>
                            </div>
                            <div id='collapseJob' className='collapse show' role='tabpanel'
                                 aria-labelledby='headingOne'>
                                <ul className='your-profile-menu'>
                                    <li>
                                        <NavLink to='/job-preferences'>
                                            <span>Job Preferences</span>
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to='/employer-preferences'>
                                            <span>Employer Preferences</span>
                                        </NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
          </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISettingsSidebarComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ISettingsSidebarComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    const userNotifies: Map<string, any> = state.getIn(['notify', 'userNotifies'])
    let notifyCount = userNotifies
        ? userNotifies.count()
        : 0
    return {
        translate: getTranslate(state.get('locale')),
        notifyCount
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((SettingsProfessionComponent as any) as any)
