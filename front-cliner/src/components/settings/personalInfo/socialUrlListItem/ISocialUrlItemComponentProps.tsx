import {UserTie} from 'core/domain/circles/index'
import {Map} from 'immutable'
import {SocialUrl} from 'core/domain/users/socialUrl'

export interface ISocialUrlItemComponentProps {
    /**
     * Social url post id
     */
     socialUrlId?: string
    /**
     * Social url type
     */
     socialUrlType?: string
    /**
     * Social url
     */
     socialUrl?: string
    /**
     * update Social url
     */
     updateSocialUrl?: (socialUrl: SocialUrl) => any
    /**
     * delete Social url
     */
     deleteSocialUrl?: (socialUrlId?: string) => any
}
