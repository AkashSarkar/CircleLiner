// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import moment from 'moment/moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SvgCamera from '@material-ui/icons/PhotoCamera'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import EventListener, {withOptions} from 'react-event-listener'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Divider from '@material-ui/core/Divider'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import {withStyles} from '@material-ui/core/styles'

// - Import app components
import ImgCover from 'components/profile/imgCover/index'
import UserAvatar from 'components/userAvatar/index'
import ImageGallery from 'components/imageGallery/index'
import AppDialogTitle from 'layouts/dialogTitle/index'
import AppInput from 'layouts/appInput/index'

// - Import API
import FileAPI from 'api/FileAPI'

// - Import actions
import * as socialUrlActions from 'store/actions/userActions/socialUrlActions'
import {ISocialUrlItemComponentProps} from './ISocialUrlItemComponentProps'
import {ISocialUrlItemComponentState} from './ISocialUrlItemComponentState'
import {push} from 'react-router-redux'
import {SocialUrl} from 'core/domain/users/socialUrl'

/**
 * Create component class
 */
export class SocialUrlItemComponent extends Component<ISocialUrlItemComponentProps, ISocialUrlItemComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */

    constructor(props: ISocialUrlItemComponentProps) {
        super(props)
        this.state = {
            isEdit: false,
            socialUrl: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleForm = this.handleForm.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        this.setState({
            socialUrl: value
        })
    }

    handleEdit = () => {
        this.setState({
            isEdit: true
        })
    }
    handleEditCancel = () => {
        this.setState({
            isEdit: false
        })
    }
    /**
     * Handle register form
     */
    handleForm = (evt: any) => {
        console.log(this.state.socialUrl,this.props.socialUrlId)
        const {
            socialUrl
        } = this.state

        const {
            updateSocialUrl,
        } = this.props
        updateSocialUrl!({
            id: this.props.socialUrlId!,
            socialType: this.props.socialUrlType!,
            socialUrl: socialUrl,
        })
        this.setState({
            isEdit: false
        })

    }
    handleDelete = (evt: any) => {
        this.props.deleteSocialUrl!(this.props.socialUrlId)
    }

    render() {
        const {} = this.props
        return (
            <>{this.state.isEdit ?
                <div className='ui-block-content'>
                    <form>
                        <div className='row'>
                            <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Url</label>
                                    <input className='form-control'
                                           type='text'
                                           onChange={this.handleInputChange}
                                           name='educationLevel'
                                           defaultValue={this.props.socialUrl}/>
                                    <span className='material-input'/>
                                </div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-6 col-6'>
                                <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                   onClick={this.handleEditCancel}
                                >Cancel</a>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-6 col-6'>
                                <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                   onClick={(evt: any) => this.handleForm(evt)}
                                >Update</a>
                            </div>
                        </div>
                    </form>
                </div> :
                <div className='row'>
                    <div className='col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'>
                        <li onDoubleClick={this.handleEdit}>
                            <label>{this.props.socialUrlType} :</label>
                                <a href={'javascript:(0)'} className='text'>{this.props.socialUrl}</a>
                        </li>
                    </div>
                    <div className='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12'>
                        <div className='more'>
                            <svg className='olymp-three-dots-icon'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                            </svg>
                            <ul className='more-dropdown'>
                                <li><a href={'javascript:(0)'} onClick={this.handleEdit}>Edit</a></li>
                                <li><a href={'javascript:(0)'}
                                       onClick={(evt: any) => this.handleDelete(evt)}>Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            }
            </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISocialUrlItemComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url)),
        updateSocialUrl: (socialUrl: SocialUrl) => dispatch(socialUrlActions.dbUpdateSocialUrl(socialUrl)),
        deleteSocialUrl: (socialUrlId: string) => dispatch(socialUrlActions.dbDeleteSocialUrl(socialUrlId)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ISocialUrlItemComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'])
    return {
        translate: getTranslate(state.get('locale')),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((SocialUrlItemComponent as any) as any)
