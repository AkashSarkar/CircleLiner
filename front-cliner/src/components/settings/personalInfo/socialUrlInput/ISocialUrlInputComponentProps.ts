import {Map} from 'immutable'
import {SocialUrl} from 'core/domain/users/socialUrl'

export interface ISocialUrlInputComponentProps {
    id?: string
    /**
     * If it's true post writing page will be open
     */
    mediaType: string
    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * Recieve request close function
     */
    onRequestClose: () => void
    /**
     * Social url post
     */
    addSocialUrl?: (socialUrl: SocialUrl, callback: Function) => any
    /**
     * style classes
     */
    classes?: any
    /**
     * Translate to locale string
     */
    translate?: (state: any, param?: {}) => any
}
