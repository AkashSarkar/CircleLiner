// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
import Button from '@material-ui/core/Button'
// - Import app components
import {PersonalInfo} from 'core/domain/users/personalInfo'
import {ISocialUrlInputComponentProps} from './ISocialUrlInputComponentProps'
import {ISocialUrlInputComponentState} from './ISocialUrlInputComponentState'
import {SocialUrl} from 'core/domain/users/socialUrl'

// - Import API

// - Import actions
import * as socialUrlActions from 'store/actions/userActions/socialUrlActions'

// - Import layouts
import LModal from 'layouts/modal/index'
import * as PostAPI from 'api/PostAPI'

/**
 * Create component class
 */
export class SocialUrlInputComponent extends Component<ISocialUrlInputComponentProps, ISocialUrlInputComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    styles = {
        avatar: {
            border: '2px solid rgb(255, 255, 255)'
        },
        paper: {
            width: '90%',
            height: '100%',
            margin: '0 auto',
            display: 'block'
        },
        title: {
            padding: '24px 24px 20px 24px',
            font: '500 20px Roboto,RobotoDraft,Helvetica,Arial,sans-serif',
            display: 'flex',
            wordWrap: 'break-word',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            flexGrow: 1
        },
        actions: {
            display: 'flex',
            justifyContent: 'flex-end',
            padding: '24px 24px 20px'
        },
        updateButton: {
            marginLeft: '10px'
        },
        dialogGallery: {
            width: '',
            maxWidth: '530px',
            borderRadius: '4px'
        },
        iconButtonSmall: {},
        iconButton: {}

    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISocialUrlInputComponentProps) {
        super(props)
        this.state = {
            socialUrl: '',
            socialInputError: ''
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    componentWillReceiveProps(nextProps: ISocialUrlInputComponentProps) {
        if (!nextProps.open) {
            this.setState({
                socialUrl: '',
                socialInputError: ''
            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const name = target.name
        const value = target.value
        this.setState({
            socialUrl: value
        })
    }
    /**
     * Handle register form
     */
    handleForm = () => {
        console.log(this.state.socialUrl)
        console.log(this.props.mediaType)
        let error = false
        const {
            socialUrl,
        } = this.state

        const {
            mediaType,
            addSocialUrl,
            onRequestClose,
        } = this.props

        if (socialUrl.trim() === '') {
            this.setState({
                socialInputError: 'This field is required',
                // disabledPost: true
            })
            error = true

        }
        // if no error,submit form
        if (!error) {
            addSocialUrl!({
                socialType: mediaType,
                socialUrl: socialUrl,
            }, onRequestClose)
            this.props.onRequestClose()
        }

    }

    render() {
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose}
                    modalTitle={'Enter Your Profile Url'}>
                {/*<div className='form-group with-icon label-floating is-empty'>*/}
                    {/*<label className='control-label'>Input with Icon</label>*/}
                    {/*<input className='form-control'*/}
                           {/*type='text'*/}
                           {/*name={'url'}*/}
                           {/*onChange={this.handleInputChange}/>*/}
                    {/*<i className='fab fa-facebook-f c-facebook' aria-hidden='true'/>*/}
                    {/*<span className='material-input'/>*/}
                    {/*<span style={{color: 'red'}}>*/}
                    {/*{this.state.socialInputError.trim() ? this.state.socialInputError : ''}*/}
                    {/*</span>*/}
                {/*</div>*/}
                <div className='form-group label-floating'>
                    <label className='control-label'>{`${this.props.mediaType} Url`}</label>
                    <input className='form-control'
                           name='url'
                           onChange={this.handleInputChange}
                           type='text'
                    />
                    <span className='material-input'/>
                    <span style={{color: 'red'}}>
                    {this.state.socialInputError.trim() ? this.state.socialInputError : ''}
                    </span>
                </div>
                <Button onClick={this.props.onRequestClose}> close </Button>
                <Button variant='raised'
                        color='primary'
                        style={this.styles.updateButton}
                        onClick={this.handleForm}
                >Confirm</Button>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISocialUrlInputComponentProps) => {
    return {
        addSocialUrl: (socialUrl: SocialUrl, callBack: Function) => dispatch(socialUrlActions.dbAddSocialUrl(socialUrl, callBack)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ISocialUrlInputComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        fullName: user.fullName
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((SocialUrlInputComponent as any) as any)
