
export interface ISocialUrlInputComponentState {
    /**
     * socialMedia url input
     *
     * @type {string}
     * @memberof ISocialListComponentState
     */
     socialUrl: string
    /**
     * socialMedia url input Error
     *
     * @type {string}
     * @memberof ISocialListComponentState
     */
     socialInputError: string
}
