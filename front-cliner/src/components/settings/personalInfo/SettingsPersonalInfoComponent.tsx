// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import {PersonalInfo} from 'core/domain/users/personalInfo'
import {ISettingsPersonalInfoComponentProps} from './ISettingsPersonalInfoComponentProps'
import {ISettingsPersonalInfoComponentState} from './ISettingsPersonalInfoComponentState'
import {SocialListComponent} from 'components/settings/personalInfo/socialList/SocialListComponent'
import {SocialUrl} from 'core/domain/users/socialUrl'
import SocialUrlListItem from 'components/settings/personalInfo/socialUrlListItem'
// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import * as socialUrlActions from 'store/actions/userActions/socialUrlActions'
// - Import layouts
import Setting from 'layouts/setting'

/**
 * Create component class
 */
export class SettingsPersonalInfoComponent extends Component<ISettingsPersonalInfoComponentProps, ISettingsPersonalInfoComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISettingsPersonalInfoComponentProps) {
        super(props)
        const {personalInfo} = props
        this.state = {
            /**
             * User's firstname
             */
            firstName: personalInfo ? personalInfo.get('firstName', '') : '',

            /**
             * User's fullname
             */
            lastName: personalInfo ? personalInfo.get('lastName', '') : '',

            /**
             * User's fullname
             */
            fullName: personalInfo ? personalInfo.get('fullName', '') : '',

            /**
             * User's email
             */
            email: personalInfo ? personalInfo.get('email', '') : '',

            /**
             * User's date of birth
             */
            dob: personalInfo ? personalInfo.get('dob', '') : '',

            /**
             * User's website
             */
            website: personalInfo ? personalInfo.get('website', '') : '',

            /**
             * User's phone
             */
            phone: personalInfo ? personalInfo.get('phone', '') : '',

            /**
             * User's birth place
             */
            birthPlace: personalInfo ? personalInfo.get('birthPlace', '') : '',

            /**
             * User's gender
             */
            gender: personalInfo ? personalInfo.get('gender', '') : '',

            /**
             * User's marital status
             */
            status: personalInfo ? personalInfo.get('status', '') : '',
            /**
             * social Modal status
             */
            socialModalOpen: false
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSocialInput = this.handleSocialInput.bind(this)
    }

    componentWillReceiveProps(nextProps: ISettingsPersonalInfoComponentProps) {
        if (!nextProps.open) {
            const {personalInfo} = this.props
            this.setState({

                /**
                 * User's firstname
                 */
                firstName: personalInfo ? personalInfo.get('firstName', '') : '',

                /**
                 * User's fullname
                 */
                lastName: personalInfo ? personalInfo.get('lastName', '') : '',

                /**
                 * User's fullname
                 */
                fullName: personalInfo ? personalInfo.get('fullName', '') : '',

                /**
                 * User's email
                 */
                email: personalInfo ? personalInfo.get('email', '') : '',

                /**
                 * User's date of birth
                 */
                dob: personalInfo ? personalInfo.get('dob', '') : '',

                /**
                 * User's website
                 */
                website: personalInfo ? personalInfo.get('website', '') : '',

                /**
                 * User's phone
                 */
                phone: personalInfo ? personalInfo.get('phone', '') : '',

                /**
                 * User's birth place
                 */
                birthPlace: personalInfo ? personalInfo.get('birthPlace', '') : '',

                /**
                 * User's gender
                 */
                gender: personalInfo ? personalInfo.get('gender', '') : '',

                /**
                 * User's marital status
                 */
                status: personalInfo ? personalInfo.get('status', '') : '',
                /**
                 * social Modal status
                 */
                socialModalOpen: false
            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSocialInput = this.handleSocialInput.bind(this)
    }

    componentWillMount() {
      const {getSocialUrl} = this.props
      getSocialUrl!()
    }

    handleInputChange = (event: any) => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        })

        switch (name) {
            case 'firstName':
                this.setState({
                    firstName: value
                })
                break
            case 'lastName':
                this.setState({
                    lastName: value
                })
                break
            case 'fullName':
                this.setState({
                    fullName: value
                })
                break
            case 'email':
                this.setState({
                    email: value
                })
                break
            case 'dob':
                this.setState({
                    dob: value
                })
                break
            case 'website':
                this.setState({
                    website: value
                })
                break
            case 'phone':
                this.setState({
                    phone: value
                })
                break
            case 'birthPlace':
                this.setState({
                    birthPlace: value
                })
                break
            case 'gender':
                this.setState({
                    gender: value
                })
                break
            case 'status':
                this.setState({
                    status: value
                })
                break
            default:
        }
    }
    handleSocialInput = () => {
        this.setState({
            socialModalOpen: true
        })

    }
    handleCloseSocialModal = () => {
        this.setState({
            socialModalOpen: false
        })
    }

    /**
     * Handle register form
     */
    handleForm = () => {
        const {
            firstName,
            lastName,
            fullName,
            email,
            dob,
            website,
            phone,
            birthPlace,
            gender,
            status,
        } = this.state
        const {
            post,
        } = this.props
        post!({
            firstName: firstName,
            lastName: lastName,
            fullName: fullName,
            email: email,
            dob: dob,
            website: website,
            phone: phone,
            birthPlace: birthPlace,
            gender: gender,
            status: status,
        })
    }
    socialUrlList = () => {
        const item: any[] = []
        const parsedSocialUrl: SocialUrl[] = []
        this.props.socialUrls!.forEach((socialUrl) => {
            parsedSocialUrl.push({
                ...Map(socialUrl!).toJS()
            })
        })
        parsedSocialUrl.map((socialUrl: SocialUrl) => {
            item.push(
                <SocialUrlListItem key={socialUrl.id!}
                                        // isAuthed={this.props.isAuthed!}
                                        socialUrlId={socialUrl.id!}
                                        socialUrlType={socialUrl.socialType!}
                                        socialUrl={socialUrl.socialUrl!}
                />
            )
        })
        return item
    }

    render() {
        const {classes, translate, currentLanguage} = this.props
        return (
            <Setting>
                <div className='ui-block'>
                    <div className='ui-block-title'>
                        <h6 className='title'>Personal Information</h6>
                    </div>
                    <div className='ui-block-content'>
                        {/* Personal Information Form  */}
                        <div className='row'>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating is-empty'>
                                    <label className='control-label'>Your Birthplace</label>
                                    <input className='form-control'
                                           name='birthPlace'
                                           onChange={this.handleInputChange}
                                           type='text'/>
                                    <span className='material-input'/></div>
                                <div className='form-group date-time-picker label-floating'>
                                    <label className='control-label'>Your Birthday</label>
                                    <input className='datetimepicker'
                                           name='dob'
                                           type='date'
                                           onChange={this.handleInputChange}
                                           placeholder='10/24/1984'/>
                                </div>
                                <div className='form-group label-floating is-empty'>
                                    <label className='control-label'>Gender</label>
                                    <input className='form-control'
                                           name='gender'
                                           onChange={this.handleInputChange}
                                           type='text'/>
                                    <span className='material-input'/></div>
                                <div className='form-group label-floating is-empty'>
                                    <label className='control-label'>Marital Status</label>
                                    <input className='form-control'
                                           name='status'
                                           onChange={this.handleInputChange}
                                           type='text'/>
                                    <span className='material-input'/></div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Write a little description about you</label>
                                    <textarea className='form-control'
                                              placeholder={'Hi, I’m James, I’m 36 and I work as a Digital Designer for the  “Daydreams” Agency in Pier 56'}/>
                                </div>
                            </div>
                            <div className='col col-lg-6 col-md-6 col-sm-12 col-12'>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Your Website</label>
                                    <input className='form-control'
                                           name='website'
                                           onChange={this.handleInputChange}
                                           type='text'
                                           defaultValue='netexpro.com'/>
                                    <span className='material-input'/></div>
                                <div className='form-group label-floating is-empty'>
                                    <label className='control-label'>Your Phone Number</label>
                                    <input className='form-control'
                                           name='phone'
                                           onChange={this.handleInputChange}
                                           type='text'/>
                                    <span className='material-input'/></div>
                                <div className='form-group label-floating'>
                                    <label className='control-label'>Your Email</label>
                                    <input className='form-control'
                                           name='email'
                                           onChange={this.handleInputChange}
                                           type='email'
                                           defaultValue='jspiegel@yourmail.com'/>
                                    <span className='material-input'/></div>
                            </div>
                            <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                {this.socialUrlList()}
                            </div>
                             <div className='col col-lg-offset-4'>
                                <a href={'javascript:(0)'}
                                   onClick={this.handleSocialInput}
                                   className='add-field'>
                                    <svg className='olymp-plus-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-plus-icon' /></svg>
                                    <span>Add Social</span>
                                </a>
                             </div>
                            <SocialListComponent open={this.state.socialModalOpen} onRequestClose={this.handleCloseSocialModal}/>
                            <div className='col col-lg-12 col-md-12 col-sm-12 col-12'>
                                <a href={'javascript:(0)'} className='btn btn-primary btn-lg full-width'
                                   onClick={this.handleForm}>save</a>
                            </div>
                        </div>
                        {/* ... end Personal Information Form  */}
                    </div>
                </div>
            </Setting>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISettingsPersonalInfoComponentProps) => {
    const uid  = '1234'
    return {
        post: (post: PersonalInfo, callBack: Function) => dispatch(userActions.dbAddPersonalInfo(post, callBack)),
        getSocialUrl: () => dispatch(socialUrlActions.dbGetUserSocialUrlByUserId(uid))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ISettingsPersonalInfoComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid], {})
    const socialUrl = state.getIn(['user', 'socialUrl', uid])
    return {
        translate: getTranslate(state.get('locale')),
        fullName: user.fullName,
        userId: uid,
        socialUrls: socialUrl ? socialUrl : []
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((SettingsPersonalInfoComponent as any) as any)
