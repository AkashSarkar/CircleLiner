// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'
// - Import app components
import {PersonalInfo} from 'core/domain/users/personalInfo'
import {ISocialListComponentProps} from './ISocialListComponentProps'
import {ISocialListComponentState} from './ISocialListComponentState'
import SocialUrlInputComponent from 'components/settings/personalInfo/socialUrlInput'

// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'

// - Import layouts
import LModal from 'layouts/modal/index'

/**
 * Create component class
 */
export class SocialListComponent extends Component<ISocialListComponentProps, ISocialListComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ISocialListComponentProps) {
        super(props)
        this.state = {
           socialType: '',
           socialUrlModal: false
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    componentWillReceiveProps(nextProps: ISocialListComponentProps) {
        if (!nextProps.open) {
            this.setState({
                socialType: '',
                socialUrlModal: false
            })
        }
        // Binding function to `this`
        this.handleForm = this.handleForm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    handleInputChange = (event: any) => {
        // this.props.onRequestClose()
        console.log(event.target.name)
        const target = event.target
        const name = target.name
        switch (name) {
            case 'Facebook':
                this.setState({
                    socialType: name,
                    socialUrlModal: true
                })
                break
            case 'Google':
                this.setState({
                    socialType: name,
                    socialUrlModal: true
                })
                break
            case 'Github':
                this.setState({
                    socialType: name,
                    socialUrlModal: true
                })
                break

            default:
        }
    }
    closeSocialUrlModal = () => {
        this.setState({
            socialUrlModal: false
        })
        this.props.onRequestClose()
    }

    /**
     * Handle register form
     */
    handleForm = () => {
    }

    render() {
        return (
            <LModal open={this.props.open} onClose={this.props.onRequestClose}
                    modalTitle={'Choose Your Social Type'}>
                <div className='checkbox'>
                    <label>
                        <input
                            name='Facebook'
                            onClick={this.handleInputChange}
                            type='checkbox'/>
                        <span className='checkbox-material'>
                         <span className='check'/></span>
                        Facebook
                    </label>
                </div>
                <div className='checkbox'>
                    <label>
                        <input
                            name='Google'
                            onClick={this.handleInputChange}
                            type='checkbox'/>
                        <span className='checkbox-material'>
                         <span className='check'/></span>
                        Google
                    </label>
                </div>
                <div className='checkbox'>
                    <label>
                        <input
                            name='Github'
                            onClick={this.handleInputChange}
                            type='checkbox'/>
                        <span className='checkbox-material'>
                         <span className='check'/></span>
                        GitHub
                    </label>
                </div>
                <SocialUrlInputComponent open={this.state.socialUrlModal} onRequestClose={this.closeSocialUrlModal}
                                         mediaType={this.state.socialType}/>
            </LModal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ISocialListComponentProps) => {
    return {
        post: (post: PersonalInfo, callBack: Function) => dispatch(userActions.dbAddPersonalInfo(post, callBack)),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ISocialListComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        fullName: user.fullName
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((SocialListComponent as any) as any)
