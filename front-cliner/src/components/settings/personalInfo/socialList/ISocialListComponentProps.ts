import { PersonalInfo } from 'core/domain/users/personalInfo'
import {Map} from 'immutable'

export interface ISocialListComponentProps {
    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * Recieve request close function
     */
    onRequestClose: () => void
}
