
export interface ISocialListComponentState {
    /**
     * socialMedia type
     *
     * @type {string}
     * @memberof ISocialListComponentState
     */
     socialType: string
    /**
     * socialMedia type
     *
     * @type {string}
     * @memberof ISocialListComponentState
     */
    socialUrlModal: boolean
}
