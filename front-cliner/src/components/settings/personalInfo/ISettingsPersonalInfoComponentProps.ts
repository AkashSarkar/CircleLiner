import { PersonalInfo } from 'core/domain/users/personalInfo'
import {Map} from 'immutable'
import {Education} from 'core/domain/users/education'
import {SocialUrl} from 'core/domain/users/socialUrl'
export interface ISettingsPersonalInfoComponentProps {

     open?: boolean

    /**
     * Personal information model
     */
     personalInfo?: Map<string, any>

    /**
     * Save a post
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
     post?: (post: PersonalInfo) => any

    /**
     * Styles
     */
    classes?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    currentLanguage?: string

    /**
     * firstName input
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    firstName?: string

    /**
     * firstName input
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    lastName?: string

    /**
     * Fullname input
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    fullName?: string
    /**
     * get social url
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
     getSocialUrl?: () => any
    /**
     * get current user id
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
     userId?: string
    /**
     * get user social url
     *
     * @memberof ISettingsPersonalInfoComponentProps
     */
    socialUrls?: Map<string, SocialUrl>
  }
