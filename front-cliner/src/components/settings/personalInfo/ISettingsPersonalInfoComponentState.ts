
export interface ISettingsPersonalInfoComponentState {
    /**
     * firstname input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    firstName: string

    /**
     * lastname input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    lastName: string

    /**
     * fullname input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    fullName: string

    /**
     * email input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    email: string

    /**
     * dob input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    dob: string

    /**
     * website input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    website: string

    /**
     * phone number input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    phone: string

    /**
     * Birthplace input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    birthPlace: string

    /**
     * gender input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    gender: string

    /**
     * status input value
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    status: string
    /**
     * social Modal open
     *
     * @type {string}
     * @memberof ISettingsPersonalInfoComponentState
     */
    socialModalOpen: boolean
}
