import {Profile} from 'core/domain/users/index'

export interface IArticleComponentProps {
    /**
     * Owner's post avatar
     *
     * @type {string}
     * @memberof IPostComponentProps
     */
    avatar?: string

    /**
     * User full name
     *
     * @type {string}
     * @memberof IPostComponentProps
     */
    fullName?: string
}