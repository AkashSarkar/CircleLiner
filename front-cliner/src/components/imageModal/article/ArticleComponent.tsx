// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

import {UserAvatarComponent} from 'components/userAvatar/UserAvatarComponent'

// - Import API

// - Import actions

import {IArticleComponentProps} from './IArticleComponentProps'
import {IArticleComponentState} from './IArticleComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import LModal from 'layouts/modal/index'

/**
 * Create component class
 */
export class PhotoProfileComponent extends Component<IArticleComponentProps, IArticleComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IArticleComponentProps) {
        super(props)
        // Defaul state
        this.state = {
        }
    }
    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <article className='hentry post'>
                <div className='post__author author vcard inline-items'>
                    {/*<img src='/img/author-page.jpg' alt='author' />*/}
                    {/*<div className='author-date'>*/}
                        {/*<a className='h6 post__author-name fn' href='02-ProfilePage.html'>James Spiegel</a>*/}
                        {/*<div className='post__date'>*/}
                            {/*<time className='published' dateTime='2017-03-24T18:18'>*/}
                                {/*2 hours ago*/}
                            {/*</time>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                    <UserAvatarComponent fullName={this.props.fullName!} fileName={this.props.avatar!} size={36}/>
                    <div className='more'><svg className='olymp-three-dots-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon' /></svg>
                        <ul className='more-dropdown'>
                            <li>
                                <a href='#'>Edit Post</a>
                            </li>
                            <li>
                                <a href='#'>Delete Post</a>
                            </li>
                            <li>
                                <a href='#'>Turn Off Notifications</a>
                            </li>
                            <li>
                                <a href='#'>Select as Featured</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <p>Here’s a photo from last month’s photoshoot. We really had a great time and got a batch of incredible shots for the new catalog.</p>
                <p>With: <a href='#'>Jessy Owen</a>, <a href='#'>Marina Valentine</a></p>
                <div className='post-additional-info inline-items'>
                    <a href='#' className='post-add-icon inline-items'>
                        <svg className='olymp-heart-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-heart-icon' /></svg>
                        <span>148</span>
                    </a>
                    <div className='comments-shared'>
                        <a href='#' className='post-add-icon inline-items'>
                            <svg className='olymp-speech-balloon-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-speech-balloon-icon' /></svg>
                            <span>61</span>
                        </a>
                        <a href='#' className='post-add-icon inline-items'>
                            <svg className='olymp-share-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-share-icon' /></svg>
                            <span>32</span>
                        </a>
                    </div>
                </div>
                <div className='control-block-button post-control-button'>
                    <a href='#' className='btn btn-control'>
                        <svg className='olymp-like-post-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-like-post-icon' /></svg>
                    </a>
                    <a href='#' className='btn btn-control'>
                        <svg className='olymp-comments-post-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-comments-post-icon' /></svg>
                    </a>
                    <a href='#' className='btn btn-control'>
                        <svg className='olymp-share-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-share-icon' /></svg>
                    </a>
                </div>
            </article>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IArticleComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IArticleComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const userProfile = state.getIn(['user', 'info', uid], {})
    return {
        avatar: userProfile ? userProfile.avatar : '',
        fullName: userProfile ? userProfile.fullName : '',
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotoProfileComponent as any) as any)