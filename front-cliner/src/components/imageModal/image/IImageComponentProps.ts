import {Profile} from 'core/domain/users/index'

export interface IImageComponentProps {
    /**
     * image url
     */
    URL: string
}
