// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

// - Import API

// - Import actions

import {IImageComponentProps} from './IImageComponentProps'
import {IImageComponentState} from './IImageComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import LModal from 'layouts/modal/index'

/**
 * Create component class
 */
export class PhotoProfileComponent extends Component<IImageComponentProps, IImageComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IImageComponentProps) {
        super(props)
        // Defaul state
        this.state = {
        }
    }
    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <div className='open-photo-thumb'>
                            <div className='swiper-container' data-effect='fade' data-autoplay={4000}>
                                {/* Additional required wrapper */}
                                <div className='swiper-wrapper'>
                                    {/* Slides */}
                                    <div className='swiper-slide'>
                                        <div className='photo-item' data-swiper-parallax={-300} data-swiper-parallax-duration={500}>
                                            <img src={`${this.props.URL}`} alt='photo' />
                                            <div className='overlay' />
                                            <a href='#' className='more'><svg className='olymp-three-dots-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon' /></svg></a>
                                            <a href='#' className='tag-friends' data-toggle='tooltip' data-placement='top' data-original-title='TAG YOUR FRIENDS'>
                                                <svg className='olymp-happy-face-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-happy-face-icon' /></svg>
                                            </a>
                                            <div className='content'>
                                                <a href='#' className='h6 title'>Photoshoot 2016</a>
                                                <time className='published' dateTime='2017-03-24T18:18'>2 weeks ago</time>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IImageComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IImageComponentProps) => {
    return {
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotoProfileComponent as any) as any)