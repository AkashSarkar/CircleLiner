// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

// - Import API

// - Import actions

import {ICommentFormComponentProps} from './ICommentFormComponentProps'
import {ICommentFormComponentState} from './ICommentFormComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import LModal from 'layouts/modal/index'

/**
 * Create component class
 */
export class PhotoProfileComponent extends Component<ICommentFormComponentProps, ICommentFormComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ICommentFormComponentProps) {
        super(props)
        // Defaul state
        this.state = {
        }
    }
    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <form className='comment-form inline-items'>
                <div className='post__author author vcard inline-items'>
                    <img src='/img/avatar73-sm.jpg' alt='author' />
                    <div className='form-group with-icon-right is-empty'>
                        <textarea className='form-control' placeholder='Press Enter to post...' defaultValue={''} />
                        <div className='add-options-message'>
                            <a href='#' className='options-message'>
                                <svg className='olymp-camera-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-camera-icon' /></svg>
                            </a>
                        </div>
                        <span className='material-input' /></div>
                </div>
            </form>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ICommentFormComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ICommentFormComponentProps) => {
    return {
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotoProfileComponent as any) as any)