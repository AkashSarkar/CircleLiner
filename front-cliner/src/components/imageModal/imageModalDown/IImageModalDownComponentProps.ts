import {Profile} from 'core/domain/users/index'

export interface IImageModalDownComponentProps {
    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * Recieve request close function
     */
    onRequestClose: () => void
    /**
     * User Id
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
    userId: string
    /**
     * User profile
     *
     * @type {Profile}
     * @memberof IInfoProfileComponentProps
     */
    info?: Profile
    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Current locale language
     */
    currentLanguage?: string
}
