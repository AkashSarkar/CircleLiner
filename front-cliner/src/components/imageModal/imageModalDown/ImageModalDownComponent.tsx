// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import ImageComponent from '../image'
import Article from '../article'
import CommentList from '../commentList'
import CommentForm from '../commentForm'
// - Import API

// - Import actions

import {IImageModalDownComponentProps} from './IImageModalDownComponentProps'
import {IImageModalDownComponentState} from './IImageModalDownComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import LModal from 'layouts/modal/index'

/**
 * Create component class
 */
export class PhotoProfileComponent extends Component<IImageModalDownComponentProps, IImageModalDownComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IImageModalDownComponentProps) {
        super(props)
        // Defaul state
        this.state = {
            isModalOpen: false,
            /**
             * If it's true, Article write will be open
             */
            openImageModal: false,
        }

        this.toggleModal = this.toggleModal.bind(this)
        this.handleOpenImageModal = this.handleOpenImageModal.bind(this)
        this.handleCloseImageModal = this.handleCloseImageModal.bind(this)
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    }
    /**
     * Open award write
     *
     *
     * @memberof StreamComponentopen ? {display: 'block
     */
    handleOpenImageModal = () => {
        this.setState({
            openImageModal: true,
        })
    }

    /**
     * Open award write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseImageModal = () => {
        this.setState({
            openImageModal: false,
        })
    }

    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate, currentLanguage} = this.props
        return (
           <div className='ui-block'>
                <LModal open={this.props.open} onClose={this.props.onRequestClose} modalClassName={'open-photo-popup open-photo-popup-v1'}>
                        {/*<ImageComponent/>*/}
                        <div className='open-photo-content'>
                            <Article />
                            {/*Difference*/}
                            <div className='mCustomScrollbar' data-mcs-theme='dark'>
                                {/*Difference*/}
                                <CommentList/>
                            </div>
                           <CommentForm/>
                        </div>
                </LModal>
           </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IImageModalDownComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IImageModalDownComponentProps) => {
    const uid = ownProps.userId
    return {
        currentLanguage: getActiveLanguage(state.get('locale')).code,
        translate: getTranslate(state.get('locale')),
        info: state.getIn(['user', 'info', uid]),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotoProfileComponent as any) as any)