export interface IImageModalDownComponentState {
    /**
     * checking modal is open or not
     *
     * @type {String}
     * @memberof IInfoProfileComponentProps
     */
     isModalOpen: boolean

    /**
     * Writing post box is open {true} or not false
     *
     * @type {boolean}
     * @memberof IStreamComponentState
     */
     openImageModal: boolean
}
