// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components
import ImageComponent from '../image'
import Article from '../article'
import CommentList from '../commentList'
import CommentForm from '../commentForm'
// - Import API

// - Import actions

import {IImageModalSideComponentProps} from './IImageModalSideComponentProps'
import {IImageModalSideComponentState} from './IImageModalSideComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import LModal from 'layouts/modal/index'

/**
 * Create component class
 */
export class PhotoProfileComponent extends Component<IImageModalSideComponentProps, IImageModalSideComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IImageModalSideComponentProps) {
        super(props)
        // Defaul state
        this.state = {
            isModalOpen: false,
            /**
             * If it's true, Article write will be open
             */
            openImageModal: false,
        }

        this.toggleModal = this.toggleModal.bind(this)
        this.handleOpenImageModal = this.handleOpenImageModal.bind(this)
        this.handleCloseImageModal = this.handleCloseImageModal.bind(this)
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    }
    /**
     * Open award write
     *
     *
     * @memberof StreamComponent
     */
    handleOpenImageModal = () => {
        this.setState({
            openImageModal: true,
        })
    }

    /**
     * Open award write
     *
     *
     * @memberof StreamComponent
     */
    handleCloseImageModal = () => {
        this.setState({
            openImageModal: false,
        })
    }

    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate, currentLanguage} = this.props
        return (
           <div className='ui-block'>
                <LModal open={this.props.open} onClose={this.props.onRequestClose} modalClassName={'open-photo-popup open-photo-popup-v2'}>
                               <ImageComponent
                               URL={this.props.URL}/>
                                    <div className='open-photo-content'>
                                        <Article />
                                        {/*Difference*/}
                                        <div className='mCustomScrollbar ps ps--theme_default' data-mcs-theme='dark' data-ps-id='20fcf4ce-4e31-9547-48f5-d64bba818ab5'>
                                            {/*Difference*/}
                                            <CommentList/>
                                            {/*Difference*/}
                                            <div className='ps__scrollbar-x-rail' style={{left: 0, bottom: 0}}>
                                                <div className='ps__scrollbar-x' tabIndex={0}
                                                     style={{left: 0, width: 0}}/>
                                            </div>
                                            <div className='ps__scrollbar-y-rail' style={{top: 0, right: 0}}>
                                                <div className='ps__scrollbar-y' tabIndex={0}
                                                     style={{top: 0, height: 0}}/>
                                            </div>
                                            {/*Difference*/}
                                        </div>
                                       <CommentForm />
                                    </div>
                </LModal>
           </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IImageModalSideComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IImageModalSideComponentProps) => {
    const uid = ownProps.userId
    return {
        currentLanguage: getActiveLanguage(state.get('locale')).code,
        translate: getTranslate(state.get('locale')),
        info: state.getIn(['user', 'info', uid]),
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotoProfileComponent as any) as any)