// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment'
import {Map} from 'immutable'

// - Import app components

// - Import API

// - Import actions

import {ICommentListComponentProps} from './ICommentListComponentProps'
import {ICommentListComponentState} from './ICommentListComponentState'

// Import Layouts
import ImageModal from 'layouts/imageModal/index'
import LModal from 'layouts/modal/index'

/**
 * Create component class
 */
export class PhotoProfileComponent extends Component<ICommentListComponentProps, ICommentListComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: ICommentListComponentProps) {
        super(props)
        // Defaul state
        this.state = {
        }
    }
    componentDidMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <ul className='comments-list'>
                <li className='comment-item'>
                    <div className='post__author author vcard inline-items'>
                        <img src='/img/avatar48-sm.jpg' alt='author' />
                        <div className='author-date'>
                            <a className='h6 post__author-name fn' href='#'>Marina Valentine</a>
                            <div className='post__date'>
                                <time className='published' dateTime='2017-03-24T18:18'>
                                    46 mins ago
                                </time>
                            </div>
                        </div>
                        <a href='#' className='more'><svg className='olymp-three-dots-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon' /></svg></a>
                    </div>
                    <p>I had a great time too!! We should do it again!</p>
                    <a href='#' className='post-add-icon inline-items'>
                        <svg className='olymp-heart-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-heart-icon' /></svg>
                        <span>8</span>
                    </a>
                    <a href='#' className='reply'>Reply</a>
                </li>
                <li className='comment-item'>
                    <div className='post__author author vcard inline-items'>
                        <img src='/img/avatar4-sm.jpg' alt='author' />
                        <div className='author-date'>
                            <a className='h6 post__author-name fn' href='#'>Chris Greyson</a>
                            <div className='post__date'>
                                <time className='published' dateTime='2017-03-24T18:18'>
                                    1 hour ago
                                </time>
                            </div>
                        </div>
                        <a href='#' className='more'><svg className='olymp-three-dots-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-three-dots-icon' /></svg></a>
                    </div>
                    <p>Dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                    <a href='#' className='post-add-icon inline-items'>
                        <svg className='olymp-heart-icon'><use xlinkHref='svg-icons/sprites/icons.svg#olymp-heart-icon' /></svg>
                        <span>7</span>
                    </a>
                    <a href='#' className='reply'>Reply</a>
                </li>
            </ul>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: ICommentListComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: ICommentListComponentProps) => {
    return {
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((PhotoProfileComponent as any) as any)