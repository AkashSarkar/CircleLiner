// - Import react components
import React, {Component} from 'react'
import {IHomePageProps} from './IHomePageProps'
import {IHomePageState} from './IHomePageState'
import {IRouterProps} from 'routes/IRouterProps'
import {Map} from 'immutable'
import {getActiveLanguage, getTranslate} from 'react-localize-redux'
import {HomeRouter} from 'routes/HomeRouter'
import PeopleSugesstion from 'src/components/sugestionBox/people/peopleSuggestions'
import AddContactSugestion from 'src/components/sugestionBox/addContactSuggestions'
import JobSugestion from 'src/components/sugestionBox/jobs'
import HomeLeftSideComponent from './homeLeftSideComponent'
import JobMenuComponent from './leftMenu'
import St from 'src/containers/stream'
import {connect} from 'react-redux'
import {Profile} from 'core/domain/users'

// - Import components
import HomeView from 'layouts/views/homeView'
import NotificationBoxComponent from 'components/sugestionBox/notifications'
// - Import actions

// - Create component class
export class HomePage extends Component<IHomePageProps, IHomePageState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IHomePageProps) {
        super(props)

        // Default state
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {data, translate, tagLine, avatar, banner, fullName} = this.props
        // console.log('------posts in home page------')
        // console.log(data)
        return (
            <HomeView>
                <div className='container'>
                    <div className='row'>
                        <main
                            className='col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12'>
                            <NotificationBoxComponent userId={this.props.userId!} isOpen={true}/>
                            <St
                                fullName={fullName}
                                avatar={avatar}
                                homeTitle={translate!('header.home')}
                                posts={data.mergedPosts}
                                loadStream={data.loadDataStream}
                                hasMorePosts={data.hasMorePosts}
                                displayWriting={true}/>
                        </main>
                        < aside
                            className='col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12'>
                            <HomeLeftSideComponent
                                tagLine={this.props.tagLine}
                                avatar={this.props.avatar}
                                banner={this.props.banner}
                                fullName={this.props.fullName}/>

                            <JobMenuComponent />
                        </aside>
                        <aside
                            className='col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12'>
                            <AddContactSugestion userId={this.props.userId!} isOpen={true}/>
                            <PeopleSugesstion/>
                            <JobSugestion userId={this.props.userId!} isOpen={true}/>
                        </aside>
                    </div>
                </div>
            </HomeView>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IHomePageProps) => {
    return {}
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IHomePageProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const userProfile = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        currentLanguage: getActiveLanguage(state.get('locale')).code,
        avatar: userProfile.avatar,
        name: userProfile.fullName,
        banner: userProfile.banner,
        tagLine: userProfile.tagLine,
        userId: uid
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((HomePage as any) as any)
