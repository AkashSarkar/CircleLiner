// - Import react components
import React, {Component} from 'react'
import {IHomeLeftSidebarComponentProps} from './IHomeLeftSidebarComponentProps'
import {IHomeLeftSidebarComponentState} from './IHomeLeftSidebarComponentState'
import {IRouterProps} from 'routes/IRouterProps'
import {Map} from 'immutable'
import {getActiveLanguage, getTranslate} from 'react-localize-redux'
import {HomeRouter} from 'routes/HomeRouter'
import {connect} from 'react-redux'

// - Import components
import UserAvatarComponent from 'src/components/userAvatar'
import {push} from 'react-router-redux'

// - Import actions

// - Create component class
export class HomeLeftSidebarComponent extends Component<IHomeLeftSidebarComponentProps, IHomeLeftSidebarComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IHomeLeftSidebarComponentProps) {
        super(props)

        // Default state
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate, userId, tagLine, fullName, banner, avatar} = this.props
        return (
            <div key={userId} className='ui-block'>
                {/* Friend Item */}
                <div className='friend-item'>
                    <div className='friend-item-content'>
                        <div className='friend-avatar'>
                            <br/>
                            <br/>
                            <br/>
                            <div className='author-thumb'
                                 onClick={() => this.props.goTo!(`/${userId}`)}
                                 style={{cursor: 'pointer'}}>
                                <UserAvatarComponent fileName={avatar} fullName={fullName} size={90}/>
                            </div>
                            <div className='author-content'>
                                <div className='h5 author-name'><a href={`/${this.props.userId}`}>{fullName}</a></div>
                                <div className='country'>Portland, OR</div>
                            </div>
                        </div>
                        <div className='swiper-container'>
                            <div className='swiper-wrapper'>
                                <div className='swiper-slide'>
                                    <div className='friend-count' data-swiper-parallax={-500}>
                                        <a href={'javascript:(0)'} className='friend-count-item'>
                                            <div className='h6'>0</div>
                                            <div className='title'>Projects</div>
                                        </a>
                                        <a href={'javascript:(0)'} className='friend-count-item'>
                                            <div className='h6'>0</div>
                                            <div className='title'>Articles</div>
                                        </a>
                                        <a href={'javascript:(0)'} className='friend-count-item'>
                                            <div className='h6'>0</div>
                                            <div className='title'>Posts</div>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {/* ... end Friend Item */}
            </div>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IHomeLeftSidebarComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url))
    }
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IHomeLeftSidebarComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid])
    return {
        userId: uid,
        fullName: user.fullName ? user.fullName : '',
        translate: getTranslate(state.get('locale')),
        currentLanguage: getActiveLanguage(state.get('locale')).code,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)((HomeLeftSidebarComponent as any) as any)
