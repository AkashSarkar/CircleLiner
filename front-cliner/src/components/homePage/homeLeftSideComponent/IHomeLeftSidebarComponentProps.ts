import {Profile } from 'core/domain/users/index'

export interface IHomeLeftSidebarComponentProps {
    /**
     * Redirect page to [url]
     */
    goTo?: (url: string) => any
    /**
     * Profile for current user {true} or not {false}
     *
     * @type {boolean}
     * @memberof IProfileHeaderComponentProps
     */
    isAuthedUser?: boolean

    /**
     * Image cover address
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    banner: string

    /**
     * User full name
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    fullName: string

    /**
     * User tag line
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    tagLine: string

    /**
     * User's avatar address
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    avatar: string

    /**
     * User identifier
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    userId?: string

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}
