export interface IHomeLeftMenuComponentProps {

    /**
     * Profile for current user {true} or not {false}
     *
     * @type {boolean}
     * @memberof IProfileHeaderComponentProps
     */
    isAuthedUser?: boolean

    /**
     * User identifier
     *
     * @type {string}
     * @memberof IProfileHeaderComponentProps
     */
    userId?: string

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

    /**
     * Styles
     */
    classes?: any
}
