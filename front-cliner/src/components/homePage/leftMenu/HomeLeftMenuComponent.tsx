// - Import react components
import React, {Component} from 'react'
import {IHomeLeftMenuComponentProps} from './IHomeLeftMenuComponentProps'
import {IHomeLeftMenuComponentState} from './IHomeLeftMenuComponentState'
import {IRouterProps} from 'routes/IRouterProps'
import {Map} from 'immutable'
import {getActiveLanguage, getTranslate} from 'react-localize-redux'
import {HomeRouter} from 'routes/HomeRouter'
import {connect} from 'react-redux'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Paper from '@material-ui/core/Paper'
import {withStyles} from '@material-ui/core/styles'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import WorkIcon from '@material-ui/icons/Work'
import PictureIcon from '@material-ui/icons/Image'
import PeopleIcon from '@material-ui/icons/People'
import ProfessionalsIcon from '@material-ui/icons/PersonPin'
import SendIcon from '@material-ui/icons/Send'
import {NavLink} from 'react-router-dom'

// - Import components
import UserAvatarComponent from 'src/components/userAvatar'
import {push} from 'react-router-redux'

// - Import actions

const styles = (theme: any) => ({
    menuItem: {
        backgroundColor: '#F9F9F9',
    },
    primary: {},
    icon: {},
    navLink: {
        '&:focus': {
            backgroundColor: '#F9F9F9',
            '& $primary, & $icon': {
                color: '#37A000',
            },
        },
        backgroundColor: '#F9F9F9',
        color: '#1a2501',
        '&:active': {
            borderLeftWidth: '0 !important'
        }
    }
})

// - Create component class
export class HomeLeftMenuComponent extends Component<IHomeLeftMenuComponentProps, IHomeLeftMenuComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IHomeLeftMenuComponentProps) {
        super(props)

        // Default state
        this.state = {}
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate, userId, classes} = this.props
        return (
            <>
                <div key={userId} className='ui-block'>
                    <div className='your-profile'>
                        <div className='ui-block-title'>
                            <h6 className='title'>Job Portal</h6>
                        </div>
                        <div>
                            <ul className='account-settings'>
                                <li>
                                    <NavLink to={`find-job`}>
                                        <svg className='olymp-menu-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                        </svg>
                                        <span>Find Job</span>
                                    </NavLink>
                                    <hr/>
                                </li>
                                <li>
                                    <NavLink to={`post-job`}>
                                        <svg className='olymp-menu-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                        </svg>
                                        <span>Post Job</span>
                                    </NavLink>
                                    <hr/>
                                </li>
                                <li>
                                    <NavLink to={`find-job-seeker`}>
                                        <svg className='olymp-menu-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                        </svg>
                                        <span>Professionals</span>
                                    </NavLink>
                                    <hr/>
                                </li>
                                <li>
                                    <NavLink to={`job-preferences`}>
                                        <svg className='olymp-menu-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                        </svg>
                                        <span>Job Preferences</span>
                                    </NavLink>
                                    <hr/>
                                </li>
                                <li>
                                    <NavLink to={`employer-preferences`}>
                                        <svg className='olymp-menu-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                        </svg>
                                        <span>Employer Preferences</span>
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div key={userId} className='ui-block'>
                    {/* Your Profile  */}
                    <div className='your-profile'>
                        <div className='ui-block-title'>
                            <h6 className='title'>Others</h6>
                        </div>
                        <div>
                            <ul className='account-settings'>
                                <li>
                                    <NavLink to={`${userId}/people`}>
                                        <svg className='olymp-menu-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                        </svg>
                                        <span>People</span>
                                    </NavLink>
                                </li>
                                <hr/>
                                <li>
                                    <NavLink to={`${userId}/photos`}>
                                        <svg className='olymp-menu-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                        </svg>
                                        <span>Photos</span>
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: any, ownProps: IHomeLeftMenuComponentProps) => {
    return {
        goTo: (url: string) => dispatch(push(url))
    }
}

/**
 * Map state to props
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IHomeLeftMenuComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const user = state.getIn(['user', 'info', uid])
    return {
        userId: uid,
        fullName: user.fullName ? user.fullName : '',
        translate: getTranslate(state.get('locale')),
        currentLanguage: getActiveLanguage(state.get('locale')).code,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles as any)(HomeLeftMenuComponent as any) as any)
