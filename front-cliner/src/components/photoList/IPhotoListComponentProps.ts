
export interface IPhotoListComponentProps {
    /**
     * photo url
     */
    URL: string
    /**
     * Date
     */
    CreationTime: number
    /**
     * Total like
     */
    Score: number
}
