// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'
import moment from 'moment/moment'
import ImageModalSide from 'src/components/imageModal/imageModalSide'

// - Import app components
import ProfileHeader from 'layouts/profileHeader/index'
import CreateAlbumBar from 'src/components/createAlbumBar'
import Album from 'src/components/profile/album'
import ProfileViewCenter from 'layouts/views/profileView/center/index'

// - Import API

// - Import actions

import {IPhotoListComponentProps} from './IPhotoListComponentProps'
import {IPhotoListComponentState} from './IPhotoListComponentState'
import {Profile} from 'core/domain/users/index'
/**
 * Create component class
 */
export class PhotoListComponent extends Component<IPhotoListComponentProps, IPhotoListComponentState> {

    constructor(props: IPhotoListComponentProps) {
        super(props)
        this.state = {
            isModalOpen: false,
            /**
             * If it's true, Article write will be open
             */
            openImageModal: false,
        }

        this.toggleModal = this.toggleModal.bind(this)
        this.handleOpenImageModal = this.handleOpenImageModal.bind(this)
        this.handleCloseImageModal = this.handleCloseImageModal.bind(this)
        this.handleForm = this.handleForm.bind(this)
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    }
    /**
     * Open image modal
     *
     *
     * @memberof StreamComponent
     */
    handleOpenImageModal = () => {
        this.setState({
            openImageModal: true,
        })
    }

    /**
     * Close image modal
     *
     *
     * @memberof StreamComponent
     */
    handleCloseImageModal = () => {
        this.setState({
            openImageModal: false,
        })
    }
    handleForm = (evt: any) => {
        console.log(this.props.URL)

    }
    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
                <div className='photo-item col-4-width' onClick={this.handleForm}>
                    <img src={`${this.props.URL}`} alt='photo'/>
                    <div className='overlay overlay-dark'/>
                    <a href={'javascript:(0)'} className='more'>
                        <svg className='olymp-three-dots-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                        </svg>
                    </a>
                    <a href={'javascript:(0)'} className='post-add-icon inline-items'>
                        <svg className='olymp-heart-icon'>
                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-heart-icon'/>
                        </svg>
                        <span>{`${this.props.Score}`}</span>
                    </a>
                    <a href={'javascript:(0)'} data-toggle='modal' data-target='#open-photo-popup-v2'
                       className='  full-block'/>
                    <div className='content'>
                        <a href={'javascript:(0)'} className='h6 title'>Header Photos</a>
                        <time className='published' dateTime='2017-03-24T18:18'>
                            {moment.unix(this.props.CreationTime!).fromNow()}
                        </time>
                    </div>
                </div>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IPhotoListComponentProps) => {
    return {

    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IPhotoListComponentProps) => {
    return {

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(PhotoListComponent as any)
