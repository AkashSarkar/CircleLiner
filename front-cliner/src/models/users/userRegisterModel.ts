export class UserRegisterModel {

  /**
   * User identifier
   *
   * @type {string}
   * @memberof UserRegisterModel
   */
  userId?: string

  /**
   * User first name
   *
   * @type {string}
   * @memberof UserRegisterModel
   */
  fullName: string

  /**
   * User first name
   *
   * @type {string}
   * @memberof UserRegisterModel
   */
  firstName: string

  /**
   * User last name
   *
   * @type {string}
   * @memberof UserRegisterModel
   */
  lastName: string

  /**
   * User email
   *
   * @type {string}
   * @memberof UserRegisterModel
   */
  email: string

  /**
   * User password
   *
   * @type {string}
   * @memberof UserRegisterModel
   */
  password: string

}