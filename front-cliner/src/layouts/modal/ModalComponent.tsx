// - Import react components
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {grey} from '@material-ui/core/colors'
import SvgClose from '@material-ui/icons/Close'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import Modal from '@material-ui/core/Modal'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import {connect} from 'react-redux'
import {getTranslate} from 'react-localize-redux'
import {Map} from 'immutable'
import ImageGallery from 'components/imageGallery'
import Img from 'components/img'

import {IModalComponentProps} from './IModalComponentProps'
import {IModalComponentState} from './IModalComponentState'

/**
 * Create component class
 */
export class ModalComponent extends Component<IModalComponentProps, IModalComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IModalComponentProps) {
        super(props)
        // Defaul state
        this.state = {}
        // Binding function to `this`
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {open, onClose, modalHeaderAction, modalClassName, modalTitle, modalFooterAction, children, translate} = this.props
        if (open) {
            document.body.classList.add('modal-open')
        } else {
            document.body.classList.remove('modal-open')
        }
        return (
            <Modal className='modal' open={open} onClose={onClose}
                   style={open ? {display: 'block'} : {display: 'none'}}>
                <div
                    className={'modal-dialog window-popup ' + (modalClassName ? modalClassName : 'choose-from-my-photo')}
                    role='document'>
                    <div className='modal-content'>
                        <SvgClose className={'close icon-close'} onClick={onClose} style={{cursor: 'pointer'}}/>
                        {modalTitle ? (<div className='modal-header'>
                            <h6 className='title'>{modalTitle}</h6>
                            {this.props.action!}
                        </div>) : ''}
                        <div className='modal-body'>
                            {modalHeaderAction! ? modalHeaderAction : (
                                ''
                            )}
                            {children}
                            {/*{modalFooterAction! ? modalFooterAction : (*/}
                            {/*''*/}
                            {/*)}*/}
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IModalComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IModalComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(ModalComponent as any)