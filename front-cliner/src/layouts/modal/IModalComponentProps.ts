import {Map} from 'immutable'

export interface IModalComponentProps {
    /**
     * If it's true post writing page will be closed
     */
    // Remove optional later when all the modal is developed
    onClose?: () => void
    /**
     * If it's true post writing page will be open
     */
    open: boolean
    /**
     * If there is extra modal title
     */
    modalTitle?: string
    /**
     * If there is extra modal header action
     */
    modalHeaderAction?: React.ReactNode
    /**
     * If there is extra modal class
     */
    modalClassName?: string
    /**
     * If there is extra modal footer action
     */
    action?: React.ReactNode
    /**
     * If there is extra modal footer action
     */
    modalFooterAction?: React.ReactNode

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any
}
