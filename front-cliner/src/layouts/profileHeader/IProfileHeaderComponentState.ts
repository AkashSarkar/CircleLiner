export interface IProfileHeaderComponentState {

    /**
     * Window size is small {true} or not {false}
     *
     * @type {boolean}
     * @memberof IProfileHeaderComponentState
     */
    isSmall: boolean

    /**
     * Image gallery dialog is open for choosing banner image {true} or not {false}
     *
     * @type {boolean}
     * @memberof IEditProfileComponentState
     */
    openBanner: boolean

    /**
     * Image gallery dialog is open for choosing banner image {true} or not {false}
     *
     * @type {boolean}
     * @memberof IEditProfileComponentState
     */
    openReportEditor: boolean
    /**
     * Image gallery dialog is open for choosing avatar image {true} or not {false}
     *
     * @type {boolean}
     * @memberof IEditProfileComponentState
     */
    openAvatar: boolean

    openBlockModal: boolean

    /**
     * User's banner URL
     *
     * @type {string}
     * @memberof IEditProfileComponentState
     */
    banner: string

    /**
     * User's avatar URL address
     *
     * @type {string}
     * @memberof IEditProfileComponentState
     */
    avatar: string
}
