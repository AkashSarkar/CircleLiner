// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Route, Switch, withRouter, Redirect, NavLink} from 'react-router-dom'
import config from 'src/config'
import {Map} from 'immutable'
import SvgCamera from '@material-ui/icons/PhotoCamera'

// - Material UI
import {grey} from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import {Parallax, Background} from 'react-parallax'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import app components
import ImgCover from 'components/profile/imgCover/index'
import UserAvatarComponent from 'components/userAvatar'
import BlockPeopleComponent from 'components/profile/blockPeople'
import ImageGallery from 'components/imageGallery'
import ReportProfile from 'src/components/profile/reportProfile/ReportProfileComponent'
// - Import API

// - Import actions
import * as userActions from 'store/actions/userActions/userActions'
import {IProfileHeaderComponentProps} from './IProfileHeaderComponentProps'
import {IProfileHeaderComponentState} from './IProfileHeaderComponentState'
import {Profile} from 'core/domain/users'

/**
 * Create component class
 */
export class ProfileHeaderComponent extends Component<IProfileHeaderComponentProps, IProfileHeaderComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IProfileHeaderComponentProps) {
        super(props)

        /**
         * Defaul state
         */
        this.state = {
            /**
             * If it's true , the window is in small size
             */
            isSmall: false,
            /**
             * It's true if the image galley for banner is open
             */
            openBanner: false,
            /**
             * It's true if the image gallery for avatar is open
             */
            openAvatar: false,
            /**
             * It's true if the image gallery for avatar is open
             */
            openReportEditor: false,

            openBlockModal: false,
            /**
             * User banner address
             */
            banner: props.banner || '',
            /**
             * User avatar address
             */
            avatar: props.avatar || '',

        }

        // Binding functions to `this`
        this.handleOpenBlockModal = this.handleOpenBlockModal.bind(this)
        this.handleCloseBlockModal = this.handleCloseBlockModal.bind(this)
        this.handleRequestSetAvatar = this.handleRequestSetAvatar.bind(this)
        this.handleRequestSetBanner = this.handleRequestSetBanner.bind(this)

    }

    handleOpenBlockModal = () => {
        this.setState({
            openBlockModal: true
        })
    }
    handleCloseBlockModal = () => {
        this.setState({
            openBlockModal: false
        })
    }

    /**
     * Handle resize event for window to change sidebar status
     * @param  {event} evt is the event is passed by winodw resize event
     */
    handleResize = () => {

        // Set initial state
        let width = window.innerWidth

        if (width > 900) {
            this.setState({
                isSmall: false
            })

        } else {
            this.setState({
                isSmall: true
            })
        }
    }

    componentDidMount() {
        this.handleResize()
    }

    handleCloseReport = () => {
        this.setState({
            openReportEditor: false
        })
    }

    /**
     * Open image gallery of avatar
     */
    openReportEditor = () => {
        this.setState({
            openReportEditor: true
        })
    }

    /**
     * Close image gallery of banner
     */
    handleCloseBannerGallery = () => {
        this.setState({
            openBanner: false
        })
    }

    /**
     * Open image gallery of banner
     */
    handleOpenBannerGallery = () => {
        this.setState({
            openBanner: true
        })
    }

    /**
     * Close image gallery of avatar
     */
    handleCloseAvatarGallery = () => {
        this.setState({
            openAvatar: false
        })
    }

    /**
     * Open image gallery of avatar
     */
    handleOpenAvatarGallery = () => {
        this.setState({
            openAvatar: true
        })
    }

    /**
     * Set banner image url
     */
    handleRequestSetBanner = (url: string) => {
        this.setState({
            banner: url
        })
    }

    /**
     * Set avatar image url
     */
    handleRequestSetAvatar = (fileName: string) => {
        this.setState({
            avatar: fileName
        })
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {translate, isAuthedUser, editProfileOpen} = this.props
        const styles = {
            avatar: {
                border: '2px solid rgb(255, 255, 255)'
            },
            iconButton: {
                fill: 'rgb(255, 255, 255)',
                height: '24px',
                width: '24px'

            },
            iconButtonSmall: {
                fill: 'rgb(0, 0, 0)',
                height: '24px',
                width: '24px'
            },

            editButton: {

                marginLeft: '20px'

            },
            editButtonSmall: {

                marginLeft: '20px',
                color: 'white',
                fill: 'blue'

            },
            aboutButton: {
                color: 'white'
            },
            aboutButtonSmall: {
                color: 'black'
            }
        }

        const iconButtonElement = (
            <IconButton style={this.state.isSmall ? styles.iconButtonSmall : styles.iconButton}>
                <MoreVertIcon
                    style={{...(this.state.isSmall ? styles.iconButtonSmall : styles.iconButton), color: grey[400]}}
                    viewBox='10 0 24 24'/>
            </IconButton>
        )

        const RightIconMenu = () => (
            <div>
                {iconButtonElement}
                <MenuItem style={{fontSize: '14px'}}>Reply</MenuItem>
                <MenuItem style={{fontSize: '14px'}}>Edit</MenuItem>
                <MenuItem style={{fontSize: '14px'}}>Delete</MenuItem>
            </div>
        )

        return (
            <div className='container'>
                <div className='row'>
                    <div className='col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                        <div className='ui-block'>
                            <div className='top-header'>
                                <div style={{position: 'relative'}}>
                                    {/*<Parallax strength={500} className='profile__parallax'*/}
                                    {/*bgStyle={{position: 'relative'}}>*/}
                                    <Background>
                                        <ImgCover width='100%' height='510px' borderRadius='2px'
                                                  fileName={this.props.banner || config.settings.defaultProfileCover}/>
                                    </Background>
                                    {/*</Parallax>*/}
                                    {isAuthedUser ? (
                                        <div className='g__circle-black' onClick={this.handleOpenBannerGallery}
                                             style={{position: 'absolute', left: '10px', top: '10px'}}>
                                            <SvgCamera
                                                style={{
                                                    fill: 'rgba(255, 255, 255, 0.88)',
                                                    transform: 'translate(6px, 6px)'
                                                }}/>
                                        </div>) : ''}
                                </div>
                                {/*<div className='top-header-thumb'>*/}
                                {/*<img src={this.props.banner || config.settings.defaultProfileCover}*/}
                                {/*alt='nature'/>*/}
                                {/*</div>*/}
                                <div className='profile-section'>
                                    <div className='row'>
                                        <div className='col col-lg-5 col-md-5 col-sm-12 col-12'>
                                            <ul className='profile-menu'>
                                                <li>
                                                    <NavLink to={`/${this.props.userId}/timeline`}>
                                                        Timeline
                                                    </NavLink>
                                                </li>
                                                <li>
                                                    <NavLink to={`/${this.props.userId}/about`}>
                                                        About
                                                    </NavLink>
                                                </li>
                                                {/*<li>*/}
                                                    {/*<NavLink to={`/${this.props.userId}/people`}>*/}
                                                        {/*People*/}
                                                    {/*</NavLink>*/}
                                                {/*</li>*/}
                                            </ul>
                                        </div>
                                        <div className='col col-lg-5 ml-auto col-md-5 col-sm-12 col-12'>
                                            <ul className='profile-menu'>
                                                <li>
                                                    <NavLink to={`/${this.props.userId}/people`}>
                                                        People
                                                    </NavLink>
                                                </li>
                                                <li>
                                                    <NavLink to={`/${this.props.userId}/photos`}>
                                                        Photos
                                                    </NavLink>
                                                </li>
                                                {/*<li>*/}
                                                    {/*<a href={'javascript:(0)'}>Videos</a>*/}
                                                {/*</li>*/}
                                                {isAuthedUser ? '' : (<li>
                                                    <div className='more'>
                                                        <svg className='olymp-three-dots-icon'>
                                                            <use
                                                                xlinkHref='/svg-icons/sprites/icons.svg#olymp-three-dots-icon'/>
                                                        </svg>
                                                        <ul className='more-dropdown more-with-triangle'>
                                                            <li>
                                                                <a href={'javascript:(0)'}
                                                                   onClick={this.openReportEditor}>Report Profile</a>
                                                            </li>
                                                            <ReportProfile
                                                                fullName={this.props.fullName}
                                                                reportedUserId={this.props.userId}
                                                                open={this.state.openReportEditor}
                                                                onRequestClose={this.handleCloseReport}
                                                            />
                                                            <li>
                                                                <a href={'javascript:(0)'}
                                                                   onClick={this.handleOpenBlockModal}>Block Profile</a>
                                                            </li>
                                                            <BlockPeopleComponent
                                                                fullName={this.props.fullName!}
                                                                blockedUserId={this.props.userId}
                                                                open={this.state.openBlockModal}
                                                                onRequestClose={this.handleCloseBlockModal}/>
                                                        </ul>
                                                    </div>
                                                </li>)}
                                            </ul>
                                        </div>
                                    </div>
                                    <div className='control-block-button'>
                                        {!isAuthedUser ? (<a href={'javascript:(0)'}
                                                             className='btn btn-control bg-blue'>
                                            <svg className='olymp-happy-face-icon'>
                                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-happy-face-icon'/>
                                            </svg>
                                        </a>) : ''}
                                        {/*Message icon in profile header*/}
                                        {/*{!isAuthedUser ? (<a href={'javascript:(0)'} className='btn btn-control bg-purple'>*/}
                                        {/*<svg className='olymp-chat---messages-icon'>*/}
                                        {/*<use*/}
                                        {/*xlinkHref='/svg-icons/sprites/icons.svg#olymp-chat---messages-icon'/>*/}
                                        {/*</svg>*/}
                                        {/*</a>) : ''}*/}
                                        {isAuthedUser ? (<div className='btn btn-control bg-primary more'>
                                            <svg className='olymp-settings-icon'>
                                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-settings-icon'/>
                                            </svg>
                                            <ul className='more-dropdown more-with-triangle triangle-bottom-right'>
                                                <li>
                                                    {/* Upload profile picture*/}
                                                    <div className='right'>
                                                        {isAuthedUser ? (<div
                                                            style={this.state.isSmall ? styles.editButtonSmall : styles.editButton}>
                                                            <Button variant='raised'
                                                                    onClick={this.props.openEditor}>
                                                                {translate!('profile.editProfileButton')}
                                                            </Button>
                                                        </div>) : ''}
                                                    </div>
                                                    {/*{isAuthedUser && editProfileOpen ? (<EditProfile*/}
                                                    {/*avatar={this.props.avatar}*/}
                                                    {/*banner={this.props.banner}*/}
                                                    {/*fullName={this.props.fullName}*/}
                                                    {/*tagLine={this.props.tagLine}*/}
                                                    {/*open={true}*/}
                                                    {/*/>) : ''}*/}
                                                    <a href={'javascript:(0)'} data-toggle='modal'
                                                       data-target='#update-header-photo'>Update
                                                        Profile Photo</a>
                                                </li>
                                                <li>
                                                    <a href={'javascript:(0)'} data-toggle='modal'
                                                       data-target='#update-header-photo'>Update
                                                        Header Photo</a>
                                                </li>
                                                <li>
                                                    <a href={'javascript:(0)'}>Account Settings</a>
                                                </li>
                                            </ul>
                                        </div>) : ''}
                                    </div>
                                </div>
                                <div className='top-header-author'>
                                    <div style={{display: 'flex', justifyContent: 'center'}}>
                                        {isAuthedUser ? (
                                            <div className='g__circle-black' onClick={this.handleOpenAvatarGallery}
                                                 style={{
                                                     zIndex: 1,
                                                     position: 'absolute',
                                                     left: '50%',
                                                     display: 'inline-block',
                                                     top: '100px',
                                                     margin: '-18px'
                                                 }}>
                                                <SvgCamera style={{
                                                    fill: 'rgba(255, 255, 255, 0.88)',
                                                    transform: 'translate(1px, 8px)'
                                                }}/>

                                            </div>) : ''
                                        }
                                        <UserAvatarComponent fullName={(this.props.fullName ? this.props.fullName : '')}
                                                             fileName={this.props.avatar} size={124}
                                        />
                                    </div>
                                    {/*<a href={'javascript:(0)'} className='author-thumb'>*/}
                                    {/*<img src={this.props.avatar} alt='author'/>*/}
                                    {/*</a>*/}
                                    <div className='author-content'>
                                        <a href={'javascript:(0)'} className='h4 author-name'> {this.props.fullName}</a>
                                        <div className='country'>{this.props.tagLine}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Image gallery for banner*/}
                <ImageGallery open={this.state.openBanner} cropCoverPic={true}
                    // set={this.handleRequestSetBanner.bind(this)}
                              close={this.handleCloseBannerGallery.bind(this)}/>
                {/* Image gallery for avatar */}
                <ImageGallery open={this.state.openAvatar} cropProfilePic={true}
                    // set={this.handleRequestSetAvatar.bind(this)}
                              close={this.handleCloseAvatarGallery.bind(this)}/>
            </div>
        )

    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IProfileHeaderComponentProps) => {
    return {
        openEditor: () => dispatch(userActions.openEditProfile()),
        openReportEditor: () => dispatch(userActions.openReportProfile()),
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IProfileHeaderComponentProps) => {
    const userId = ownProps.userId
    const uid = state.getIn(['authorize', 'uid'], 0)
    const userProfile = state.getIn(['user', 'info', userId], {}) as Profile
    return {
        translate: getTranslate(state.get('locale')),
        avatar: userProfile.avatar,
        fullName: userProfile.fullName,
        banner: userProfile.banner,
        tagLine: userProfile.tagLine,
        isAuthedUser: userId === uid,
        userId,
        editProfileOpen: state.getIn(['user', 'openEditProfile'])
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(ProfileHeaderComponent as any)
