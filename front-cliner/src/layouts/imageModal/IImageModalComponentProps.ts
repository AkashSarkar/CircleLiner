import {Map} from 'immutable'

export interface IImageModalComponentProps {
    /**
     * If it's true post writing page will be closed
     */
    // Remove optional later when all the modal is developed
    onRequestClose?: () => void
    /**
     * If it's true post writing page will be open
     */
    open: boolean
}
