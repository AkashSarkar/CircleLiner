// - Import react components
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {grey} from '@material-ui/core/colors'
import SvgClose from '@material-ui/icons/Close'
import Button from '@material-ui/core/Button'
import Modal from '@material-ui/core/Modal'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import {connect} from 'react-redux'
import {getTranslate} from 'react-localize-redux'
import {Map} from 'immutable'
import ImageGallery from 'components/imageGallery'
import Img from 'components/img'

import {IImageModalComponentProps} from './IImageModalComponentProps'
import {IImageModalComponentState} from './IImageModalComponentState'

/**
 * Create component class
 */
export class ImageModalComponent extends Component<IImageModalComponentProps, IImageModalComponentState> {

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IImageModalComponentProps) {
        super(props)
        // Defaul state
        this.state = {}

        // Binding functions to `this`
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        const {open, onRequestClose, children} = this.props
        if (open) {
            document.body.classList.add('modal-open')
        } else {
            document.body.classList.remove('modal-open')
        }
        return (
            <Modal className='modal' open={open} onClose={onRequestClose} style={{width: 700}}>
                <div className='modal-body'>
                    {children}
                </div>
            </Modal>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IImageModalComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IImageModalComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),

    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(ImageModalComponent as any)