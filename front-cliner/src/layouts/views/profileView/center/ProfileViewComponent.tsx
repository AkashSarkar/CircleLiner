// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import RaisedButton from '@material-ui/core/Button'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'
import {Map} from 'immutable'

// - Import app components
import ProfileHeader from 'layouts/profileHeader'
import StreamComponent from 'containers/stream/index'
import InfoProfile from 'src/components/profile/infoProfile'
import HomeView from 'layouts/views/homeView'
// - Import API

// - Import actions
import * as postActions from 'src/store/actions/postActions'
import * as userActions from 'src/store/actions/userActions/userActions'
import * as globalActions from 'src/store/actions/globalActions'
import {IProfileViewComponentProps} from './IProfileViewComponentProps'
import {IProfileViewComponentState} from './IProfileViewComponentState'
import {Profile} from 'core/domain/users'

/**
 * Create component class
 */
export class ProfileViewComponent extends Component<IProfileViewComponentProps, IProfileViewComponentState> {

    static propTypes = {}

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IProfileViewComponentProps) {
        super(props)
    }

    componentWillMount() {
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <HomeView>
                <ProfileHeader userId={this.props.userId}/>
                {this.props.children}
            </HomeView>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IProfileViewComponentProps) => {
    const userId = ownProps.userId
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IProfileViewComponentProps) => {
    const userId = ownProps.userId
    return {
        translate: getTranslate(state.get('locale')),
        userId,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(ProfileViewComponent as any)
