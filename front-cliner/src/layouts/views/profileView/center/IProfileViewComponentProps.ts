export interface IProfileViewComponentProps {
    userId: string
    /**
     * Translate to locale string
     */
    translate?: (state: any, params?: {}) => any
}
