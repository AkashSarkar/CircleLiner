import {Map} from 'immutable'
import {Industry} from 'core/domain/users'

export interface IHomeViewComponentProps {

    /*
    * User id
    *
    * */
    userId?: string

    /*
    * User id
    *
    * */
    industryId?: string
    /**
     * Load user's profile
     *
     * @memberof IProfileComponentProps
     */
    loadUserInfo?: () => any

    industries?: Map<string, Industry>
}