// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Map} from 'immutable'
import {getTranslate} from 'react-localize-redux'
import {NavLink} from 'react-router-dom'

// -Import app components
import HomeHeader from 'layouts/homeHeader'

import {IHomeViewComponentProps} from './IHomeViewComponentProps'
import {IHomeViewComponentState} from './IHomeViewComponentState'

import {Industry} from 'core/domain/users'

export class HomeViewComponent extends Component<IHomeViewComponentProps, IHomeViewComponentState> {
    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IHomeViewComponentProps) {
        super(props)

        const {userId} = props

        this.state = {}
        // Binding function to `this`
    }

    industry = () => {
        const parsedIndustry: Industry[] = []
        if (this.props.industries) {
            this.props.industries!.forEach((industry) => {
                parsedIndustry.push({
                    ...Map(industry!).toJS()
                })
            })
        }
        let hasIndustry = parsedIndustry[0] ? parsedIndustry[0].concentratedIndustryName : ''

    }

    render() {
        return (
            <>
                <HomeHeader userId={this.props.userId}/>
                <div className='header-spacer'/>
                {this.industry()}
                {this.props.children}
            </>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IHomeViewComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IHomeViewComponentProps) => {
    const uid = state.getIn(['authorize', 'uid'], 0)
    const industries: Map<string, Industry> = state.getIn(['user', 'industry', uid])
    return {
        translate: getTranslate(state.get('locale')),
        userId: uid,
        industries: industries,
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(HomeViewComponent as any)
