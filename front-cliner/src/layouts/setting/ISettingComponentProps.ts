export interface ISettingComponentProps {
    /**
     * If it's true post writing page will be closed
     */
    // Remove optional later when all the modal is developed
    onClose?: () => void
    /**
     * If it's true post writing page will be open
     */
    open?: boolean
    /**
     * User identifier
     *
     * @type {string}
     * @memberof IHomeComponentProps
     */
    userId?: string
    /**
     * Sidebar is open {true} or not {false}
     *
     * @type {boolean}
     * @memberof IHomeHeaderComponentProps
     */
    drawerStatus?: boolean

    /**
     * Logout user
     *
     * @memberof IHomeHeaderComponentProps
     */
    logout?: () => void

    /**
     * Handle on resize window event
     *
     * @memberof IHomeHeaderComponentProps
     */
    handleResize?: (event: any) => void

    /**
     * Number of notifications
     *
     * @memberof IHomeHeaderComponentProps
     */
    notifyCount?: number

    /**
     * User full name
     *
     * @type {string}
     * @memberof IHomeHeaderComponentProps
     */
    fullName?: string

    /**
     * User's avatar URL address
     *
     * @type {string}
     * @memberof IHomeHeaderComponentProps
     */
    avatar?: string

    /**
     * Top bar title
     *
     * @type {string}
     * @memberof IHomeHeaderComponentProps
     */
    title?: string

}
