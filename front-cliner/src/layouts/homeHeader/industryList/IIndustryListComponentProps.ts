import StringAPI from 'api/StringAPI'
import {Industry} from 'core/domain/users'

export interface IIndustryListComponentProps {
    /**
     * Industry id
     */
    industryId?: string
    /**
     * Industry name
     */
    industryName?: string
    /**
     * concentrated Industry name
     */
    concentratedIndustryName?: string

    /**
     * Material ui theme style
     */
    classes?: any
    /**
     * Theme
     */
    theme?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

}
