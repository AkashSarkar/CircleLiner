// - Import react components
import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {Map} from 'immutable'

// - Material UI
import {Manager, Target, Popper} from 'react-popper'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import components
import UserAvatar from 'components/userAvatar'
import Notify from 'components/sugestionBox/notifications/notify/index'

// - Import actions
import {authorizeActions} from 'store/actions/index'
import {IIndustryListComponentProps} from './IIndustryListComponentProps'
import {IIndustryListComponentState} from './IIndustryListComponentState'

// - Create HomeHeader component class
export class IndustryListComponent extends Component<IIndustryListComponentProps, IIndustryListComponentState> {

    styles = {
        avatarStyle: {
            margin: 5,
            cursor: 'pointer'
        }

    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IIndustryListComponentProps) {
        super(props)

        // Default state
        this.state = {
        }
    }

    // Render app DOM component
    render() {
        const {classes, translate, theme} = this.props
        return (
                <span>
                    {this.props.industryName ?
                        < NavLink to={`/feed/${this.props.industryId}`} className='link-find-friend'>
                            {this.props.industryName}
                        </NavLink> : ''}
                    {this.props.concentratedIndustryName ?
                        <NavLink to='/' className='link-find-friend'>
                            {this.props.concentratedIndustryName}
                        </NavLink> : ''}
                    <div className='search-friend inline-items'>
                        <a href={'javascript:(0)'} className='js-sidebar-open'>
                            <svg className='olymp-menu-icon'>
                                <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                            </svg>
                        </a>
                    </div>
                </span>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: Function, ownProps: IIndustryListComponentProps) => {
    return {
        logout: () => dispatch(authorizeActions.dbLogout())
    }
}

// - Map state to props
const mapStateToProps = (state: Map<string, any>, ownProps: IIndustryListComponentProps) => {

    const uid = state.getIn(['authorize', 'uid'], 0)
    const industries = state.getIn(['user', 'industry',uid])
    const userNotifies: Map<string, any> = state.getIn(['notify', 'userNotifies'])
    let notifyCount = userNotifies
        ? userNotifies
            .filter((notification) => !notification.get('isSeen', false)).count()
        : 0
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        avatar: user.avatar || '',
        fullName: user.fullName || '',
        title: state.getIn(['global', 'headerTitle'], ''),
        industries: industries ? industries : [],
        uid,
        notifyCount
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(IndustryListComponent as any)
