// - Import react components
import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {Map} from 'immutable'

// - Material UI
import {Manager, Target, Popper} from 'react-popper'
import {getTranslate, getActiveLanguage} from 'react-localize-redux'

// - Import components
import UserAvatar from 'components/userAvatar'
import Notify from 'components/sugestionBox/notifications/notify/index'
import IndustryList from 'layouts/homeHeader/industryList'

// - Import actions
import {authorizeActions} from 'store/actions/index'
import {IHomeHeaderComponentProps} from './IHomeHeaderComponentProps'
import {IHomeHeaderComponentState} from './IHomeHeaderComponentState'
import {Industry} from 'core/domain/users'

// - Create HomeHeader component class
export class HomeHeaderComponent extends Component<IHomeHeaderComponentProps, IHomeHeaderComponentState> {

    styles = {
        avatarStyle: {
            margin: 5,
            cursor: 'pointer'
        },
        logo: {
            width: '50px',
             // marginTop: '-8px',
            // marginLeft: '45%',
        }
    }

    /**
     * Component constructor
     * @param  {object} props is an object properties of component
     */
    constructor(props: IHomeHeaderComponentProps) {
        super(props)

        // Default state
        this.state = {
            /**
             * User avatar popover is open if true
             */
            openAvatarMenu: false,
            /**
             * Show header title or not (true/false)
             */
            showTitle: true,
            /**
             * If true notification menu will be open
             */
            openNotifyMenu: false
        }

        // Binding functions to `this`
        this.handleCloseNotify = this.handleCloseNotify.bind(this)

    }

    /**
     * Handle close notification menu
     *
     *
     * @memberof HomeHeader
     */
    handleCloseNotify = () => {
        this.setState({
            openNotifyMenu: false
        })
    }

    /**
     * Handle notification touch
     *
     *
     * @memberof HomeHeader
     */
    handleNotifyTouchTap = (event: any) => {
        // This prevents ghost click.
        event.preventDefault()

        this.setState({
            openNotifyMenu: true,
            anchorEl: event.currentTarget
        })
    }

    /**
     * Handle touch on user avatar for popover
     *
     *
     * @memberof HomeHeader
     */
    handleAvatarTouchTap = (event: any) => {
        this.setState({
            openAvatarMenu: true,
            anchorEl: event.currentTarget
        })
    }

    /**
     * Handle logout user
     *
     *
     * @memberof HomeHeader
     */
    handleLogout = () => {
        this.props.logout!()
    }

    /**
     * Handle close popover
     *
     *
     * @memberof HomeHeader
     */
    handleRequestClose = () => {
        this.setState({
            openAvatarMenu: false,
            anchorEl: null
        })
    }

    componentDidMount() {
    }

    industryList = () => {
        const item: any[] = []
        const parsedIndustry: Industry[] = []
        this.props.industries!.forEach((industry) => {
            parsedIndustry.push({
                ...Map(industry!).toJS()
            })
        })
        parsedIndustry.map((industry: Industry) => {
            item.push(
                <IndustryList key={industry.id!}
                              industryId={industry.id!}
                              industryName={industry.industryName!}
                              concentratedIndustryName={industry.concentratedIndustryName!}
                />
            )
        })
        return item
    }

    // Render app DOM component
    render() {
        const {classes, avatar, fullName, translate, theme} = this.props
        return (
            <header className='header' id='site-header'>
                <div className='page-title logo'>
                    <NavLink to='/'>
                            <img style={this.styles.logo} src='/img/circle-liner-logo.png'/>
                    </NavLink>
                </div>
                <div className='header-content-wrapper'>
                    {/*{industryName ?*/}
                        {/*< NavLink to='/' className='link-find-friend'>*/}
                            {/*{industryName}*/}
                        {/*</NavLink> : ''}*/}
                    {/*{concentratedIndustryName ?*/}
                        {/*<NavLink to='/' className='link-find-friend'>*/}
                            {/*{concentratedIndustryName}*/}
                        {/*</NavLink> : ''}*/}
                    {/*<div className='search-friend inline-items'>*/}
                        {/*<a href={'javascript:(0)'} className='js-sidebar-open'>*/}
                            {/*<svg className='olymp-menu-icon'>*/}
                                {/*<use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>*/}
                            {/*</svg>*/}
                        {/*</a>*/}
                    {/*</div>*/}
                    {this.industryList()}
                    <div className='control-block'>
                        {this.props.notifyCount! > 0 ? (
                                <div className='control-icon more has-items'>
                                    <svg onMouseOver={this.handleNotifyTouchTap} className='olymp-thunder-icon'>
                                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-thunder-icon'/>
                                    </svg>
                                    <div className='label-avatar bg-primary'>{this.props.notifyCount}</div>
                                    <Notify open={this.state.openNotifyMenu} anchorEl={this.state.anchorEl}
                                            onRequestClose={this.handleCloseNotify}/>
                                </div>
                            )
                            : (
                                <div className='control-icon more has-items'>
                                    <svg onMouseOver={this.handleNotifyTouchTap} className='olymp-thunder-icon'>
                                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-thunder-icon'/>
                                    </svg>
                                    <Notify open={this.state.openNotifyMenu} anchorEl={this.state.anchorEl}
                                            onRequestClose={this.handleCloseNotify}/>
                                </div>
                            )}

                        <div className='author-page author vcard inline-items more'>
                            <div className='author-thumb'>
                                <UserAvatar fileName={avatar} fullName={fullName} size={36}/>
                                <span className='icon-status online'/>
                                <div className='more-dropdown more-with-triangle'>
                                    <div className='mCustomScrollbar' data-mcs-theme='dark'>
                                        <div className='ui-block-title ui-block-title-small'>
                                            <h6 className='title'>Your Account</h6>
                                        </div>
                                        <ul className='account-settings'>
                                            <li>
                                                <NavLink to='/settings'>
                                                    <svg className='olymp-menu-icon'>
                                                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                                    </svg>
                                                    <span>Profile Settings</span>
                                                </NavLink>
                                            </li>
                                            <li>
                                                <NavLink to={'/terms-and-conditions'}>
                                                    <svg className='olymp-menu-icon'>
                                                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                                    </svg>
                                                    <span>Privacy policy</span>
                                                </NavLink>
                                            </li>
                                            {/*<li>*/}
                                                {/*<NavLink to={'/FAQs'}>*/}
                                                        {/*<span>FAQs</span>*/}
                                                {/*</NavLink>*/}
                                            {/*</li>*/}
                                            <li>
                                                <a href={'javascript:(0)'}>
                                                    <svg className='olymp-menu-icon'>
                                                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-menu-icon'/>
                                                    </svg>
                                                    <span>Feedback</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href={'javascript:(0)'} onClick={this.handleLogout.bind(this)}>
                                                    <svg className='olymp-logout-icon'>
                                                        <use
                                                            xlinkHref='/svg-icons/sprites/icons.svg#olymp-logout-icon'/>
                                                    </svg>
                                                    <span>{translate!('header.logout')}</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            {this.props.userId ? (<NavLink to={`/${this.props.userId}`} className='author-name fn'>
                                <div className='author-title'>
                                    {this.props.fullName!}
                                    <svg className='olymp-dropdown-arrow-icon'>
                                        <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon'/>
                                    </svg>
                                </div>
                                <span className='author-subtitle'>SPACE COWBOY</span>
                            </NavLink>) : (
                                <a href={'javascript:(0)'} className='author-name fn' onClick={this.handleAvatarTouchTap}>
                                    <div className='author-title'>
                                        {this.props.fullName!}
                                        <svg className='olymp-dropdown-arrow-icon'>
                                            <use xlinkHref='/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon'/>
                                        </svg>
                                    </div>
                                    <span className='author-subtitle'>SPACE COWBOY</span>
                                </a>
                            )}
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch: Function, ownProps: IHomeHeaderComponentProps) => {
    return {
        logout: () => dispatch(authorizeActions.dbLogout())
    }
}

// - Map state to props
const mapStateToProps = (state: Map<string, any>, ownProps: IHomeHeaderComponentProps) => {

    const uid = state.getIn(['authorize', 'uid'], 0)
    const industries = state.getIn(['user', 'industry',uid])
    const userNotifies: Map<string, any> = state.getIn(['notify', 'userNotifies'])
    let notifyCount = userNotifies
        ? userNotifies
            .filter((notification) => !notification.get('isSeen', false)).count()
        : 0
    const user = state.getIn(['user', 'info', uid], {})
    return {
        translate: getTranslate(state.get('locale')),
        avatar: user.avatar || '',
        fullName: user.fullName || '',
        title: state.getIn(['global', 'headerTitle'], ''),
        industries: industries ? industries : [],
        uid,
        notifyCount
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(HomeHeaderComponent as any)
