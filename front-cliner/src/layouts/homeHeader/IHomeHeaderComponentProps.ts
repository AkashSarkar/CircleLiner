import StringAPI from 'api/StringAPI'
import {Industry} from 'core/domain/users'

export interface IHomeHeaderComponentProps {
    /**
     * Router match
     *
     * @type {*}
     * @memberof IProfileComponentProps
     */
    match?: any
    /**
     * User identifier
     *
     * @type {string}
     * @memberof IHomeComponentProps
     */
    userId?: string
    /**
     * Logout user
     *
     * @memberof IHomeHeaderComponentProps
     */
    logout?: () => void

    /**
     * Handle on resize window event
     *
     * @memberof IHomeHeaderComponentProps
     */
    handleResize?: (event: any) => void

    /**
     * Number of notifications
     *
     * @memberof IHomeHeaderComponentProps
     */
    notifyCount?: number

    /**
     * User full name
     *
     * @type {string}
     * @memberof IHomeHeaderComponentProps
     */
    fullName?: string

    /**
     * User's avatar URL address
     *
     * @type {string}
     * @memberof IHomeHeaderComponentProps
     */
    avatar?: string

    /**
     * Top bar title
     *
     * @type {string}
     * @memberof IHomeHeaderComponentProps
     */
    title?: string
    /**
     * User's industry list
     *
     * @type {string}
     * @memberof IHomeHeaderComponentProps
     */
    industries?: Map<string, Industry>
    /**
     * Material ui theme style
     */
    classes?: any
    /**
     * Theme
     */
    theme?: any

    /**
     * Translate to locale string
     */
    translate?: (state: any) => any

}
