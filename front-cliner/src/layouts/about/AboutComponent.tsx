// - Import react components
import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {grey} from '@material-ui/core/colors'
import SvgClose from '@material-ui/icons/Close'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
import Dialog from '@material-ui/core/Dialog'

// - Import app component
import HomeView from 'layouts/views/homeView'
import {IAboutComponentProps} from './IAboutComponentProps'
import {IAboutComponentState} from './IAboutComponentState'
import AboutSidebarComponent from 'src/components/profile/about/sidebarAbout/AboutSidebarComponent'
import ProfileView from 'layouts/views/profileView/center'
// import ProfileViewCenter from 'layouts/views/profileView/center'

import {ProfileHeaderComponent} from 'layouts/profileHeader/ProfileHeaderComponent'
import {Map} from 'immutable'
import {IProfileHeaderComponentProps} from 'layouts/profileHeader/IProfileHeaderComponentProps'
import * as userActions from 'store/actions/userActions/userActions'
import {getTranslate} from 'react-localize-redux'

/**
 * Create component class
 */
export class AboutComponent extends Component<IAboutComponentProps, IAboutComponentState> {

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor (props: IAboutComponentProps) {
    super(props)

    // Defaul state
    this.state = {
        drawerOpen: false,
    }

    // Binding functions to `this`

  }
    handleDrawerToggle = () => {
        this.setState({drawerOpen: !this.state.drawerOpen})
    }

    /**
     * Reneder component DOM
     * @return {react element} return the DOM which rendered by component
     */
    render() {
        return (
            <ProfileView userId={this.props.userId!}>
                <div className='container'>
                    <div className='row'>
                        <div
                            className='col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12'>
                            {this.props.children}
                        </div>
                        <div
                            className='col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none'>
                            <AboutSidebarComponent userId={this.props.userId!} />
                        </div>
                    </div>
                </div>
            </ProfileView>
        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch: any, ownProps: IAboutComponentProps) => {
    return {}
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state: Map<string, any>, ownProps: IAboutComponentProps) => {
    return {
        translate: getTranslate(state.get('locale')),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutComponent as any)